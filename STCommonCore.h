/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STCommonCore.h
	Object Name:				STCommonCore
   Programmer Name:			Royce Souther
   By Commond of:				Silicon Tao Technology Systems
   Day Zero:					02102001, February 10, 2001
   Target Platform:			Linux
   Compiler Used:				GNU g++
   Compiler Settings:      N/A
   Linker Settings:        N/A
   Title of program:
   Description:				This object...
=============================================================================*/

#ifndef STCommonCore_included
#define STCommonCore_included

#ifndef BYTE
#define BYTE	unsigned char
#endif

#ifndef byte
#define byte	unsigned char
#endif

#ifndef WORD
#define WORD 	unsigned short int
#endif

#ifndef DWORD
#define DWORD 	unsigned long int
#endif

#endif //  STCommonCore_included
