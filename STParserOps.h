/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STParserOps.h
   Object Name: 				STParserOps
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Day Zero:					02152000, February 15, 2000
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:      	STList.h
   Ports Used:           	None
   Title of program:			Object library
   Description:
=============================================================================*/

#ifndef STParserOps_included
#define STParserOps_included

#include <STObjectController.h>
#include <STString.h>

/**
 * Functionality to parse strings in to usable parts. Parts are defined as words and blocks.
 *
 * Words: A word is a block of text that is wholly alphabetic.
 * For example " this is four words " "(this is three)" " this twoWords " " this is 2 "
 *
 *	Identifier: An identifier is a block of text that is wholly alpha-numeric and not starting with a number.
 * For example " this_is_an_Identifier " "(this_is_an_Identifier)"
 * " this_is two_Identifiers " " this_is_2 Identifiers " " this_is 1_Identifier "
 * These would be similar to names in C and C++ for varibles and function names.
 *
 * Blocks: A block is any amount of text containing alpha, numerics or symbols that is preseeded by a
 * block identifier and followed by a block identifier. In C "{" and "}" are block identifiers. The text
 * code between them is the block.
 *
 * @short Functionality to parse strings in to usable parts.
 */
class STParserOps: public STObjectController
{
   private:
      STString ReturnString;
   protected:
      //
   public:
      /**
       * Remove unwanted chars off the front and back of a string.
		 * You can set the TheChar to any char or use [:ws:] to remove white spaces and tabs.
       */
      STString Strip(STString& TextString, const char TheChar);
      STString Strip(STString& TextString, const char* TheChar);

      /**
       * Compairs two strings, one containing wild cards of * or ? for
       * sections of any thing any length represented by a * or and single
       * char represented by ?.
       */
      bool WildCardCompairStrings(const STString& CompairString1, const STString& CompairString2, bool CaseSensitive);

      /**
       * From a given string this function will return the word that is WordIndex
       * counts in from the beginning. A word is a continiuous block of ASCII characters A to Z in upper or lower case.
       */
      STString ExtractWord(const STString& SourceString, int WordIndex);

      /**
       * From a given string this function will return the entire string except the
       * word that is WordIndex counts in from the beginning. A word is a continiuous block of ASCII characters A to Z in upper or lower case.
       */
      STString RemoveWord(const STString& SourceString, int WordIndex);

      /**
       * From a given string this function will return a string with
       * every thing from the word at WordIndex counts from the beginning to the end. A word is a continiuous block of ASCII characters A to Z in upper or lower case.
       */
      STString TrucateUpToWord(const STString &SourceString, int WordIndex);

      /**
       * From a given string this function will return a string with
       * every thing from beginning untill the word at WordIndex counts from the beginning. A word is a continiuous block of ASCII characters A to Z in upper or lower case.
       */
      STString TrucateAfterWord(const STString& SourceString, int WordIndex);

      /**
       * From a given string this function will return a string that is the first word in the string. A word is a continiuous block of ASCII characters A to Z in upper or lower case.
       */
      STString FindFirstWord(const STString& SourceString);

      /**
       * From a given string this function will return a string that is the next word
       * after the word at WordIndex. A word is a continiuous block of ASCII characters A to Z in upper or lower case.
       */
      STString FindNextWord(const STString& SourceString, int WordIndex);

      /**
       * From a given string this function will return a string that is the
       * first identifier in the string. An identifier is a continiuous block of ASCII characters A to Z in upper or lower case or 0 to 9 or an underscore _ but the very first charactor must be an alpha.
       */
      STString FindFirstIdentifier(const STString& SourceString);

      /**
       * From a given string this function will return a string that is the next
       * identifier after the identifier at IdentifierIndex. An identifier is a continiuous block of ASCII characters A to Z in upper or lower case or 0 to 9 or an underscore _ but the very first charactor must be an alpha.
       */
      STString FindNextIdentifier(const STString &SourceString, int IdentifierIndex);

      /**
       * Returns the number of identifiers. An identifier is a continiuous block of ASCII characters A to Z in upper or lower case or 0 to 9 or an underscore _ but the very first charactor must be an alpha.
       */
		int CountIdentifiers(const STString& SourceString);

      /**
		 * Returns the very last identifier. An identifier is a continiuous block of ASCII characters A to Z in upper or lower case or 0 to 9 or an underscore _ but the very first charactor must be an alpha.
       */
		STString FindLastIdentifier(const STString& SourceString);

      /**
		 * Returns the next last identifier from the negative index value end position. An identifier is a continiuous block of ASCII characters A to Z in upper or lower case or 0 to 9 or an underscore _ but the very first charactor must be an alpha.
       */
		STString FindNextLastIdentifier(const STString& SourceString, int IdentifierIndex);

		/**
       * Used to find if a given char is alpha.
       */
      bool IsAlpha(char TheChar);

      /**
       * Used to find if a given char is an ASCII number.
       */
      bool IsNumber(char TheChar);

      /**
       * Used to find if a given char is a symbol.
       */
      bool IsSymbol(char TheChar);

      /**
       * Used to find if a given char is a white space like space, tab, line feed or return.
       */
      bool IsWhiteSpace(char TheChar);

      /**
       * From a given string this function will return the block that is BlockIndex
       * counts in from the beginning.
       */
      STString ExtractBlock(const STString& SourceString, int BlockIndex, const STString& BlockIndicators);

      /**
       * From a given string this function will return the entire string except the
       * block that is BlockIndex counts in from the beginning.
       */
      STString RemoveBlock(const STString& SourceString, int BlockIndex, const STString& BlockIndicators);

      /**
       * This function will attempt to extract a number string from the midle of
       * a give string. Any symbols like $ % or # and so on will be truncated. If more
       * then one number is found in the string then the function will keep looking for
       * whole numbers until it reaches WordIndex counts of numbers found. The decimal
       * number symbol in a number like 6.43 will be included in the returned string
       * so that floating point numbers can be grabed.
       */
      STString ExtractNumberString(const STString& SourceString, int WordIndex);

      /**
       * Constructor
       */
      STParserOps();

      /**
       * Destructor
       */
      ~STParserOps();
};

#endif // STParserOps_included
