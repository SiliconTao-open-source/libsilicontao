/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:
      - fix STStringList so this will work again
      - test speed of new VS malloc VS STMalloc
      - add more functionality
         - define what that functionality should be

=============================================================================*/

#include <STFreeBase.h>
#include <STCommonCore.h>
#include <STStringList.h>
#include <STStringsEx.h>
#include <STParserOps.h>
#include <STList.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

DWORD DebugPos;

struct TDBFileHeader
{
   WORD STFreeBaseVersion;
   DWORD GenerationDate;
   DWORD LastTouchedDate;
   WORD DBType;
   char DBReadSecurity[PASSWORD_SIZE];
   char DBWriteSecurity[PASSWORD_SIZE];
   char FutureUse[FUTURE_SIZE];
   DWORD NumberOfFields;
   DWORD NumberOfRecords;
};

struct TFieldStructure
{
   char FieldName[20];
   BYTE FieldType;
   BYTE FieldSize;
};


STFreeBase::STFreeBase()
{
   Active=false;
   TableName = NULL;
   FieldsConstructList = new STList;
   NewFieldsList = new STStringList;
   IndexColumnList = new STList;
   DBFileHeader = new TDBFileHeader;
   ParserOps = new STParserOps();
}

STFreeBase::~STFreeBase()
{
   Close();
   delete FieldsConstructList;
   delete NewFieldsList;
   delete IndexColumnList;
   delete DBFileHeader;
   delete ParserOps;
}

void STFreeBase::Close()
{
	DWORD ALoopCounter;
   TFieldStructure *FieldStructureNode;

   if(Active == true)
   {
   	// Free the field map, NumberOfFields + 1 for the index field
   	for(ALoopCounter=0;ALoopCounter<(DWORD)DBFileHeader->NumberOfFields+1;ALoopCounter++)
      {
      	FieldStructureNode = (TFieldStructure *)FieldsConstructList->Item(0);
         delete FieldStructureNode;
         FieldsConstructList->Delete(0);
      }

      if((DBFileHeader->DBType & 1) == MEMORY_RESIDENT)
      {
      	for(ALoopCounter=0;ALoopCounter<IndexColumnList->Count();ALoopCounter++)
         {
         	RecordContainer = (BYTE *)IndexColumnList->Item(0);
            delete RecordContainer;
            IndexColumnList->Delete(0);
         }
      } else
      {
       	delete RecordContainer;
      }

      if(FilePointer != NULL)
      {
      	fclose(FilePointer);
         FilePointer = NULL;
      }
      if(TableName != NULL)
      {
         delete TableName;
         TableName = NULL;
      }

      DataChanged = false;
      Active = false;
   }
}

void STFreeBase::Next()
{
	if(Active==true)
   {
   	if((RecNo + 1) < DBFileHeader->NumberOfRecords)
      {
   		RecNo++;
      	ReloadRecord();
      }
   }
}

bool STFreeBase::GoToRecord(DWORD RecordToGoTo)
{
	if(Active==true)
   {
   	if((RecordToGoTo + 1) < DBFileHeader->NumberOfRecords)
      {
   		RecNo = RecordToGoTo;
      	ReloadRecord();
      }
   }
   return(true);
}

void STFreeBase::ReloadRecord()
{
	if(Active==true)
   {
   	if((DBFileHeader->DBType & 1) == MEMORY_RESIDENT)
   	{
			RecordContainer = (BYTE *)IndexColumnList->Item(RecNo);
   	} else // FILE_RESIDENT
   	{
      	if(DataChanged == true)
	      {
				WriteRecord(LastRecNo,(BYTE *)RecordContainer);
         }
         ReadRecord(RecNo,(BYTE *)RecordContainer);
   	}
	}
}

void STFreeBase::Previous()
{
	if(Active==true)
   {
   	if(RecNo > 0)
      {
   		RecNo--;
      	ReloadRecord();
      }
   }
}

DWORD  STFreeBase::GetVersionInt()
{
	DWORD TempInt;

   // TempInt = 0;
   // TempInt += String To DWORD MM Version
   // TempInt *= 100;
   // TempInt += String To DWORD mm Version
   // TempInt *= 10000;
   // TempInt += String To DWORD bbbb Version

   // While not completed just return a number
	TempInt = 1;

   return(TempInt);
}

void STFreeBase::First()
{
	if(Active==true)
   {
   	RecNo = 0;
    	ReloadRecord();
   }
}

void STFreeBase::Last()
{
	if(Active==true)
   {
      RecNo = 0;
      if(DBFileHeader->NumberOfRecords > 0)
      {
   	  RecNo = DBFileHeader->NumberOfRecords - 1;
      }
	  	ReloadRecord();
   }
}

DWORD STFreeBase::RecordCount()
{
   return(DBFileHeader->NumberOfRecords);
}

DWORD STFreeBase::RecordNumber()
{
   return(RecNo);
}

char *STFreeBase::FieldName(DWORD FieldNumberToName)
{
   return((char*)"not yet");
}

DWORD STFreeBase::FieldNumber(const char *FieldNameToNumber)
{
   return(0);
}

BYTE STFreeBase::GetFieldType(DWORD FieldIndex)
{
	BYTE ReturnType;
	TFieldStructure *FieldStructureNode;

   ReturnType = NULL_FIELD;
	if(Active==true)
   {
   	FieldStructureNode = (TFieldStructure *)FieldsConstructList->Item(FieldIndex);
      ReturnType = FieldStructureNode->FieldType;
   }
   return(ReturnType);
}

DWORD STFreeBase::SizeOfCell(DWORD FieldIndex)
{
   DWORD ReturnValue;
   TFieldStructure *FieldStructureNode;

   ReturnValue = 0;
	if(Active==true)
   {
   	FieldStructureNode = (TFieldStructure *)FieldsConstructList->Item(FieldIndex);
      ReturnValue = FieldStructureNode->FieldSize;
   }
   return(ReturnValue);
}

DWORD STFreeBase::CalculateOffsetInRow(DWORD FieldIndex)
{
   DWORD ALoopCounter;
   DWORD ReturnValue;
   TFieldStructure *FieldStructureNode;

   ReturnValue = 0;
	if(Active==true)
   {
   	for(ALoopCounter=0;ALoopCounter<FieldIndex;ALoopCounter++)
      {
      	FieldStructureNode = (TFieldStructure *)FieldsConstructList->Item(ALoopCounter);
			ReturnValue += FieldStructureNode->FieldSize;
      }
   }
   return(ReturnValue);
}

bool STFreeBase::GetValue(DWORD FieldIndex,void *Container)
{
	DWORD ALoopCounter;
   DWORD DataOffset;
   BYTE *FakeAPointer;
   bool ValueReadCorrectly;

   ValueReadCorrectly = false;
   FakeAPointer = (BYTE *)Container;
	if((Active==true) && (DBFileHeader->NumberOfRecords > 0))
   {
   	DataOffset = CalculateOffsetInRow(FieldIndex);
   	for(ALoopCounter=0;ALoopCounter<SizeOfCell(FieldIndex);ALoopCounter++)
      {
      	FakeAPointer[ALoopCounter] = RecordContainer[DataOffset + ALoopCounter];
      }
      ValueReadCorrectly = true;
   }
   return(ValueReadCorrectly);
}

void STFreeBase::SetIndexField(DWORD NewIndex)
{
	DWORD ALoopCounter;
   BYTE *FakeAPointer;

   FakeAPointer = (BYTE *)&NewIndex;
   if(Active==true)
   {
      for(ALoopCounter=0;ALoopCounter<sizeof(DWORD);ALoopCounter++)
      {
      	RecordContainer[ALoopCounter] = FakeAPointer[ALoopCounter];
   	}
   }
}

void STFreeBase::SetValue(DWORD FieldIndex,void *Container)
{
	DWORD ALoopCounter;
   DWORD DataOffset;
   BYTE *FakeAPointer;
   BYTE FieldType;

   FieldType = GetFieldType(FieldIndex);
   FakeAPointer = (BYTE *)Container;
   if((Active==true) && (FieldIndex > 0))
   {
   	// FieldIndex can not be 0, users can not set the index field
   	DataOffset = CalculateOffsetInRow(FieldIndex);
      for(ALoopCounter=0;ALoopCounter<SizeOfCell(FieldIndex);ALoopCounter++)
      {
      	RecordContainer[DataOffset + ALoopCounter] = FakeAPointer[ALoopCounter];
   		if(FieldType == STRING_FIELD)
   		{
         	if(FakeAPointer[ALoopCounter] == '\0')
            {
					break;
            }
   		}
   	}
   }
}

void STFreeBase::Post()
{
	DWORD ALoopCounter;
   bool CloseTheFile;

   CloseTheFile = false;
	if(Active==true)
   {
		if((DBFileHeader->DBType & 1) == MEMORY_RESIDENT)
		{
       	FilePointer = fopen(TableName,"r+b");
			if(FilePointer > NULL)
			{
				for(ALoopCounter=0;ALoopCounter<IndexColumnList->Count();ALoopCounter++)
				{
					RecordContainer = (BYTE *)IndexColumnList->Item(ALoopCounter);
					WriteRecord(ALoopCounter,(BYTE *)RecordContainer);
				}
				DataChanged = false;
				CloseTheFile = true;
			} else
			{
				fprintf(stderr, "STFreeBase::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
			}
      } else // FILE_RESIDENT
      {
			WriteRecord(RecNo,(BYTE *)RecordContainer);
      }
      WriteHeader();
		if(CloseTheFile == true)
      {
         if(FilePointer != NULL)
         {
            fclose(FilePointer);
            FilePointer = NULL;
         }
      }
   }
}

void STFreeBase::WriteHeader()
{
   char *WriteHeaderPointer;
   DWORD ALoopCounter;

   WriteHeaderPointer = (char *)DBFileHeader;

	if(FilePointer > 0)
   {
   	fseek(FilePointer,0,SEEK_SET);
   	for(ALoopCounter=0;ALoopCounter<sizeof(TDBFileHeader);ALoopCounter++)
   	{
   		//fputc((char)DBFileHeader[ALoopCounter],FilePointer);
      	fputc(WriteHeaderPointer[ALoopCounter],FilePointer);
      	//DebugPos = ftell(FilePointer);
      }
   }
}

void STFreeBase::New()
{
	DWORD ALoopCounter;

	Close();

	DBFileHeader->STFreeBaseVersion=GetVersionInt();
	DBFileHeader->GenerationDate=0;
	DBFileHeader->LastTouchedDate=0;
   DBFileHeader->DBType = FILE_RESIDENT;
	DBFileHeader->NumberOfFields=0;
   for(ALoopCounter=0;ALoopCounter<PASSWORD_SIZE;ALoopCounter++)
   {
   	DBFileHeader->DBReadSecurity[ALoopCounter]='\0';
      DBFileHeader->DBWriteSecurity[ALoopCounter]='\0';
   }
   for(ALoopCounter=0;ALoopCounter<FUTURE_SIZE;ALoopCounter++)
   {
      DBFileHeader->FutureUse[ALoopCounter]=' ';
   }
}

void STFreeBase::AddField(const char *NewFieldToAdd)
{
	if(Active==false)
   {
   	NewFieldsList->Add(NewFieldToAdd);
   }
}

bool STFreeBase::EndOfFile()
{
	// If the last read of the file reads that last byte feof
   // will not truthfully report the end of the file.
   bool TempReturn;

   TempReturn = true;
   fgetc(FilePointer);
	TempReturn = feof(FilePointer);
	if(TempReturn == false)
   {
    	fseek(FilePointer,-1,SEEK_CUR);
      TempReturn = false;
   }
   return(TempReturn);
}

long STFreeBase::FileSize()
{
	long PosInFile;
   long TheSize;

   TheSize = 0;
   if(FilePointer > 0)
   {
		PosInFile = ftell(FilePointer);
		fseek(FilePointer,0,SEEK_END);
      TheSize = ftell(FilePointer);
      fseek(FilePointer,PosInFile,SEEK_SET);
   }
   return(TheSize);
}

bool STFreeBase::ReadRecord(DWORD RecordToRead,BYTE *DataContainer)
{
	// Read the record RecordToRead count from beginning of file database.
   // Put data in memory at location DataContainer
   // Return true if data read properly, return false if could not read data
   bool KeepFileOpen;
	DWORD ALoopCounter;
   long MoveToLocationInFile;
   bool TempReturn;

   TempReturn = false;
	if(DBFileHeader->NumberOfRecords > RecordToRead)
   {
		if(Active==true)
   	{
      	KeepFileOpen = true;
      	if(FilePointer == NULL)
         {
         	FilePointer = fopen(TableName,"rb");
				if(FilePointer > NULL)
				{
      			KeepFileOpen = false;
				} else
				{
					fprintf(stderr, "STFreeBase::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
				}
         }
      	TempReturn = true;
   		MoveToLocationInFile = (RecordToRead * SizeOfARecord);
			MoveToLocationInFile += StartOfData;
         if(MoveToLocationInFile >= FileSize())
         {
         	// Can not read beyond end of file
         	TempReturn = false;
         } else
         {
      		fseek(FilePointer,MoveToLocationInFile,SEEK_SET);
   			for(ALoopCounter=0;ALoopCounter<(DWORD)SizeOfARecord;ALoopCounter++)
   			{
					DataContainer[ALoopCounter]=fgetc(FilePointer);
            	if(EndOfFile()==true)
            	{
						// Error happened here, file may be damaged.
               	TempReturn = false;
             		break;
               }
            }
      	}
         if((DBFileHeader->DBType & 1) == MEMORY_RESIDENT)
         {
         	if(KeepFileOpen == false)
            {
               if(FilePointer != NULL)
               {
          		   fclose(FilePointer);
                  FilePointer = NULL;
               }
            }
			}
      }
	}
   return(TempReturn);
}

bool STFreeBase::WriteRecord(DWORD RecordToWrite,BYTE *DataContainer)
{
	// Write the record RecordToRead count from beginning of file database.
   // Write data from memory at location DataContainer
   // Return true if data write properly, return false if could not write data
	DWORD ALoopCounter;
   long MoveToLocationInFile;
   bool TempReturn;
   bool KeepFileOpen;

   TempReturn = false;
	if(DBFileHeader->NumberOfRecords > RecordToWrite)
   {
		if(Active==true)
   	{
      	KeepFileOpen = true;
      	if(FilePointer == NULL)
         {
         	FilePointer = fopen(TableName,"r+b");
				if(FilePointer > NULL)
				{
            	KeepFileOpen = false;
				} else
				{
					fprintf(stderr, "STFreeBase::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
				}
         }
      	LastRecNo = RecNo;
      	TempReturn = true;
   		MoveToLocationInFile = (RecordToWrite * SizeOfARecord);
			MoveToLocationInFile += StartOfData;
         if(MoveToLocationInFile > FileSize())
         {
         	// Can write in file or at end only
         	TempReturn = false;
         } else
         {
      		fseek(FilePointer,MoveToLocationInFile,SEEK_SET);
            //DebugPos = ftell(FilePointer);
            //printf("DebugPos at begin write record = %d\n",DebugPos);
   			for(ALoopCounter=0;ALoopCounter<SizeOfARecord;ALoopCounter++)
   			{
               fputc(DataContainer[ALoopCounter],FilePointer);
            }
            //DebugPos = ftell(FilePointer);
            //printf("DebugPos after write record = %d and wrote %d bytes\n",DebugPos,ALoopCounter);
      	}
         if((DBFileHeader->DBType & 1) == MEMORY_RESIDENT)
         {
         	if(KeepFileOpen == false)
            {
               if(FilePointer != NULL)
               {
                  fclose(FilePointer);
                  FilePointer = NULL;
               }
            }
			} else // FILE_RESIDENT
         {
         	DataChanged = false;
         }
      }
	}
   return(TempReturn);
}

void STFreeBase::Open(const char *OpenTableName)
{
   DWORD ALoopCounter,BLoopCounter;
   //char FieldNameTypeAndSizeString[50];
   char *ReadHeader;
   char DebugByte;
   TFieldStructure *FieldStructureNode;
   //DWORD RemoveTrailingSpaces;
   DWORD ReadingRecordCount;
   STString OpenTableNameString;
   int PositionOfExtention, StringLength;
   STStringsEx StringX;

   OpenTableNameString = OpenTableName;
   StringX.Upper(OpenTableNameString);
      //if(OpenTableNameString.find(".FDB",0,false) == OpenTableNameString.Len() - 3)
   if(Active == false)
   {
   	ReadHeader = (char *)DBFileHeader;
   	FieldStructureNode = new TFieldStructure;

      if(TableName != NULL)
      {
      	delete TableName;
         TableName = NULL;
      }
      PositionOfExtention = OpenTableNameString.Pos(".FDB");
      StringLength = (int)OpenTableNameString.Len();
      if((PositionOfExtention > -1)&&(PositionOfExtention + 4 == StringLength))
   	{
      	TableName = new char[strlen(OpenTableName) + 1];
      	strcpy(TableName,OpenTableName);
   	} else
   	{
      	TableName = new char[strlen(OpenTableName) + 5];
      	strcpy(TableName,OpenTableName);
      	strcat(TableName,".fdb");
   	}

      // if file exists, open file
      FilePointer = fopen(TableName,"r+b");
      if(FilePointer > NULL)
      {
         // fill header
         SizeOfARecord = 0;
         IndexColumnList->Clear();
         for(ALoopCounter = 0;ALoopCounter < sizeof(TDBFileHeader);ALoopCounter++)
         {
         	DebugByte = fgetc(FilePointer);
            ReadHeader[ALoopCounter] = DebugByte;
         }
         //DebugPos = ftell(FilePointer);
         //printf("DebugPos at end of read header = %d\n",DebugPos);

         FieldsConstructList->Clear();

         // Read in the Index field
         strcpy(FieldStructureNode->FieldName,"IndexField");
         FieldStructureNode->FieldType=fgetc(FilePointer);
         FieldStructureNode->FieldSize=fgetc(FilePointer);
         SizeOfARecord+=FieldStructureNode->FieldSize;

         // Read in the Field Map
         FieldsConstructList->Add(FieldStructureNode);
         for(ALoopCounter=0;ALoopCounter<DBFileHeader->NumberOfFields;ALoopCounter++)
         {
            FieldStructureNode = new TFieldStructure;
            for(BLoopCounter = 0; BLoopCounter < 20; BLoopCounter++)
            {
               FieldStructureNode->FieldName[BLoopCounter]=fgetc(FilePointer);
            }
            FieldStructureNode->FieldType=fgetc(FilePointer);
            FieldStructureNode->FieldSize=fgetc(FilePointer);
            SizeOfARecord+=FieldStructureNode->FieldSize;

            // Fill FieldsConstructList;
            FieldsConstructList->Add(FieldStructureNode);
         }
         StartOfData = ftell(FilePointer);

         RecNo = 0;

         // Once the header and the field map are loaded into memory we can begin
         // to read and write to the database. Internally we call this active.
         Active=true;


         // This section will load the data into a memory resident copy of the database.
         if((DBFileHeader->DBType & 1) == MEMORY_RESIDENT)
         {
         	ReadingRecordCount = 0;
          	for(ALoopCounter=0;ALoopCounter<DBFileHeader->NumberOfRecords;ALoopCounter++)
            {
					RecordContainer = new BYTE[SizeOfARecord];
               IndexColumnList->Add(RecordContainer);
               ReadRecord(ReadingRecordCount,(BYTE *)RecordContainer);
               ReadingRecordCount++;
            }
            if(FilePointer != NULL)
            {
               fclose(FilePointer);
               FilePointer = NULL;
            }
         } else // FILE_RESIDENT
         {
         	RecordContainer = new BYTE[SizeOfARecord];
         	if(EndOfFile()==false)
            {
            	ReadRecord(0,(BYTE *)RecordContainer);
            }
         }
      } else
		{
			fprintf(stderr, "STFreeBase::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
		}
   }
}

void STFreeBase::Append()
{
	DWORD NewIndex;

   if(Active==true)
   {
   	Last();
   	if(GetValue(0,&NewIndex)==false)
      {
      	NewIndex=0;
      }
   	DBFileHeader->NumberOfRecords++;
   	if((DBFileHeader->DBType & 1) == MEMORY_RESIDENT)
   	{
			RecordContainer = new BYTE[SizeOfARecord];
         IndexColumnList->Add(RecordContainer);
   	}
   	Last();
   	ClearRecord();
      NewIndex++;
      SetIndexField(NewIndex);
      WriteRecord(RecNo,RecordContainer);
      WriteHeader();
   }
}

void STFreeBase::ClearRecord()
{
	DWORD ALoopCounter;

   if(Active==true)
   {
   	for(ALoopCounter=0;ALoopCounter<SizeOfARecord;ALoopCounter++)
      {
      	RecordContainer[ALoopCounter]=0;
      }
   }
}

void STFreeBase::Create(const char *NewTableName)
{
   unsigned int ALoopCounter;
   STString FieldNameTypeAndSizeString,PrepString;
   TFieldStructure *FieldStructureNode;
   //DWORD RemoveTrailingSpaces;
   char WritingChars;
   STString NewTableNameString;
   int PositionOfExtention, StringLength;
   STStringsEx StringX;

   NewTableNameString = NewTableName;
   StringX.Upper(NewTableNameString);

   if(Active==false)
   {
      if(TableName != NULL)
      {
      	delete TableName;
         TableName = NULL;
      }

      PositionOfExtention = NewTableNameString.Pos(".FDB");
      StringLength = NewTableNameString.Len();
      if((PositionOfExtention > -1)&&(PositionOfExtention + 4 == StringLength))
   	{
      	TableName = new char[strlen(NewTableName) + 1];
      	strcpy(TableName,NewTableName);
   	} else
   	{
      	TableName = new char[strlen(NewTableName) + 5];
      	strcpy(TableName,NewTableName);
      	strcat(TableName,".fdb");
   	}

   	DBFileHeader->STFreeBaseVersion=GetVersionInt();
   	DBFileHeader->GenerationDate=0;
   	DBFileHeader->LastTouchedDate=0;
   	DBFileHeader->NumberOfFields=NewFieldsList->Count();
   	DBFileHeader->NumberOfRecords = 0;
   	for(ALoopCounter=0;ALoopCounter<PASSWORD_SIZE;ALoopCounter++)
   	{
   		DBFileHeader->DBReadSecurity[ALoopCounter]='\0';
      	DBFileHeader->DBWriteSecurity[ALoopCounter]='\0';
   	}
   	for(ALoopCounter=0;ALoopCounter<FUTURE_SIZE;ALoopCounter++)
   	{
      	DBFileHeader->FutureUse[ALoopCounter]='\0';
   	}

   	//DebugPos=0;
   	FilePointer = fopen(TableName,"wb");
   	if(FilePointer > NULL)
   	{
   		WriteHeader();

      	// Store info for an Index field, no name needed it is the index field
         FieldStructureNode = new TFieldStructure;
      	FieldStructureNode->FieldType=INDEX_FIELD;
      	FieldStructureNode->FieldSize=sizeof(DWORD);
      	fputc(FieldStructureNode->FieldType,FilePointer);
      	fputc(FieldStructureNode->FieldSize,FilePointer);

      	for(ALoopCounter = 0;ALoopCounter < NewFieldsList->Count();ALoopCounter++)
      	{
         	// Store the field Name
            PrepString = NewFieldsList->Item(ALoopCounter);
            FieldNameTypeAndSizeString = ParserOps->FindFirstWord(PrepString);
         	strcpy(FieldStructureNode->FieldName,(const char *)FieldNameTypeAndSizeString);
         	for(unsigned int i = 0; i < 20; i++)
         	{
            	if(i > FieldNameTypeAndSizeString.Len())
               {
                  WritingChars = 0;
               } else
               {
                  WritingChars = FieldStructureNode->FieldName[i];
               }
            	fputc(WritingChars, FilePointer);
         	}

         	// Store the field Type
            FieldStructureNode->FieldSize = 0;
            PrepString = NewFieldsList->Item(ALoopCounter);
            FieldNameTypeAndSizeString = ParserOps->FindNextWord(PrepString,2);
         	if(FieldNameTypeAndSizeString.Pos("ftString") == 0)
         	{
            	FieldStructureNode->FieldType=STRING_FIELD;
               PrepString = NewFieldsList->Item(ALoopCounter);
               FieldNameTypeAndSizeString = ParserOps->FindNextIdentifier(PrepString,3);
            	FieldStructureNode->FieldSize = StringX.Str2Int(FieldNameTypeAndSizeString);
         	} else
         	if(FieldNameTypeAndSizeString.Pos("ftChar") == 0)
         	{
            	FieldStructureNode->FieldType=CHAR_FIELD;
            	FieldStructureNode->FieldSize=sizeof(char);
         	} else
         	if(FieldNameTypeAndSizeString.Pos("ftInt") == 0)
         	{
            	FieldStructureNode->FieldType=INT_FIELD;
            	FieldStructureNode->FieldSize=sizeof(int);
         	} else
         	if(FieldNameTypeAndSizeString.Pos("ftBool") == 0)
         	{
            	FieldStructureNode->FieldType=BOOL_FIELD;
            	// Store as bool but have space for char.
            	// If a bool in most compiler is 1 bit I
            	// can not save to a file properly.
            	FieldStructureNode->FieldSize=sizeof(char);
         	} else
         	if(FieldNameTypeAndSizeString.Pos("ftBlob") == 0)
         	{
            	FieldStructureNode->FieldType=BLOB_FIELD;
               PrepString = NewFieldsList->Item(ALoopCounter);
               FieldNameTypeAndSizeString = ParserOps->FindNextIdentifier(PrepString,3);
            	FieldStructureNode->FieldSize = StringX.Str2Int(FieldNameTypeAndSizeString);
         	}
         	fputc(FieldStructureNode->FieldType,FilePointer);
         	fputc(FieldStructureNode->FieldSize,FilePointer);
         	//DebugPos = ftell(FilePointer);
      	}
      	//DebugPos = ftell(FilePointer);
         if(FilePointer != NULL)
         {
            fclose(FilePointer);
            FilePointer = NULL;
         }
      	NewFieldsList->Clear();
         delete FieldStructureNode;
      } else
		{
			fprintf(stderr, "STFreeBase::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
		}
      delete TableName;
      TableName = NULL;
   }
}
