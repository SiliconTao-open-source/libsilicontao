/*=============================================================================

    Copyright (C) 2007 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#ifndef STEthernetDevices_included
#define STEthernetDevices_included

#include <STObjectController.h>
#include <STString.h>

class STStringList;

/**
 * STEthernetDevices provides
 *
 * @short network device information.
 */
class STEthernetDevices: public STObjectController
{
	private:
		STString ReturnString;
		STStringList* ReturnList;

   public:
      /**
       * Constructor
       */
      STEthernetDevices();

      /**
       * Destructor
       */
      ~STEthernetDevices();

		/**
		 *	Returns a list of ethernet device names.
		 */
		STStringList *DeviceList();

		/**
		 *	Returns an ASCII string of the IP address of an ethernet device.
		 */
		STString& IpAddress(const STString& DeviceName);

		/**
		 *	Returns an ASCII string of the network address of an ethernet device.
		 */
		STString& NetworkAddress(const STString& DeviceName);

		/**
		 * Returns an ASCII string of the net mask.
		 */
		STString& NetMask(const STString& DeviceName);

		/**
		 *	Returns an ASCII string of the MAC address of an ethernet device.
		 */
		STString& MacAddress(const STString& DeviceName);

		/**
		 *	Returns an ethernet device name that this address is could be connected to given an IP or network address. The address you use does not have to be for the local system.
		 */
		STString& IsOnNetwork(const STString& Address);

		/**
		 *	Returns an ethernet device name that this address localy belongs to given an IP address. Returns blank if it is not a local IP address.
		 */
		STString& IsOurIpAddress(const STString& Address);

		/**
		 * Returns an integer value of the IP address given as a string.
		 */
		unsigned int IpStringToInt(const STString& Address);
};

#endif // STEthernetDevices_included
