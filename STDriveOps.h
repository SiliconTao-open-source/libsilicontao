/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STDriveOps.h
	Object Name:				STDriveOps
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Day Zero:					02152000, February 15, 2000
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:       	alloc.h, stdio.h, string.h
   Ports Used:           	None
   Title of program:			Object library
=============================================================================*/
#ifndef STDriveOps_included
#define STDriveOps_included

#include <STString.h>
#include <STObjectController.h>

class STParserOps;
class STTime;
class STStringList;

//#define ENOENT 	 2
//#define ENOMEM 	12
//#define EACCES 	13
//#define ENOTDIR 	20
//#define ENFILE 	23
//#define EMFILE 	24

/**
 * Provides directory and file functionality that are commanly used.
 *
 * @short Provide directory and file functionality.
 */
class STDriveOps: public STObjectController
{
   private:
      STString ReturnString;
      STParserOps *ParserOps1;
      STTime *Time1;
		double LastFileChangeCheckTime;

   public:
      /**
       * Constructor
       */
      STDriveOps();

      /**
       * Destructor
       */
      ~STDriveOps();

      /**
       * ReturnParts lets you define what information is retuned in the lists when
       * @ref RecurseFiles is called.
       */
      struct TReturnParts
      {
         bool IncludeDrive;
         bool IncludePath;
         bool IncludeFileName;
         bool IncludeFileExtention;
         bool IncludeFileSize;
         bool IncludeFileDate;
         bool IncludeFileAttibutes;
         bool IncludePathSize;
         bool IncludePathDate;
         bool IncludeDirectoryAttributes;
         bool IncludeFileAttributes;
      };
      TReturnParts ReturnParts;

      /**
       * SearchPeramiters lets you define what information is qualified to be retuned
       * in the lists when @ref GetFiles is called.
       */
      /*struct TSearchPeramiters
      {
         STString 	FileAndPathMatch,
              		FileAndPathAvoid,
                  FileMatch,
                  FileAvoid,
                  FileExtentionMatch,
                  FileExtentionAvoid,
                  PathMatch,
                  PathAvoid,
                  DriveMatch,
                  DriveAvoid;
         int 	FileSizeMin,
            	FileSizeMax;
         long 	FileMinDate,
            	FileMaxDate;
         int   FileAttributesMatch,
            	FileAttributesAvoid;
      };
      TSearchPeramiters SearchPeramiters;*/
		void SetMatch(const char* Patern);

      /**
       * Any directories created will used this value for permissions.
       *
       * Default is 777.
       */
      int DefaultPermissions;

      /**
       * Any file I/O errors are handled here and out put a text description.
       */
      //void FileIOErrorCodes(const STString& WhereFrom);

      /**
       * <PRE>
       * Removes path redirectors
       * Each /../, leading ../ or ending /.. is a redirection
       * Various combinations
       * /A/B/C         Proper absolute path
       * ./A/B/C        Proper relative path
       * ../A/B/C       Redirection relative path
       * A/B/C          Valid inproper relative path           <-- correct this to be ./A/B/C
       * /A//B/C        Valid inproper absolute path           <-- correct this to be /A/B/C
       * /A/./B/C       Valid inproper absolute path           <-- correct this to be /A/B/C
       * /A/../B/C      Valid inproper absolute path           <-- correct this to be /B/C
       * /A/B/../../C   Valid inproper compound redirection    <-- correct this to be /C
       * /A/..          Valid inproper absolute path           <-- correct this to be /
       * .A/B/C         Valid inproper file name               <-- correct this to be ./.A/B/C
       * /A../B/C       Valid file name
       * /A/..B/C       Valid file name
       * /A..B/C        Valid file name
		 * </PRE>
       */
		const char* StripPathRedirection(const STString& OriginalPath);

      /**
       * Returns true if the file exists and no processes have the file open for reading or writing.
		 * Read /proc/<PID>/maps for all know PID's to find if the file is open by any processes.
       */
		bool IsFileOpen(const char* FileName);
		bool IsFileOpen(const STString& FileName);

      /**
       * Removes the parent path name from the given path name.
       */
		const char* StripParentPath(const STString& OriginalPath);

      /**
       * Attempts to return the path part of a path to a file.
       * The path does not have to exist, therefor if the given path to a file
       * is only a path to a directory, the parent directory will be returned.
       */
      const char* ExtractPath(const STString& ProvidedPathFile);
      const char* ExtractPath(const char *ProvidedPathFile);

      /**
       * Attempts to return the file part of a path and file.
       * The path and file do not have to exist, therefor if the given path and file
       * is only a path to a directory, the parent directory will be returned.
       */
      const char* ExtractFileName(const STString& PathFileName);
      const char* ExtractFileName(const char* PathFileName);

		/**
		 * Tests the given file name to only contain valid characters. Does not currently test for escaped characters.
		 */
		bool ValidFileName(const STString& FileName);
		bool ValidFileName(const char* FileName);

      /**
       * Returns a double that is the number of days and fraction of a day accorning
       * to @ref STTime::Now since the file was last modified.
       */
      double GetFileMDate(const STString& PathFileName);
      double GetFileMDate(const char* PathFileName);

      /**
       * Returns true if a given file exists.
       */
      bool FileExists(const STString& PathFileName);
      bool FileExists(const char* PathFileName);

      /**
       * Returns the final file object pointed to by the link. Will follow links until the final object is located.
		 * Final object could be any thing including a file, directory, device node or socket.
		 * If givien object is not a link this call will return the given object so it is safe to use if you don't know if the object is a link or not.
		 * If the given object is a link that points to a non-existant object the return string is blank.
       */
		const char* ResolveLink(const STString& LinkPath);
		const char* ResolveLink(const char* LinkPath);

      /**
       * Returns true if a given path exists.
       */
      bool DirExists(const STString& PathFileName);
      bool DirExists(const char* PathFileName);

      /**
       * Tests every part in a given path and creats them if needed.
       */
      bool MakePath(const STString& PathToCreate);

      /**
       * Tests every part in a given path and creats them if needed.
       */
      bool MakePath(const char* PathToCreate);

      /**
       * Recursivly enters sub directories form the current directory looking for
       * files, all files found are placed in the string list. If LevelOfRecurtion = 0
       * then the recursion will continue for as many deepths as the directories go or as far
       * as the system memory will support. If LevelOfRecurtion > 0 then recursion will
       * continue for as many sub-directory deepths as LevelOfRecurtion's value. The starting
       * directory is has the level of 1. If LevelOfRecurtion = 1 then only files in the
       * starting directory will be added to the list, no sub directories will be checked.
       * If LevelOfRecurtion = 2 then the files in the starting directory and the files that
       * are found imediatly in the sub-directories of the starting directory will be added to
       * the list.
       * @ref TReturnParts
       */
      void RecurseFiles(STStringList* FilesStringList, int LevelOfRecurtion);

      /**
       * Works just like RecurseFiles(STStringList *,int) but lets you set the directory that
       * you want to start searching from.
       */
      void RecurseFiles(STStringList* FilesStringList, int LevelOfRecurtion, const STString& DirectoryToStartIn);
      void RecurseFiles(STStringList* FilesStringList, int LevelOfRecurtion, const char* DirectoryToStartIn);

      /**
       * Recursivly enters sub directories form the current directory looking for
       * directories, all directories found are placed in the string list. If LevelOfRecurtion = 0
       * then the recursion will continue for as many deepths as the directories go or as far
       * as the system memory will support. If LevelOfRecurtion > 0 then recursion will
       * continue for as many sub-directory deepths as LevelOfRecurtion's value. The starting
       * directory is has the level of 1. If LevelOfRecurtion = 1 then only directories in the
       * starting directory will be added to the list, no sub directories contents will be checked.
       * If LevelOfRecurtion = 2 then the directories in the starting directory and the directories that
       * are found imediatly in the sub-directories of the starting directory will be added to
       * the list.
       */
      void RecursePath(STStringList* PathStringList, int LevelOfRecurtion);

      /**
       * Works just like RecursePath(STStringList *,int) but lets you set the directory that
       * you want to start searching from.
       */
      void RecursePath(STStringList* PathStringList, int LevelOfRecurtion, const STString& DirectoryToStartIn);
      void RecursePath(STStringList* PathStringList, int LevelOfRecurtion, const char *DirectoryToStartIn);

      /**
       * Returns the name of the current directory that the application is in.
       *
       * Example:
       * <PRE>
       * STString TempString;
       * STDriveOps *DriveOps1;
       *
       * DriveOps1 = new STDriveOps();
       *	TempString = DriveOps1->GetDir();
       *	LineEdit1->setText(TempString);
       * delete DriveOps1;
       * </PRE>
       */
      const char* GetDir();

      /**
       * Tries to find the full path to the parent dir of a given file or path.
       *
       * @li Things you can give it
       * @li /
       * @li ../ (and mutiples of)
       * @li ../../../
       * @li ./
       * @li Aname/ (and mutiples of)
       * @li Aname/Bname/
       *
       * @li Things you can not give it.
       * @li .././
       * @li ./../
       * @li ..//Aname
       * @li Aname/Bname/../Cname/
       * @li Aname/Bname/./Cname/
       * @li Some day I may add the smarts to detect these, but not now.
       */
      const char* GetParentDir(const STString& ChildPath);

		/**
		 * Returns the directory portion of the given target. Target maybe a file or a directory.
		 */
		const char* DirName(const STString& TargetObject);
		const char* DirName(const char* TargetObject);

		/**
		 * Returns the target object name without the path portion.
		 */
		const char* BaseName(const STString& TargetObject);
		const char* BaseName(const char* TargetObject);

      /**
       * Fills a given list with name of directories in the current directory.
       *
       * Example:
       * <PRE>
       * int ALoopCounter;
       * STString OriginalDir;
       * STStringList *Directories;
       * STDriveOps *DriveOps1;
       *
       * Directories = new STStringList();
       * DriveOps1 = new STDriveOps();
       * OriginalDir = DriveOps1->GetDir();
       * chdir("/");
       *
       * DriveOps1->GetDirectories(Directories);
       * for(ALoopCounter=0;ALoopCounter<Directories->Count();ALoopCounter++)
       * {
       * 	MultiLineEdit1->append(Directories->Item(ALoopCounter));
       * }
       * chdir(OriginalDir);
       *
       * delete Directories;
       */
      void GetDirectories(STStringList* DirectoryList);

      /**
       * Fills a given list with name of directories in a spacific directory.
       *
       */
      void GetDirectories(STStringList* DirectoryList, const STString &StartingDir);

      /**
       * Fills a given list with name of files in the current directory.
       *
       * @ref TSearchPeramiters
       *
       * Example:
       * <PRE>
       * int ALoopCounter;
       * STString OriginalDir;
       * STStringList *TheFiles;
       * STDriveOps *DriveOps1;
       *
       * TheFiles = new STStringList();
       * DriveOps1 = new STDriveOps();
       * OriginalDir = DriveOps1->GetDir();
       * chdir("/");
       *
       * // Only look for *.txt files.
       * DriveOps1->SearchPeramiters.FileMatch = "*.txt";
       * DriveOps1->GetFiles(TheFiles);
       * for(ALoopCounter=0;ALoopCounter<TheFiles->Count();ALoopCounter++)
       * {
       * 	MultiLineEdit1->append(TheFiles->Item(ALoopCounter));
       * }
       * chdir(OriginalDir);
       *
       * delete TheFiles;
       */
      void GetFiles(STStringList* FilesList);

      /**
       * Fills a given list with name of files in a spacific directory.
       *
       */
      void GetFiles(STStringList *FilesList, const STString& StartingDir);

      /**
       * Attempts to delete a given file.
       */
      int KillFile(const STString& FileToDelete);
      int KillFile(const char* FileToDelete);

      /**
       * Attempts to delete a given directroy and every thing in it.
       */
      int KillDir(const STString& FileToDelete);
      int KillDir(const char* FileToDelete);

      /**
       * Returns the number of bytes in a given file.
       */
      int FileSize(const STString& FilePathName);

      /**
       *
       */
      //void ResetSearchPeramiters();

	private:
		STString PaternMatch;

      /**
       * Any directories created will used this value for the group.
       *
       * Default is nobody.
       */
      STString DefaultGroup;

      /**
       * Any directories created will used this value for the owner.
       *
       * Default is nobody.
       */
      STString DefaultOwner;

};

#endif // STDriveOps_included
