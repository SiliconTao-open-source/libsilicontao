/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:

=============================================================================*/

#include <STPortMap.h>
#include <STCommonCore.h>

STPortMap::STPortMap()
{
    SetBits(0x00);   
}

void STPortMap::SetBits(BYTE NewBits)
{
   BYTE BitMask;
   
   BitMask = NewBits;
   BIT0 = BitMask & 0x01;
   BitMask >>= 1;
   BIT1 = BitMask & 0x01;
   BitMask >>= 1;
   BIT2 = BitMask & 0x01;
   BitMask >>= 1;
   BIT3 = BitMask & 0x01;
   BitMask >>= 1;
   BIT4 = BitMask & 0x01;
   BitMask >>= 1;
   BIT5 = BitMask & 0x01;
   BitMask >>= 1;
   BIT6 = BitMask & 0x01;
   BitMask >>= 1;
   BIT7 = BitMask & 0x01;

   SetPort();
}

void STPortMap::SetPort()
{
   BYTE BitMask;
   
   BitMask = BIT7;
   BitMask <<= 1;
   BitMask += BIT6;
   BitMask <<= 1;
   BitMask += BIT5;
   BitMask <<= 1;
   BitMask += BIT4;
   BitMask <<= 1;
   BitMask += BIT3;
   BitMask <<= 1;
   BitMask += BIT2;
   BitMask <<= 1;
   BitMask += BIT1;
   BitMask <<= 1;
   BitMask += BIT0;
   
   PortValue = BitMask;
}

STPortMap::operator BYTE()
{   
   SetPort();
   //printf("here = %d\n",PortValue);
   return(PortValue);
}

STPortMap &STPortMap::operator+(const int Incroment)
{
   BYTE BitMaskA, BitMaskB;

   SetPort();
   BitMaskA = PortValue;
   BitMaskB = Incroment;
   SetBits(BitMaskA + BitMaskB);
   return(*this);   
}

STPortMap &STPortMap::operator=(const BYTE NewValue)
{
   BYTE BitMaskB;
   
   SetPort();
   BitMaskB = NewValue;
   SetBits(BitMaskB);   
   return(*this);   
}

STPortMap &STPortMap::operator+=(const STPortMap& Incroment)
{
   BYTE BitMaskA, BitMaskB;
   
   SetPort();
   BitMaskA = PortValue;
   BitMaskB = Incroment.PortValue;
   SetBits(BitMaskA + BitMaskB);   
   return(*this);   
}

STPortMap &STPortMap::operator+=(const int Incroment)
{
   BYTE BitMaskA, BitMaskB;
   
   SetPort();
   BitMaskA = PortValue;
   BitMaskB = Incroment;
   SetBits(BitMaskA + BitMaskB);   
   return(*this);   
}

STPortMap &STPortMap::operator++(int)
{
   BYTE BitMaskA, BitMaskB;
   
   SetPort();
   BitMaskA = PortValue;
   BitMaskB = 1;
   SetBits(BitMaskA + BitMaskB);   
   return(*this);   
}

STPortMap &STPortMap::operator>>=(const int RotateRight)
{
   BYTE BitMask;
   
   SetPort();
   BitMask = PortValue;
   BitMask >>= RotateRight;
   SetBits(BitMask);   
   return(*this);   
}

STPortMap &STPortMap::operator<<=(const int RotateLeft)
{
   BYTE BitMask;
   
   SetPort();
   BitMask = PortValue;
   BitMask <<= RotateLeft;
   SetBits(BitMask);   
   return(*this);   
}

STPortMap &STPortMap::operator|=(const int OrValue)
{
   BYTE BitMaskA, BitMaskB;
   
   SetPort();
   BitMaskA = PortValue;
   BitMaskB = OrValue;
   SetBits(BitMaskA | BitMaskB);   
   return(*this);   
}

STPortMap &STPortMap::operator&=(const int AndValue)
{
   BYTE BitMaskA, BitMaskB;
   
   SetPort();
   BitMaskA = PortValue;
   BitMaskB = AndValue;
   SetBits(BitMaskA & BitMaskB);   
   return(*this);   
}

STPortMap &STPortMap::operator!=(const int NotValue)
{
   BYTE BitMask, Result, Compliment;
   
   SetPort();
   BitMask = PortValue;
   Compliment ^= NotValue;
   Result = BitMask & Compliment;
   SetBits(Result);   
   return(*this);   
}
