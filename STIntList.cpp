/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:
      - test speed of new VS malloc VS STMalloc
      - Make more efficient. It is inefficient to malloc 8 bytes of memory to store
        4 bytes of data. It should really use an array of about 100 ints that is
        a block that can be malloced and freed by the block. That way 100 ints
        can be stored into 404 bytes instead of 800 bytes.

=============================================================================*/

/*=============================================================================
   Note:
      - STMalloc uses STList to track memory leeks so STIntList can not use
        STMalloc to get memory.

=============================================================================*/

#include <STIntList.h>
#include <STCommonCore.h>

// Do not use STMalloc here, would cause recursive loop.
#include <stdlib.h>

STIntList::STIntList(const char* NewObjectName)
: STList(NewObjectName)
{
	SetClassName("STIntList");
}

STIntList::STIntList()
: STList()
{
	SetClassName("STIntList");
}

STIntList::~STIntList()
{
   STIntList::Clear();
}

unsigned int STIntList::Add(int NewInt)
{
	unsigned int NewCount = 0;
   int *NewIntPointer;

   NewIntPointer = new int;
   *NewIntPointer = NewInt;
   NewCount = STList::Add(NewIntPointer);
	return(NewCount);
}

unsigned int STIntList::Insert(unsigned int Position, int NewInt)
{
	unsigned int NewCount = 0;
   int *NewIntPointer;

   NewIntPointer = new int;
   *NewIntPointer = NewInt;
   NewCount = STList::Insert(Position, NewIntPointer);
	return(NewCount);
}

void STIntList::Delete(int CellToKill)
{
   int *NewIntPointer;

   NewIntPointer = (int *)STList::Item(CellToKill);
   delete NewIntPointer;
   STList::Delete(CellToKill);
}

void STIntList::Clear()
{
   unsigned int ALoopCounter,ListCount;

   ListCount = Count();
   for(ALoopCounter = 0;ALoopCounter < ListCount;ALoopCounter++)
   {
      STIntList::Delete(0);
   }
}

int STIntList::Item(int ItemIndex)
{
   int *NewIntPointer;
   int TempReturn;

   NewIntPointer = (int *)STList::Item(ItemIndex);
   TempReturn = *NewIntPointer;

   return(TempReturn);
}
