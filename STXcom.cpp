/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/
#include <STXcom.h>
#include <STCommonCore.h>
#include <STSystemAPI.h>
#include <STStringList.h>
//	STString PortAvailabilityCmd;
#include <STString.h>
#include <string.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>

STXcom::STXcom(const STString& PortNumber)
{
   InitSTXcom(PortNumber);
}

STXcom::STXcom(const char *PortNumber)
{
   STString TempString;

   TempString = PortNumber;
   InitSTXcom(TempString);
}

void STXcom::InitSTXcom(const STString& PortNumber)
{
   //SystemTimer = new QTime;
	printf("STXcom initialize port device '%s'\n",(const char*)PortNumber);
   DebugLevel=0;
	TransmitDelay = 0;
   BaudRate = B9600;
   PortIsActive = false;
   WordLength = WORD_LENGTH_8;
   StopBits = STOP_BITS_1;
   ParityBit = NO_PARITY;
   //PortDialog = NULL;
   Port=PortNumber;
   FileDevice = 0;
}

STXcom::~STXcom()
{
   // Destructor
   //delete SystemTimer;

   if(DebugLevel > 2)
   {
      printf("STXcom::~STXcom\n");
   }
   if(PortIsActive == true)
   {
      Deactivate();
   }
   /*if(PortDialog != NULL)
   {
      delete PortDialog;
   } */
}

int STXcom::SetWordLength(int SelectedWordLength)
{
   if(DebugLevel > 2)
   {
      printf("STXcom::SetWordLength(%d)\n", SelectedWordLength);
   }
   if(PortIsActive==false)
   {
	  WordLength=SelectedWordLength;
   }
   return(0);
}

int STXcom::SetParity(int ParityValue)
{
   if(DebugLevel > 2)
   {
      printf("STXcom::SetParity(%d)\n", ParityValue);
   }
   if(PortIsActive==false)
   {
      ParityBit = ParityValue;
   }
   return(0);
}

bool STXcom::SetDebugLevel(int NewDebugLevel)
{
   if(DebugLevel != NewDebugLevel)
   {
      DebugLevel = NewDebugLevel;
      printf("STXcom::SetDebugLevel = %d\n",DebugLevel);
   }
   return(true);
}

void STXcom::SetDataTerminalReady(bool Value)
{
   /* Modem lines in ioctl-types.h
   TIOCM_LE
   TIOCM_DTR
   TIOCM_RTS
   TIOCM_ST
   TIOCM_SR
   TIOCM_CTS
   TIOCM_CAR
   TIOCM_RNG
   TIOCM_DSR
   TIOCM_CD
   TIOCM_RI
   */
   int HandShakeBitStatus;

   if(PortIsActive==true)
   {
      ioctl(FileDevice,TIOCMGET,&HandShakeBitStatus);
      if(Value == 1)
      {
         HandShakeBitStatus |= TIOCM_DTR;
      } else
      {
         HandShakeBitStatus &= ~TIOCM_DTR;
      }
      ioctl(FileDevice,TIOCMSET,&HandShakeBitStatus);
   }
}

void STXcom::SetRequestToSend(bool Value)
{
   int HandShakeBitStatus;

   if(PortIsActive==true)
   {
      ioctl(FileDevice,TIOCMGET,&HandShakeBitStatus);
      if(Value == 1)
      {
         HandShakeBitStatus |= TIOCM_RTS;
      } else
      {
         HandShakeBitStatus &= ~TIOCM_RTS;
      }
      ioctl(FileDevice,TIOCMSET,&HandShakeBitStatus);
   }
}

bool STXcom::GetDataSetReady()
{
   int HandShakeBitStatus;
   bool TempReturn;

   if(PortIsActive==true)
   {
      ioctl(FileDevice,TIOCMGET,&HandShakeBitStatus);
      TempReturn = HandShakeBitStatus & TIOCM_DSR;
   }
   return(TempReturn);
}

bool STXcom::GetClearToSend()
{
   int HandShakeBitStatus;
   bool TempReturn;

   if(PortIsActive==true)
   {
      ioctl(FileDevice,TIOCMGET,&HandShakeBitStatus);
      TempReturn = HandShakeBitStatus & TIOCM_CTS;
   }
   return(TempReturn);
}

bool STXcom::GetCarrierDetect()
{
   int HandShakeBitStatus;
   bool TempReturn;

   if(PortIsActive==true)
   {
      ioctl(FileDevice,TIOCMGET,&HandShakeBitStatus);
      TempReturn = HandShakeBitStatus & TIOCM_CAR;
   }
   return(TempReturn);
}

bool STXcom::GetRingIndicator()
{
   int HandShakeBitStatus;
   bool TempReturn;

   if(PortIsActive==true)
   {
      ioctl(FileDevice,TIOCMGET,&HandShakeBitStatus);
      TempReturn = HandShakeBitStatus & TIOCM_RI;
   }
   return(TempReturn);
}

int STXcom::SetStopBits(int NumberOfStopBits)
{
   if(DebugLevel > 2)
   {
      printf("STXcom::SetStopBits(%d)\n", NumberOfStopBits);
   }
   if(PortIsActive==false)
   {
	  StopBits=NumberOfStopBits;
   }
	return(0);
}

byte STXcom::Getb()
{
   byte Temp;

   if(DebugLevel > 5)
   {
      printf("STXcom::Getb\n");
   }
   Temp=(byte)Getc();
   return(Temp);
}

/*void STXcom::StartTimeOutTimer(int TimeOutMax)
{
   TimeOutCount = 0;
   LastTime = SystemTimer->second();
   MaxTimeCount = TimeOutMax;
}

bool STXcom::TimeIsUp()
{
   int ThisSecond;
   bool TempReturn;

   TempReturn = false;
   ThisSecond = SystemTimer->second();
   if(ThisSecond != LastTime)
   {
      TimeOutCount++;
   }
   if(TimeOutCount > MaxTimeCount)
   {
      TempReturn = true;
   }
   return(TempReturn);
}*/

char STXcom::Getc()
{
	char Data = 0;
   //int DataCount;

   if(DebugLevel > 5)
   {
      printf("STXcom::Getc\n");
   }
   if(PortIsActive==true)
   {
      IsDataWaiting();
      if(RxDataIsWaiting==true)
      {
         Data=RxDataBuffer;
         RxDataBuffer=0;
         RxDataIsWaiting=false;
      }
   }
	return(Data);
}

bool STXcom::IsDataWaiting()
{
   char ReadData;
   int DataCount;
   bool TempResult;

   if(DebugLevel > 5)
   {
      printf("STXcom::IsDataWaiting()\n");
   }
   TempResult=false;
   if(PortIsActive==true)
   {
      if(RxDataIsWaiting==true)
      {
         TempResult=true;
      } else
      {
         if(GetNumberOfBytesWaiting() > 0)
         {
            DataCount=read(FileDevice,&ReadData,1);

            if(DataCount == 1)
            {
               RxDataBuffer=ReadData;
               RxDataIsWaiting=true;
               TempResult=true;
            }
         }
      }
   }
	return(TempResult);
}

int STXcom::GetNumberOfBytesWaiting()
{
   int TempReturn;

   if(DebugLevel > 5)
   {
      printf("STXcom::GetNumberOfBytesWaiting()\n");
   }
   TempReturn = 0;
   if(PortIsActive==true)
   {
      ioctl(FileDevice,FIONREAD,&TempReturn);
   }
   return(TempReturn);
}

int STXcom::SetBaud(int BaudCode)
{
   bool ValidCode;
   int ReturnErrorCode;

   if(DebugLevel > 2)
   {
      printf("STXcom::SetBaud(%d)\n", BaudCode);
   }
   ReturnErrorCode = ST_ERROR_NO_ERROR;
   if(PortIsActive==false)
   {
      ValidCode = false;
      switch(BaudCode)
      {
         // All these listed values are valid.
         case 0:
         case 50:
         case 75:
         case 110:
         case 134:
         case 150:
         case 200:
         case 300:
         case 600:
         case 1200:
         case 1800:
         case 2400:
         case 4800:
         case 9600:
         case 19200:
         case 38400:
         case 57600:
         case 115200:
         case 230400:
         case 460800:    ValidCode = true;  		break;
         default:        ValidCode = false;  	break;
      }
      if(ValidCode == true)
      {
         BaudRate=BaudCode;
      } else
      {
         ReturnErrorCode = ST_ERROR_INVALID_BAUD_RATE;
         printf("ST_ERROR_INVALID_BAUD_RATE\n");
      }
   }
	return(ReturnErrorCode);
}

void STXcom::SetTransmitDelay(int MicroSeconds)
{
   if(DebugLevel > 2)
   {
      printf("STXcom::SetTransmitDelay(%d)\n", MicroSeconds);
   }
   TransmitDelay = MicroSeconds;
	if(TransmitDelay > 300000000)
	{
		printf("STXcom::SetTransmitDelay(%d); WARNING delay is greater then 5 minutes!\n",MicroSeconds);
	}
}

int STXcom::PutBytes(byte *BytesStoragePointer,int WriteSize)
{
   int ALoopCounter;

   if(DebugLevel > 2)
   {
      printf("STXcom::PutBytes(*, %d)\n", WriteSize);
   }

   for(ALoopCounter=0;ALoopCounter<WriteSize;ALoopCounter++)
   {
      if(ALoopCounter % 100 == 0)
      {
         //emit DataReadWrite(ALoopCounter);
      }
      if(TransmitDelay > 0)
      {
         usleep(TransmitDelay);
      }
      if(DebugLevel > 2)
      {
         printf("0x%02X = %c\n",BytesStoragePointer[ALoopCounter],BytesStoragePointer[ALoopCounter]);
      }
      Putc(BytesStoragePointer[ALoopCounter]);
   }
   return(0);
}

int STXcom::GetBytes(byte *BytesStoragePointer,int ReadSize)
{
   // should do a system time out here
   //int ALoopCounter,BLoopCounter,CLoopCounter,DataIndexCounter;
   int ReturnCount,TempByteCount,ByteCount;
   //bool Done;

   ReturnCount = 0;

   // RxDataIsWaiting == true only if data is waiting and we
   // previously called IsDataWaiting that read one byte and
   // stored it into RxDataBuffer. If we did that we must get
   // that byte first.
   if(RxDataIsWaiting == true)
   {
      ReadSize--;
      BytesStoragePointer[0]=RxDataBuffer;
      RxDataBuffer=0;
      RxDataIsWaiting=false;
      BytesStoragePointer++;
      ReturnCount = 1;
   }

   ByteCount = GetNumberOfBytesWaiting();
   if(ByteCount > ReadSize)
   {
      ByteCount = ReadSize;
   }

   if(ByteCount > 0)
   {
      TempByteCount = read(FileDevice,BytesStoragePointer,ByteCount);

      // It is possible that we only received one byte and it is in
      // RxDataBuffer, if so then ReturnCount is now -1, that is wrong.
      if(TempByteCount < 0)
      {
         TempByteCount = 0;
      }

      ReturnCount += TempByteCount;
   }
   return(ReturnCount);
}

STString STXcom::Gets()
{
    ReturnString="";
    return(ReturnString);
}

int STXcom::Putc(char Data)
{
   int TempReturn;

   TempReturn = -1;
   if(DebugLevel > 5)
   {
      printf("STXcom::Putc\n");
   }

   if(PortIsActive == true)
   {
      TempReturn = write(FileDevice, &Data, 1);

		// The documentation for write seems to be wrong. The return value is the number of bytes written if successful.
		if(TempReturn == 1) TempReturn = 0;


      if(TransmitDelay > 0)
      {
         usleep(TransmitDelay);
      }
   }
   return(TempReturn); // Should be and error code
}

int STXcom::Puts(const char* CharArray)
{
   STString TempString = CharArray;
   return(Puts(TempString));
}

int STXcom::Puts(const STString& String)
{
   if(DebugLevel > 5)
   {
      printf("STXcom::Puts\n");
   }
   if(PortIsActive == true)
   {
      for(unsigned int i = 0; i < String.Len(); i++)
      {
         Putc(String[i]);
      }
   }
   return(0);
}

STString STXcom::StopBitsAsText()
{
   if(DebugLevel > 2)
   {
      printf("STXcom::StopBitsAsText\n");
   }
   switch(StopBits)
   {
      case STOP_BITS_1:		ReturnString="STOP_BITS_1";  	break;
      case STOP_BITS_2:		ReturnString="STOP_BITS_2"; 	break;
      case STOP_BITS_15:   ReturnString="STOP_BITS_1.5";	break;
   }
   return(ReturnString);
}

STString STXcom::ParityAsText()
{
   if(DebugLevel > 2)
   {
      printf("STXcom::ParityAsText\n");
   }
   switch(ParityBit)
   {
      case NO_PARITY:     ReturnString="NO_PARITY";  	break;
      case EVEN_PARITY:   ReturnString="EVEN_PARITY"; break;
      case ODD_PARITY:    ReturnString="ODD_PARITY";	break;
   }
   return(ReturnString);
}

STString STXcom::WordLengthAsText()
{
   if(DebugLevel > 2)
   {
      printf("STXcom::WordLengthAsText\n");
   }
   switch(WordLength)
   {
      case WORD_LENGTH_7:    ReturnString="WORD_LENGTH_7";	break;
      case WORD_LENGTH_8:    ReturnString="WORD_LENGTH_8";	break;
   }
   return(ReturnString);
}

STString STXcom::BaudRateAsText()
{
   if(DebugLevel > 2)
   {
      printf("STXcom::BaudRateAsText\n");
   }
	ReturnString.Sprintf("B%d", BaudRate);
   return(ReturnString);
}

bool STXcom::Activate()
{
   int ConfigurationBits;
   bool TempReturn, ReadyToActivate;

   if(DebugLevel > 1)
   {
      fprintf(stderr, "STXcom::Activate Port = %s\n",(const char*)Port);
   }

   ReadyToActivate=true;
   TempReturn=false;

   // I left this here so you are forced into sending it a
   // /dev/ device path, that way you know you are connecting to
   // a device of some kind.
   if(Port.Pos("/dev/") != 0)
   {
      ReadyToActivate = false;
      fprintf(stderr, "I can't start the port %s\n",(const char *)Port);
      fprintf(stderr, "Make sure that you are accessing a /dev/ device!\n");
   }
   if(PortIsActive==true)
   {
      ReadyToActivate=false;
      fprintf(stderr, "That port already active\n");
   }
	STSystemAPI SystemAPI;
	STStringList OpenPorts;
	STString PortAvailabilityCmd;
	PortAvailabilityCmd.Sprintf("dmesg | grep %s$ | awk '{print $1\" \"$2\" \"$NF}'", (const char*)Port);
	SystemAPI.SystemCommand(PortAvailabilityCmd, &OpenPorts);
	if(OpenPorts.Count() > 0)
	{
		fprintf(stderr, "STXcom::Activate Port is currently in use by another process.\n");
		while(OpenPorts.Count() > 0)
		{
			fprintf(stderr, "%s\n", (const char*)OpenPorts.Item(0));
			OpenPorts.Delete(0);
		}
	}
   if(ReadyToActivate == true)
   {
      ConfigurationBits = (CLOCAL | CREAD);

      //if(HardWareFlowControl == true) ConfigurationBits |= CRTSCTS;
      if(DebugLevel > 2)
		{
			fprintf(stderr, "Stop bits = %s\n", (const char*)StopBitsAsText());
			fprintf(stderr, "Parity = %s\n", (const char*)ParityAsText());
			fprintf(stderr, "Word length = %s\n", (const char*)WordLengthAsText());
			fprintf(stderr, "Baud rate = %s\n", (const char*)BaudRateAsText());
		}
      switch(ParityBit)
      {
         case NO_PARITY:     ConfigurationBits &= ~(PARENB|PARODD);  break;
         case EVEN_PARITY:   ConfigurationBits |= PARENB;            break;
         case ODD_PARITY:    ConfigurationBits |= (PARENB| PARODD);  break;
      }

      switch(WordLength)
      {
         case WORD_LENGTH_7:    ConfigurationBits|=CS7;     break;
         case WORD_LENGTH_8:    ConfigurationBits|=CS8;     break;
      }

      switch(BaudRate)
      {
         case 0:         ConfigurationBits|=B0;     	break;
         case 50:        ConfigurationBits|=B50;    	break;
         case 75:        ConfigurationBits|=B75;    	break;
         case 110:       ConfigurationBits|=B110;   	break;
         case 134:       ConfigurationBits|=B134;   	break;
         case 150:       ConfigurationBits|=B150;   	break;
         case 200:       ConfigurationBits|=B200;   	break;
         case 300:       ConfigurationBits|=B300;   	break;
         case 600:       ConfigurationBits|=B600;   	break;
         case 1200:      ConfigurationBits|=B1200;  	break;
         case 1800:      ConfigurationBits|=B1800;  	break;
         case 2400:      ConfigurationBits|=B2400;  	break;
         case 4800:      ConfigurationBits|=B4800;  	break;
         case 9600:      ConfigurationBits|=B9600;  	break;
         case 19200:     ConfigurationBits|=B19200; 	break;
         case 38400:     ConfigurationBits|=B38400; 	break;
         case 57600:     ConfigurationBits|=B57600; 	break;
         case 115200:    ConfigurationBits|=B115200;	break;
         case 230400:    ConfigurationBits|=B230400;	break;
         case 460800:    ConfigurationBits|=B460800;	break;
         default:        ConfigurationBits|=B19200; 	break;
      }

      RxDataBuffer=0;
      RxDataIsWaiting=false;

		if(DebugLevel > 1)
		{
			fprintf(stderr, "STXcom - FileDevice = open('%s', O_RDWR | O_NOCTTY);\n",(const char*)Port);
		}
      FileDevice = open((const char*)Port, O_RDWR | O_NOCTTY);
		if(DebugLevel > 1)
		{
			fprintf(stderr, "STXcom - open returned %d\n", FileDevice);
		}
      if(FileDevice > -1)
      {
			if(DebugLevel > 1)
			{
				fprintf(stderr, "STXcom - port open\n");
				fprintf(stderr, "STXcom - tcgetattr(FileDevice,&OldTerminalIOPointer);\n");
			}
         // save current port settings
         tcgetattr(FileDevice, &OldTerminalIOPointer);
			if(DebugLevel > 1)
			{
				fprintf(stderr, "STXcom - bzero(&NewTerminalIOPointer,sizeof(NewTerminalIOPointer));\n");
			}
         bzero(&NewTerminalIOPointer, sizeof(NewTerminalIOPointer));

         // set new port settings
         NewTerminalIOPointer.c_cflag = ConfigurationBits;
         NewTerminalIOPointer.c_iflag = IXANY; //IGNPAR | ICRNL;
         NewTerminalIOPointer.c_oflag = CR0;

         NewTerminalIOPointer.c_lflag = 0;

         // Wait for 0 bytes after 0 seconds, because we use this
         // we must also use the ioctl to find if data is waiting.
         NewTerminalIOPointer.c_cc[VMIN]=0;
         NewTerminalIOPointer.c_cc[VTIME]=0;

			if(DebugLevel > 1)
			{
				fprintf(stderr, "STXcom - tcflush(FileDevice, TCIFLUSH);\n");
			}
         tcflush(FileDevice, TCIFLUSH);
			if(DebugLevel > 1)
			{
				fprintf(stderr, "STXcom - tcflush(FileDevice, TCIFLUSH);\n");
			}
         tcsetattr(FileDevice,TCSANOW, &NewTerminalIOPointer);
         PortIsActive = true;
         TempReturn = true;
         /*if(PortDialog != NULL)
         {
            PortDialog->SetActive(true);
         } */
      } else
		{
			fprintf(stderr, "STXcom::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
			FileDevice = 0;
			if(DebugLevel > 1)
			{
				fprintf(stderr, "STXcom - port failed to open\n");
			}
		}
   }
   return(TempReturn);
}

void STXcom::Flush()
{
 tcflush(FileDevice, TCIFLUSH);
}

bool STXcom::IsPortActive()
{
   return(PortIsActive);
}

int STXcom::Deactivate()
{
	printf("DebugLevel = %d\n", DebugLevel);
	if(PortIsActive == true)
	{
      printf("STXcom::Deactivate\n");
      if(FileDevice > 0)
		{
         PortIsActive = false;
			printf("tcsetattr(FileDevice,TCSANOW,&OldTerminalIOPointer);\n");
         tcsetattr(FileDevice,TCSANOW,&OldTerminalIOPointer);

			printf("close(FileDevice %d);\n", FileDevice);
         close(FileDevice);

			printf("FileDevice = 0;\n");
			FileDevice = 0;
			printf("PortIsActive = false;\n");
			PortIsActive = false;
      } else
		{
         fprintf(stderr,"STXcom::Deactivate ERROR, device not found!\n");
		}
	} else
	{
      if(DebugLevel > 1)
      {
         printf("STXcom::Deactivate END\n");
      }
	}
	return(0);
}
