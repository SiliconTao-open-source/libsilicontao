/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STTime.h
   Object Name: 				STTime
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Day Zero:					02152000, February 15, 2000
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:      	STList.h
   Ports Used:           	None
   Title of program:			Object library
   Description:
=============================================================================*/

#ifndef STTime_included
#define STTime_included

#include <STString.h>
#include <time.h>
#include <sys/time.h>

class STIntList;
class STStringList;

/**
 * Things for time and date functionality.
 *
 * FormattingOfString:
 * @li Bits		State 		Defined   			Effect
 * @li 0:1		0				DF_MONTH_NUMBER	Shows months as a number 1 to 12
 * @li 0:1		1				DF_MONTH_SHORT		Shows months as a short text string, Ex Jan Feb Mar Apr May Jun Jul Aug \<BR\>
 * @li 0:1		2				DF_MONTH_LONG		Shows months as a long text string, Ex January February March...
 * @li 2:3		0				DF_YEAR_SHORT		Shows year as a two digit number, Ex 79 = 1979, 54 = 1954, 01 = ????
 *	@li 2:3		1				DF_YEAR_LONG		Shows year as a four digit number, Ex 1979, 1954, 2001, 2701
 * @li 2:3		2				DF_YEAR_WINDED		Shows year as verbose text words, Ex 1981 = Nineteen hundred and eighty one.
 *
 *
 *
 *
 * @short Commanly used time functions.
 */
class STTime
{
   private:
      STString ReturnString;
		struct timeval LastTimeValue;

	//protected:
      //

   public:
      /**
       * Constructor
       */
      STTime();

      /**
       * Destructor
       */
      ~STTime();

      /**
       *
       */
      int DaysSinceBce(int YearNumber);

      /**
       *
       */
      bool LeapYear(int YearNumber);

      /**
       * Returns the number of days in each month. Does not test for leap year. Month numbers are
       * from 0 to 11 where 0 = Jan and 11 = Dec.
       */
      int NumberOfDaysInMonth(int MonthNumber);

      /**
       * When given a list of events as date time values, this function will return the
       * index value of the first event that the current time has passed. If an entery in the
       * list does not have a date, the current date is used and the event is compared only by
       * time. If a time is not given the the event is commaired as if time was midnight the start
       * of the given date. The passing of the first event would return a 0, no events are represented
       * by -1.
       *
       * Change the IntList to a double list.
       */
      int EventPassed(STIntList *ListOfEvents);

      /**
       * When given a list of events as text dates and times, this function will return the
       * index value of the first event that the current time has passed. If an entery in the
       * list does not have a date, the current date is used and the event is compared only by
       * time. If a time is not given the the event is commaired as if time was midnight the start
       * of the given date. The passing of the first event would return a 0, no events are represented
       * by -1.
       */
      int EventPassed(STStringList *ListOfEvents);

      /**
       *	Returns the amount of fraction of a day from a given time.
       */
      double TimeInFracOfDay(time_t TimeValues);

      /**
       *	Just like TimeInFracOfDay but with micro seconds of resolution.
       */
		double TimeInFracOfDayHiRes(struct timeval TimeValues);

      /**
       * Returns the name of the month when given the number of days since January 1 of
       * the same year. The current year is use do find if the year is a leap year or not.
       */
      STString DayOfYearToMonth(int DayCount,int CurrentYear);

      /**
       * Returns day of the month when given the number of days since January 1 of
       * the same year. The current year is use do find if the year is a leap year or not.
       */
      int DayOfYearToDayMonth(int DayCount,int CurrentYear);

      /**
       *	Returns the amount of days since December 30 1899 from a given time.
       */
      double DaysCount(time_t TimeValues);

      /**
       *	Returns the amount of elapsed time since Midnight of the curren day.
       */
      double Time();

      /**
       *	Returns the amount of days since December 30 1899.
       */
      double Date();

      /**
       *	Returns the amount of elapsed time since December 30 1899. Days are whole numbers,
       * while hours, minites and seconds are represented in the fractional part of the number.
       * A number like 0.25 whould be 6:00 AM December 30 1899, and 3.75 would be January 2, 1900
       * at 6:00 PM. This number should be directoy compatible with Win32 date and time.
       */
      double Now();

      /**
       *	Returns a string showing the now date and time in a formatted maner.
       */
      STString DateTimeString(int FormattingOfString);

      /**
       * Attempts to convert a formatted or unformatted text string to a date time value.
       */
      double String2DateTime(const STString& TheDateTimeString, int FormattingOfString);

      /**
       * From a given number between 1 and 12 this will return a text string of the name for
       * the corisponding month with the format provided.
       */
      STString IntMonthToString(int MonthNumber, int FormattingOfString);

      /**
       * Converts a date time value in to a formatted string.
       *
       * Formatting is like so.
       * @li TF_ONE : MM/DD/YYYY HH:MM:SS TT
       */
      STString DateTime2String(double TheDateTimeValue,int FormattingOfString);

      /**
       * Returns a string that is the Year Month and Day like 20010905
       */
      STString DateYMDCode();

      // Month formatting
      const static int DF_MONTH_NUMBER = 0;
      const static int DF_MONTH_SHORT = 1;
      const static int DF_MONTH_LONG = 2;
      const static int DF_MONTH_MASK = 3;

      // Year formatting
      const static int DF_YEAR_SHORT = 0;
      const static int DF_YEAR_LONG = 4;
      const static int DF_YEAR_WINDED = 8;
      const static int DF_YEAR_MASK = 12;

      /**
       * Prints to STDOUT the current system seconds and microseconds and the elapsed seconds and microseconds since last time called.
		 * This helps to find bottle necks in code but is not recommeded for use outside of debugging.
       */
		void PrintTimeMarker();

      /**
       * Get the elapsid time as a float.
       */
		float GetElapsidTime();

      /**
       * Resets the timmer without printing.
       */
		void ResetTimeMarker();
};

#endif // STTime_included
