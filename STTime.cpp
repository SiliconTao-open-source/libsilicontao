/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:

=============================================================================*/

#include <STTime.h>
#include <STCommonCore.h>
#include <STIntList.h>
#include <STStringList.h>
#include <STStringsEx.h>
#include <math.h>
#include <stdio.h>

STTime::STTime()
{
	LastTimeValue.tv_sec = 0;
	LastTimeValue.tv_usec = 0;
}

STTime::~STTime()
{
}

double STTime::DaysCount(time_t TimeValues)
{
   double TempReturn;
   struct tm *TimeStruct;
   double DaysInteger,DaysThisYear;
   double ThisLeepYear;

   TimeStruct = localtime(&TimeValues);

   //tm_year = number of year since January 1 1900
   //tm_mon = number of months since Januay 1 current year
   //tm_day = number of days since the first of this month

   // There are 365.2475 days a year

   DaysInteger = rint(TimeStruct->tm_year * 365.2475);
   //printf("Years = %d\n",TimeStruct->tm_year);

   //printf("DaysInteger with part of leep year = %g\n",DaysInteger);
   DaysInteger -= floor(TimeStruct->tm_year / 100);

   //printf("DaysInteger till Jan 1 of current year = %g\n",DaysInteger);

   // If this year is a leep year then add one day
   ThisLeepYear = rint(TimeStruct->tm_year * 365.2475) - floor(TimeStruct->tm_year * 365.2475);

   //printf("LeepYear calculation part a = %g\n",floor(TimeStruct->tm_year * 365.25 - 0.25));
   //printf("LeepYear calculation part b = %g\n",rint(TimeStruct->tm_year * 365.25 - 0.25));
   //printf("LeepYear final value = %g\n",LeepYear);

   DaysThisYear = 0;
   if(TimeStruct->tm_mon > 0)
   {
      // Days in Januray
      DaysThisYear += 31;
   }
   if(TimeStruct->tm_mon > 1)
   {
      // Days in February
      DaysThisYear += 28 + ThisLeepYear;
   }
   if(TimeStruct->tm_mon > 2)
   {
      // Days in March
      DaysThisYear += 31;
   }
   if(TimeStruct->tm_mon > 3)
   {
      // Days in April
      DaysThisYear += 30;
   }
   if(TimeStruct->tm_mon > 4)
   {
      // Days in May
      DaysThisYear += 31;
   }
   if(TimeStruct->tm_mon > 5)
   {
      // Days in June
      DaysThisYear += 30;
   }
   if(TimeStruct->tm_mon > 6)
   {
      // Days in July
      DaysThisYear += 31;
   }
   if(TimeStruct->tm_mon > 7)
   {
      // Days in August
      DaysThisYear += 31;
   }
   if(TimeStruct->tm_mon > 8)
   {
      // Days in Septemper
      DaysThisYear += 30;
   }
   if(TimeStruct->tm_mon > 9)
   {
      // Days in October
      DaysThisYear += 31;
   }
   if(TimeStruct->tm_mon > 10)
   {
      // Days in November
      DaysThisYear += 31;
   }
   //printf("DaysThisYear = %g\n",DaysThisYear);

   // December is not here because any month greather then the 12th month is
   // a lesser month of the next year.
   DaysInteger += DaysThisYear;

   //if(TimeStruct->tm_mday > 1)
   //{
      DaysInteger += TimeStruct->tm_mday;
   //}

   //printf("Days this month = %g\n",TimeStruct->tm_mday);

   // Why is this measured from December 30 1899???
   // 1 ) No body at Microshit can count, or
   // 2 ) Microshit did not invent the christian calander so they don't want to use it.
   DaysInteger+=2;

   TempReturn=DaysInteger;
   return(TempReturn);
}

double STTime::TimeInFracOfDay(time_t TimeValues)
{
   struct tm *TimeStruct;

   double TempReturn, DaysFraction;
   double SecondsTotal, SecondsInHours, SecondsInMinutes;
	//double MilliSeconds;
   TimeStruct = localtime(&TimeValues);

   // To detrmin the day fraction. I did not want to have a race condition because
   // of floating point math errors in the Intel architecture. So to remove this
   // problem we will add up all the seconds in a day and then find that value as
   // the fraction. Total seconds in a day is 86400
   SecondsInHours = TimeStruct->tm_hour * 60 * 60;
   SecondsInMinutes = TimeStruct->tm_min * 60;
   SecondsTotal = SecondsInHours + SecondsInMinutes + TimeStruct->tm_sec;
   DaysFraction = SecondsTotal / 86400;
   TempReturn=DaysFraction;
   return(TempReturn);
}

double STTime::TimeInFracOfDayHiRes(struct timeval TimeValues)
{
   struct tm *TimeStruct;

   double TempReturn, DaysFraction;
   double SecondsTotal, SecondsInHours, SecondsInMinutes;
	double MilliSeconds;
   TimeStruct = localtime(&TimeValues.tv_sec);

   // To detrmin the day fraction. I did not want to have a race condition because
   // of floating point math errors in the Intel architecture. So to remove this
   // problem we will add up all the seconds in a day and then find that value as
   // the fraction. Total seconds in a day is 86400
   SecondsInHours = TimeStruct->tm_hour * 60 * 60;
   SecondsInMinutes = TimeStruct->tm_min * 60;
   SecondsTotal = SecondsInHours + SecondsInMinutes + TimeStruct->tm_sec;
   DaysFraction = SecondsTotal / 86400;
	//printf("	old DaysFraction = %6.12f\tTimeValues.tv_usec = %f\t", DaysFraction, (double)TimeValues.tv_usec);
	MilliSeconds = (double)TimeValues.tv_usec / 1000000;
	DaysFraction += MilliSeconds / 100000;
	//printf("	new DaysFraction = %6.12f\n", DaysFraction);
   TempReturn=DaysFraction;

   return(TempReturn);
}

double STTime::Time()
{
   //time_t TimeValues;
   //time(&TimeValues);
   //return(TimeInFracOfDay(TimeValues));
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
	return(TimeInFracOfDayHiRes(tv));
}

int STTime::EventPassed(STIntList *ListOfEvents)
{
   printf("STTime::EventPassed Has not been writen yet.\n");
   return(-1);
}

int STTime::EventPassed(STStringList *ListOfEvents)
{
   printf("STTime::EventPassed Has not been writen yet.\n");
   return(-1);
}

double STTime::Date()
{
   time_t TimeValues;

   time(&TimeValues);
   return(DaysCount(TimeValues));
}

int STTime::DayOfYearToDayMonth(int DayCount,int CurrentYear)
{
   bool ThisYearIsLeapYear;
   int TempReturn;
   int ALoopCounter;

   TempReturn = 0;
   ThisYearIsLeapYear = LeapYear(CurrentYear);
   TempReturn = DayCount;
   for(ALoopCounter=0;ALoopCounter<12;ALoopCounter++)
   {
      DayCount -= NumberOfDaysInMonth(ALoopCounter);
      if((ALoopCounter == 1)&&(ThisYearIsLeapYear == true))
      {
         DayCount--;
      }

      if(DayCount < 1)
      {
         break;
      } else
      {
         TempReturn = DayCount;
      }
   }

   return(TempReturn);
}

STString STTime::DayOfYearToMonth(int DayCount,int CurrentYear)
{
   bool ThisYearIsLeapYear;
   int ALoopCounter;

   ReturnString = "";
   ThisYearIsLeapYear = LeapYear(CurrentYear);
   for(ALoopCounter=0;ALoopCounter<12;ALoopCounter++)
   {
      DayCount -= NumberOfDaysInMonth(ALoopCounter);
      if((ALoopCounter == 1)&&(ThisYearIsLeapYear == true))
      {
         DayCount--;
      }

      if(DayCount < 1)
      {
         ReturnString = IntMonthToString(ALoopCounter+1,DF_MONTH_LONG);
         break;
      }
   }

   return(ReturnString);
}

int STTime::NumberOfDaysInMonth(int MonthNumber)
{
   int TempReturn;

   TempReturn = 0;
   if((MonthNumber >= 0)&&(MonthNumber <= 11))
   {
      TempReturn = 31;
      switch(MonthNumber)
      {
         case 1: TempReturn = 28; 	break;
         case 3: TempReturn = 30; 	break;
         case 5: TempReturn = 30; 	break;
         case 8: TempReturn = 30; 	break;
         case 10: TempReturn = 30; 	break;
      }
   }
   return(TempReturn);
}

STString STTime::IntMonthToString(int MonthNumber, int FormattingOfString)
{
   STString MonthString;
   STStringsEx StringX;

   MonthString = "";

   if((FormattingOfString & DF_MONTH_MASK) != DF_MONTH_NUMBER)
   {
      switch(MonthNumber)
      {
         case 1:
            MonthString = "January";
            break;

         case 2:
            MonthString = "February";
            break;

         case 3:
            MonthString = "March";
            break;

         case 4:
            MonthString = "April";
            break;

         case 5:
            MonthString = "May";
            break;

         case 6:
            MonthString = "June";
            break;

         case 7:
            MonthString = "July";
            break;

         case 8:
            MonthString = "August";
            break;

         case 9:
            MonthString = "September";
            break;

         case 10:
            MonthString = "October";
            break;

         case 11:
            MonthString = "November";
            break;

         case 12:
            MonthString = "December";
            break;
      }
      if((FormattingOfString & DF_MONTH_MASK) == DF_MONTH_SHORT)
      {
         MonthString = StringX.Remove(MonthString,3,MonthString.Len());
      }
   } else
   {
      MonthString.Sprintf("%d",MonthNumber);
   }
   ReturnString = MonthString;
   return(ReturnString);
}

bool STTime::LeapYear(int YearNumber)
{
   bool TempReturn;
   double TempCalculation;
   int OddDaysCount;

   TempReturn = false;
   TempCalculation = YearNumber * 365.2475;
   TempCalculation -= (int)TempCalculation;
   TempCalculation *= 100;

   OddDaysCount = (int)TempCalculation;
   if((OddDaysCount > 25)||(OddDaysCount < 75))
   {
      TempReturn = true;
   }

   return(TempReturn);
}

int STTime::DaysSinceBce(int YearNumber)
{
   return((int)(YearNumber * 365.2475));
}

STString STTime::DateTimeString(int FormattingOfString)
{
   time_t TimeValues;
   struct tm *TimeStruct;
   STString MonthString, YearString, DayString, HourString, MinuteString, SecondsString;

   time(&TimeValues);
   TimeStruct = localtime(&TimeValues);

   //printf("STTime::DateTimeString Year not formatted\n");
   //printf("STTime::DateTimeString Day not formatted\n");
   //printf("STTime::DateTimeString Hour not formatted\n");
   //printf("STTime::DateTimeString Date may not be formatted properly!!!\n");

   YearString.Sprintf("%d",TimeStruct->tm_year + 1900);

   MonthString = IntMonthToString(TimeStruct->tm_mon + 1, FormattingOfString);

   DayString.Sprintf("%d",TimeStruct->tm_mday);

   HourString.Sprintf("%d",TimeStruct->tm_hour);

   MinuteString.Sprintf("%02d",TimeStruct->tm_min);
   SecondsString.Sprintf("%02d",TimeStruct->tm_sec);

   // This is where I should do some formatting
   ReturnString = MonthString+" "+DayString+", "+YearString+" "+HourString+":"+MinuteString+":"+SecondsString;

   return(ReturnString);
}

double STTime::Now()
{
   return(Date()+Time());
}

double STTime::String2DateTime(const STString& TheDateTimeString, int FormattingOfString)
{
   printf("STTime::String2DateTime Has not been writen yet.\n");
   return(0);
}

STString STTime::DateTime2String(double TheDateTimeValue,int FormattingOfString)
{
   int YearsCount,DaysCount,HoursCount,MinutesCount,SecondsCount;
   STString BuildDateText;
   STString ConvertText;

   ReturnString = "";
   BuildDateText = "";

   TheDateTimeValue -= 1;

   // Calculate the year
   YearsCount = (int)(TheDateTimeValue / 365.2475);
   TheDateTimeValue -= (YearsCount * 365.2475);
   YearsCount += 1900;
   DaysCount = (int)TheDateTimeValue;

   // Calculate the month
   BuildDateText = DayOfYearToMonth(DaysCount,YearsCount);
   ConvertText.Sprintf("%d",DayOfYearToDayMonth(DaysCount,YearsCount));
   BuildDateText += " "+ConvertText;
   ConvertText.Sprintf("%d",YearsCount);
   BuildDateText += ", "+ConvertText;

   SecondsCount = (int)( (TheDateTimeValue - (int)TheDateTimeValue) * 86400);
   MinutesCount = (int)(SecondsCount / 60);
   SecondsCount = (SecondsCount % 60);
   HoursCount = (int)(MinutesCount / 60);
   MinutesCount = (MinutesCount % 60);
	if(FormattingOfString == 0)
	{
	   ConvertText.Sprintf("%02d:%02d:%02d",HoursCount,MinutesCount,SecondsCount);
	}
	if(FormattingOfString == 1)
	{
		int MiliSeconds = (int)( (TheDateTimeValue - (int)TheDateTimeValue) * 86400000);
		MiliSeconds %= 1000;
	   ConvertText.Sprintf("%02d:%02d:%02d.%03d",HoursCount,MinutesCount,SecondsCount,MiliSeconds);
	}
   BuildDateText += " "+ConvertText;

   ReturnString = BuildDateText;
   return(ReturnString);
}

STString STTime::DateYMDCode()
{
   time_t TimeValues;
   struct tm *TimeStruct;

   time(&TimeValues);
   TimeStruct = localtime(&TimeValues);

   ReturnString.Sprintf("%d%02d%02d",TimeStruct->tm_year+1900,TimeStruct->tm_mon+1,TimeStruct->tm_mday);
   return(ReturnString);
}

void STTime::ResetTimeMarker()
{
	struct timezone NotUsed;
	gettimeofday(&LastTimeValue, &NotUsed);
}

float STTime::GetElapsidTime()
{
	struct timeval TimeValue;
	struct timeval MathTimeValue;
	struct timezone NotUsed;
	float ReturnElapsidTime;

	gettimeofday(&TimeValue, &NotUsed);
	MathTimeValue.tv_sec = TimeValue.tv_sec;
	MathTimeValue.tv_usec = TimeValue.tv_usec;
	//printf("Last Time %d:%06d ", LastTimeValue.tv_sec, LastTimeValue.tv_usec);
	if(LastTimeValue.tv_sec == 0)
	{
		LastTimeValue.tv_sec = TimeValue.tv_sec;
		LastTimeValue.tv_usec = TimeValue.tv_usec;
	}
	//printf("Last Time %d:%06d ", LastTimeValue.tv_sec, LastTimeValue.tv_usec);
	//printf("Obj %s Time %d:%06d ", (const char*)ObjectName, TimeValue.tv_sec, TimeValue.tv_usec);
	//printf("LastTime %d:%06d ", LastTimeValue.tv_sec, LastTimeValue.tv_usec);
	if(TimeValue.tv_sec != LastTimeValue.tv_sec)
	{
		if(TimeValue.tv_usec < LastTimeValue.tv_usec)
		{
			MathTimeValue.tv_sec--;
			MathTimeValue.tv_usec += 1000000;
		}
	}
	ReturnElapsidTime = MathTimeValue.tv_sec - LastTimeValue.tv_sec + ((MathTimeValue.tv_sec - LastTimeValue.tv_sec) / 100000);
	LastTimeValue.tv_sec = TimeValue.tv_sec;
	LastTimeValue.tv_usec = TimeValue.tv_usec;
	return(ReturnElapsidTime);
}

void STTime::PrintTimeMarker()
{
	printf("Elapsid time %f\n", GetElapsidTime());
}
