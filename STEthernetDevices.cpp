/*=============================================================================

    Copyright (C) 2007 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#include <STEthernetDevices.h>
#include <STCommonCore.h>
#include <STStringList.h>
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>

/****** 
	Some documentation on if requests.
	SIOCGIFADDR       Get interface address
	SIOCGIFBRDADDR    Get the broadcast address
	SIOCGIFDSTADDR    Get the destination address
	SIOCGIFNETMASK    Get the netmask for the interface
	SIOCGIFNETMASK		Get the netmask address

	The SIOCG can be changed to SIOCS to set the same values.


	Helpful information here
	http://linux.die.net/man/7/netdevice
*/

STEthernetDevices::STEthernetDevices() 
: STObjectController()
{
	ReturnList = new STStringList();	  
}

STEthernetDevices::~STEthernetDevices() 
{
	delete ReturnList;
}

STStringList *STEthernetDevices::DeviceList()
{
	struct ifconf InterFaceConf;
	int DeviceCount;
		
	// Build a list of device names on local system.
	ReturnList->Clear();
	int SocketHandle = socket (AF_INET, SOCK_DGRAM, 0);
	
	// Create space for the configs of the devices, max 30 devices.
	InterFaceConf.ifc_len = 30 * sizeof(struct ifreq);
	//printf("InterFaceConf.ifc_len = %d\n", InterFaceConf.ifc_len);
	InterFaceConf.ifc_buf = new char[InterFaceConf.ifc_len];
	
	// Get the device list
	ioctl(SocketHandle, SIOCGIFCONF, &InterFaceConf);
	//printf("InterFaceConf.ifc_len = %d\n", InterFaceConf.ifc_len);
	//printf("sizeof(struct ifreq) = %d\n", sizeof(struct ifreq));
	DeviceCount = InterFaceConf.ifc_len / sizeof(struct ifreq);
	//printf("Number of devices = %d\n", DeviceCount);

	for(int EachDev = 0; EachDev < DeviceCount; EachDev++)
	{
		ReturnList->Add(InterFaceConf.ifc_req[EachDev].ifr_name);
		//printf("Device name = %s\n", InterFaceConf.ifc_req[EachDev].ifr_name);
	}
	
	//getifstats getifaddrs	
	delete[] InterFaceConf.ifc_buf;
	
	close(SocketHandle);
	return(ReturnList);
}
	
STString& STEthernetDevices::IpAddress(const STString& DeviceName)
{
	struct ifreq InterFace;
		
	int SocketHandle = socket (AF_INET, SOCK_DGRAM, 0);
	
	// Set the device we want to query, 16 is required length do not use the real string length
	strncpy(InterFace.ifr_name, (const char*)DeviceName, 16);
	
	// Get the device info	
	ioctl(SocketHandle, SIOCGIFADDR, &InterFace);

	ReturnString = inet_ntoa( ((struct sockaddr_in*)&InterFace.ifr_addr)->sin_addr );
	//printf("
	
	close(SocketHandle);
	return(ReturnString);
}
		
STString& STEthernetDevices::NetMask(const STString& DeviceName)
{
	struct ifreq InterFace;
		
	int SocketHandle = socket (AF_INET, SOCK_DGRAM, 0);
	
	// Set the device we want to query, 16 is required length do not use the real string length
	strncpy(InterFace.ifr_name, (const char*)DeviceName, 16);
	
	// Get the device info	
	ioctl(SocketHandle, SIOCGIFNETMASK, &InterFace);

	ReturnString = inet_ntoa( ((struct sockaddr_in*)&InterFace.ifr_addr)->sin_addr );
	
	close(SocketHandle);
	return(ReturnString);
}
			
STString& STEthernetDevices::NetworkAddress(const STString& DeviceName)
{
	struct ifreq InterFace;
		
	int SocketHandle = socket (AF_INET, SOCK_DGRAM, 0);
	
	// Set the device we want to query, 16 is required length do not use the real string length
	strncpy(InterFace.ifr_name, (const char*)DeviceName, 16);
	
	// Get the device info	
	ioctl(SocketHandle, SIOCGIFBRDADDR, &InterFace);

	ReturnString = inet_ntoa( ((struct sockaddr_in*)&InterFace.ifr_addr)->sin_addr );
	
	close(SocketHandle);
	return(ReturnString);
}
			
unsigned int STEthernetDevices::IpStringToInt(const STString& Address)
{
	unsigned int ReturnInt = 0;
	STString NextBlock;
	STString WorkString;
	
	// Strip any invalid crap out of the IP address like : / space or anything else.
	for(unsigned int i = 0; i < Address.Len(); i++)
	{
		if((Address[i] >= '0') && (Address[i] <= '9'))
		{
			WorkString += Address[i];
		}
		if(Address[i] == '.')
		{
			WorkString += Address[i];
		}
	}
	while(WorkString.Len())
	{
		NextBlock = WorkString;
		if(WorkString.Pos(".") > -1)
		{
			WorkString.Remove(0, WorkString.Pos(".") + 1);
			NextBlock.Remove(NextBlock.Pos("."), NextBlock.Len());
		} else
		{
			WorkString = "";
		}
		ReturnInt <<= 8;
		ReturnInt += NextBlock.ToInt();
	}
	return(ReturnInt);
}

STString& STEthernetDevices::MacAddress(const STString& DeviceName)
{
	struct ifreq InterFace;
	STString Converter;
		
	int SocketHandle = socket (AF_INET, SOCK_DGRAM, 0);
	
	// Set the device we want to query
	strncpy(InterFace.ifr_name, (const char*)DeviceName, DeviceName.Len());
	
	// Get the device info	
	ioctl(SocketHandle, SIOCGIFHWADDR, &InterFace);
	
	ReturnString = "";
	for(int Blocks = 0; Blocks < 6; Blocks++)
	{
		Converter.Sprintf("%02X", (unsigned char)InterFace.ifr_netmask.sa_data[Blocks]);
		ReturnString += Converter;
		if(Blocks < 5)
		{
			ReturnString += ":";
		}
	}
	close(SocketHandle);
	return(ReturnString);
}

STString& STEthernetDevices::IsOnNetwork(const STString& Address)
{
	unsigned int AddressMask, IpAddress, TestAddress, MaskedTestIp, MaskedCheckIp;
	STString ConvertString;
	struct ifreq InterFace;		
	STString TestDevName;
	TestAddress = IpStringToInt(Address);
	DeviceList();
	for(unsigned int DevI = 0; DevI < ReturnList->Count(); DevI++)
	{
		TestDevName = ReturnList->Item(DevI);
		strncpy(InterFace.ifr_name, (const char*)TestDevName, 16);
		// Set the device we want to query
		
		// Get the device info	
		int SocketHandle = socket (AF_INET, SOCK_DGRAM, 0);
		ioctl(SocketHandle, SIOCGIFNETMASK, &InterFace);	
		ConvertString = inet_ntoa(((struct sockaddr_in*)&InterFace.ifr_addr)->sin_addr);
		close(SocketHandle);		

		AddressMask = IpStringToInt(ConvertString);

		SocketHandle = socket (AF_INET, SOCK_DGRAM, 0);
		ioctl(SocketHandle, SIOCGIFADDR, &InterFace);
		ConvertString = inet_ntoa(((struct sockaddr_in*)&InterFace.ifr_addr)->sin_addr);
		close(SocketHandle);		
		
		IpAddress = IpStringToInt(ConvertString);
		
		MaskedTestIp = TestAddress & AddressMask;
		MaskedCheckIp = IpAddress & AddressMask;
		if(MaskedCheckIp == MaskedTestIp)
		{
			ReturnString = TestDevName;
		}
	}
	return(ReturnString);
}

STString& STEthernetDevices::IsOurIpAddress(const STString& Address)
{
	DeviceList();
	STString MatchDevice = "";
	
	while(ReturnList->Count())
	{
		if(Address == IpAddress(ReturnList->Item(0)))
		{
			MatchDevice = ReturnList->Item(0);
		}
		ReturnList->Delete(0);
	}
	
	ReturnString = MatchDevice;
	return(ReturnString);
}
