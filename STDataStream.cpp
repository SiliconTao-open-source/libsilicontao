/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#include <STCommonCore.h>
#include <STDataStream.h>
#include <STCommonCore.h>
#include <STDynamicDataLink.h>
#include <STStringList.h>
#include <STList.h>
#include <STString.h>
#include <stdlib.h>
#include <stdio.h>

#define BlockSize 500

STDataStream::STDataStream(int UseMode)
{
   byte *TempBlock;

   SelectedUseMode = UseMode;
   ExternalStream = NULL;
   BlockList = new STList();
   //DdLink = new STDynamicDataLink();

   //TempBlock = (byte *)malloc(BlockSize);
	TempBlock = new byte[BlockSize];
   BlockList->Add(TempBlock);
   WriteIndex = 0;
   ReadIndex = 0;
   ConnectedState = false;
}

STDataStream::~STDataStream()
{
   int ALoopCounter, BlockCount;
   byte *TempBlock;

   printf("deleting %d nodes\n",BlockList->Count());
   BlockCount = BlockList->Count();
   for(ALoopCounter=0;ALoopCounter<BlockCount;ALoopCounter++)
   {
      TempBlock = (byte *)BlockList->Item(0);
      delete TempBlock;
      BlockList->Delete(0);
   }
   delete BlockList;
   //delete DdLink;
}

/*void STDataStream::AutoConnectRespond()
{
   STString TestString,ConnectorString, ExteralAddressString;
   STStringList *BufferList;
   int TempAddress;
   bool ReceivedAMessage;
   bool GoodNumber;

   //printf("STDataStream::AutoConnectRespond\n");
   BufferList = new STStringList;

   ReceivedAMessage = false;
   while(DdLink->IsMessageWaiting() == true)
   {
      ReceivedAMessage = true;
      //printf("found a message\n");
      TestString = DdLink->ReadMessage();
      ConnectorString = DdLink->ExtractMessage(TestString);
      printf("I received this: %s ",(const char *)ConnectorString);
      if(ConnectorString.Pos("STDataStream@") == 0)
      {
         printf("I accept it\n");
         break;
      } else
      {
         // We are not looking for that one
         BufferList->Add(TestString);
         ConnectorString = "";
      }
   }

   if(ConnectorString.Pos("STDataStream@") == 0)
   {
      // Some one has called me to link with them
      ConnectorString.remove(0,ConnectorString.Pos("x")+1);
      TempAddress = ConnectorString.toInt(&GoodNumber,16);
      if((GoodNumber == true) && (TempAddress > 0))
      {
         printf("I will now make a connect call to STDataStream @ 0x%X\n",TempAddress);
         printf("And tell it I am STDataStream @ 0x%X\n",(unsigned int)this);
         ExternalStream = (STDataStream *)TempAddress;
         ExternalStream->RemoteConnect(this);
         printf("ExternalStream->ConnectedState = %d\n",ExternalStream->ConnectedState);
      } else
      {
         printf("Error connecting to STDataStream @ 0x%X\n",TempAddress);
         if(GoodNumber == true)
         {
            printf("No good\n");
         } else
         {
            printf("TempAddress = %d\n",TempAddress);
         }
      }
      //AutoConnectTimer->start(3000,true);
   } else
   if(ReceivedAMessage == true)
   {
      printf("AutoConnectTimer not found\n");
   } else
   {
      //AutoConnectTimer->start(3000,true);
   }

   if(ExternalStream > NULL)
   {
      printf("ExternalStream->ConnectedState = %d\n",ExternalStream->ConnectedState);
      printf("ExternalStream->ExternalStream = %X\n",(unsigned int)ExternalStream->ExternalStream);
      //printf("ExternalStream->this = %X\n",(int *)(ExternalStream->this));
      printf("ExternalStream = %X\n",(unsigned int)ExternalStream);
      printf("ExternalStream->BlockList = %X\n",(unsigned int)ExternalStream->BlockList);
   } else
   {
      printf("ConnectedState = %d\n",ConnectedState);
      printf("ExternalStream = %X\n",(unsigned int)ExternalStream);
      printf("this = %X\n",(unsigned int)this);
      printf("BlockList = %X\n",(unsigned int)BlockList);
   }
   printf("\n");

   while(BufferList->Count() > 0)
   {
      TestString = BufferList->Item(0);
      BufferList->Remove(0);
      DdLink->ReplaceMessage(TestString);
   }

   delete BufferList;
}*/

void STDataStream::QuickLink(STString& AppName)
//void STDataStream::QuickLink(STString &AppName, STDataStream **SelfPointer)
{
   STString Message2Send;

   if(SelectedUseMode == UM_CONNECT)
   {
      if(IsConnected() == true)
      {
         Disconnect();
      }

      // Try to connect
      printf("QuickLink ConnectedState = %d\n",ConnectedState);
      printf("I will try to connect to %s\n",(const char *)AppName);

      printf("And tell it I am STDataStream @ 0x%X\n",(unsigned int)this);
      Message2Send.Sprintf("STDataStream@0x%X",(unsigned int)this);

      //printf("And tell it I am STDataStream @ 0x%X\n",SelfPointer);
      //Message2Send.Sprintf("STDataStream@0x%X",SelfPointer);

		printf("fix this void STDataStream::QuickLink(STString& AppName)\n");
      //DdLink->SendMessage(Message2Send,0,AppName);
      printf("QuickLink ConnectedState = %d\n",ConnectedState);
   }
}

bool STDataStream::IsConnected()
{
   return(ConnectedState);
}

void STDataStream::Disconnect()
{
   // Nothing is distroied, communiction would be safe still we are
   // just stopping it.

   if(ExternalStream > NULL)
   {
      ExternalStream->RemoteDisconnect();
      RemoteDisconnect();
   }
}

void STDataStream::RemoteDisconnect()
{
   ExternalStream = NULL;
   ConnectedState = false;
}

STDataStream *STDataStream::RemoteConnect(STDataStream *RemoteStream)
{
   if(SelectedUseMode == UM_CONNECT)
   {
      printf("STDataStream::RemoteConnect in 0x%X\n",(unsigned int)this);
      if((ConnectedState == false) && (ExternalStream == NULL))
      {
         printf("I am connected now!!!!!!!!!!!!!!\n");
         ExternalStream = RemoteStream;
         ConnectedState = true;
      } else
      {
         printf("I think I am already connected!\n");
         printf("ConnectedState = %d\n",ConnectedState);
         printf("ExternalStream = %X\n",(unsigned int)ExternalStream);
         if(ConnectedState != false)
         {
            printf("ConnectedState\n");
         }
         if(ExternalStream != NULL)
         {
            printf("ExternalStream\n");
         }
      }
   } else
   {
      printf("I was called but I am not in UM_CONNECT mode\n");
   }
   return(this);
}

int STDataStream::DataWaitingCount()
{
   int TempReturn;

   TempReturn = 0;
   if(BlockList->Count() > 1)
   {
      TempReturn += (BlockSize * BlockList->Count());
      TempReturn += WriteIndex;
   } else
   if(WriteIndex > ReadIndex)
   {
      TempReturn += WriteIndex;
   }
	return(TempReturn);
}

void STDataStream::WriteData(byte *DataBuffer, int BufferSize)
{
   if(SelectedUseMode == UM_CONNECT)
   {
      if(ExternalStream > NULL)
      {
         ExternalStream->RemoteWriteData(DataBuffer, BufferSize);
      }
   } else
   {
      RemoteWriteData(DataBuffer, BufferSize);
   }
}

void STDataStream::RemoteWriteData(byte *DataBuffer, int BufferSize)
{
   byte *TempBlock;
   int BufferReadIndex;

   BufferReadIndex = 0;

   TempBlock = (byte *)BlockList->Item(BlockList->Count()-1);

   while(BufferReadIndex < BufferSize)
   {
      TempBlock[WriteIndex] = DataBuffer[BufferReadIndex];
      BufferReadIndex++;
      WriteIndex++;
      if(WriteIndex >= BlockSize)
      {
         //printf("Time for a new block\n");
         WriteIndex = 0;

         // TempBlock = (byte *)malloc(BlockSize);
         TempBlock = new byte[BlockSize];
         //printf("TempBlock = %X\n",TempBlock);
         BlockList->Add(TempBlock);
      }
   }
}

int STDataStream::ReadData(BYTE *DataBuffer, int MaxRead)
{
   byte *TempBlock;
   int BufferWriteIndex;

   BufferWriteIndex = 0;
   TempBlock = (byte *)BlockList->Item(0);
   while(BufferWriteIndex < MaxRead)
   {
      DataBuffer[BufferWriteIndex] = TempBlock[ReadIndex];
      BufferWriteIndex++;
      ReadIndex++;

      if(ReadIndex >= BlockSize)
      {
         if(BlockList->Count() > 0)
         {
            ReadIndex = 0;
            delete TempBlock;
            BlockList->Delete(0);
            TempBlock = (byte *)BlockList->Item(0);
         }
      } else
      if(ReadIndex >= WriteIndex)
      {
         break;
      }
   }
	return(BufferWriteIndex);
}
