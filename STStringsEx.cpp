/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#include <STStringsEx.h>
#include <stdio.h>
#include <STCommonCore.h>
#include <STString.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

STStringsEx::STStringsEx()
{}

STStringsEx::~STStringsEx()
{}

int STStringsEx::LastPos(const char* SubString, const STString& TheFullString)
{
   STString TempSub;

   TempSub = "";
   TempSub += SubString;
   return(LastPos(TempSub,TheFullString));
}

int STStringsEx::LastPos(const char SubString, const STString& TheFullString)
{
   STString TempSub;

   TempSub = "";
   TempSub += SubString;
   return(LastPos(TempSub,TheFullString));
}

int STStringsEx::LastPos(const STString& SubString, const STString& TheFullString)
{
   STString ParsingString;
   int TempReturn;

   printf("Warning: STStringsEx::LastPos could be more efficient!\n");
   // I must change this to use the STString::Pos negative index
   // It could be much faster.

   ParsingString = TheFullString;
   TempReturn = 0;
   while(ParsingString.Pos(SubString) > -1)
   {
      TempReturn += ParsingString.Pos(SubString) + 1;
      ParsingString = Remove(ParsingString,0,ParsingString.Pos(SubString)+1);
   }
   return(TempReturn-1);
}

int STStringsEx::InsertCrLf(STString* InsertingToString, int SpaceNear, char CrLfChar)
{
   bool Done;
   int LastCrLf, ALoopCounter;
   int InsertPoint;
   int NumberOfTextLines;
   STString CopiedString;
   //QChar ShortString;

   //ShortString=CrLfChar;
   Done=false;
   NumberOfTextLines=1;
   while(Done==false)
   {
      Done=true;
      LastCrLf=0;
      InsertPoint=0;
      CopiedString=InsertingToString[0];
      for(ALoopCounter=0;ALoopCounter<(int)InsertingToString->Len();ALoopCounter++)
      {
         if(CopiedString[ALoopCounter]==CrLfChar)
         {
            LastCrLf=0;
         } else
         if(CopiedString[ALoopCounter]==' ')
         {
            InsertPoint=ALoopCounter;
         } else
         {
            LastCrLf++;
         }
         if((LastCrLf>SpaceNear)&&(InsertPoint>0))
         {
      		printf("Error here: STStringsEx::InsertCrLf line %d\n", __LINE__);
            //InsertingToString->replace(InsertPoint,1,&ShortString,1);
            NumberOfTextLines++;
            Done=false;
            break;
         }
      }
   }
   return(NumberOfTextLines);
}

const char* STStringsEx::HexStr2Ascii(const STString& SourceString, bool MaskLow, bool MaskHi, char MaskChar)
{
   int ByteValue;
   char TempChar;
   STString ParsingString;
   STString ConvertByte, DecByte;

   ReturnString="";
   ParsingString=SourceString;
   while(ParsingString.Len()>0)
   {
      StripChars(ParsingString,' ');
      if(ParsingString.Pos("0x") == 0)
      {
         ParsingString = Remove(ParsingString, 0, 2);
      }
      ConvertByte=ParsingString;
      ConvertByte = Remove(ConvertByte, 2, ConvertByte.Len());
      ParsingString = Remove(ParsingString, 0, 2);
      DecByte = Hex2Dec(ConvertByte);

      printf("Error here: STStringsEx::HexStr2Ascii line %d\n", __LINE__);
      //ByteValue=DecByte.toInt(0,10);

      TempChar=ByteValue;
      if((MaskLow == true) && (TempChar > 0) && (TempChar < 32))
      {
         TempChar=MaskChar;
      }
      ReturnString+=TempChar;
   }
   return(ReturnString);
}

const char* STStringsEx::StripClean(const char* TheString)
{
	STString NewString = TheString;
	return(NewString);
}

const char* STStringsEx::StripClean(const STString& TheString)
{
   bool Done, KillThisChar;
   char TestChar;
   STString TempReturnString;

   TempReturnString = TheString;

   // Leading any thing is Remove(d if not A-Z or a-z or 0-9
   if(TheString.Len() > 0)
   {
      Done=false;
      while(Done==false)
      {
         KillThisChar=true;
         if(TempReturnString.Len() > 0)
         {
            TestChar=((const char*)TempReturnString)[0];
            //printf(" b length = %d ",TempReturnString.Len());
            if((TestChar>='A')&&(TestChar<='Z'))
            {
               KillThisChar=false;
            } else
            if((TestChar>='a')&&(TestChar<='z'))
            {
               KillThisChar=false;
            } else
            // Note: Integer leading blocks must be qualified by the caller.
            if((TestChar>='0')&&(TestChar<='9'))
            {
               KillThisChar=false;
            }
            //printf(" c = %d\n",(char)TestChar);

            if(KillThisChar==true)
            {
               TempReturnString = Remove(TempReturnString,0,1);
            } else
            {
               Done=true;
            }
            if(TempReturnString.Len() == 0)
            {
               Done=true;
            }
         } else
         {
            Done=true;
         }
      }

      // Trailing any thing is Remove(d if not A-Z or a-z or 0-9
      Done=false;
      while(Done==false)
      {
         //printf(" e length = %d ",TempReturnString.Len());
         KillThisChar=true;
         if(TempReturnString.Len() > 0)
         {
            TestChar=((const char*)TempReturnString)[TempReturnString.Len()-1];
            if((TestChar>='A')&&(TestChar<='Z'))
            {
               KillThisChar=false;
            } else
            if((TestChar>='a')&&(TestChar<='z'))
            {
               KillThisChar=false;
            } else
            // Note: Integer leading blocks must be qualified by the caller.
            if((TestChar>='0')&&(TestChar<='9'))
            {
               KillThisChar=false;
            }
            //printf(" c = %d\n",(char)TestChar);

            if(KillThisChar==true)
            {
               TempReturnString = Remove(TempReturnString, TempReturnString.Len()-1,1);
            } else
            {
               Done=true;
            }
            if(TempReturnString.Len() == 0)
            {
               Done=true;
            }
         } else
         {
            Done=true;
         }
      }
   }
   ReturnString = TempReturnString;
   return(ReturnString);
}

const char* STStringsEx::ParseIdentifier(const STString& TheString, int IdentifierIndex)
{
   STString BuildString, WorkString;
   bool Done, KillThisChar;
   char TestChar;
   int IndexCounter;

	WorkString = TheString;

   ReturnString = WorkString;
   BuildString = "";
   Done = false;
   while(Done == false)
   {
      KillThisChar = true;
      TestChar = ReturnString[0];
      if((TestChar >= 'A') && (TestChar<= 'Z'))
      {
         KillThisChar = false;
      } else
      if((TestChar >='a') && (TestChar <= 'z'))
      {
         KillThisChar = false;
      } else
      // Note: Integer leading blocks must be qualified by the caller.
      if((TestChar >= '0') && (TestChar <= '9'))
      {
         KillThisChar = false;
      }

      // Move char to destination
      if(KillThisChar == false)
      {
         BuildString += TestChar;
      } else
      {
         if(BuildString.Len()>0)
         {
            if(IdentifierIndex == IndexCounter)
            {
               Done = true;
               WorkString = BuildString;
               break;
            }
         }
         WorkString = Remove(WorkString, 0,1);
      }
   }
   return(ReturnString);
}

const char* STStringsEx::StripChars(const char* TheString,char TheChar)
{
	STString NewString = TheString;
	return(StripChars(NewString, TheChar));
}

const char* STStringsEx::StripChars(const STString& TheString, char TheChar)
{
   STString TempReturnString;

   TempReturnString = TheString;
   while((TempReturnString.Len() > 0)&&(TempReturnString[0]==TheChar))
   {
      TempReturnString = Remove(TempReturnString, 0,1);
   }
   while((TempReturnString.Len() > 0)&&(TempReturnString[TempReturnString.Len()-1]==TheChar))
   {
      TempReturnString = Remove(TempReturnString, TempReturnString.Len()-1,1);
   }
   ReturnString = TempReturnString;
   return(ReturnString);
}

const char* STStringsEx::Bin2Dec(const STString& SourceString)
{
   int ReturnValue;
   bool LeadingCrap;

   STString WorkString = SourceString;
   ReturnValue = 0;
   LeadingCrap = true;
   while(LeadingCrap == true)
   {
      if((WorkString[0] != '0') && (WorkString[0] != '1') && (WorkString[0] > 0))
      {
         WorkString = Remove(WorkString, 0, 1);
      } else
      {
         LeadingCrap = false;
      }
   }
   while(WorkString[0] > 0)
   {
      ReturnValue *= 2;

      if(WorkString[0] == '1')
      {
         ReturnValue++;
      } else
      if(WorkString[0] != '0')
      {
         break;
      }
      WorkString = Remove(WorkString, 0, 1);
   }
   ReturnString.Sprintf("%d", ReturnValue);
   return(ReturnString);
}

const char* STStringsEx::Hex2Dec(const STString& SourceString)
{
   unsigned long int HexLength, ExponentValue, CounterValue, DecNumber;
   char TempValue;
   char RealChar;
   CounterValue = 0;
   DecNumber = 0;
   TempValue = 0;
	//fprintf(stderr, "STStringsEx::Hex2Dec(%s)\n", (const char*)SourceString);
	STString WorkString = SourceString;
   HexLength = WorkString.Len()-1;
   ExponentValue = HexLength;
   while(CounterValue <= HexLength)
   {
		char TestChar = WorkString[CounterValue];
		//fprintf(stderr, "TestChar = %c\t", TestChar);
      // Letter from A to F
      if((TestChar >= 'A') && (TestChar <= 'F'))
      {
         RealChar = TestChar;
         RealChar -= 'A' - 10;
      } else
      if((TestChar >= 'a') && (TestChar <= 'f'))
      {
         RealChar = TestChar;
         RealChar -= 'a' - 10;
      } else
      // Number from 0 to 9
      if((TestChar >= '0') && (TestChar <= '9'))
      {
         RealChar = TestChar;
         RealChar -= '0';
      } else
      {
      	// Invalid
         DecNumber=0;
         break;
      }
		DecNumber += RealChar * (unsigned long int)pow(16, ExponentValue);
      CounterValue++;
      ExponentValue--;
     	//fprintf(stderr, "DecNumber = %lu\n", DecNumber);
   }
	//ReturnString.SetStringDebug(true);
   ReturnString.Sprintf("%lu", DecNumber);
	//fprintf(stderr, " ReturnString = '%s'\n", (const char*)ReturnString);
	//ReturnString.SetStringDebug(false);
   return(ReturnString);
}

const char* STStringsEx::Dec2Hex(const STString& SourceString)
{
   unsigned long int TheIntValue, RemainderValue;
   char AddChar;
   STString TempString;

	STString WorkString = SourceString;
   TheIntValue = WorkString.ToInt();
   ReturnString = "";
   while(TheIntValue > 0)
   {
      RemainderValue = TheIntValue % 16;
      TheIntValue /= 16;
      if(RemainderValue > 9)
      {
         // 'A' - 9 = 55
         AddChar = RemainderValue + 55;
      } else
      {
         // '0' = 48
         AddChar = RemainderValue + 48;
      }
      //TempString = &AddChar;
      //TempString += ReturnString;
      //ReturnString = TempString;
      ReturnString = &AddChar + ReturnString;
   }
   while(ReturnString.Len() % 2)
   {
      ReturnString = "0" + ReturnString;
   }
   return(ReturnString);
}

const char* STStringsEx::Dec2Bin(const STString& SourceString)
{
   unsigned int TheIntValue;
   STString TempString;

	STString WorkString = SourceString;
   TheIntValue = (unsigned int)WorkString.ToInt();
   ReturnString = "";
   while(TheIntValue > 0)
   {
      if(TheIntValue & 0x0001)
      {
			ReturnString = "1" + ReturnString;
      } else
      {
			ReturnString = "0" + ReturnString;
      }
		TheIntValue >>= 1;
   }
   while(ReturnString.Len() % 8)
   {
      ReturnString = "0" + ReturnString;
   }
   return(ReturnString);
}

int STStringsEx::Str2Int(const STString& StringObject)
{
   return(Str2Int((const char*)StringObject));
}

int STStringsEx::Str2Int(const char* CharArray)
{
   bool Negative;
   bool NumberStart;
   int NumberIndex;
   int TempReturn;
   int StringLength;

   StringLength = strlen(CharArray);
   TempReturn = 0;
   NumberStart = false;
   Negative = false;
   NumberIndex = 0;
   while(NumberStart == false)
   {
      if (NumberIndex >= StringLength)
      {
         break;
      }
      if ((CharArray[NumberIndex] >= '0')&&(CharArray[NumberIndex] <= '9'))
      {
         NumberStart = true;
         break;
      }

      // If the number has a - in front it is considered to be a negative number.
      // Only a space can be between the - and the number.
      if (CharArray[NumberIndex] == '-')
      {
         Negative = true;
      } else
      if (CharArray[NumberIndex] != ' ')
      {
         Negative = false;
      }

      NumberIndex++;
   }

   if (NumberStart == true)
   {
      while ((CharArray[NumberIndex] >= '0')&&(CharArray[NumberIndex] <= '9'))
      {
         TempReturn *= 10;
         TempReturn += CharArray[NumberIndex] - '0';
         NumberIndex++;
      }
      if (Negative == true)
      {
         TempReturn *= -1;
      }
   }

   return(TempReturn);
}

float STStringsEx::Str2Float(const STString& StringObject)
{
   return(Str2Float((const char*)StringObject));
}

float STStringsEx::Str2Float(const char* CharArray)
{
   float ReturnFloat = 0.0;
	float WorkFloat = 0.0;
	bool Done = false;
	int i = 0;
	int Limit = 9;
	int Direction = -1;
	while(! Done)
	{
		if((CharArray[i] <= '9') && (CharArray[i] >= '0'))
		{
			WorkFloat = CharArray[i] - '0';
			if(Direction == -1)
			{
				ReturnFloat *= 10;
				ReturnFloat += WorkFloat;
			} else
			{
				for(int FrakIt = 0; FrakIt < Direction; FrakIt++)
				{
					WorkFloat /= 10;
				}
				ReturnFloat += WorkFloat;
			}
		}

		i++;
		if(CharArray[i] == '\0') Done = true;
		if((CharArray[i] > '9') || (CharArray[i] < '0')) Done = true;
		if(Direction >= 0) Direction++;
		if((CharArray[i] == '.') && (Direction == -1))
		{
			Direction = 0;
			Done = false;
		}
		if(i > Limit) Done = true;
	}
	return(ReturnFloat);
}

double STStringsEx::Str2Double(const STString& StringObject)
{
   return(Str2Double((const char*)StringObject));
}

double STStringsEx::Str2Double(const char* CharArray)
{
   double ReturnFloat = 0.0;
	double WorkFloat = 0.0;
	bool Done = false;
	int i = 0;
	int Limit = 12;
	int Direction = -1;
	while(! Done)
	{
		if((CharArray[i] <= '9') && (CharArray[i] >= '0'))
		{
			WorkFloat = CharArray[i] - '0';
			if(Direction == -1)
			{
				ReturnFloat *= 10;
				ReturnFloat += WorkFloat;
			} else
			{
				for(int FrakIt = 0; FrakIt < Direction; FrakIt++)
				{
					WorkFloat /= 10;
				}
				ReturnFloat += WorkFloat;
			}
		}

		i++;
		if(CharArray[i] == '\0') Done = true;
		if((CharArray[i] > '9') || (CharArray[i] < '0')) Done = true;
		if(Direction >= 0) Direction++;
		if((CharArray[i] == '.') && (Direction == -1))
		{
			Direction = 0;
			Done = false;
		}
		if(i > Limit) Done = true;
	}
	return(ReturnFloat);
}

const char* STStringsEx::Remove(const STString& StringObject, unsigned int StartPos, unsigned int Length)
{
	STString LocalObject = (const char*)StringObject;
   ReturnString = LocalObject;
	if(LocalObject.Len() > 0)
	{
		if((StartPos >= 0) && (Length > 0) && (StartPos < LocalObject.Len()))
		{
			unsigned int i;
			unsigned int SizeOfString = LocalObject.Len();
			char* WorkingBuffer = new char[SizeOfString + 1];
			for (i = 0; i < StartPos; i++)
			{
				WorkingBuffer[i] = ((const char*)LocalObject)[i];
				if (i >= LocalObject.Len())
				{
					break;
				}
			}
			for (i = StartPos; i < SizeOfString; i++)
			{
				if (Length + i >= LocalObject.Len())
				{
					break;
				}
				WorkingBuffer[i] = ((const char*)LocalObject)[Length + i];
			}
			WorkingBuffer[i] = '\0';
			ReturnString = WorkingBuffer;
			delete[] WorkingBuffer;
		}
	}
   return(ReturnString);
}

void STStringsEx::Upper(STString& ModifyString)
{
	char* WorkArray = (char*)((const char*)ModifyString);
	for(unsigned int i = 0; i < ModifyString.Len(); i++)
	{
		if ((WorkArray[i] >= 'a') && (WorkArray[i] <= 'z'))
		{
			WorkArray[i] -= 'a' - 'A';
		}
	}
}

/*const char* STStringsEx::Upper(const STString& OriginalString)
{
   return(Upper((const char*)OriginalString));
}

const char* STStringsEx::Upper(const char* CharArray)
{
   int ALoopCounter;
   char* WorkingBuffer;
   int SizeOfString;
   STString LocalObject = CharArray;

   ReturnString = "";
   SizeOfString = LocalObject.Len();
   if (SizeOfString > 0)
   {
      WorkingBuffer = new char[SizeOfString + 1];
      for (ALoopCounter = 0;ALoopCounter < SizeOfString;ALoopCounter++)
      {
         WorkingBuffer[ALoopCounter] = ((const char*)LocalObject)[ALoopCounter];
         if ((WorkingBuffer[ALoopCounter] >= 'a') && (WorkingBuffer[ALoopCounter] <= 'z'))
         {
            WorkingBuffer[ALoopCounter] -= 'a';
            WorkingBuffer[ALoopCounter] += 'A';
         }
			WorkingBuffer[ALoopCounter + 1] = 0;
      }
      ReturnString = WorkingBuffer;
      delete[] WorkingBuffer;
   }
   return(ReturnString);
}*/

void STStringsEx::Lower(STString& ModifyString)
{
	char* WorkArray = (char*)((const char*)ModifyString);
	for(unsigned int i = 0; i < ModifyString.Len(); i++)
	{
		if ((WorkArray[i] >= 'A') && (WorkArray[i] <= 'Z'))
		{
			WorkArray[i] += 'a' - 'A';
		}
	}
}

/*const char* STStringsEx::Lower(const STString& OriginalString)
{
   return(Lower((const char*)OriginalString));
}

const char* STStringsEx::Lower(const char* CharArray)
{
   int ALoopCounter;
   char* WorkingBuffer;
   int SizeOfString;
   STString LocalObject = CharArray;

   ReturnString = "";
   SizeOfString = LocalObject.Len();
   if (SizeOfString > 0)
   {
      WorkingBuffer = new char[SizeOfString + 1];
      for (ALoopCounter = 0;ALoopCounter < SizeOfString;ALoopCounter++)
      {
         WorkingBuffer[ALoopCounter] = ((const char*)LocalObject)[ALoopCounter];
         if ((WorkingBuffer[ALoopCounter] >= 'A') && (WorkingBuffer[ALoopCounter] <= 'Z'))
         {
            WorkingBuffer[ALoopCounter] -= 'A';
            WorkingBuffer[ALoopCounter] += 'a';
         }
      }
      ReturnString = WorkingBuffer;
      delete[] WorkingBuffer;
   }
   return(ReturnString);
}*/

const char* STStringsEx::SubStr(const STString& StringObject, int StartPos, int Length)
{
   printf("Error STStringsEx::SubStr does not work yet\n");
   exit(0);
   return(ReturnString);
}

const char* STStringsEx::Insert(const STString& StringObject,const char* NewCharArray , int StartPos, int Length)
{
   printf("Error STStringsEx::Insert does not work yet\n");
   exit(0);
   return(ReturnString);
}

const char* STStringsEx::Insert(const STString& StringObject,const STString& NewStringObject , int StartPos, int Length)
{
   printf("Error STStringsEx::Insert does not work yet\n");
   exit(0);
   return(ReturnString);
}

const char* STStringsEx::Replace(const STString& StringObject, const STString& OldString, const STString& NewString)
{
   return(Replace(StringObject,(const char*)OldString,(const char*)NewString));
}

const char* STStringsEx::Replace(const STString& StringObject, const char* OldString, const STString& NewString)
{
   return(Replace(StringObject,OldString,(const char*)NewString));
}

const char* STStringsEx::Replace(const STString& StringObject, const STString& OldString, const char* NewString)
{
   return(Replace(StringObject,(const char*)OldString,NewString));
}

const char* STStringsEx::Replace(const STString& StringObject, const char* OldString, const char* NewString)
{
	int OldStrLen = strlen(OldString);
	STString FrontStr, EndStr;
	STString OrigStr = StringObject;
	int NextPos = OrigStr.Pos(OldString);
	while(NextPos > -1)
	{
		FrontStr = OrigStr.Left(NextPos);
		EndStr = OrigStr.Right(OrigStr.Len() - NextPos - OldStrLen);
		//printf("OrigStr = '%s'\n", (const char*)OrigStr);
		//printf("FrontStr = '%s'\n", (const char*)FrontStr);
		//printf("EndStr = '%s'\n", (const char*)EndStr);
		OrigStr = FrontStr;
		OrigStr += NewString;
		OrigStr += EndStr;
		//printf("NewStr = '%s'\n\n\n", (const char*)OrigStr);
		NextPos = OrigStr.Pos(OldString);
	}
	ReturnString = OrigStr;
   return(ReturnString);
}

int STStringsEx::FloatStringToInt(const STString& StringToConvert, unsigned int DecimalPlaces)
{
   bool Negative;
   int TempReturn, WorkingInt, HighInt, LowInt;
   STString WorkingString;

   TempReturn = 0;
   Negative = false;
   // Is this a negative number
   WorkingString = StringToConvert;
   if(WorkingString.Pos("-") == 0)
   {
      Negative = true;
      WorkingString.Remove(0,1);
   }

   // Is there a decimal in the number
   if(WorkingString.Pos(".") == -1)
   {
      WorkingString += ".0";
   }

   // Digits after the decimal
   WorkingString.Remove(0, WorkingString.Pos(".") + 1);
   LowInt = WorkingString.ToInt();
   if(WorkingString.Len() < DecimalPlaces)
   {
      WorkingInt = DecimalPlaces - WorkingString.Len();
      while(WorkingInt)
      {
         LowInt *= 10;
         WorkingInt--;
      }
   } else
   if(WorkingString.Len() > DecimalPlaces)
   {
      WorkingInt = WorkingString.Len() - DecimalPlaces;
      while(WorkingInt)
      {
         LowInt /= 10;
         WorkingInt--;
      }
   }

   // Digits before the decimal
   WorkingString = StringToConvert;
   WorkingString.Remove(WorkingString.Pos("."), WorkingString.Len());
   HighInt = WorkingString.ToInt();
   WorkingInt = DecimalPlaces;
   while(WorkingInt)
   {
      HighInt *= 10;
      WorkingInt--;
   }
   TempReturn = HighInt + LowInt;

   if(Negative)
   {
      TempReturn *= -1;
   }
   return(TempReturn);
}
