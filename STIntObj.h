/*=============================================================================

    Copyright (C) 2011 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#ifndef STIntObj_included
#define STIntObj_included

/*************************************************************************************************

	This may seem like a  object but it is needed by STHashInts to handle integers as objects.
	I was going to build it into STHashInts but I am making it's own object for it so could be
	used for other things in the future.

*************************************************************************************************/
class STIntObj
{
	public:
      /**
       *  Constructors
       */
      STIntObj();

      /**
       *  Destructor
       */
      ~STIntObj();

      /**
       *  Assignment operators
       */
      STIntObj& operator =(const int);
      // STIntObj& operator =(const int*);
      STIntObj& operator =(const STIntObj&);

      /**
       *
       */
		operator int(void) const { return (*InternalInt); };

      /**
       *  Addition
       */
      STIntObj& operator +=(const int Int);
      STIntObj& operator *=(const int Int);
      STIntObj& operator /=(const int Int);
      STIntObj& operator -=(const int Int);
		STIntObj& operator --(const int Int);
		STIntObj& operator ++(const int Int);
   private:
		int* InternalInt;
};

/**
 *  Other operators. These have to be global because they must return a new object that is created inline.
 */
//bool operator ==(const STIntObj& IntObj, const int Int);
//bool operator ==(const STIntObj& IntObj, const STIntObj& IntObj2);
//bool operator !=(const STIntObj& OtherIntObj, const int Int);
//bool operator !=(const STIntObj& OtherIntObj1, const STIntObj& OtherIntObj2);

#endif // STIntObj_included
