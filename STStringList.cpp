/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:

=============================================================================*/

#include <STStringList.h>
#include <STCommonCore.h>
#include <STStringsEx.h>
#include <STList.h>
#include <STString.h>
#include <STObjectController.h>
#include <STDriveOps.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

STStringList::STStringList(const char* NewObjectName)
: STList(NewObjectName)
{
	CommonConstructor();
}

STStringList::STStringList()
: STList()
{
	CommonConstructor();
}

STStringList::~STStringList()
{
   STStringList::Clear();
   if(FractalAddSavingOn == true)
   {
      DoneFractalAddSaving();
   }
}

void STStringList::CommonConstructor()
{
	//printf("STStringList::STStringList(%s)\n", GetObjectName());
   DiskIOBufferSize = 8192;
	SetClassName("STStringList");
   FractalAddSavingOn = false;
}

STString& STStringList::operator[](unsigned int IndexOfCell)
{
	return(Item(IndexOfCell));
}

void STStringList::StartFractalAddSaving(const STString& FilePathAndName)
{
   if(FractalAddSavingOn == false)
   {
      FractalAddSavingOn = true;
      FractalSaveFileName = FilePathAndName;
      FractalFileIOBuffer = new char[DiskIOBufferSize];
      WipeFile(FractalSaveFileName);

      FractalFilePointer = open(FractalSaveFileName, O_WRONLY|O_CREAT,S_IRWXU|S_IRWXG|S_IRWXO);
		if(FractalFilePointer < 0)
		{
			fprintf(stderr, "STStringList::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
			FractalAddSavingOn = false;
		}
   } else
   {
      printf("ERROR: STStringList::SetFractalAddSaving\n");
      printf("       Attempting to activte twice!\n");
   }
}

void STStringList::StartFractalAddSaving(const char* FilePathAndName)
{
   STString TempString;

   TempString = FilePathAndName;
   StartFractalAddSaving(TempString);
}

void STStringList::DoneFractalAddSaving()
{
   if(FractalAddSavingOn == true)
   {
      FractalAddSavingOn = false;
      FractalSaveToFile();
      delete[] FractalFileIOBuffer;
      close(FractalFilePointer);
   } else
   {
      printf("ERROR: STStringList::SetFractalAddSaving\n");
      printf("       Attempting to activte twice!\n");
   }
}

STString& STStringList::Item(unsigned int Index)
{
	ReturnString.Wrapper("");
   if(Index < Count())
   {
      ReturnString.Wrapper((const char *)STList::Item(Index));
   }
   return(ReturnString);
}

STString& STStringList::Dump()
{
	STString TempString;
   unsigned int ItemsTotal,ALoopCounter;

   ItemsTotal = Count();
   for(ALoopCounter = 0; ALoopCounter < ItemsTotal; ALoopCounter++)
   {
      TempString += STStringList::Item(ALoopCounter);
		TempString += "\n";
   }
	ReturnString.Wrapper(TempString.Unwrap());
	return(ReturnString);
}

unsigned int STStringList::Remove(unsigned int IndexToRemove)
{
   return(STStringList::Delete(IndexToRemove));
}

unsigned int STStringList::Delete(unsigned int IndexToRemove)
{
   char *NewStringPointer;

	if(STList::Count() > IndexToRemove)
	{
		// DebugMessage(3, InLineDbStr.Sprintf("STStringList::Delete(%d) ",IndexToRemove));

		NewStringPointer = (char *)STList::Item(IndexToRemove);

		delete[] NewStringPointer;
		STList::Delete(IndexToRemove);
	}
   return(Count());
}

unsigned int STStringList::LoadFromFile(const char* FilePathAndName)
{
   STString FileName;

   FileName = FilePathAndName;
   return(LoadFromFile(FileName));
}

unsigned int STStringList::LoadFromFile(const STString& FilePathAndName)
{
   char *FileIOBuffer;
   int DataInBufferSize,ALoopCounter;
	// unsigned int TotalDataLoaded = 0;
   //FILE *FilePointer;
   int FilePointer;
   char ReadChar;
   int ReadCharIndex;
   char ReallyBigString[1024];
   STString LoadFileName;
   STStringsEx StringsEx1;
   STString SwapString;

	STDriveOps DriveOps1;
   STStringList::Clear();
	LoadFileName = FilePathAndName;
	StringsEx1.StripChars(LoadFileName,'"');
	//printf("STStringList::LoadFromFile('%s')\n", (const char*)LoadFileName);
	if(DriveOps1.FileExists(LoadFileName))
	{
		//printf("It looks like this file existes = '%s'\n", (const char*)LoadFileName);
		FileIOBuffer = new char[DiskIOBufferSize];

		FilePointer = open(LoadFileName, O_RDONLY);
		//printf("FilePointer = %d\n", FilePointer);
		if(FilePointer > -1)
		{
			SwapString = "";
			ReadCharIndex = 0;
			ReallyBigString[ReadCharIndex] = 0;
			DataInBufferSize = read(FilePointer,FileIOBuffer,DiskIOBufferSize);
			// TotalDataLoaded = DataInBufferSize;
			while(DataInBufferSize > 0)
			{
				for(ALoopCounter = 0; ALoopCounter < DataInBufferSize; ALoopCounter++)
				{
					ReadChar = FileIOBuffer[ALoopCounter];


					// This is a debug to find a memory leak that only happens on embedded ARM systems.
					//if((ReadCharIndex > 690) && (ReadCharIndex < 700))
					//{
					//	printf("ALoopCounter %d, ReadCharIndex %d, ReadChar '%c' %d\n", ALoopCounter, ReadCharIndex, ReadChar, ReadChar);
					//}
					// End debug code.



					if((ReadChar > 31) || (ReadChar == '\t'))
					{
						ReallyBigString[ReadCharIndex] = ReadChar;
						ReallyBigString[ReadCharIndex + 1] = 0;
						ReadCharIndex++;
						if(ReadCharIndex > 1022)
						{
							SwapString += ReallyBigString;
							ReadCharIndex = 0;
							ReallyBigString[ReadCharIndex] = 0;
						}
					} else
					if((ReadChar == '\n')||(ReadChar == '\r'))
					{
						SwapString += ReallyBigString;
						ReadCharIndex = 0;
						ReallyBigString[ReadCharIndex]=0;
						// DebugMessage(3, SwapString);
						Add(SwapString);
						SwapString = "";
					}
				}
				DataInBufferSize = read(FilePointer,FileIOBuffer,DiskIOBufferSize);
				// TotalDataLoaded += DataInBufferSize;
			}
			if(ReadCharIndex > 0)
			{
				SwapString += ReallyBigString;
				ReadCharIndex = 0;
				ReallyBigString[ReadCharIndex]=0;
				Add(SwapString);
				// DebugMessage(3, SwapString);
				SwapString = "";
			}
			//fclose(FilePointer);
			close(FilePointer);
		} else
		{
			fprintf(stderr, "STStringList::%s line %d errno=%d '%s' on '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno), (const char*)LoadFileName);
		}
   	delete[] FileIOBuffer;
	} else
	{
		fprintf(stderr, "STStringList::%s line %d errno=%d '%s' on '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno), (const char*)LoadFileName);
	}
	// printf("TotalDataLoaded = %d\n", TotalDataLoaded);
	return(Count());
}

// For debugging programs that use this library. Counts the number of bytes used in all loaded strings.
unsigned int STStringList::MemCount()
{
	unsigned int ReturnCount = 0;
	STString LineString;

	for(unsigned int i = 0; i < Count(); i++)
	{
		LineString = Item(i);
		ReturnCount += LineString.Len() + 1;
	}
	return(ReturnCount);
}

unsigned int STStringList::Replace(unsigned int Index, const char* NewString)
{
	STString TempString;
	TempString = NewString;
	return(Replace(Index, TempString));
}

unsigned int STStringList::Replace(unsigned int Index, const STString& NewString)
{
   char *NewStringPointer;
   int LengthOfString;

	//printf("STStringList::Replace(\"%s\", %d) of %d\n",(const char*)NewString, Index, Count());
   //DebugMessage(2, InLineDbStr.Sprintf("STStringList::Replace(\"%s\", %d)\n",(const char*)NewString, Index));

   NewStringPointer = (char *)STList::Item(Index);

   delete[] NewStringPointer;

   LengthOfString = NewString.Len();
   NewStringPointer = new char[NewString.Len()+1];

   if(NewStringPointer != NULL)
   {
		//printf("Replace goes well\n");
      strcpy(NewStringPointer,(const char *)NewString);
      NewStringPointer[LengthOfString] = 0;
	}

	// Even if it is null we need to replace it.
   STList::Replace(Index, NewStringPointer);

   return(Count());
}

unsigned int STStringList::Add(const char* NewStringToAdd)
{
   STString TempString;
   unsigned int TempReturn;

   TempString = NewStringToAdd;
   TempReturn = Add(TempString);
   return(TempReturn);
}

unsigned int STStringList::Add(const STString& NewStringToAdd)
{
   unsigned int TempReturn = 0;
   char *NewStringPointer;
   int LengthOfString;

   //DebugMessage(2, InLineDbStr.Sprintf("STStringList::Add(%s)\n",(const char *)NewStringToAdd));

   LengthOfString = NewStringToAdd.Len();
   NewStringPointer = new char[NewStringToAdd.Len() + 1];
   //DebugMessage(3, InLineDbStr.Sprintf("STStringList::Add(%s) @ 0x%08X -> 0x%08X\n",(const char *)NewStringToAdd, (uint)NewStringPointer,(uint)NewStringPointer + NewStringToAdd.Len() + 1));

   if(NewStringPointer != NULL)
   {
      strcpy(NewStringPointer,(const char *)NewStringToAdd);
      NewStringPointer[LengthOfString] = 0;
      TempReturn = STList::Add(NewStringPointer);
      //STList::Add((void *)NewStringPointer);
   } else
   {
      printf("List topped out at %d\n",Count());
   }

   if(FractalAddSavingOn == true)
   {
      FractalLineCount++;
      if(TempReturn > 100)
      {
         FractalSaveToFile();
      }
      TempReturn = FractalLineCount;
   }

   return(TempReturn);
}

unsigned int STStringList::Insert(unsigned int Position, const char* NewStringToAdd)
{
   STString TempString;
   unsigned int TempReturn;

   TempString = NewStringToAdd;
   TempReturn = Insert(Position, TempString);
   return(TempReturn);
}

unsigned int STStringList::Insert(unsigned int Position, const STString& NewStringToAdd)
{
   unsigned int TempReturn = 0;
   char *NewStringPointer;
   int LengthOfString;

   LengthOfString = NewStringToAdd.Len();
   NewStringPointer = new char[NewStringToAdd.Len() + 1];

   if(NewStringPointer != NULL)
   {
      strcpy(NewStringPointer,(const char *)NewStringToAdd);
      NewStringPointer[LengthOfString] = 0;
      TempReturn = STList::Insert(Position, NewStringPointer);
   } else
   {
      printf("List topped out at %d\n",Count());
   }
	return(TempReturn);
}

void STStringList::FractalSaveToFile()
{
   STString WriteString;
   int CutStringShort;
   int FileIOBufferIndex,LengthOfString;
   STStringsEx StringX;

   FileIOBufferIndex = 0;
   WriteString = "";
   LengthOfString = 0;
   while((Count())||(LengthOfString > 0))
   {
      if(LengthOfString == 0)
      {
         WriteString = Item(0);
         STStringList::Remove(0);
      }
      LengthOfString = WriteString.Len();
      if((FileIOBufferIndex + LengthOfString) < DiskIOBufferSize)
      {
         memcpy(FractalFileIOBuffer + FileIOBufferIndex,(const char *)WriteString,LengthOfString);
         FileIOBufferIndex += LengthOfString;
         if(FractalFileIOBuffer[FileIOBufferIndex - 1] != '\n')
         {
            FractalFileIOBuffer[FileIOBufferIndex] = '\n';
            FileIOBufferIndex++;
         }
         WriteString = "";
      } else
      {
         CutStringShort = DiskIOBufferSize - FileIOBufferIndex;
         memcpy(FractalFileIOBuffer + FileIOBufferIndex,(const char *)WriteString,CutStringShort);
         write(FractalFilePointer,FractalFileIOBuffer,DiskIOBufferSize);
         WriteString = StringX.Remove(WriteString, 0,CutStringShort);
         FileIOBufferIndex = 0;
      }
      LengthOfString = WriteString.Len();
   }
   if(LengthOfString > 0)
   {
      memcpy(FractalFileIOBuffer + FileIOBufferIndex,(const char *)WriteString,LengthOfString);
      FileIOBufferIndex += LengthOfString;
      if(FractalFileIOBuffer[LengthOfString] != '\n')
      {
         LengthOfString++;
         FractalFileIOBuffer[LengthOfString] = '\n';
      }
   }
   if(FileIOBufferIndex > 0)
   {
      write(FractalFilePointer,FractalFileIOBuffer,FileIOBufferIndex);
   }
}

void STStringList::Clear()
{
   unsigned int ItemsToDelete,ALoopCounter;

	//printf("STStringList::Count() = %d\n", Count());
   ItemsToDelete = Count();
   for(ALoopCounter = 0; ALoopCounter < ItemsToDelete; ALoopCounter++)
   {
      STStringList::Remove(0);
   }
}

unsigned int STStringList::SaveToFile(const char* FilePathAndName)
{
   STString TheFileName;

   TheFileName = FilePathAndName;
   return(SaveToFile(TheFileName));
}

void STStringList::WipeFile(const STString& WipePathAndFileName)
{
   FILE *EraseFilePointer;

   // I need to blank the file here. I did it in this goofy way because of two problems.
   // 1) Qt uses remove() to remove enteries from a list so I can not call remove to remove the file.
   // 2) open has no way to blank a file that exists, when writing to a file the old data in the
   //    file remains in it, that is not wanted.
   EraseFilePointer = fopen(WipePathAndFileName,"w");
	if(EraseFilePointer == 0)
	{
		fprintf(stderr, "STStringList::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
	} else
	{
		fclose(EraseFilePointer);
	}
}

unsigned int STStringList::SaveToFile(const STString& FilePathAndName)
{
   unsigned int ALoopCounter;
   char *FileIOBuffer;
   int FilePointer;
   STString SaveFileName;
   STStringsEx StringX;
   STString WriteString;
   int CutStringShort;
   int FileIOBufferIndex,LengthOfString;

	// This function writes the strings to a text file in blocks of DiskIOBufferSize. This is faster then writing the file one line at a time because RAM is much faster then block devices.

	SaveFileName=FilePathAndName;
   StringX.StripChars(SaveFileName,'"');

	// Make the buffer larger by 2, one for the \n if we need to add it an one for the null end of string
   FileIOBuffer = new char[DiskIOBufferSize + 2];

   WipeFile(SaveFileName);

	FilePointer = open(SaveFileName, O_WRONLY|O_CREAT,S_IRWXU|S_IRWXG|S_IRWXO);
	if(FilePointer < 0)
	{
		fprintf(stderr, "ERROR! Cannot write to file STStringList::SaveToFile(%s) line %d errno=%d '%s'\n", (const char*)SaveFileName, __LINE__, errno, strerror(errno));
	} else
	{
		FileIOBufferIndex = 0;
		WriteString = "";
		ALoopCounter = 0;
		LengthOfString = 0;
		while((ALoopCounter < Count())||(LengthOfString > 0))
		{
			if(LengthOfString == 0)
			{
				WriteString = Item(ALoopCounter);
			}
			LengthOfString = WriteString.Len();
			if((FileIOBufferIndex + LengthOfString) < DiskIOBufferSize)
			{
				if(GetDebugLevel() == 99)
				{
					fprintf(stderr, "STStringList::SaveToFile(%s) not enought data to write to buffer\nWriteString.Len = %d\tFileIOBufferIndex = %d\n", (const char *)WriteString, WriteString.Len(), FileIOBufferIndex);
				}
				memcpy(FileIOBuffer + FileIOBufferIndex,(const char *)WriteString, LengthOfString);
				FileIOBufferIndex += LengthOfString;
				//if(FileIOBuffer[FileIOBufferIndex] != '\n')
				if(WriteString[WriteString.Len()] != '\n')
				{
					if(GetDebugLevel() == 99) fprintf(stderr, "STStringList::SaveToFile adding a \\n to string\n");
					FileIOBuffer[FileIOBufferIndex] = '\n';
					FileIOBufferIndex++;
				} else
				{
					if(GetDebugLevel() == 99)
					{
						fprintf(stderr, "STStringList::SaveToFile not adding a \\n to string\n");
					}
				}
				FileIOBuffer[FileIOBufferIndex] = 0;
				WriteString = "";
				ALoopCounter++;
			} else
			{
				CutStringShort = DiskIOBufferSize - FileIOBufferIndex;
				memcpy(FileIOBuffer + FileIOBufferIndex,(const char *)WriteString,CutStringShort);
				WriteString = StringX.Remove(WriteString, 0, CutStringShort);
				if(GetDebugLevel() == 99)
				{
					fprintf(stderr, "STStringList::SaveToFile buffer full and ready to write to file\nWriteString cut short = '%s'\nFileIOBuffer = '%s", (const char *)WriteString, (const char *)FileIOBuffer);
				}
				write(FilePointer,FileIOBuffer,DiskIOBufferSize);
				/*for(ALoopCounter = 0;ALoopCounter < DiskIOBufferSize;ALoopCounter++)
				{
					FileIOBuffer[ALoopCounter] = 0;
				}*/
				FileIOBufferIndex = 0;
			}
			LengthOfString = WriteString.Len();
		}
		if(LengthOfString > 0)
		{
			fprintf(stderr, "STStringList::SaveToFile(%s) Check line 538, this should not happen.", (const char *)WriteString);
			memcpy(FileIOBuffer + FileIOBufferIndex,(const char *)WriteString,LengthOfString);
			FileIOBufferIndex += LengthOfString;
			if(FileIOBuffer[LengthOfString] != '\n')
			{
				LengthOfString++;
				FileIOBuffer[LengthOfString] = '\n';
			}
		}
		if(FileIOBufferIndex > 0)
		{
			write(FilePointer,FileIOBuffer,FileIOBufferIndex);
		}
		close(FilePointer);
	}
	delete[] FileIOBuffer;
	return(Count());
}

char* STStringList::Pop(unsigned int Index)
{
	// Could other objects like STHash make use of these features?
   char *StringPointer = 0;

	if(STList::Count() > Index)
	{
		StringPointer = (char *)STList::Item(Index);
		STList::Delete(Index);
	} else
	{
		fprintf(stderr, "STStringList::Pop(%d) no such nodule!\n", Index);
	}
   return(StringPointer);
}

unsigned int STStringList::Push(char* NewCharArray)
{
	unsigned int TempReturn = 0;

	if(NewCharArray != 0)
	{
      TempReturn = STList::Add(NewCharArray);
	}
	return(TempReturn);
}
