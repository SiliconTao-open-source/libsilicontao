/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#ifndef STDynamicLibrary_included
#define STDynamicLibrary_included

#include <STString.h>

class STDynamicLibrary
{
	public:
		bool ControllerLibLoaded;
		STString LibraryName;
		void *HandleLibPointer;
		void *GetMember(const char* MemberName);
		int CallMember(const char* MemberName, ...);
		STDynamicLibrary(const char* OpenLibraryName);
		~STDynamicLibrary();

	private:
		int LoadTheLibrary();
};


#endif // STDynamicLibrary_included
