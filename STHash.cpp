/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:

=============================================================================*/

#include <STString.h>
#include <STStringList.h>
#include <STList.h>
#include <stdio.h>
#include <STHash.h>

STHash::STHash(const char* NewObjectName)
: STObjectController(NewObjectName)
{
	CommonConstructor(NewObjectName);
}

STHash::STHash()
{
	CommonConstructor("unknown");
}

void STHash::CommonConstructor(const char* NewObjectName)
{
	STString Morph;
	Morph = NewObjectName;
	Morph += "::DataValue";
	DataValue = new STList((const char*)Morph);
	Morph = NewObjectName;
	Morph += "::CellName";
	CellName = new STStringList((const char*)Morph);	
	SetClassName("STHash");
}

STHash::~STHash()
{
	Clear();
	delete DataValue;
	delete CellName;	
}

void STHash::Clear()
{
	while(DataValue->Count())
	{
		Delete((unsigned int)0);
	}
}

void STHash::Delete(int IndexOfCell)
{
	Delete((unsigned int)IndexOfCell);
}
	
void STHash::Delete(unsigned int IndexOfCell)
{
	STString *DataPointer;
	if(DataValue->Count() > IndexOfCell)
	{
		DataPointer = (STString *)DataValue->Item(IndexOfCell);
		delete DataPointer;
		CellName->Delete(IndexOfCell);
		DataValue->Delete(IndexOfCell);
	}	
}

void STHash::Delete(const char* NameOfCell)
{
	for(unsigned int i = 0; i < CellName->Count(); i++)
	{
		if(CellName->Item(i) == NameOfCell)
		{
			Delete(i);
			break;
		}
	}	
}

void STHash::Delete(const STString& NameOfCell)
{
	for(unsigned int i = 0; i < CellName->Count(); i++)
	{
		if(CellName->Item(i) == NameOfCell)
		{
			Delete(i);
			break;
		}
	}	
}

STString& STHash::operator[](const char* NameOfCell)
{
	return(HashPipe(NameOfCell));	
}

STString& STHash::operator[](const int IndexOfCell)
{	
	return(ByIndex((unsigned int) IndexOfCell));
}

STString& STHash::operator[](const unsigned int IndexOfCell)
{
	return(ByIndex(IndexOfCell));	
}

int STHash::IndexOf(const char* NameOfCell)
{
	for(unsigned int IndexCounter = 0; IndexCounter < CellName->Count(); IndexCounter++)
	{
		if(CellName->Item(IndexCounter) == NameOfCell)
		{
			return(IndexCounter);
		}
	}	
	return(-1);
}

STString& STHash::ByIndex(unsigned int IndexOfCell)
{
	STString *DataPointer;
	unsigned int IndexCounter;
	bool CellFound;
	STString NameOfCell;
	
	CellFound = false;
	IndexCounter = 0;
	if(IndexOfCell < CellName->Count())
	{
		IndexCounter = IndexOfCell;
		CellFound = true;
	}
	if(CellFound == false)
	{
		DataPointer = new STString();
		DataValue->Add(DataPointer);
		NameOfCell.Sprintf("%d",IndexOfCell);
		CellName->Add(NameOfCell);
		IndexCounter = DataValue->Count() - 1;
	}

	DataPointer = (STString *)DataValue->Item(IndexCounter);

	return(*DataPointer);
}

unsigned int STHash::Count()
{
	return(CellName->Count());
}

STString STHash::Key(unsigned int IndexOfCell)
{
	bool CellFound;
	
	ReturnStr = "";
	CellFound = false;
	if(IndexOfCell < CellName->Count())
	{
		CellFound = true;
		ReturnStr = CellName->Item(IndexOfCell);
	} else
	{
		fprintf(stderr, "WARNING: STHash::Key(%u) beyond range of hash\n", IndexOfCell);
	}

	return(ReturnStr);	
}

STString& STHash::HashPipe(const char* NameOfCell)
{
	STString* DataPointer;
	unsigned int IndexCounter;
	bool CellFound;
	
	CellFound = false;
	for(IndexCounter = 0; IndexCounter < CellName->Count(); IndexCounter++)
	{
		if(CellName->Item(IndexCounter) == NameOfCell)
		{
			CellFound = true;
			break;
		}
	}
	if(CellFound == false)
	{
		//fprintf(stderr, "STHash::HashPipe(%s) new value added\n", NameOfCell);
		DataPointer = new STString();
		(*DataPointer) = "";
		DataValue->Add(DataPointer);
		CellName->Add(NameOfCell);
		IndexCounter = DataValue->Count() - 1;
	} //else
	//{
	//	fprintf(stderr, "STHash::HashPipe(%s) value located\n", NameOfCell);
	//}

	DataPointer = (STString *)DataValue->Item(IndexCounter);

	return(*DataPointer);
}

STString& STHash::operator[](const STString& NameOfCell)
{
	return(HashPipe((const char*)NameOfCell));
}
