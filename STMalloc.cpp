/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:
      - test speed of new VS malloc VS STMalloc

=============================================================================*/

#include <STMalloc.h>
#include <STList.h>
#include <STCommonCore.h>
#include <stdio.h>
#include <stdlib.h>

struct TMemController
{
   char *MemoryAddress;
   bool Active;
   int MemorySize;
};

STMalloc::STMalloc()
{
	//Control = new TMemController;
   ControlList = new STList();
}

STMalloc::~STMalloc()
{
	unsigned int ALoopCounter;

   for(ALoopCounter=0;ALoopCounter<ControlList->Count();ALoopCounter++)
   {
   	Control = (TMemController *)ControlList->Item(ALoopCounter);
   	if(Control->Active != false)
      {
       	// printf("Memory cell %d at 0x%8X was not freed! It will now be auto freed.\n", ALoopCounter,(unsigned int)Control[ALoopCounter].MemoryAddress);
			delete Control->MemoryAddress;
			Control->MemoryAddress = 0;
       	Control->MemorySize = 0;
        	Control->Active = false;
      }
   }
   ControlList->Clear();
   delete(ControlList);
}

void *STMalloc::MemAlloc(int SizeOfMemToAlloc)
{
   char *TempReturn;

   TempReturn = 0;
   // Control = (TMemController *)malloc(sizeof(TMemController));
   Control = new TMemController;
   // TempReturn = malloc(SizeOfMemToAlloc);
   TempReturn = new char[SizeOfMemToAlloc];
   if(TempReturn != NULL)
   {
      ControlList->Add(Control);
      Control->MemoryAddress = TempReturn;
      Control->MemorySize = SizeOfMemToAlloc;
      Control->Active = true;
   } else
   {
      printf("Error: STMalloc::MemAlloc(%d) Could not allocate memory!\n",SizeOfMemToAlloc);
   }
   return(TempReturn);
}

bool STMalloc::MemFree(void *AddressToFree)
{
	unsigned int ALoopCounter;
   bool TempReturn;
   bool OurMemory;

   TempReturn = false;

   OurMemory = false;

   for(ALoopCounter=0;ALoopCounter<ControlList->Count();ALoopCounter++)
   {
   	Control = (TMemController *)ControlList->Item(ALoopCounter);
   	if(Control->MemoryAddress == AddressToFree)
      {
         OurMemory = true;
   		if(Control->Active == true)
      	{
         	LastCellFreed = ALoopCounter;
				delete (char*)AddressToFree;
            ControlList->Delete(ALoopCounter);
            ALoopCounter--;
            delete Control;
            TempReturn = true;
         	break;
         } else
         {
            printf("ERROR! STMalloc::MemFree Control reports memory not active!\n");
         }
         break;
      }
   }

   if(OurMemory == false)
   {
      printf("ERROR! STMalloc::MemFree Control reports ");
      printf("memory location not under this objects control!\n");
      printf("Was looking for 0x%08X\n",(unsigned int)AddressToFree);
	}

   return(TempReturn);
}

unsigned int STMalloc::Count()
{
   return(ControlList->Count());
}
