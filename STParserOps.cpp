/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:

=============================================================================*/

#include <STParserOps.h>
#include <STCommonCore.h>
#include <STObjectController.h>
#include <STStringsEx.h>
#include <stdio.h>

STParserOps::STParserOps()
: STObjectController()
{

}

STParserOps::~STParserOps()
{
}

STString STParserOps::Strip(STString& TextString, const char TheChar)
{
	while(TextString.Pos(TheChar) == 0)
	{
		TextString.Remove(0, 1);
	}
	while(TextString.Pos(TheChar, -1) == (int)TextString.Len())
	{
		TextString.Remove(TextString.Len() - 1, 1);
	}
	ReturnString = TextString;
	return(ReturnString);
}

STString STParserOps::Strip(STString& TextString, const char *TheChar)
{
	STString Option = TheChar;
	STString MultiStrip = "";

	if(Option == "[:ws:]")
	{
		MultiStrip = Strip(TextString, ' ');
		MultiStrip = Strip(MultiStrip, '\t');
	}
	ReturnString = MultiStrip;
	return(ReturnString);
}

bool STParserOps::WildCardCompairStrings(const STString& OriginalString1, const STString& OriginalString2, bool CaseSensitive)
{
   STString Constructed1;
   STString PartString1;
   STString CompairString1,CompairString2;
   bool Done, TempReturn;
   int NextAsteriks, NextQMark;
   int Position1, Position2;
   char RemoveChar;
   bool Stripped;
   STStringsEx StringX;

   CompairString1 = OriginalString1;
   CompairString2 = OriginalString2;

   // DebugMessage(2, InLineDbStr.Sprintf(" - CompairString1 = %s , CompairString2 = %s\n",(const char *)CompairString1, (const char *)CompairString2));

   Done = false;
   PartString1 = "";
   TempReturn = false;

   if((CompairString1 != "")&&(CompairString2 != ""))
   {
      // Strip unwanted chars from front and back
      Stripped = false;
      while(Stripped == false)
      {
         Stripped = true;
         RemoveChar = CompairString1[0];

         // DebugMessage(2, "Stripping CompairString1[0]");

         // Because a char is signed, this will remove any abs < 32 and > 127
         if(RemoveChar < 33)
         {
            CompairString1 = StringX.Remove(CompairString1, 0,1);
            Stripped = false;
         }
         if(CompairString1 == "")
         {
            break;
         }
      }
      Stripped = false;
      while(Stripped == false)
      {
         Stripped = true;

         // DebugMessage(2, "Stripping CompairString1[CompairString1.Len()-1]\n");

         // Because a char is signed, this will remove any abs < 32 and > 127
         RemoveChar = CompairString1[CompairString1.Len()-1];
         if(RemoveChar < 33)
         {
            CompairString1 = StringX.Remove(CompairString1, CompairString1.Len()-1,1);
            Stripped = false;
         }
         if(CompairString1 == "")
         {
            break;
         }
      }
      Stripped = false;
      while(Stripped == false)
      {
         Stripped = true;
         RemoveChar = CompairString2[0];

         // DebugMessage(2, "Stripping CompairString2[0]");

         // Because a char is signed, this will remove any abs < 32 and > 127
         if(RemoveChar < 33)
         {
            CompairString2 = StringX.Remove(CompairString2, 0,1);
            Stripped = false;
         }
         if(CompairString2 == "")
         {
            break;
         }
      }
      Stripped = false;
      while(Stripped == false)
      {
         Stripped = true;

         // DebugMessage(2, "Stripping CompairString2[CompairString2.Len()-1]");

         // Because a char is signed, this will remove any abs < 32 and > 127
         RemoveChar = CompairString2[CompairString2.Len()-1];
         if(RemoveChar < 33)
         {
            CompairString2 = StringX.Remove(CompairString2, CompairString2.Len()-1,1);
            Stripped = false;
         }
         if(CompairString2 == "")
         {
            break;
         }
      }
   }


   // Make CompairString1 the wildcard string
   NextAsteriks = CompairString2.Pos("*");
   NextQMark = CompairString2.Pos("?");

   if((NextAsteriks > -1)||(NextQMark > -1))
   {
      Constructed1 = CompairString2;
      CompairString2 = CompairString1;
      CompairString1 = Constructed1;
   }

   // Both strings can't have wildcards
   NextAsteriks = CompairString2.Pos("*");
   NextQMark = CompairString2.Pos("?");
   if(! ((NextAsteriks > -1)||(NextQMark > -1)))
   {
      TempReturn = true;
      while(Done == false)
      {
         // DebugMessage(2, "Testing strings\n");

         NextAsteriks = CompairString1.Pos("*");
         NextQMark = CompairString1.Pos("?");

         if(NextAsteriks == 0)
         {
            // DebugMessage(2, InLineDbStr.Sprintf("NextAsteriks = 0 in %s\n",(const char *)CompairString1));

            // Remove from Par CompairString1 the leading wildcard
            // Two leading wildcards in a row are rejected, first wild card is used
            while((NextAsteriks == 0)||(NextQMark == 0))
            {
               CompairString1 = StringX.Remove(CompairString1, 0,1);
               NextAsteriks = PartString1.Pos("*");
               NextQMark = PartString1.Pos("?");
            }

            PartString1 = CompairString1;

            // Remove from PartString1 any thing after the next wildcard
            // If there are no more wildcards then CompairString1 must = CompairString2
            NextAsteriks = PartString1.Pos("*");
            NextQMark = PartString1.Pos("?");
            if((NextAsteriks > -1)&&((NextAsteriks < NextQMark)||(NextQMark == -1)))
            {
               PartString1 = StringX.Remove(PartString1, NextAsteriks,PartString1.Len());
            } else
            if((NextQMark > -1)&&((NextQMark < NextAsteriks)||(NextAsteriks == -1)))
            {
               PartString1 = StringX.Remove(PartString1, NextQMark,PartString1.Len());
            } else
            if((NextAsteriks == -1)&&(NextQMark == -1))
            {
               CompairString1 = PartString1;
            }

            // If PartString1 is found in CompairString2 anywhere then
            // Remove every thing up to that point
            if(CompairString2.Pos(PartString1) > -1)
            {
               Position1 = CompairString2.Pos(PartString1);
               CompairString2 = StringX.Remove(CompairString2, 0,Position1);
            }

            // At this point in the code it is possible that CompairString1 is
            // empty because it's last char was a star, if so it is a match
            if(CompairString1 == "")
            {
               break;
            }

            // At this point in the code PartString1 should be at the start of
            // CompairString1 and CompairString2 delete them and search again
            Position1 = CompairString2.Pos(PartString1);
            //printf("Position1 = %s , CompairString2 = %s\n",(const char *)PartString1,
            //   (const char *)CompairString2);
            if(Position1 > -1)
            {
               // DebugMessage(2, InLineDbStr.Sprintf("CompairString1 = %s , CompairString2 = %s\n",(const char *)CompairString1, (const char *)CompairString2));

               CompairString1 = StringX.Remove(CompairString1, 0,PartString1.Len());
               CompairString2 = StringX.Remove(CompairString2, 0,PartString1.Len());

               if(WildCardCompairStrings(CompairString1,CompairString2,CaseSensitive)==false)
               {
                  TempReturn = false;
                  Position1 = CompairString2.Pos(PartString1);
                  while(Position1 > -1)
                  {
                     Position1 = CompairString2.Pos(PartString1);
                     CompairString2 = StringX.Remove(CompairString2, 0,Position1-1);
                     CompairString2 = StringX.Remove(CompairString2, 0,PartString1.Len());
                     // DebugMessage(2, InLineDbStr.Sprintf("CompairString1 = %s , CompairString2 = %s\n",(const char *)CompairString1, (const char *)CompairString2));

                     if(WildCardCompairStrings(CompairString1,CompairString2,CaseSensitive)==true)
                     {
                        //printf("we keep it\n");
                        TempReturn = true;
                        break;
                     }
                  }
               } else
               // CompairString1 & CompairString2 could both be blank now
               if(CompairString1 == CompairString2)
               {
                  TempReturn = true;
               }
            } else
            {
               //printf("problem with it\n");
               TempReturn = false;
            }
            Done = true;
         } else
         if(NextQMark == 0)
         {
            // DebugMessage(2, InLineDbStr.Sprintf("NextQMark = 0 in %s\n",(const char *)CompairString1));

            // Remove from Par CompairString1 the leading wildcard
            // Two leading wildcards in a row are rejected, first wild card is used
            while((NextAsteriks == 0)||(NextQMark == 0))
            {
               CompairString1 = StringX.Remove(CompairString1, 0,1);
               NextAsteriks = PartString1.Pos("*");
               NextQMark = PartString1.Pos("?");
            }

            PartString1 = CompairString1;

            NextAsteriks = PartString1.Pos("*");
            NextQMark = PartString1.Pos("?");

            // Remove from PartString1 any thing after the next wildcard }
            // If there are no more wildcards then CompairString1 must = CompairString2 }
            if((NextAsteriks > -1)&&((NextAsteriks < NextQMark)||(NextQMark == -1)))
            {
               PartString1 = StringX.Remove(PartString1, NextAsteriks,PartString1.Len());
            } else
            if((NextQMark > -1)&&((NextQMark < NextAsteriks)||(NextAsteriks == -1)))
            {
               PartString1 = StringX.Remove(PartString1, NextQMark,PartString1.Len());
            } else
            if((NextAsteriks == -1)&&(NextQMark == -1))
            {
               CompairString1 = PartString1;
            }

            // If PartString1 is found in CompairString2 @ ? allowed location then }
            // Remove every thing up to that point }
            if(CompairString2.Pos(PartString1) == 1)
            {
               CompairString2 = StringX.Remove(CompairString2, 0,1);
            } else
            {
               //printf("died here 1\n");
               TempReturn = false;
               break;
            }

            // At this point in the code PartString1 should be at the start of }
            // CompairString1 and CompairString2 delete them and search again  }
            Position1 = CompairString2.Pos(PartString1);
            if(Position1 > -1)
            {
               CompairString1 = StringX.Remove(CompairString1, 0,PartString1.Len());
               CompairString2 = StringX.Remove(CompairString2, 0,PartString1.Len());
               TempReturn &= WildCardCompairStrings(CompairString1,CompairString2,CaseSensitive);
            } else
            {
               //printf("died here 2\n");
               TempReturn = false;
            }
            Done = true;
         } else
         if((NextAsteriks > -1)&&((NextAsteriks < NextQMark)||(NextQMark == -1)))
         {
            // DebugMessage(2, "((NextAsteriks > -1)&&((NextAsteriks < NextQMark)||(NextQMark == -1)))\n");
            // DebugMessage(2, InLineDbStr.Sprintf(" in %s\n",(const char*)CompairString1));

            PartString1 = CompairString1;
            PartString1 = StringX.Remove(PartString1, NextAsteriks,PartString1.Len());
            Position2 = PartString1.Len();
            CompairString1 = StringX.Remove(CompairString1, 0,Position2);
            if(CompairString2.Pos(PartString1) == 0)
            {
               Position1 = PartString1.Len();
               CompairString2 = StringX.Remove(CompairString2, 0,Position1);
               TempReturn &= WildCardCompairStrings(CompairString1,CompairString2,CaseSensitive);
            } else
            {
               TempReturn = false;
            }
            Done = true;
         } else
         if((NextQMark > -1)&&((NextQMark < NextAsteriks)||(NextAsteriks == -1)))
         {
            // DebugMessage(2, "((NextQMark > -1)&&((NextQMark < NextAsteriks)||(NextAsteriks == -1)))\n");
            // DebugMessage(2, InLineDbStr.Sprintf(" in %s\n",(const char*)CompairString1));

            PartString1 = CompairString1;
            PartString1 = StringX.Remove(PartString1, NextQMark,PartString1.Len());
            Position1 = CompairString1.Pos(PartString1);
            Position2 = Position1 + PartString1.Len();
            CompairString1 = StringX.Remove(CompairString1, Position1,Position2);
            if(CompairString2.Pos(PartString1)==0)
            {
               Position1 = PartString1.Len();
               CompairString2 = StringX.Remove(CompairString2, 0,Position1);
               TempReturn &= WildCardCompairStrings(CompairString1,CompairString2,CaseSensitive);
            } else
            {
               TempReturn = false;
            }
            Done = true;
         } else
         if((NextAsteriks == -1)&&(NextQMark == -1))
         {
            // DebugMessage(2, "((NextAsteriks == -1)&&(NextQMark == -1))\n");
            // DebugMessage(2, InLineDbStr.Sprintf(" in %s\n",(const char*)CompairString1));

            if(CompairString1 != CompairString2)
            {
               TempReturn = false;
            }
            Done = true;
         } else
         {
            // DebugMessage(2, InLineDbStr.Sprintf("NextAsteriks = %d, NextQMark = %d\n",NextAsteriks,NextQMark));
         }
      }
   }
   return(TempReturn);
}

STString STParserOps::ExtractWord(const STString& SourceString, int WordIndex)
{
   ReturnString = "";
   printf("ERROR! Your data is wrong because this function is not writen yet! STParserOps::ExtractWord\n");
   return(ReturnString);
}

STString STParserOps::RemoveWord(const STString& SourceString, int WordIndex)
{
   ReturnString = "";
   printf("ERROR! Your data is wrong because this function is not writen yet! STParserOps::RemoveWord\n");
   return(ReturnString);

}

STString STParserOps::TrucateUpToWord(const STString& SourceString, int WordIndex)
{
   ReturnString = "";
   printf("ERROR! Your data is wrong because this function is not writen yet! STParserOps::TrucateUpToWord\n");
   return(ReturnString);

}

STString STParserOps::TrucateAfterWord(const STString& SourceString, int WordIndex)
{
   ReturnString = "";
   printf("ERROR! Your data is wrong because this function is not writen yet! STParserOps::TrucateAfterWord\n");
   return(ReturnString);

}

STString STParserOps::FindFirstWord(const STString& SourceString)
{
	return(FindNextWord(SourceString,1));
}

STString STParserOps::FindNextWord(const STString& SourceString, int WordIndex)
{
   bool TestLetter;
   char LetterToTest;
   int ALoopCounter, BLoopCounter, NextStrech;
	STString WorkingString;

	WorkingString = SourceString;
	ReturnString = "";
   for(ALoopCounter = 0;ALoopCounter < WordIndex;ALoopCounter++)
   {
		ReturnString = "";
		NextStrech = WorkingString.Len();
		for(BLoopCounter = 0; BLoopCounter < NextStrech; BLoopCounter++)
		{
			// Look for start of an identifyer
			TestLetter = false;
			LetterToTest = ((const char*)WorkingString)[0];
			TestLetter |= ((LetterToTest >= 'A')&&(LetterToTest <= 'Z'));
			TestLetter |= ((LetterToTest >= 'a')&&(LetterToTest <= 'z'));
			WorkingString.Remove(0, 1);
			if(TestLetter)
			{
				ReturnString += LetterToTest;
			} else
			{
				if(ReturnString.Len() > 0)
				{
					break;
				}
			}
			if(WorkingString.Len() < 1)
			{
				break;
			}
		}
		if(WorkingString.Len() < 1)
		{
			break;
		}
   }
   return(ReturnString);
}

STString STParserOps::FindFirstIdentifier(const STString& SourceString)
{
   return(FindNextIdentifier(SourceString,1));
}


int STParserOps::CountIdentifiers(const STString& SourceString)
{
	STString WorkingString;
	STString PartsOfString;
	int TempReturn = 0;

	printf("CountIdentifiers(%s)\n",(const char*)SourceString);
	WorkingString = SourceString;
	WorkingString.Strip();
	PartsOfString = FindNextIdentifier(WorkingString, 1);
	while(PartsOfString != "")
	{
		TempReturn++;
		WorkingString.Remove(0, WorkingString.Pos(PartsOfString, 0) + PartsOfString.Len() + 1);
		WorkingString.Strip();
		PartsOfString = FindNextIdentifier(WorkingString, 1);
		PartsOfString.Strip();
	}

	printf("CountIdentifiers() returns %d\n",TempReturn);
	return(TempReturn);
}

STString STParserOps::FindLastIdentifier(const STString& SourceString)
{
	return(FindNextLastIdentifier(SourceString, -1));
}

STString STParserOps::FindNextLastIdentifier(const STString& SourceString, int IdentifierIndex)
{
	int TotalIdents, IdentToFind;
	ReturnString = "";
	if(IdentifierIndex < 0)
	{
		TotalIdents = CountIdentifiers(SourceString);
		if(TotalIdents > 0)
		{
			IdentToFind = TotalIdents + IdentifierIndex;
			if((IdentToFind > 0) && (IdentToFind <= TotalIdents))
			{
				ReturnString = FindNextIdentifier(SourceString, IdentToFind + 1);
			}
		}
	}
	return(ReturnString);
}

STString STParserOps::FindNextIdentifier(const STString& SourceString, int IdentifierIndex)
{
   bool TestLetter;
   char LetterToTest;
   int ALoopCounter, BLoopCounter, NextStrech;
	STString WorkingString;

	WorkingString = SourceString;
	ReturnString = "";
   for(ALoopCounter = 0;ALoopCounter < IdentifierIndex;ALoopCounter++)
   {
		ReturnString = "";
		NextStrech = WorkingString.Len();
		for(BLoopCounter = 0; BLoopCounter < NextStrech; BLoopCounter++)
		{
			// Look for start of an identifyer
			TestLetter = false;
			LetterToTest = ((const char*)WorkingString)[0];
			TestLetter |= ((LetterToTest >= 'A')&&(LetterToTest <= 'Z'));
			TestLetter |= ((LetterToTest >= 'a')&&(LetterToTest <= 'z'));
			TestLetter |= LetterToTest == '_';
			if(ReturnString != "")
			{
				TestLetter |= ((LetterToTest >= '0')&&(LetterToTest <= '9'));
			}
			WorkingString.Remove(0, 1);
			if(TestLetter)
			{
				ReturnString += LetterToTest;
			} else
			{
				if(ReturnString.Len() > 0)
				{
					break;
				}
			}
			if(WorkingString.Len() < 1)
			{
				break;
			}
		}
		if(WorkingString.Len() < 1)
		{
			break;
		}
   }
   return(ReturnString);
}

bool STParserOps::IsAlpha(char TheChar)
{
   bool ReturnValue = false;
	if((TheChar >= 'A') && (TheChar <= 'Z'))
	{
		ReturnValue = true;
	}
	if((TheChar >= 'a') && (TheChar <= 'z'))
	{
		ReturnValue = true;
	}
   return(ReturnValue);
}

bool STParserOps::IsNumber(char TheChar)
{
   bool ReturnValue = false;
	if((TheChar >= '0') && (TheChar <= '9'))
	{
		ReturnValue = true;
	}
   return(ReturnValue);
}

bool STParserOps::IsSymbol(char TheChar)
{
   bool ReturnValue = false;
	if((! IsAlpha(TheChar)) && (! IsNumber(TheChar)) && (! IsWhiteSpace(TheChar)))
	{
		ReturnValue = true;
	}
   return(ReturnValue);
}

bool STParserOps::IsWhiteSpace(char TheChar)
{
   bool ReturnValue = false;
	if((TheChar == ' ') || (TheChar == '\t') || (TheChar == '\n'))
	{
		ReturnValue = true;
	}
   return(ReturnValue);
}

STString STParserOps::ExtractBlock(const STString& SourceString, int BlockIndex, const STString& BlockIndicators)
{
	int NextMarkerPosition, StringIndex;

	// If BlockIndicators does not exits then return should be blank unless we want the first block
	ReturnString = "";
	if(SourceString.Len() > 0)
	{
		ReturnString = SourceString;

		StringIndex = 0;
		NextMarkerPosition = SourceString.Pos(BlockIndicators);
		//printf("NextMarkerPosition = %d\n", NextMarkerPosition);
		if(NextMarkerPosition > -1)
		{
			// Strip off the leading blocks by marker positions
			while(StringIndex < BlockIndex)
			{
				StringIndex++;
				NextMarkerPosition = ReturnString.Pos(BlockIndicators);
				ReturnString.Remove(0, NextMarkerPosition + 1);
			}

			// No more leading blocks, Remove ending blocks
			NextMarkerPosition = ReturnString.Pos(BlockIndicators);
			if(NextMarkerPosition > 0)
			{
				ReturnString.Remove(NextMarkerPosition, ReturnString.Len() + 1);
			}
		} else
		if(BlockIndex != 0)
		{
			ReturnString = "";
		}
	}
   return(ReturnString);
}

STString STParserOps::RemoveBlock(const STString& SourceString, int BlockIndex, const STString& BlockIndicators)
{
   ReturnString = "";
   printf("ERROR! Your data is wrong because this function is not writen yet! STParserOps::RemoveBlock\n");
   return(ReturnString);

}

STString STParserOps::ExtractNumberString(const STString& SourceString, int WordIndex)
{
   ReturnString = "";
   printf("ERROR! Your data is wrong because this function is not writen yet! STParserOps::ExtractNumberString\n");
   return(ReturnString);
}
