/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STStringList.h
	Object Name:				STStringList
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Day Zero:					02152000, February 15, 2000
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:       	alloc.h, stdio.h, string.h
   Ports Used:           	None
   Title of program:			Object library
   Description:
=============================================================================*/

#ifndef STStringList_included
#define STStringList_included

#include <STList.h>
#include <STString.h>

struct STStringListNodule
{
	unsigned int SlcID;
	char* StringPointer;
};

/**
 * STStringList is an easy to use string list that takes char* or STStrings and
 * returns STStrings. STStringList can load it's strings from a common text file
 * and save it's list in to a text file.
 *
 * @short Easy to use full featured string list system.
 */
class STStringList : public STList
{
   public:
      int DiskIOBufferSize;
      STString ReturnString;

      /**
       * Constructor with a given object name.
       */
      STStringList(const char* NewObjectName);

		/**
       * Constructor with the object name unknown.
       */
      STStringList();

      /**
       * Destructor.
       */
      ~STStringList();

      /**
       * Returns the string contained at the given index.
       */
      STString& Item(unsigned int Index);

      /**
       * Returns the string contained at the given index.
       */
		STString& operator[](unsigned int IndexOfCell);

      /**
       * Returns all strings contained in the list as one large string. See also @ref STString::Split
       */
      STString& Dump();

      /**
       * Removes all items from the list.
       */
      void Clear();

      /**
       * Removes a string from the list. Returns the list count after the
       * string has been removed. As named after the Qt prefered name remove.
       */
      unsigned int Remove(unsigned int IndexToRemove);

      /**
       * Replaces an existing string with a new one at the location of index. Returns the list count.
       */
      unsigned int Replace(unsigned int Index, const STString& NewString);
		unsigned int Replace(unsigned int Index, const char* NewString);

      /**
       * Removes a string from the list. Returns the list count after the
       * string has been removed. As named after the Delphi prefered name Delete.
       */
      unsigned int Delete(unsigned int IndexToRemove);

      /**
       * Appends a string to the list. Returns the count after the new string has been added.
       */
      unsigned int Add(const STString& NewStringToAdd);
      unsigned int Add(const char* NewStringToAdd);

      /**
       * Adds a new string to the list at a given index position and returns it's index number. If the given index position is beyond the end of the list the item is added to the end.
       */
      unsigned int Insert(unsigned int Position, const STString& NewStringToAdd);
      unsigned int Insert(unsigned int Position, const char* NewStringToAdd);

      /**
       * Read a text file in to the string list. All previous data
       * in the list is lost. Returns the number of lines loaded.
       */
      unsigned int LoadFromFile(const STString& FilePathAndName);
      unsigned int LoadFromFile(const char *FilePathAndName);

		/**
		 *	To help debug programs that use this library.
		 * Counts all the bytes used in all loaded strings.
		 */
		unsigned int MemCount();

      /**
       * Saves the contents of the string list to a text file. All
       * previos contents of the text file is lost. Returns the number of lines saved.
       */
      unsigned int SaveToFile(const STString& FilePathAndName);
      unsigned int SaveToFile(const char* FilePathAndName);

      /**
       * For faster saving of large lists, this will save to the list as you add to
       * it and not buffer to swap memory. This is great for large lists like 100 MB
       * text files that I have tried when my memory is 100% and the rest is in swap.
       * After activating this you should only add to the list, do not attempt any
       * other list actions. When done adding use DoneFractalAddSaving to return
       * list to normal operations. This list will be empty, you will have to load it
       * to use it again. If you planned on using the list when done you should not
       * use this feature as it will only save you speed if you intend on writing a
       * text file and then being finished with the list.
       */
      void StartFractalAddSaving(const STString& FilePathAndName);
      void StartFractalAddSaving(const char* FilePathAndName);

      /**
       * Use this to tell terminal the FractalAddSaving and return the list to normal
       * operations. The list will be completley empty after calling this.
       */
      void DoneFractalAddSaving();

		/**
		 *	Append a char array to the end of an STStringList. Saves system resourses because no new or delete functions are called and very little memory is moved.
		 * The receiving STStringList assumes full responsibility for memory management. The sender must abbandon full control of the memory and the memory must not be a constant.
		 * The sendinger will no longer control the allocation of the memory, the receiving list will now control the memory.
		 * Strings can move between @ref STStrings::Unwrap and @ref STStringLists::Pop like so.
		 * <PRE>
		 * unsigned int NewCount = TargetSTStringList.Push(SourceSTStringList.Pop(0));
		 * NewCount = TargetSTStringList.Push(SourceSTString.Unwrap());
		 * </PRE>
		 */
		//unsigned int Push(struct STStringListNodule* NewContainer);
		unsigned int Push(char* NewCharArray);

		/**
		 *	Remove an entry from this STStringList at a given index and hand over memory management responsibility to another like @ref STStringList::Push or @ref STString::Adopt.
		 * Saves system resourses because no new or delete functions are called and very little memory is moved.
		 * The sending STStringList will no longer be responsible for memory management, the receiving list will now control the memory.
		 * <PRE>
		 * unsigned int NewCount = TargetSTStringList.Push(SourceSTStringList.Pop(0));
		 * STString TempString;
		 * TempString.Adpot(SourceSTStringList.Pop(0));
		 * TempString += " that was a lot of fun. I got you now string!";
		 * </PRE>
		 */
		//STStringListNodule* Pop(unsigned int Index);
		char* Pop(unsigned int Index);

   private:
      bool FractalAddSavingOn;
      int FractalLineCount;
      char *FractalFileIOBuffer;
      unsigned int FractalFilePointer;
      STString FractalSaveFileName;
      void CommonConstructor();
      void FractalSaveToFile();
      void WipeFile(const STString& WipePathAndFileName);
};

#endif // STStringList_included
