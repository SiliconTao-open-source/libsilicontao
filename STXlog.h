/*=============================================================================

    Copyright (C) 2009 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
	Object Name:				STSTXlog
   Programmer Name:			Royce Souther & Robert Lewko
   Description:				To log messages to the system logger daemon.
=============================================================================*/

#include <STString.h>

class STXlog;
class STStringList;
extern STXlog *xlog;

/**
 * @short Send log event messages to the Linux system logging server. You can by-pass the local sysklogd server and send logs
 * directly to a log server by setting the IP address in one of the following confing files /etc/<B>AppName</B>/conf or /etc/<B>AppName</B>.conf or /etc/<B>AppName</B>/<B>AppName</B>.conf or ./<B>AppName</B>.conf
 * The line to set the log server reads like this...
 * <PRE>
 * LOG SERVER = 127.0.0.1
 * </PRE>
 */

class STXlog
{
	public:
		STXlog(bool SetDebugMode);
		~STXlog();
		void log(const char *format, ...);
		void AddServer(const STString& NewServer);

	private:
		bool DebugMode;
		STString AppName;
		STStringList* LoggingServers;
		unsigned int UdpPort;
		int UdpSocket;
};
