/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:

=============================================================================*/

#include <STOnlyOne.h>

#include <STCommonCore.h>
#include <STIntList.h>
#include <STStringList.h>
#include <STSystemAPI.h>
#include <time.h>
#include <math.h>

unsigned int STOnlyOne::NumberOfInstances()
{
   unsigned int TempReturn;
   int MyPidNumber;
   STString MyAppName;
   STIntList ListOfApps;
   STSystemAPI SystemAPI1;
   
   TempReturn = 1; // That is a gime.   
   MyPidNumber = SystemAPI1.GetAppPid();
   MyAppName = SystemAPI1.GetAppNameFromPid(MyPidNumber);
   SystemAPI1.GetPidList(MyAppName,&ListOfApps,true);
   TempReturn = ListOfApps.Count();
   return(TempReturn);
}
