/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#include "STAsciiForms.h"
#include "STGrid.h"
#include <stdio.h>
#include <unistd.h>
#include <STList.h>

// Used to get terminal display size
#include <sys/ioctl.h>

/*
http://en.wikipedia.org/wiki/Code_page_437
http://www.cl.cam.ac.uk/~mgk25/unicode.html#utf-8
http://en.wikipedia.org/wiki/Box_drawing_characters

*/

STAsciiForms::STAsciiForms(STAsciiForms* parent)
: STObjectController()
{
	FormParent = parent;
	StandardConstructor();
	int NewWidth = GetColumnCount();
	int NewHeight = GetRowCount();
	printf("NewWidth %d, NewHeight %d\n", NewWidth, NewHeight);
	sleep(25);
	SetGeometry(0, 0, NewWidth, NewHeight);
}

STAsciiForms::STAsciiForms(STAsciiForms* parent, int NewXPosition, int NewYPosition, int NewWidth, int NewHeight)
: STObjectController()
{
	FormParent = parent;
	StandardConstructor();
	SetGeometry(NewXPosition, NewYPosition, NewWidth, NewHeight);
}

void STAsciiForms::StandardConstructor()
{
	SetClassName("STAsciiForms");
	PreviousColor = 65535;
	Grid = NULL;
	Hidden = 0;
	BorderColour = cnYellow;
	TextColour = cnWhite;
}

STAsciiForms::~STAsciiForms()
{
	ClearBox();
	delete Grid;
}

int STAsciiForms::GetRowCount()
{
	struct winsize TermSize;
	if(! isatty(fileno(stdout))) return(0);

	ioctl (fileno(stdout), TIOCGWINSZ, &TermSize);
   return(TermSize.ws_row);
}

int STAsciiForms::GetColumnCount()
{
	struct winsize TermSize;
	if(! isatty(fileno(stdout))) return(0);

	ioctl (fileno(stdout), TIOCGWINSZ, &TermSize);
   return(TermSize.ws_col);
}

void STAsciiForms::NewGrid(int NewWidth, int NewHeight)
{
	Grid = new STGrid(NewWidth, NewHeight, 2);
	int BlankCell, i, c;
	BlankCell = cnOff;
	BlankCell <<= 8;
	BlankCell |= ' ';

	char *RowPointer;
	for(i = 0; i < NewHeight; i++)
	{
		RowPointer = (char*)Grid->RowArray->Item(i);
		for(c = 0; c < NewWidth; c++)
		{
			*RowPointer = cnOff;
			RowPointer++;
			*RowPointer = ' ';
			RowPointer++;
		}
	}
}

void STAsciiForms::SetBorderColour(int NewBorderColour)
{
	BorderColour = NewBorderColour;
}

void STAsciiForms::SetTextColour(int NewTextColour)
{
	TextColour = NewTextColour;
}

void STAsciiForms::Restore()
{
	// Redraw the form from the data in the Grid
	Hidden = 0;
	char *RowPointer;
	int CellColor;
	PreviousColor = 65535;
	SetColor(PreviousColor);
	int i, c;

	for(i = 0; i < Height; i++)
	{
		RowPointer = (char*)Grid->RowArray->Item(i);
		GoToXy(XPosition, YPosition + i);
		for(c = 0; c < Width; c++)
		{
			CellColor = *RowPointer;
			SetColor(CellColor);
			RowPointer++;
			printf("%c", *RowPointer);
			RowPointer++;
		}
	}
	fflush(NULL);
}

void STAsciiForms::Map(int XPosition, int YPosition, int CellColor, int CellValue)
{
	char *CellPointer = (char*)Grid->CellPointer(XPosition, YPosition);
	if(CellPointer != 0)
	{
		*CellPointer = CellColor;
		CellPointer++;
		*CellPointer = CellValue;
		if(Hidden == 0)
		{
			printf("%s", AsciiGlyph(CellValue));
		}
	}
}

void STAsciiForms::AsciiTable()
{
	int a, b, c;
	int Start = 160;

	STGrid *KeepGrid = Grid;
	int HoldXPosition = XPosition;
	int HoldYPosition = YPosition;
	int HoldWidth = Width;
	int HoldHeight = Height;

	XPosition = 0;
	YPosition = 0;
	Width = 89;
	Height = 23;
	NewGrid(Width, Height);
	DrawAsciiBox();

	Print(0, 8, "ASCII code table");
	HBar(1);

	for (a = 0; a < 6; a++)
	{
		for(b = 0; b < 20; b++)
		{
			GoToXy(a * 15 + 5, b + 1 + 3);
			c = (a * 15) + b + Start;
			printf("%d '%s'", c, AsciiGlyph(c));
			fflush(NULL);
		}
	}
	delete Grid;
	Grid = KeepGrid;
	XPosition = HoldXPosition;
	YPosition = HoldYPosition;
	Width = HoldWidth;
	Height = HoldHeight;
	Restore();
}

void STAsciiForms::ClearScreen()
{
   // Clear screen
	printf("%c[2J",27);
   GoToXy(0, 0);
}

void STAsciiForms::DrawAsciiBox()
{
   int i;
   // Note that \33[Y;XH for positioning

	SetColor(BorderColour);

   // Top line
	GoToXy(XPosition + 1, YPosition);
   for(i = 1; i < Width - 1; i++)
   {
		Map(i, 0, BorderColour, 205);
   }

   // Left line
   for(i = 1; i < Height - 1; i++)
   {
		GoToXy(XPosition, YPosition + i);
		Map(0, i, BorderColour, 179);
   }

   // Bottem line
	GoToXy(XPosition + 1, YPosition + Height - 1);
   for(i = 1; i < Width - 1; i++)
   {
		Map(i, Height - 1, BorderColour, 196);
   }

   // Right line
   for(i = 1; i < Height - 1; i++)
   {
		GoToXy(XPosition + Width - 1, YPosition + i);
		Map(Width - 1, i, BorderColour, 179);
   }

   // Top left corner
   GoToXy(XPosition, YPosition);
	Map(0, 0, BorderColour, 213);

   // Top right corner
   GoToXy(XPosition + Width - 1, YPosition);
	Map(Width - 1, 0, BorderColour, 184);

   // Bottem left corner
   GoToXy(XPosition, YPosition + Height - 1);
	Map(0, Height - 1, BorderColour, 192);

   // Bottem right corner
   GoToXy(XPosition + Width - 1, YPosition + Height - 1);
	Map(Width - 1, Height - 1, BorderColour, 217);

	fflush(NULL);
}

const char* STAsciiForms::AsciiGlyph(unsigned char CharValue)
{
	int GlyphStringIndex = 0;
	GlyphString[GlyphStringIndex] = 0;

	const unsigned int LookUpTable[] =
	{
		0x0020, 0x263A, 0x263B, 0x2665, 0x2666, 0x2663, 0x2660, 0x2022,
		0x25D8, 0x25CB, 0x25D9, 0x2642, 0x2640, 0x266A, 0x266B, 0x263C,
		0x25BA, 0x25C4, 0x2195, 0x203C, 0x00B6, 0x00A7, 0x25AC, 0x21A8,
		0x2191, 0x2193, 0x2192, 0x2190, 0x221F, 0x2194, 0x25B2, 0x25BC,
		0x0020, 0x0021, 0x0022, 0x0023, 0x0024, 0x0025, 0x0026, 0x0027,
		0x0028, 0x0029, 0x002A, 0x002B, 0x002C, 0x002D, 0x002E, 0x002F,
		0x0030, 0x0031, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037,
		0x0038, 0x0039, 0x003A, 0x003B, 0x003C, 0x003D, 0x003E, 0x003F,
		0x0040, 0x0041, 0x0042, 0x0043, 0x0044, 0x0045, 0x0046, 0x0047,
		0x0048, 0x0049, 0x004A, 0x004B, 0x004C, 0x004D, 0x004E, 0x004F,
		0x0050, 0x0051, 0x0052, 0x0053, 0x0054, 0x0055, 0x0056, 0x0057,
		0x0058, 0x0059, 0x005A, 0x005B, 0x005C, 0x005D, 0x005E, 0x005F,
		0x0060, 0x0061, 0x0062, 0x0063, 0x0064, 0x0065, 0x0066, 0x0067,
		0x0068, 0x0069, 0x006A, 0x006B, 0x006C, 0x006D, 0x006E, 0x006F,
		0x0070, 0x0071, 0x0072, 0x0073, 0x0074, 0x0075, 0x0076, 0x0077,
		0x0078, 0x0079, 0x007A, 0x007B, 0x007C, 0x007D, 0x007E, 0x2302,
		0x00C7, 0x00FC, 0x00E9, 0x00E2, 0x00E4, 0x00E0, 0x00E5, 0x00E7,
		0x00EA, 0x00EB, 0x00E8, 0x00EF, 0x00EE, 0x00EC, 0x00C4, 0x00C5,
		0x00C9, 0x00E6, 0x00C6, 0x00F4, 0x00F6, 0x00F2, 0x00FB, 0x00F9,
		0x00FF, 0x00D6, 0x00DC, 0x00A2, 0x00A3, 0x00A5, 0x20A7, 0x0192,
		0x00E1, 0x00ED, 0x00F3, 0x00FA, 0x00F1, 0x00D1, 0x00AA, 0x00BA,
		0x00BF, 0x2310, 0x00AC, 0x00BD, 0x00BC, 0x00A1, 0x00AB, 0x00BB,
		0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
		0x2555, 0x2563, 0x2551, 0x2557, 0x255D, 0x255C, 0x255B, 0x2510,
		0x2514, 0x2534, 0x252C, 0x251C, 0x2500, 0x253C, 0x255E, 0x255F,
		0x255A, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256C, 0x2567,
		0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256B,
		0x256A, 0x2518, 0x250C, 0x2588, 0x2584, 0x258C, 0x2590, 0x2580,
		0x03B1, 0x00DF, 0x0393, 0x03C0, 0x03A3, 0x03C3, 0x00B5, 0x03C4,
		0x03A6, 0x0398, 0x03A9, 0x03B4, 0x221E, 0x03C6, 0x03B5, 0x2229,
		0x2261, 0x00B1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00F7, 0x2248,
		0x00B0, 0x2219, 0x00B7, 0x221A, 0x207F, 0x00B2, 0x25A0, 0x00A0
	};

	if(CharValue < 128)
	{
		GlyphString[GlyphStringIndex++] = CharValue;
	} else
	{
		int Utf8Code = LookUpTable[CharValue];
		if(Utf8Code < 0x0800)
		{
			GlyphString[GlyphStringIndex++] = 0xC0 | Utf8Code >> 6;
			GlyphString[GlyphStringIndex++] = (0x80 | Utf8Code) & 0x3F;
		} else
		{
			GlyphString[GlyphStringIndex++] = 0xE0 | Utf8Code >> 12;
			GlyphString[GlyphStringIndex++] = (0x80 | Utf8Code) >> 6 & 0x3F;
			GlyphString[GlyphStringIndex++] = (0x80 | Utf8Code) & 0x3F;
		}
	}
	GlyphString[GlyphStringIndex] = 0;
	return(GlyphString);
}

void STAsciiForms::HBar(int LineNumber)
{
   GoToXy(XPosition + 1, YPosition + LineNumber + 1);
	SetColor(BorderColour);
	int i;
	for(i = 1; i < Width - 1; i++)
   {
		Map(i, LineNumber + 1, BorderColour, 196);
   }
	fflush(NULL);
}

void STAsciiForms::GoToX(int MoveToXPosition)
{
	printf("%c[99D", 27);
	printf("%c[%dC", 27, MoveToXPosition);
}

void STAsciiForms::GoToXy(int MoveToXPosition, int MoveToYPosition)
{
   // Note that \33[Y;XH for positioning
	// The screen position is goofy. Top left corner is 1x1 not 0x0 like one would expect.
	// Because of the Grid mapping we need to use 0x0 as top left so we need to add 1 to X and Y here.
	printf("%c[%d;%dH",27, MoveToYPosition + 1, MoveToXPosition + 1);
}

void STAsciiForms::SetColor(int ColorNumber)
{
	if(PreviousColor != ColorNumber)
	{
		PreviousColor = ColorNumber;
		if(ColorNumber==cnOff)
		{
			printf("%c[0m",27);
		} else
		{
			printf("%c[01;%dm",27,ColorNumber);
		}
	}
}

void STAsciiForms::Print(int LineNumber, const char *LineText)
{
	STString TempStr = LineText;
	Print(LineNumber, 0, TempStr);
}

void STAsciiForms::Print(int LineNumber, STString LineText)
{
	Print(LineNumber, 0, LineText);
}

void STAsciiForms::Print(int LineNumber, int LinePosition, const char *LineText)
{
	STString TempStr = LineText;
	Print(LineNumber, LinePosition, TempStr);
}

void STAsciiForms::Print(int LineNumber, int LinePosition, STString LineText)
{
	PreviousColor = 65535;
   GoToXy(XPosition + LinePosition + 1, YPosition + LineNumber + 1);
	SetColor(TextColour);
	char ThisChar;
	int i;
	for(i = 0; i < (int)LineText.Len(); i++)
	{
		if(LinePosition + 1 + i < Width - 1)
		{
			ThisChar = LineText[i];
			Map(LinePosition + 1 + i, LineNumber + 1, TextColour, ThisChar);
		}
	}
	i += LinePosition ;
	while(i < Width - 2)
	{
		Map(1 + i, LineNumber + 1, TextColour, ' ');
		i++;
	}

	SetColor(cnOff);
	fflush(NULL);
}

void STAsciiForms::Move(int NewXPosition, int NewYPosition)
{
	ClearBox();
	XPosition = NewXPosition;
	YPosition = NewYPosition;
	Restore();
}

void STAsciiForms::SetGeometry(int NewXPosition, int NewYPosition, int NewWidth, int NewHeight)
{
	if(Grid != NULL)
	{
		delete Grid;
	}
	XPosition = NewXPosition;
	YPosition = NewYPosition;
	Width = NewWidth;
	Height = NewHeight;
	ClearBox();
	NewGrid(Width, Height);
	DrawAsciiBox();
}

void STAsciiForms::Hide()
{
	Hidden = 1;
	ClearBox();
}

void STAsciiForms::ClearBox()
{
	STString BlankLine;
	int Line;
	PreviousColor = 65535;

	BlankLine = "";

	for(Line = 0; Line < Width; Line++)
	{
		BlankLine += " ";
	}
	for(Line = 0; Line < Height; Line++)
	{
		GoToXy(XPosition, YPosition + Line);
		printf("%s", (const char*)BlankLine);
	}
	fflush(NULL);
}
