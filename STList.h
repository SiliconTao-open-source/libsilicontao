/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STList.h
	Object Name:				STList
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Day Zero:					02152000, February 15, 2000
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:       	alloc.h, stdio.h, string.h
   Ports Used:           	None
   Title of program:			Object library
   Description:				Basic linked list
=============================================================================*/
#ifndef STList_included
#define STList_included

#include <STObjectController.h>

#define ByName 1
#define BySize 0

// For NodePointer see CommanSTCore

/**
 * A list for pointers.
 *
 * @short A simple list.
 */
class STList: public STObjectController
{
   public:
      /**
       * Constructor with a given object name.
       */
		STList(const char* NewObjectName);

      /**
       * Constructor with the object name unknown.
       */
      STList();

      /**
       * Destructor
       */
      ~STList();

      /**
       * Tells the number of nodes in the list.
       */
      unsigned int Count();

      /**
       * Appends a new node to the end of the list and returns it's index number.
       */
      unsigned int Add(void *NewItemToAdd);

      /**
       * Adds a new node to the list at a given index position and returns it's index number. If the given index position is beyond the end of the list the item is added to the end.
       */
      unsigned int Insert(unsigned int Position, void *NewItemToAdd);

      /**
       * Deletes a node from the list.
       */
      void Delete(unsigned int CellToKill);

      /**
       * Replace a node in the list and return the list count.
       */
		unsigned int Replace(unsigned int Index, void *NewItemToAdd);

      /**
       * Deletes all nodes from the list.
       */
      void Clear();

      /**
       * Returns a pointer contained in a node of the list.
       */
      void *Item(unsigned int ItemIndex);

      /**
       * Moves a node in the list from Source to Dest
       *
       * Has not been implemented yet.
       */
      void MoveTo(unsigned int SourceIndex, unsigned int DestIndex);

		/**
		 * Returns the index in the list for the item stored at the given memory location.
		 */
		int GetIndex(void *AddressOfData);

   private:
		void CommonConstructor();

      struct NodePointer
      {
         void *Item;
         NodePointer  *PointerToNextCell;
         NodePointer  *PointerToPreviousCell;
      };

      NodePointer *FirstCell, *PreviousCell, *CurrentCell, *NextCell, *LastCell;

		/**
		 * Moves to the givin cell index and returns a non-zero if errors.
		 */
      int MoveToCell(unsigned int CellToGoto);
		unsigned int CurrentIndex;
      unsigned int NodeCount;
		void PrintCellInfo();
};

#endif // STList_included
