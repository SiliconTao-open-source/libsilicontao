/*=============================================================================

    Copyright (C) 2011 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:

=============================================================================*/

#include <STString.h>
#include <STStringList.h>
#include <STIntList.h>
#include <stdio.h>
#include <STHashInts.h>

STHashInts::STHashInts(const char* NewObjectName)
: STObjectController(NewObjectName)
{
	CommonConstructor(NewObjectName);
}

STHashInts::STHashInts()
{
	CommonConstructor("unknown");
}

void STHashInts::CommonConstructor(const char* NewObjectName)
{
	STString Morph;
	Morph = NewObjectName;
	Morph += "::DataValue";
	DataValue = new STIntList((const char*)Morph);
	Morph = NewObjectName;
	Morph += "::CellName";
	CellName = new STStringList((const char*)Morph);	
	SetClassName("STHashInts");
}

STHashInts::~STHashInts()
{
	Clear();
	delete DataValue;
	delete CellName;	
}

void STHashInts::Clear()
{
	while(DataValue->Count())
	{
		Delete((unsigned int)0);
	}
}

void STHashInts::Delete(int IndexOfCell)
{
	Delete((unsigned int)IndexOfCell);
}
	
void STHashInts::Delete(unsigned int IndexOfCell)
{
	if(DataValue->Count() > IndexOfCell)
	{
		CellName->Delete(IndexOfCell);
		DataValue->Delete(IndexOfCell);
	}	
}

void STHashInts::Delete(const char* NameOfCell)
{
	for(unsigned int i = 0; i < CellName->Count(); i++)
	{
		if(CellName->Item(i) == NameOfCell)
		{
			Delete(i);
			break;
		}
	}	
}

void STHashInts::Delete(const STString& NameOfCell)
{
	Delete((const char*)NameOfCell);
}

STIntObj& STHashInts::operator[](const char* NameOfCell)
{
	return(HashPipe(NameOfCell));	
}

STIntObj& STHashInts::operator[](const int IndexOfCell)
{	
	return(ByIndex((unsigned int) IndexOfCell));
}

STIntObj& STHashInts::operator[](const unsigned int IndexOfCell)
{
	return(ByIndex(IndexOfCell));	
}

int STHashInts::IndexOf(const char* NameOfCell)
{
	for(unsigned int IndexCounter = 0; IndexCounter < CellName->Count(); IndexCounter++)
	{
		if(CellName->Item(IndexCounter) == NameOfCell)
		{
			return(IndexCounter);
		}
	}	
	return(-1);
}

STIntObj& STHashInts::ByIndex(unsigned int IndexOfCell)
{
	STIntObj* DataPointer;
	unsigned int IndexCounter;
	bool CellFound;
	STString NameOfCell;
	
	CellFound = false;
	IndexCounter = 0;
	if(IndexOfCell < CellName->Count())
	{
		IndexCounter = IndexOfCell;
		CellFound = true;
	}
	if(CellFound == false)
	{
		DataPointer = new STIntObj;
		(*DataPointer) = 0;
		DataValue->Add((unsigned int)DataPointer);
		NameOfCell.Sprintf("%d",IndexOfCell);
		CellName->Add(NameOfCell);
		IndexCounter = DataValue->Count() - 1;
	}

	DataPointer = (STIntObj *)DataValue->Item(IndexCounter);

	return(*DataPointer);
}

unsigned int STHashInts::Count()
{
	return(CellName->Count());
}

STString STHashInts::Key(unsigned int IndexOfCell)
{
	bool CellFound;
	
	ReturnStr = "";
	CellFound = false;
	if(IndexOfCell < CellName->Count())
	{
		CellFound = true;
		ReturnStr = CellName->Item(IndexOfCell);
	} else
	{
		fprintf(stderr, "WARNING: STHashInts::Key(%u) beyond range of hash\n", IndexOfCell);
	}

	return(ReturnStr);	
}

STIntObj& STHashInts::HashPipe(const char* NameOfCell)
{
	STIntObj* DataPointer;
	unsigned int IndexCounter;
	bool CellFound;
	
	CellFound = false;
	for(IndexCounter = 0; IndexCounter < CellName->Count(); IndexCounter++)
	{
		if(CellName->Item(IndexCounter) == NameOfCell)
		{
			CellFound = true;
			break;
		}
	}
	if(CellFound == false)
	{
		//fprintf(stderr, "STHashInts::HashPipe(%s) new value added\n", NameOfCell);
		DataPointer = new STIntObj;
		(*DataPointer) = 0;
		DataValue->Add((unsigned int)DataPointer);
		CellName->Add(NameOfCell);
		IndexCounter = DataValue->Count() - 1;
	} 

	DataPointer = (STIntObj *)DataValue->Item(IndexCounter);

	return(*DataPointer);
}

STIntObj& STHashInts::operator[](const STString& NameOfCell)
{
	return(HashPipe((const char*)NameOfCell));
}
