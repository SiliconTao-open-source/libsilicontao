/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#include <STAverage.h>
#include <stdio.h>

STAverage::STAverage()      
{
	MinCells = 0; // unlimited
	MaxCells = 0; // unlimited
	MaxValue = 0;
	MinValue = (unsigned int)-1;
	Target = 0;
	Range = 0;
}

STAverage::STAverage(unsigned int SetMaxCells, unsigned int SetMinCells)
{
	MinCells = SetMinCells;
	MaxCells = SetMaxCells;
	MaxValue = 0;
	MinValue = (unsigned int)-1;
	Target = 0;
	Range = 0;
}

unsigned int STAverage::Add(unsigned int NewInt)
{
	if(NewInt > MaxValue)
	{
		MaxValue = NewInt;
	}
	if(NewInt < MinValue)
	{
		MinValue = NewInt;
	}
	if(Target != 0)
	{
		if((NewInt + Range) < Target)
		{
			Clear();
			return(0);
		}
		if((Target + Range) < NewInt)
		{
			Clear();
			return(0);
		}
	}

	STIntList::Add(NewInt);
	if((MaxCells > 0) && (Count() > MaxCells))
	{
		Delete(0);
	}
	return(Average());
}

unsigned int STAverage::Average()
{
	if(Count() == 0) return(0);
	if((MinCells > 0) && (Count() < MinCells)) return(0);
		
	unsigned int Total = 0;
	unsigned int i = 0;
	for(i = 0; i < Count(); i++)
	{
		printf("%d ", Item(i));
		Total += Item(i);
	}
	printf(" = %d / %d = %d\n", Total, i, Total / i);
	return(Total / i);
}

unsigned int STAverage::Max()
{
	return(MaxValue);
}

unsigned int STAverage::Min()
{
	return(MinValue);
}

void STAverage::FlushRange(unsigned int NewTarget, unsigned int NewRange)
{
	Target = NewTarget;
	Range = NewRange;
}
