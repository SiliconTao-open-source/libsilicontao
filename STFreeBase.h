/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STFreeBase.h
   Object Name: 				STFreeBase
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Day Zero:					02152000, February 15, 2000
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:      	STList.h
   Ports Used:           	None
   Title of program:			Object library
   Description:				Bass linked list
=============================================================================*/

#ifndef STFreeBase_included
#define STFreeBase_included

#include <STCommonCore.h>
#include <stdio.h>

struct TDBFileHeader;
struct TFieldStructure;
class STParserOps;
class STList;
class STStringList;
class STStringsEx;

// FieldType
#define NULL_FIELD 	0x00
#define INDEX_FIELD 	0x01
#define CHAR_FIELD 	0x02
#define INT_FIELD		0x03
#define BOOL_FIELD	0x04
#define STRING_FIELD	0x05
#define BLOB_FIELD	0x06

// DBType
#define MEMORY_RESIDENT 	0x00
#define FILE_RESIDENT	 	0x01

// Defined sizes
#define PASSWORD_SIZE		20
#define FUTURE_SIZE			50

/*
Over view
--------------------------------------------------------------------------
| File Header (108 +- bytes) | Field Map (22 bytes * number of fields)   |
--------------------------------------------------------------------------
| The data ( number of fields * number of bytes per field)               |
--------------------------------------------------------------------------


File Header // Details of the type of DB file and pre map it's contents
------------------------------
| WORD FreeBaseVersion       | // The version number of FreeBase used to make this file.
------------------------------
| DWORD GenerationDate       | // The date this file was origanly created.
------------------------------
| DWORD LastTouchedDate      | // The date this file was last accessed for read or write.
------------------------------
| WORD DBType                | // Indicates the format of the database.
------------------------------
| char[20] DBReadSecurity    | // Encrypted password to access reading of database.
------------------------------
| char[20] DBWriteSecurity   | // Encrypted password to access writing of database.
------------------------------
| char[50] FutureUse         | // To be decided.
------------------------------
| WORD NumberOfFields        | // The number of fields in the database, needed to reconstruct the map.
------------------------------
| WORD NumberOfRecords       | // The number of records in the database, used to speed read and writing of data.
------------------------------

Field Map	// Details the layout of each field and combined to detail each row
Note: The first field is called IndexField is if type INDEX_FIELD and size of sizeof(int).
There is only one index field and it is allways there.
------------------------------
| char[20] FieldName         | // The name of this field.
------------------------------
| BYTE FieldType             | // The type of field.
------------------------------
| BYTE FieldSize             | // The size in bytes of this field.
------------------------------

// Defined values

FieldType
	NULL_FIELD		: Unknown, void, undefined field
	INDEX_FIELD		: Index field.
	CHAR_FIELD		: A singel char.
	INT_FIELD		: An integer.
	BOOL_FIELD		: A boolean field, uses file and memory space the same as char field.
	STRING_FIELD	: A string of chars, null is assumed, appended and not counted in size of field.
	BLOB_FIELD		: An array of bytes.

DBType
Bit 0 ( MEMORY_RESIDENT || FILE_RESIDENT )
----------------------
	MEMORY_RESIDENT	: While DB file is active all reading and writing happen to a memory copy of the file.
   							Note: This is best for speed when DB file is small. Loss of data will happen if application
                        crashes before posting or closing of database file.
   FILE_RESIDENT		: While DB file is active all reading and writing happen directly to the hard drive file.
   							Note: Best for speed if database file is large. Uses less memory. Loss of data is at minimum
                        risk during application crashes. Constanly accesses hard ware resources.
----------------------


// In the object

FieldsConstructList contains the information about the Field Map.

IndexColumnList is not the same as the IndexField. It is a linked list of the rows list.
	Each entry in this list ponts to a row list containing TFieldStructure's

FreeBaseVersion is masked like this MMmmbbbb
MM 	Major version number
mm 	minor version number
bbbb  build number

*/

// For TDBFileHeader see CommanSTCore
// For TFieldStructure see CommanSTCore

/**
 * This simple database object is designed to be easy to use, run fast and make very small table files.
 * @li
 * Example of making a new table:
 * <PRE>
 * STFreeBase *FreeBase;
 *
 * FreeBase = new STFreeBase();
 * FreeBase->New();
 * FreeBase->AddField("Name = ftString = 60");
 * FreeBase->AddField("Phone = ftString = 20");
 * FreeBase->AddField("ANumber = ftInt");
 * FreeBase->Create("MyTable.fdb");
 * delete FreeBase;
 * </PRE>
 *
 * @li
 * Example of posting data to a table:
 * <PRE>
 * STFreeBase *FreeBase;
 * int SetNumber;
 *
 * FreeBase = new STFreeBase();
 * FreeBase->Open("MyFirstTable.fdb");
 * FreeBase->Append();
 * FreeBase->SetValue(1,(void *)"Royce Souther");
 * FreeBase->SetValue(2,(void *)"308-3287");
 * SetNumber = 456;
 * FreeBase->SetValue(3,(void *)&SetNumber);
 * FreeBase->Post();
 * FreeBase->Close();
 * delete FreeBase;
 * </PRE>
 * @li
 * Example of reading data from a table:
 * <PRE>
 * STFreeBase *FreeBase;
 * uint ALoopCounter;
 * int PrintNumber;
 * char PrintText[50];
 *
 * FreeBase = new STFreeBase();
 * FreeBase->Open("MyFirstTable.fdb");
 * FreeBase->First();
 * for(ALoopCounter = 0;ALoopCounter < FreeBase->RecordCount();ALoopCounter++)
 * {
 *    FreeBase->GetValue(1,PrintText);
 *    printf("Name = %20s ",PrintText);
 *    FreeBase->GetValue(2,PrintText);
 *    printf("\tPhone = %13s",PrintText);
 *    FreeBase->GetValue(3,&PrintNumber);
 *    printf("\tNumber = %d\n",PrintNumber);
 *    FreeBase->Next();
 * }
 * FreeBase->Close();
 * delete FreeBase;
 * </PRE>
 *
 * @short Simple stand alone database functionality.
 */
class STFreeBase
{
   private:
      STParserOps *ParserOps;
      FILE *FilePointer;
      DWORD StartOfData;
      DWORD SizeOfARecord;
      STList *IndexColumnList;
      DWORD RecNo, LastRecNo;
      BYTE *RecordContainer;
      bool DataChanged;
      bool ReadRecord(DWORD RecordToRead,BYTE *DataContainer);
      bool WriteRecord(DWORD RecordToWrite,BYTE *DataContainer);
      void ClearRecord();
      void ReloadRecord();
      DWORD CalculateOffsetInRow(DWORD FieldIndex);
      void SetIndexField(DWORD NewIndex);
      void WriteHeader();

      TDBFileHeader *DBFileHeader;
      STList *FieldsConstructList;
      STStringList *NewFieldsList;
      char *TableName;
      STStringsEx *StringsEx;
      bool Active;
      bool EndOfFile();
      long FileSize();

   //protected:
      //
   public:
      //const char *Version="0.0.1\0";
      //char *Version="0.0.1\0";

      /**
       * Constructor.
       */
      STFreeBase();

      /**
       * Destructor.
       */
      ~STFreeBase();

      /**
       * After initalizing a new table using @ref New you then add fields by passing text
       * strings to AddField then create the table using @ref Create. Added fields use text
       * that is a string of three parts 1) The name of the field, 2) the type of data in the
       * field, 3) the size of the field if needed. The name of the field must be 20 chars or
       * less. Field data types can be one the these ftString, ftChar, ftInt, ftBool, ftStr, ftBlob.
       *
       */
      void AddField(const char* NewFieldToAdd);

      /**
       * After initalizing a new table using @ref New you then add fields by passing text
       * strings to @ref AddField then create the table using Create.
       *
       */
      void Create(const char* NewTableName);

      /**
       * Use Open to connect to a table that was previously created.
       */
      void Open(const char* OpenTableName);

      /**
       * Use when you are done using a table. This will close the file and free memory. It must be used
       * every time you open a table.
       */
      void Close();

      /**
       * After initalizing a new table using New you then add fields by passing text
       * strings to @ref AddField then create the table using @ref Create.
       *
       */
      void New();

      /**
       * Advance to the next record in the open table.
       */
      void Next();

      /**
       * Reverse back to the previous record in the open table.
       */
      void Previous();

      /**
       * Move to the first record in the table.
       */
      void First();

      /**
       * Move to the last record in the table.
       */
      void Last();

      /**
       * Commit the changes to the table.
       */
      void Post();

      /**
       * Returns the number of records in the table.
       */
      DWORD RecordCount();

      /**
       * Returns the current record number in the table.
       */
      DWORD RecordNumber();

      /**
       * Returns the size in bytes of a data field at the given index.
       */
      DWORD SizeOfCell(DWORD FieldIndex);

      /**
       * Returns the name of the field at a given index.
       */
      char *FieldName(DWORD FieldNumberToName);

      /**
       * Finds the index of a field and returns the number.
       */
      DWORD FieldNumber(const char* FieldNameToNumber);

      /**
       * Returns the type of field.
       */
      BYTE GetFieldType(DWORD FieldIndex);

      /**
       * Fills a continer with the value in the cell at a given in index in the current record.
       */
      bool GetValue(DWORD FieldIndex, void* Container);

      /**
       * Fills a cell with the value in a given continer at a given in index in the current record.
       */
      void SetValue(DWORD FieldIndex, void* Container);

      /**
       * Creats a new empty record at the end of the open table.
       */
      void Append();

      /**
       * Returns the version number of STFreeBase. Does not work yet.
       */
      DWORD  GetVersionInt();

      /**
       * Move to the give record in the open table. If given record does not exist no move happens and
       * the position in the table remains the same.
       */
      bool GoToRecord(DWORD RecordToGoTo);
};

#endif // STFreeBase_included
