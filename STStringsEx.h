/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STStringsEx.h
   Object Name: 				STStringsEx
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Day Zero:					02152000, February 15, 2000
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:       	alloc.h, string.h
   Ports Used:           	None
   Title of program:			Object library
   Description:				Advanced string functionality.
     								Soon to add
                           Hex2Dec
                           Hex2Bin
                           Dex2Hex
                           Dex2Bin
                           Bin2Hex
                           Bin2Dec
                           LastPos
                           ParseBlock
                           ParseLargeBlock
                           WildCardSearch
                           ...


  Note: No e-mails about ANSI C++ string support please. When I needed these
  functions a long time ago there was no such thing as a string type. Thanks.
=============================================================================*/

#ifndef STStringsEx_included
#define STStringsEx_included

using namespace std;

#include <STString.h>

/**
 * Things you need to do to a string.
 *
 * @short Command string mangeling functions.
 */
class STStringsEx
{
   public:
      // Global string for returns
      STString ReturnString;

      /**
       * Constructor
       */
      STStringsEx();

      /**
       * Destructor
       */
      ~STStringsEx();

      /**
       * This function returns the last position of a sub string in a string.
       *
       * I found that STString::find(QChar ,int ,bool) when the QChar is "/" and
       * int is a negative like -1, it errors. I feel this is a bug in STString::find
       * but I decided to fix it this way.
       */
      int LastPos(const STString& SubString, const STString& TheFullString);
      int LastPos(const char* SubString, const STString& TheFullString);
      int LastPos(const char SubString, const STString& TheFullString);

      /**
       * Removes only chars that are the same value as TheChar from the front and end
       * of the given string.
       */
      const char* StripChars(const char* TheString,char TheChar);
      const char* StripChars(const STString& TheString, char TheChar);

      /**
       * Strips any chars that are not alpha-numeric from the front and end
       * of the given string.
       */
      const char* StripClean(const char* TheString);
      const char* StripClean(const STString& TheString);

      /**
       * Converts a string of binary as text to a string of decimal as text.
       */
      const char* Bin2Dec(const STString& SourceString);

      /**
       * Converts a string of decimal as text to a string of hex as text.
       */
      const char* Dec2Hex(const STString& SourceString);

      /**
       * Converts a string of hex as text to a string of decimal as text.
       */
      const char* Hex2Dec(const STString& SourceString);

      /**
       * Converts a string of decimal as text to a string of binary as text.
       */
		const char* Dec2Bin(const STString& SourceString);

      /**
       * In an effort to be forgiving of symbols like % or $ we will look for the first
       * number and convert it to an int. This lets you pass things like "John has $4 to spend" and
       * this function will return 4.
       */
      int Str2Int(const char* CharArray);
      int Str2Int(const STString& StringObject);

      /**
       * Works just like Str2Int only returns a float.
       */
      float Str2Float(const char* CharArray);
      float Str2Float(const STString& StringObject);

      /**
       * Works just like Str2Float only returns a double.
       */
      double Str2Double(const char* CharArray);
      double Str2Double(const STString& StringObject);

      /**
       *
       */
      const char* HexStr2Ascii(const STString& SourceString, bool MaskLow, bool MaskHi, char MaskChar);

      /**
       *
       */
      int InsertCrLf(STString* InsertingToString, int SpaceNear, char CrLfChar);

      /**
       *
       */
      const char* ParseIdentifier(const STString& TheString, int IdentifierIndex);

      /**
       * Modify an STString to convert all alphabetical charactors to upper case.
		 * <PRE>
		 * STString Ustr = OrigString;
		 * StringX.Upper(Ustr);
		 * </PRE>
       */
      void Upper(STString& ModifyString);

      /**
       * Modify an STString to convert all alphabetical charactors to lower case.
		 * <PRE>
		 * STString Lstr = OrigString;
		 * StringX.Lower(Lstr);
		 * </PRE>
       */
      void Lower(STString& ModifyString);

      /**
       * Remove unwanted text from in a string starting at StartPos and for as many as Length chars.
       */
      const char* Remove(const STString& StringObject, unsigned int StartPos, unsigned int Length);

      /**
       * Replace all instances of OldString with NewString. If NewString has zero length then it will remove
       * all instances of OldString.
       */
      const char* Replace(const STString& StringObject, const STString& OldString, const STString& NewString);
      const char* Replace(const STString& StringObject, const char* OldString, const STString& NewString);
      const char* Replace(const STString& StringObject, const char* OldString, const char* NewString);
      const char* Replace(const STString& StringObject, const STString& OldString, const char* NewString);

      /**
       * Insert new text in to a given string starting at StartPos.
       */
      const char* Insert(const STString& StringObject,const char* NewCharArray , int StartPos, int Length);
      const char* Insert(const STString& StringObject,const STString& NewStringObject , int StartPos, int Length);

      /**
       * Returns a sub string that is in the given string starting at position StartPos
       * and for as many as Length chars.
       */
      const char* SubStr(const STString& StringObject, int StartPos, int Length);

      /**
       * Convert a string float value to an integer with a limited number of decimal places.
       */
      int FloatStringToInt(const STString& StringToConvert, unsigned int DecimalPlaces);
};

#endif // STStringsEx_included
