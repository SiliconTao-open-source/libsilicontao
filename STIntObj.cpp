#include <STIntObj.h>

STIntObj::STIntObj()
{
	InternalInt = new int;
	(*InternalInt) = 0;
}

STIntObj::~STIntObj()
{
	delete InternalInt;
}

STIntObj& STIntObj::operator =(const int Int)
{
	(*InternalInt) = Int;
	return(*this);
}

STIntObj& STIntObj::operator =(const STIntObj& IntObj)
{
	(*InternalInt) = (int)IntObj;
	return(*this);
}

STIntObj& STIntObj::operator +=(const int Int)
{
	(*InternalInt) += Int;
	return(*this);
}

STIntObj& STIntObj::operator *=(const int Int)
{
	(*InternalInt) *= Int;
	return(*this);
}

STIntObj& STIntObj::operator /=(const int Int)
{
	(*InternalInt) /= Int;
	return(*this);
}

STIntObj& STIntObj::operator -=(const int Int)
{
	(*InternalInt) -= Int;
	return(*this);
}

STIntObj& STIntObj::operator --(const int Int)
{
	(*InternalInt) --;
	return(*this);
}

STIntObj& STIntObj::operator ++(const int Int)
{
	(*InternalInt) ++;
	return(*this);
}

/*bool operator ==(const STIntObj& IntObj, const int Int)
{
	return ((int)IntObj == Int);
}

bool operator ==(const STIntObj& IntObj, const STIntObj& IntObj2)
{
	return ((int)IntObj == (int)IntObj2);
}*/
