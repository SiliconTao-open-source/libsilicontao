
/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#include <STObjectController.h>
#include <stdio.h>

STObjectController::STObjectController(const char* NewObjectName)
{	
	STString MemStr = NewObjectName;
	if(MemStr.Len() == 0) MemStr = "unknown";
	ObjectName = MemStr.Unwrap();
	MemStr = "unknown";
	ClassName = MemStr.Unwrap();
   DebugLevel = 0;
}

STObjectController::STObjectController()
{	
	STString MemStr = "unknown";
	ObjectName = MemStr.Unwrap();
	MemStr = "unknown";
	ClassName = MemStr.Unwrap();
   DebugLevel = 0;
}

void STObjectController::SetObjectName(const char* NewName)
{
	delete[] ObjectName;
	STString MemStr = NewName;
	ObjectName = MemStr.Unwrap();
}

void STObjectController::SetClassName(const char* NewName)
{
	delete[] ClassName;
	STString MemStr = NewName;
	ClassName = MemStr.Unwrap();
}

STObjectController::~STObjectController()
{
	delete[] ClassName;
	delete[] ObjectName;
}

void STObjectController::DebugMessage(unsigned int ForDebugLevel, const char* TheMessage)
{
	if(DebugLevel >= ForDebugLevel)
	{
		fprintf(stderr, "%s\n", TheMessage);
	}
}
	
unsigned int STObjectController::GetDebugLevel()
{
	return(DebugLevel);	
}

const char* STObjectController::GetVersion()
{
	// #define VER SION "1.5.178"
	// return(VERSION);
	#define MM_VERSION "1.5"
	return(VERSION_DEF);
}

const char* STObjectController::GetClassName()
{
	return(ClassName);
}

const char* STObjectController::GetObjectName()
{
	return(ObjectName);
}
	
void STObjectController::SetDebugLevel(unsigned int NewLevel)
{
	DebugLevel = NewLevel;
}
