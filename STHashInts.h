/*=============================================================================

    Copyright (C) 2011 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#ifndef STHashInts_included
#define STHashInts_included

#include <STIntObj.h>
#include <STString.h>
#include <STObjectController.h>

class STStringList;
class STIntList;

/**
 * Provides a hash list for integers.
 * @li
 * Example of using STHashInts:
 * <PRE>
 * STHashInts Data;
 *	Data["first"] = 1234;
 *	Data["second"] = 5678;
 *	Data["number"] = 9012;
 *	Data["third"] = 3456;
 *
 * </PRE>
 *
 * @short An STStringList combined with an STIntList where cells can be accessed by key name or index number.
 */

class STHashInts: public STObjectController
{
	public:
		/**
		 * To clear the hash.
		 */
		void Clear();

      /**
       * Constructor with a given object name.
       */
      STHashInts(const char* NewObjectName);

		/**
       * Constructor with the object name unknown.
       */
      STHashInts();

      /**
       * Destructor.
       */
		~STHashInts();

      /**
       * Returns the number of entries in the hash.
       */
		unsigned int Count();

      /**
       * Returns the name of the hash key at a given index.
       */
		STString Key(unsigned int IndexOfCell);

      /**
       * Removes an entry from the hash using the index of the cell.
       */
		void Delete(unsigned int IndexOfCell);
		void Delete(int IndexOfCell);

      /**
       * Removes an entry from the hash using the name of the cell.
       */
		void Delete(const char* NameOfCell);

      /**
       * Removes an entry from the hash using the name of the cell.
       */
		void Delete(const STString& NameOfCell);

      /**
       * Finds the index of a cell by the key name. Returns -1 if cell is not in the list.
       */
		int IndexOf(const char* NameOfCell);

		STIntObj& operator[](const char* NameOfCell);
		STIntObj& operator[](const STString& NameOfCell);
		STIntObj& operator[](const unsigned int IndexOfCell);
		STIntObj& operator[](const int IndexOfCell);
		STIntObj& ByIndex(unsigned int IndexOfCell);

	private:
		STString ReturnStr;
		STIntList *DataValue;
		STStringList *CellName;
		STIntObj& HashPipe(const char* NameOfCell);
		void CommonConstructor(const char* NewObjectName);
};

#endif // StHash_included
