/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STDataStream.h
	Object Name:				STDataStream
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   Day Zero:					02102000, February 10, 2000
   Target Platform:			Linux
   Compiler Used:				GNU g++
   Compiler Settings:      N/A
   Linker Settings:        N/A
   Title of program:
   Description:				This object provides two way data transpher between objects.
      							These objects can be in different applications or in the
                           same app. They could even be the same object.

                           This object can be used like so...

                           1 ) By-directional. Two apps communicate through two instances
                           of the object. Each writes to the others object through
                           their own object. This lets the developer use the object by
                           easly just reading and writing to it after setting the other
                           applications name. Use the connect mode UM_CONNECT

                           2 ) Single direction stream. One event writes and another
                           event reads. One object instance. Use mode UM_SINGLE

=============================================================================*/

#ifndef STDataStream_included
#define STDataStream_included

#define UM_CONNECT 0x00
#define UM_SINGLE	 0x01

#include <STCommonCore.h>
#include <STString.h>

class STDataStream;
class STList;
class STDynamicDataLink;

/**
 * Highly experimental. DO NOT USE!!!
 *
 * STDataStream can be used in two ways. One, UM_CONNECT to connect two applications or
 * objects with a hi-speed data link. The second way UM_SINGLE to use it is to send data
 * from one object to another inside the same application.
 *
 * @short Hi-speed inter-object data link.
 */
class STDataStream
{
	public:
      /**
       * Indicates how many if any bytes of data are waiting in your buffer.
       */
      int DataWaitingCount();

      /**
       * Write data to the other objects buffer.
       */
      void WriteData(byte *DataBuffer, int BufferSize);

      /**
       * Read data from your buffer.
       */
      int ReadData(byte *DataBuffer, int MaxRead);

      /**
       * Connect to another application for UM_CONNECT mode.
       */
      void QuickLink(STString &AppName);
      //void QuickLink(STString &AppName, STDataStream **SelfPointer);

      /**
       * Tells you if your UM_CONNECT STDataStream is connected to another application.
       */
      bool IsConnected();

      /**
       * Disconnect for the other application if in UM_CONNECT mode.
       */
      void Disconnect();

      /**
       * Constructor. UseMode can be UM_CONNECT or UM_SINGLE.
       */
      STDataStream(int UseMode);

      /**
       * Destructor
       */
      ~STDataStream();

	private:
      /**
       *
       */
      //QTimer *AutoConnectTimer;

      /**
       *
       */
      void RemoteDisconnect();

      /**
       *
       */
      STDataStream *RemoteConnect(STDataStream *RemoteStream);

      /**
       *
       */
      STDataStream *ExternalStream;

      /**
       *
       */
      void RemoteWriteData(byte *DataBuffer, int BufferSize);

      /**
       *
       */
      int SelectedUseMode;

      /**
       *
       */
      bool ConnectedState;

      /**
       *
       */
      int WriteIndex, ReadIndex;

      /**
       *
       */
      STList *BlockList;

      /**
       *
       */
      STDynamicDataLink *DdLink;

   //private slots:

      /**
       *
       */
      //void AutoConnectRespond();
};

#endif //  STDataStream_included
