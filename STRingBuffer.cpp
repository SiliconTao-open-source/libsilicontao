/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#include <STRingBuffer.h>    
#include <stdlib.h>
#include <stdio.h>

STRingBuffer::STRingBuffer(int BufferSize)
{
	MaximumBufferSize = BufferSize;
	ReadPointer = 0;
	WritePointer = 0;
	// BufferArray = (char *)malloc(BufferSize + 1);
	BufferArray = new char[BufferSize + 1];
	BufferArray[BufferSize] = '\0';  // To let debuggers view the buffer as a string.
}

STRingBuffer::~STRingBuffer()
{
	delete BufferArray;
}

int STRingBuffer::Count()
{
	int TempReturn;
	TempReturn = 0;
	if(ReadPointer > WritePointer)
	{
		TempReturn = MaximumBufferSize - ReadPointer - WritePointer;
	} else
	if(ReadPointer < WritePointer)
	{
		TempReturn = WritePointer - ReadPointer;
	}
	return(TempReturn);
}

int STRingBuffer::Write(byte NewByte)
{
	BufferArray[WritePointer] = NewByte;
	WritePointer++;
	if(WritePointer > MaximumBufferSize)
	{
		WritePointer = 0;
	}
	if(WritePointer == ReadPointer)
	{
		ReadPointer++;
		if(ReadPointer > MaximumBufferSize)
		{
			ReadPointer = 0;
		}
	}
	return(Count());
}
	
int STRingBuffer::BlockWrite(byte *NewByteArray, int BlockSize)
{
	int TempReturn;
	int ALoopCounter;
	
	TempReturn = 0;
	for(ALoopCounter = 0;ALoopCounter < BlockSize;ALoopCounter++)
	{
		TempReturn = Write(NewByteArray[ALoopCounter]);
	}
	return(TempReturn);
}
		
byte STRingBuffer::Read()
{
	byte TempReturn;
	
	TempReturn = 0;
	if(ReadPointer != MaximumBufferSize)
	{
		TempReturn = BufferArray[ReadPointer];
		ReadPointer++;
		if(ReadPointer > MaximumBufferSize)
		{
			ReadPointer = 0;
		}		
	}
	
	return(TempReturn);
}
			
int STRingBuffer::BlockRead(byte *ReadBuffer, int MaximumSize)
{
	int TempReturn;
	
	TempReturn = 0;
	while(Count() && (TempReturn < MaximumSize))
	{
		ReadBuffer[TempReturn] = Read();
		TempReturn++;
	}
	return(TempReturn);	
}
