/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#ifndef STString_included
#define STString_included

#include <stdarg.h>

class STStringList;

/**
 * Provides common string functionality. There are many string objects, all are very good and easy to use so why would I make another.
 * It makes no sence to me to have a bloated object that has thousands of members when a thin faster object would do just fine.
 * I put in this object all of the functions that I find my self using a lot of and let the rest of them work from other objects.
 *
 * @short High-performance string functionality.
 */

class STString
{
   public:
      /**
       *  Constructors
       */
      STString();
      STString(const char);
      STString(const char*);
      STString(const STString&);
		void CommonConstructor();

      /**
       *  Destructor
       */
      ~STString();

      /**
       *  Assignment operators
       */
      STString& operator =(const char);
      STString& operator =(const char*);
      STString& operator =(const STString&);

      /**
       *  Type casting to char array. Returns a pointer to the string as a char array. It is null terminated.
       */
      //* operator char(void) const { if(CharString) {return CharString;}else{return (const char*)&"\0"; } };
		* operator char(void) const { return CharString; };

      /**
       *  Concatination
       */
      STString& operator +=(const char Char);
      STString& operator +=(const char* CharArray);
      STString& operator +=(const STString& StringObject);

      STString& Concatenate(const char*);

      /**
       * Convertions.
       */
		int ToInt(const char*TextInt);
		int ToInt();
		bool ToBool();

      /**
       * Char indexing.
       */
      const char operator [](int IndexPosition);

      /**
       * Returns the length of the string not counting the null terminator at the end.
       */
      unsigned int Len() const;

		/**
       * Returns the length of a char array passed in. Does not effect the contents of the string.
       */
      unsigned int Len(const char* CharArray);

      /**
       * Remove unwanted text at StartPos and for as many as Length chars. Removes from end of string if StartPos is negative, last char begins at -1.
       */
      STString& Remove(int StartPos, unsigned int Length);

      /**
       * Return the left most length of string.
       */
      const char* Left(unsigned int Length);

      /**
       * Return the right most length of string.
       */
      const char* Right(unsigned int Length);

      /**
       * Return the left most length of string starting from position. If position is negative the result will be the right most text from the end.
       */
      const char* Mid(int Position, unsigned int Length);

      /**
       * Remove unwanted white space and extended ASCII chars from the front and end of string.
       */
		STString& Strip();

      /**
       * Insert a sub string into the string at a given location.
       */
		void Insert(const char* NewText, unsigned int NewPosition);

		/**
		 * Brake up a string into parts and populate an STStringList with the parts. Splits on white space on extended ASCII > 127. Returns number of parts. See also @ref STStringList::Dump
		 */
		unsigned int Split(STStringList* TargetList);

		/**
		 * Brake up a string into parts and populate an STStringList with the parts. Splits on FieldDelimiter char. Returns number of parts.
		 */
		unsigned int Split(const char FieldDelimiter, STStringList* TargetList);

		/**
		 * Gives all the features of an STString object to a constant char array without allocating more memory or moving the chars. Calling this function perminantly locks the string object in read-only mode so
		 * read-only like @ref ToInt(), @ref Pos(), @ref Mid, @ref Left, @ref Right and comparison opperators work as expected by the string cannot be changed by assignment, @ref Insert or @ref Delete.
		 * A wrapper string object can be re-used by calling @ref Wrapper again with a new char array. The STString will not attempt to free the char array memory when the STString is distroyed.
		 * Transfer of responsibility for memory management is not implied. The original owner of the memory or the new owner maybe responsible for memory management, Wrapper makes no distinction. That is way the
		 * memory is in readonly mode. Use of Wrapper and the string it returns must be instant. The owner of the memory may free it at any time without informing the Wrapper STString.
		 * <PRE>
		 * STString Convert;
		 * int NewValue = Conver.Wrapper("12345").ToInt() + 5;
		 * </PRE>
		 */
		STString& Wrapper(const char* NewData);

		/**
		 * Hands off control of the allocated memory to another STString object, @ref STStringList or char pointer. Use this when an STString is giving over control of text like when using @ref STString::Adopt.
		 * If handing over control to @ref STString::Wrapper you need to some how regain control of the memory to free it.
		 * This will abandon control of the memory. The receiver will be responsible for freeing the memory.
		 * <PRE>
		 * unsigned int NewCount = TargetSTStringList.Push(SourceSTStringList.Pop(0));
		 * NewCount = TargetSTStringList.Push(SourceSTString.Unwrap());
		 *
		 * char* NewCharArray;
		 * STString BuildString;
		 * BuildString.Sprintf("Important numbers to remember %d, %d %d", 42, 69, 1337);
		 * NewCharArray = BuildString.Unwrap();
		 * printf("%s\n", NewCharArray);
		 * printf("Old string lenght %d == 0\n", BuildString.Len());
		 * delete[] NewCharArray;
		 * </PRE>
		 */
		char* Unwrap();

		/**
		 *	Take over full STString control of a char array from an object like @ref STString::Unwrap or @ref STStringList::Pop. Full responisility for the string memory is assumed and all functions will work.
		 * STString accelloraters are disabled at first.
		 * <PRE>
		 * unsigned int NewCount = TargetSTStringList.Push(SourceSTStringList.Pop(0));
		 * STString TempString;
		 * TempString.Adopt(SourceSTStringList.Pop(0));
		 * TempString += " that was a lot of fun. I got you now string!";
		 * </PRE>
		 */
		STString& Adopt(char* NewData);

      /**
       * Returns the position of the substring starting at StartPosition. If StartPosition is negative then
		 * position is found from end of string. Searches are case sensitive. For case insensitve searches use
		 * @ref STStringsEx::Upper.
		 * <PRE>
		 * printf("Testing position finding.\n");
		 * test2 = "There are no turkeys today.";
		 * printf("'%s' <- set text\n", (const char*)test2);
		 * printf("Pos of 'e' = %d\n", test2.Pos('e'));
		 * int OldPos = test2.Pos('e');
		 * printf("Pos of second 'e' = %d\n", test2.Pos('e', OldPos + 1));
		 * OldPos = test2.Pos('e');
		 * OldPos = test2.Pos('e', OldPos + 1);
		 * printf("Pos of third 'e' = %d\n", test2.Pos('e', OldPos + 1));
		 * printf("Testing reverse position finding.\n");
		 * OldPos = test2.Pos('e', -1);
		 * printf("Pos of last 'e' = %d\n", OldPos);
		 * OldPos -= test2.Len() + 1;
		 * OldPos = test2.Pos('e', OldPos);
		 * printf("Pos of second last 'e' = %d\n", OldPos);
		 * OldPos -= test2.Len() + 1;
		 * OldPos = test2.Pos('e', OldPos);
		 * printf("Pos of third last 'e' = %d\n", OldPos);
		 * </PRE>
       */
      int Pos(const STString& SubString, int StartPosition) const;
      int Pos(const char SubChar) const;
      int Pos(const char* SubCharArray) const;
      int Pos(const STString& SubString) const;
      int Pos(const char SubChar, int StartPosition) const;
      int Pos(const char* SubCharArray, int StartPosition) const;

      /**
       * Create a string from a formatted input array.
       */
      STString& Sprintf(const char *format, ...);

		/**
		 * The core function of the Sprintf pulled out and avilable to create custom functions that accept variable arry input.
		 * <PRE>
		 *	void MyObject::Log(const char* NewLogMessage, ...)
		 *	{
		 *		va_list argp;  // Create an argument list pointer
		 *		va_start(argp, NewLogMessage); // unstack the first argument
		 *		STString LogMsg;
		 *		LogMsg.VaListFormatting(NewLogMessage, &argp); // populate the string
		 *		PageContent->Log((const char*)LogMsg);
		 *		va_end(argp);
		 *	}
		 * </PRE>
		 */
		STString& VaListFormatting(const char *format, va_list* argp);

		/**
		 * Deletes all string content.
		 */
      void EmptyString();

		/**
		 * Enable debug messages to STDERR.
		 */
		void SetStringDebug(bool NewDebugState);

   private:
		bool ConstantLockedData;
		unsigned int GrowExtendedBufferSize;
		unsigned int ExtendedBufferSize;
		unsigned int ExtendedLimitReachedCount;
		unsigned int ExtendedBufferUsed;
		char* CharString;
		char* SubString;
      unsigned int MemorySizeAllocated;
		void KillSubString();
		void MoveBufferBoundary(unsigned int Length);
		bool StringDebug;
};

/**
 *  Other operators. These have to be global because they must return a new object that is created inline.
 */
STString operator +(const STString& StringObject, const char* CharArray);
STString operator +(const char* CharArray, const STString& StringObject);
STString operator +(const STString& StringObject1, const STString& StringObject2);
bool operator ==(const STString& StringObject, const char Char);
bool operator ==(const STString& StringObject, const char* CharArray);
bool operator ==(const STString& StringObject1, const STString& StringObject2);
bool operator !=(const STString& StringObject, const char Char);
bool operator !=(const STString& StringObject, const char* CharArray);
bool operator !=(const STString& StringObject1, const STString& StringObject2);

#endif // STString_included
