/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#include "STGrid.h"
#include <STList.h>
#include <stdio.h>

STGrid::STGrid(int XCells, int YCells, int CellSize)
: STObjectController()
{
	CellByteSize = CellSize;
	Width = XCells;
	Height = YCells;
	GridActive = 0;
	CreateGrid();
}

STGrid::~STGrid()
{
	FreeGrid();
	delete RowArray;
}

void STGrid::FreeGrid()
{
	char *OldRow;
	
	if(GridActive)
	{
		while(RowArray->Count() > 0)
		{
			OldRow = (char*)RowArray->Item(0);
			RowArray->Delete(0);
			delete OldRow;
		}
	}
}

void STGrid::CreateGrid()
{
	char *NewRow;
	
	if(GridActive)
	{
		FreeGrid();
	}
	GridActive = 1;
	RowArray = new STList();
	int RowSize = CellByteSize * Width;
	for(int i = 0; i < Height; i++)
	{
		NewRow = new char[RowSize];
		RowArray->Add(NewRow);
	}
}

void* STGrid::CellPointer(int CellX, int CellY)
{
	void *GetRow;
	int ReturnPointer = 0;
	
	if ((CellX < Width) && (CellY < Height))
	{
		GetRow = RowArray->Item(CellY);
		int CellIndex = CellX * CellByteSize;
		ReturnPointer = (int)GetRow + CellIndex;
	}
	return((void*)ReturnPointer);
}

