/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:

=============================================================================*/

#include <STCommonCore.h>
#include <STDriveOps.h>
#include <STDynamicLibrary.h>
#include <dlfcn.h>
#include <stdio.h>

STDynamicLibrary::STDynamicLibrary(const char* OpenLibraryName)
{
	ControllerLibLoaded = false;
	HandleLibPointer = 0;

   STDriveOps DriveOps;	
	LibraryName = OpenLibraryName;
	if(DriveOps.FileExists(OpenLibraryName))
	{
		LoadTheLibrary();		
	} else
	{
		fprintf(stderr, "Error: No such library '%s'\n", (const char*)LibraryName);
	}
}

STDynamicLibrary::~STDynamicLibrary()
{
	if(ControllerLibLoaded)
	{
		dlclose(HandleLibPointer);
		HandleLibPointer = 0;
		ControllerLibLoaded = false;
	}
}

int STDynamicLibrary::LoadTheLibrary()
{
	int TempError = 0;
	HandleLibPointer = dlopen((const char*)LibraryName, RTLD_LAZY);      
	if(HandleLibPointer)
	{
		ControllerLibLoaded = true;
	}
	return(TempError);
}

int STDynamicLibrary::CallMember(const char* MemberName, ...)
{
   //va_list argp;  // Create an argument list pointer
   // Set the address of the format char array to be the first
   // pointer on the stack
   //va_start(argp,format);
	// Do something with it.
	// Close off the pointer stack
   //va_end(argp);
	return(0);
}

int STDynamicLibraryNullMember()
{
	fprintf(stderr, "Error: No such member in library.\n");
	return(-1);
}

void *STDynamicLibrary::GetMember(const char* MemberName)
{
	int (*FunctionPointer)();
	void *GenericPointer = dlsym(HandleLibPointer, MemberName);
	if(! GenericPointer)
	{
		fprintf(stderr, "Error: No such member '%s' in '%s'\n",MemberName, (const char*)LibraryName);
		FunctionPointer = (int (*)())STDynamicLibraryNullMember;
	} else
	{
		FunctionPointer = (int (*)())GenericPointer;
	}
	return((void *)FunctionPointer);
}
