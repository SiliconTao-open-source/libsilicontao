/*=============================================================================

    Copyright (C) 2007 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#include <STXlog.h>
#include <STSystemAPI.h>
#include <STString.h>
#include <STDriveOps.h>
#include <STStringList.h>
#include <STEthernetDevices.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <stdarg.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

//#define PRODUCTION 1

STXlog::STXlog(bool SetDebugMode)
{
	STSystemAPI SystemAPI;
	int AppPid = SystemAPI.GetAppPid();
	AppName = SystemAPI.GetAppNameFromPid(AppPid);
	DebugMode = SetDebugMode;
	UdpPort = 514;
	UdpSocket = -1;

	LoggingServers = new STStringList();

	// Look for a conf file /etc/${AppName}/conf or /etc/${AppName}.conf or /etc/${AppName}/${AppName}.conf or ./${AppName}.conf
	STDriveOps Do;
	STString LogConfFile;
	STStringList ConfFileContents;
	LogConfFile.Sprintf("/etc/%s/conf", (const char*)AppName);
	if(Do.FileExists(LogConfFile))
	{
		ConfFileContents.LoadFromFile(LogConfFile);
	}
	if(ConfFileContents.Count() == 0)
	{
		LogConfFile.Sprintf("/etc/%s.conf", (const char*)AppName);
		if(Do.FileExists(LogConfFile))
		{
			ConfFileContents.LoadFromFile(LogConfFile);
		}
	}
	if(ConfFileContents.Count() == 0)
	{
		LogConfFile.Sprintf("/etc/%s/%s.conf", (const char*)AppName, (const char*)AppName);
		if(Do.FileExists(LogConfFile))
		{
			ConfFileContents.LoadFromFile(LogConfFile);
		}
	}
	if(ConfFileContents.Count() == 0)
	{
		LogConfFile.Sprintf("./%s.conf", (const char*)AppName);
		if(Do.FileExists(LogConfFile))
		{
			ConfFileContents.LoadFromFile(LogConfFile);
		}
	}

	STString SampleStr;
	while(ConfFileContents.Count() > 0)
	{
		SampleStr = ConfFileContents.Item(0);
		ConfFileContents.Delete(0);
		if(SampleStr.Pos("LOG SERVER =") > -1)
		{
			SampleStr.Remove(0, SampleStr.Pos("=") + 1);
			SampleStr.Strip();
			SampleStr.Split(',', LoggingServers);
		}
	}
	if(LoggingServers->Count() > 0)
	{
		//printf("using LoggingServer '%s'\n", (const char*)LoggingServer);
		UdpSocket = socket(AF_INET, SOCK_DGRAM, 0);
		//printf("UdpSocket = %d\n", UdpSocket);
		fcntl(UdpSocket, F_SETFL, O_NONBLOCK);

		// Set the socket to non-broadcast mode. Is this nessary???
		int Broadcast = 0;
		setsockopt(UdpSocket, SOL_SOCKET, SO_BROADCAST, &Broadcast, sizeof(Broadcast));
	} else
	{
	   openlog((const char*)AppName, LOG_PID, LOG_LOCAL2);
	}
}

STXlog::~STXlog()
{
	if(UdpSocket >= 0)
	{
		close(UdpSocket);
		UdpSocket = -1;
	} else
	{
		closelog();
	}
	delete LoggingServers;
}

void STXlog::log(const char *format, ...)
{
   va_list argp;  // Create an argument list pointer
	va_start(argp, format);
	STString ArgString;
	ArgString.VaListFormatting(format, &argp);
	va_end(argp);

	STEthernetDevices Ed;

	//printf("log UdpSocket = %d\n", UdpSocket);
	if(UdpSocket >= 0)
	{
		STString FullLogStr = AppName;
		FullLogStr += ": ";
		FullLogStr += ArgString;
		for(unsigned int li = 0; li < LoggingServers->Count(); li++)
		{
			STString SampleStr;
			SampleStr = LoggingServers->Item(li);
			SampleStr.Strip();
			struct hostent *he;
			if(Ed.IsOnNetwork(SampleStr))
			{
				if(he = gethostbyname((const char*)SampleStr))
				{
					struct sockaddr_in sockaddress;
					sockaddress.sin_family = AF_INET;
					sockaddress.sin_port = htons(UdpPort);
					sockaddress.sin_addr = *((struct in_addr *)he->h_addr);
					memset(&(sockaddress.sin_zero),'\0',8);
					//printf("sendto socket\n");
					sendto(UdpSocket, (const char*)FullLogStr, FullLogStr.Len(), 0, (struct sockaddr *)&sockaddress, sizeof(struct sockaddr));
				}
			}
		}
	} else
	{
		syslog(LOG_NOTICE, "%s", (const char*)ArgString);
	}

	if(DebugMode)
	{
		if(ArgString[ArgString.Len() - 1] != '\n') ArgString += "\n";
		fprintf(stderr, "%s", (const char*)ArgString);
	}
}

void STXlog::AddServer(const STString& NewServer)
{
	bool New = true;
	STString CleanName;
	CleanName = NewServer;
	CleanName.Strip();
	if(CleanName.Len() == 0) return;
	STEthernetDevices Ed;
	if(Ed.IsOnNetwork(NewServer))
	{
		for(unsigned int li = 0; li < LoggingServers->Count(); li++)
		{
			if(CleanName == LoggingServers->Item(li))
			{
				New = false;
			}
		}
		if(New)
		{
			LoggingServers->Add(NewServer);
		}
	}
}
