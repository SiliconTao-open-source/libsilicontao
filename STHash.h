/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#ifndef STHash_included
#define STHash_included

#include <STString.h>
#include <STObjectController.h>

class STStringList;
class STList;

/**
 * Provides a hash varible like that found in Perl. I made this because I like to use the
 * hash in Perl. I could have use a map template to do this but that started to look very
 * clumsy. This is simple and clean.
 * @li
 * Example of using STHash:
 * <PRE>
 * STHash Data;
 *	Data["first"] = "This is my first line.";
 *	Data["second"] = "I then have a second line.";
 *	Data["number"] = "643";
 *	Data["third"] = "My last line is very nice.";
 *
 * STString KeyName;
 * KeyName.Sprintf("field_%d", 4);
 * Data[KeyName] = "dynamic key name";
 *
 *	printf("Data[\"first\"] = %s\n",(const char*)Data["first"]);
 *	printf("Data[\"third\"] = %s\n",(const char*)Data["third"]);
 *	printf("Data[\"second\"] = %s\n",(const char*)Data["second"]);
 *	printf("Data[\"number\"] = %d\n", Data["number"].ToInt());
 *
 * // This block of code would print out lines that look like the assignement lines above.
 *	STString TestString;
 *	for(i = 0; i < Data.Count(); i++)
 *	{
 *	  TestString = "Data['" + Data.Key(i) + "'] = '" + Data[Data.Key(i)] + "'";
 *	  printf("%s\n", (const char*)TestString);
 *	}
 *
 * STHash* DataPointer = new STHash();
 * (*DataPointer)["test"] = "This works also so you can pass the hash to other objects to populate";
 * delete DataPointer;
 * </PRE>
 *
 * @short Two dimentional STStringList where cells can be accessed by key name or index number.
 */

class STHash: public STObjectController
{
	public:
		/**
		 * To clear the hash.
		 */
		void Clear();

      /**
       * Constructor with a given object name.
       */
      STHash(const char* NewObjectName);

		/**
       * Constructor with the object name unknown.
       */
      STHash();

      /**
       * Destructor.
       */
		~STHash();

      /**
       * Returns the number of entries in the hash.
       */
		unsigned int Count();

      /**
       * Returns the name of the hash key at a given index.
       */
		STString Key(unsigned int IndexOfCell);

      /**
       * Removes an entry from the hash using the index of the cell.
       */
		void Delete(unsigned int IndexOfCell);
		void Delete(int IndexOfCell);

      /**
       * Removes an entry from the hash using the name of the cell.
       */
		void Delete(const char* NameOfCell);

      /**
       * Removes an entry from the hash using the name of the cell.
       */
		void Delete(const STString& NameOfCell);

      /**
       * Finds the index of a cell by the key name. Returns -1 if cell is not in the list.
       */
		int IndexOf(const char* NameOfCell);

		STString& operator[](const char* NameOfCell);
		STString& operator[](const STString& NameOfCell);
		STString& operator[](const unsigned int IndexOfCell);
		STString& operator[](const int IndexOfCell);
		STString& ByIndex(unsigned int IndexOfCell);

	private:
		STString ReturnStr;
		STList *DataValue;
		STStringList *CellName;
		STString& HashPipe(const char* NameOfCell);
		void CommonConstructor(const char* NewObjectName);
};

#endif // StHash_included
