/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:

=============================================================================*/

#include <STString.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define DEFAULT_BUFFER_EXTEND 5

// Used by Split. Safe to recursively include because headers block multiple inclusions.
#include <STStringList.h>

STString::STString()
{
	CommonConstructor();
}

STString::STString(const char Char)
{
	CommonConstructor();
	CharString[0] = Char;
	CharString[1] = '\0';
   MemorySizeAllocated = 1;
}

STString::STString(const char* CharArray)
{
	CommonConstructor();
   Concatenate(CharArray);
}

STString::STString(const STString& StringObject)
{
	CommonConstructor();
   Concatenate((const char*)StringObject);
}

void STString::CommonConstructor()
{
	GrowExtendedBufferSize = DEFAULT_BUFFER_EXTEND;
	ExtendedBufferSize = DEFAULT_BUFFER_EXTEND;
	ExtendedLimitReachedCount = 0;
	ExtendedBufferUsed = 0;
	ConstantLockedData = false;
   CharString = new char[1 + ExtendedBufferSize];
	//printf("new char[] @ 0x%08X\n", CharString);
	CharString[0] = '\0';
   MemorySizeAllocated = 0;
	SubString = 0;
	StringDebug = false;
}

STString::~STString()
{
	if(ConstantLockedData == false)
	{
   	delete[] CharString;
	}
	if(SubString != 0)
	{
		delete[] SubString;
	}
}

void STString::EmptyString()
{
   if((MemorySizeAllocated != 0) && (ConstantLockedData == false))
   {
		CharString[0] = '\0';
		MoveBufferBoundary(MemorySizeAllocated);
   }
}

STString& STString::Sprintf(const char *format, ...)
{
	if(ConstantLockedData == false)
	{
		va_list argp;  // Create an argument list pointer
		va_start(argp, format);
		VaListFormatting(format, &argp);
		va_end(argp);
	} else
	if(StringDebug)
	{
		fprintf(stderr, "STString::Sprintf ConstantLockedData\n");
	}
	return(*this);
}

STString& STString::VaListFormatting(const char *format, va_list* argp)
{
   // Not knowing the number of arguments or the length they will be after being converted we
   // will try to make enough room for them. We will stop if 100k bytes are not enough.
   if(ConstantLockedData == false)
	{
		int ErrorCode;
		int NewBufferSize;
		char* TempStringBuffer;

		NewBufferSize = 1024;
		EmptyString();
		Concatenate((const char*)format);
		int CountMarkers = 0;
		int LastMarkerPos = Pos('%');
		if(StringDebug)
		{
			fprintf(stderr, "STString::VaListFormatting LastMarkerPos = %d\n", LastMarkerPos);
		}
		while(LastMarkerPos > -1)
		{
			if((CharString[LastMarkerPos] == '%') && (CharString[LastMarkerPos + 1] != '%'))
			{
				CountMarkers++;
				LastMarkerPos = Pos('%', LastMarkerPos + 1);
			} else
			{
				LastMarkerPos = Pos('%', LastMarkerPos + 2);
			}
		}
		if(StringDebug)
		{
			fprintf(stderr, "STString::VaListFormatting CountMarkers = %d\n", CountMarkers);
		}

		if(CountMarkers == 0)
		{
			// No markers given, just print raw text.
			if(StringDebug)
			{
				fprintf(stderr, "STString::VaListFormatting no format markers given.\n");
			}
			return(*this);
		}
		EmptyString();
		TempStringBuffer = new char[NewBufferSize];

		bool Done = false;
		while(! Done)
		{
			ErrorCode = vsnprintf(TempStringBuffer, NewBufferSize, format, *argp);
			if((ErrorCode > -1) && (ErrorCode < NewBufferSize))
			{
				Done = true;
				Concatenate((const char*)TempStringBuffer);
			} else
			{
				if(StringDebug)
				{
					fprintf(stderr, "STString::Sprintf vsnprintf ErrorCode = %d\n", ErrorCode);
					fprintf(stderr, "Error msg = '%s'\n", strerror(errno));
				}
				// Concatenate("Format string exceeded safe memory limit in STString::VaListFormatting");
				NewBufferSize += 1024;
				delete[] TempStringBuffer;
				TempStringBuffer = new char[NewBufferSize];
			}
		}
		delete[] TempStringBuffer;
	}
	return(*this);
}

int STString::ToInt(const char*TextInt)
{
	STString TempCalc = TextInt;
	return(TempCalc.ToInt());
}

int STString::ToInt()
{
   bool Negative;
   bool NumberStart;
   int NumberIndex;
   int TempReturn;
   int StringLength;

   StringLength = MemorySizeAllocated + ExtendedBufferUsed;
   TempReturn = 0;
   NumberStart = false;
   Negative = false;
   NumberIndex = 0;
	if(StringLength > 0)
	{
		while(NumberStart == false)
		{
			if (NumberIndex >= StringLength)
			{
				break;
			}
			if ((CharString[NumberIndex] >= '0')&&(CharString[NumberIndex] <= '9'))
			{
				NumberStart = true;
				break;
			}

			// If the number has a - in front it is considered to be a negative number.
			// Only a space can be between the - and the number.
			if (CharString[NumberIndex] == '-')
			{
				Negative = true;
			} else
			if (CharString[NumberIndex] != ' ')
			{
				Negative = false;
			}

			NumberIndex++;
		}

		if (NumberStart == true)
		{
			while ((CharString[NumberIndex] >= '0')&&(CharString[NumberIndex] <= '9'))
			{
				TempReturn *= 10;
				TempReturn += CharString[NumberIndex] - '0';
				NumberIndex++;
			}
			if (Negative == true)
			{
				TempReturn *= -1;
			}
		}
	}
   return(TempReturn);
}

bool STString::ToBool()
{
   bool TempReturn;

	TempReturn = false;
	// Typical tests: true, TRUE, 1
   if(MemorySizeAllocated > 0)
	{
		if((CharString[0] == 't')||(CharString[0] == 'T')||(CharString[0] == '1'))
		{
			TempReturn = true;
		}

		// Other tests: yes, YES
		if((CharString[0] == 'y')||(CharString[0] == 'Y'))
		{
			TempReturn = true;
		}
	}
   return(TempReturn);
}

STString& STString::Concatenate(const char* NewText)
{
	if(ConstantLockedData == false)
	{
		unsigned int i, StringLength;

		if(NewText != NULL)
		{
			//printf("STString::Concatenate(%s)\n", NewText);
			StringLength = strlen(NewText);

			if(StringLength > 0)
			{
				if(StringLength + ExtendedBufferUsed > ExtendedBufferSize)
				{
					char* PreviousLocation = 0;
					unsigned int PreviousMemSize = 0;
					PreviousMemSize = MemorySizeAllocated + ExtendedBufferUsed;
					PreviousLocation = CharString;
					if(MemorySizeAllocated > 0)
					{
						ExtendedBufferSize += StringLength;
						if(ExtendedBufferSize < GrowExtendedBufferSize)
						{
							ExtendedBufferSize += GrowExtendedBufferSize;
						} else
						{
							GrowExtendedBufferSize = ExtendedBufferSize;
						}
					}
					// CharString = (char *)malloc(PreviousMemSize + StringLength + 1);
					//printf("CharString = new char[PreviousMemSize %d + StringLength %d + 1 + ExtendedBufferSize %d = %d ]; <<<<<<<<<<<<<<<<<<<< ----------------------------------------\n", PreviousMemSize, StringLength, ExtendedBufferSize, PreviousMemSize + StringLength + ExtendedBufferSize);
					CharString = new char[PreviousMemSize + StringLength + 1 + ExtendedBufferSize];
					//printf("new char[] @ 0x%08X\n", CharString);
					if ((unsigned int)CharString < 100)
					{
						printf("ERROR: STString could not allocate memory! (Requested mem size = %d, error = %d)\n", PreviousMemSize + StringLength + 1 + ExtendedBufferSize,  (unsigned int)CharString);
					} else
					{
						MemorySizeAllocated = PreviousMemSize + StringLength;
						for (i = 0; i < PreviousMemSize + ExtendedBufferUsed; i++)
						{
							CharString[i] = PreviousLocation[i];
						}
						//printf("delete[] PreviousLocation @ 0x%08X\n", PreviousLocation);
						delete[] PreviousLocation;
						for (i = 0; i < StringLength; i++)
						{
							CharString[i + PreviousMemSize] = NewText[i];
						}
						CharString[i + PreviousMemSize + ExtendedBufferUsed] = 0;
						ExtendedBufferUsed = 0;
					}
				} else
				{
					for (i = 0; i < StringLength; i++)
					{
						CharString[i + MemorySizeAllocated + ExtendedBufferUsed] = NewText[i];
					}
					CharString[i + MemorySizeAllocated + ExtendedBufferUsed] = 0;
					MemorySizeAllocated += StringLength;
					ExtendedBufferSize -= StringLength;
					ExtendedBufferUsed = 0;
					// ExtendedBufferUsed += StringLength;
				}
			}
		}
		//printf("MemorySizeAllocated %d\tExtendedBufferUsed %d\tExtendedBufferSize %d total allocated memory %d\t%s\n\n", MemorySizeAllocated, ExtendedBufferUsed, ExtendedBufferSize, MemorySizeAllocated + ExtendedBufferSize, CharString);
	}
   return(*this);
}

const char STString::operator [](int IndexPosition)
{
   char TempReturn;

	//printf("const char STString::operator [](%d)\n", IndexPosition);
   TempReturn = '\0';
   if ((IndexPosition > -1) && ((unsigned int)IndexPosition < MemorySizeAllocated + ExtendedBufferUsed))
   {
      TempReturn = CharString[IndexPosition];
   }
   return(TempReturn);
}

STString& STString::operator =(const char Char)
{
	if(ConstantLockedData == false)
	{
		char TwoChars[2];

		TwoChars[0] = Char;
		TwoChars[1] = '\0';
		EmptyString();
		return (Concatenate(TwoChars));
	}
	return(*this);
}

STString& STString::Remove(int StartPos, unsigned int Length)
{
	if(ConstantLockedData == false)
	{
		unsigned int i;
		if(StartPos < 0)
		{
			int EndStartPos = MemorySizeAllocated + ExtendedBufferUsed + StartPos;
			if(EndStartPos > -1)
			{
				StartPos = EndStartPos;
			}
		}
		if(StartPos + Length > MemorySizeAllocated + ExtendedBufferUsed)
		{
			//printf("StartPos %d + Length %d > MemorySizeAllocated %d + ExtendedBufferUsed %d\n", StartPos, Length, MemorySizeAllocated, ExtendedBufferUsed);
			Length = MemorySizeAllocated + ExtendedBufferUsed - StartPos;
			//printf("StartPos %d + Length %d == MemorySizeAllocated %d + ExtendedBufferUsed %d\n", StartPos, Length, MemorySizeAllocated, ExtendedBufferUsed);
		}
		if(StartPos >= 0) // Remove string using a forward counting offset.
		{
			if((unsigned int)StartPos < MemorySizeAllocated + ExtendedBufferUsed) // Within range of string
			{
				if(Length + StartPos > MemorySizeAllocated + ExtendedBufferUsed)
				{
					CharString[StartPos] = 0;
					ExtendedBufferSize = ExtendedBufferSize + (MemorySizeAllocated - StartPos);
					MemorySizeAllocated = StartPos;
					ExtendedBufferUsed = 0;
				} else
				{
					//for(i = StartPos; i < MemorySizeAllocated + ExtendedBufferUsed; i++)
					for(i = StartPos; i + Length < MemorySizeAllocated + ExtendedBufferUsed; i++)
					{
						//printf("CharString[%d] = CharString[%d];\n", i, i + Length);
						CharString[i] =  CharString[i + Length];
					}
					MoveBufferBoundary(Length);

					CharString[MemorySizeAllocated + ExtendedBufferUsed] = 0;
				}
			} // else StartPos is off the end of the string, nothing to remove.
		}
		//printf("MemorySizeAllocated %d\tExtendedBufferUsed %d\tExtendedBufferSize %d total allocated memory %d\t'%s'\n", MemorySizeAllocated, ExtendedBufferUsed, ExtendedBufferSize, MemorySizeAllocated + ExtendedBufferSize, CharString);
	}
	return(*this);
}

// We when remove text from the string we do not want to free the memory and re-allocate incase the string grows again.
// So we reduce the ExtendedBufferUsed and MemorySizeAllocated and increase the ExtendedBufferSize if we move the boundary.
// Effectively we keep the same space but as we reduce the MemorySizeAllocated we also increase the ExtendedBufferSize to balance it out.
void STString::MoveBufferBoundary(unsigned int Length)
{
	if(ConstantLockedData == false)
	{
		if(ExtendedBufferUsed > 0)
		{
			if(Length >= ExtendedBufferUsed)
			{
				Length -= ExtendedBufferUsed;
				ExtendedBufferUsed = 0;
			} else
			{
				ExtendedBufferUsed -= Length;
				Length = 0;
			}
		}
		if(Length > 0)
		{
			MemorySizeAllocated -= Length;
			ExtendedBufferSize += Length;
		}
	}
}

void STString::KillSubString()
{
	// The SubString is a temporary holder for return values. We don't care what happens to it. It is not the important data.
	if(SubString != 0)
	{
		delete[] SubString;
		SubString = 0;
	}
}

const char* STString::Left(unsigned int Length)
{
	unsigned int i;
	KillSubString();
	//printf("STString& STString::Left(%d)\n", Length);
	unsigned int SizeOfString = MemorySizeAllocated + ExtendedBufferUsed;
	if(Length < 0) Length = 0;
	if(Length > SizeOfString) Length = SizeOfString;
	if(Length < SizeOfString)
	{
		//printf("Length %d < SizeOfString %d\n", Length, SizeOfString);
		//printf("CharString = '%s'\n", CharString);
		if(SubString != 0)
		{
			//printf("delete[] SubString; \n");
			delete[] SubString;
		}
		SubString = new char[Length + 1];
		i = 0;
		for(i = 0; i < Length; i++)
		{
			//printf("%c", CharString[i]);
			//fflush(NULL);
			SubString[i] = CharString[i];
		}
		SubString[i] = '\0';
		return(SubString);
	}
	// If the sub string you requested is the same size as what is in the main string, just return the main string.
	return(CharString);
}

const char* STString::Right(unsigned int Length)
{
	unsigned int i;
	KillSubString();
	//printf("STString& STString::Right(%d)\n", Length);
	unsigned int SizeOfString = MemorySizeAllocated + ExtendedBufferUsed;

	if(Length < 0) Length = 0;
	if(Length > SizeOfString) Length = SizeOfString;
	if(Length < SizeOfString)
	{
		int Position = SizeOfString - Length;
		if(SubString != 0) delete[] SubString;
		SubString = new char[Length + 1];
		i = 0;
		for(i = 0; i < Length; i++)
		{
			SubString[i] = CharString[Position + i];
		}
		SubString[i] = '\0';
		return(SubString);
	}
	// If the sub string you requested is the same size as what is in the main string, just return the main string.
	return(CharString);
}

const char* STString::Mid(int Position, unsigned int Length)
{
	unsigned int i;
	KillSubString();
	//printf("STString& STString::Mid(%d, %d)\n", Position, Length);
	unsigned int SizeOfString = MemorySizeAllocated;
	if(Length < 0) Length = 0;
	if(Position < 0) Position = 0;
	if((Position > 0) && ((unsigned int)Position >= SizeOfString))
	{
		Position = SizeOfString;
		Length = 0;
	}
	if(Position == 0)
	{
		return(Left(Length));
	}
	if((Position > 0) && ((unsigned int)Position >= SizeOfString))
	{
		if(SubString != 0) delete[] SubString;
		SubString = new char[1];
		SubString[0] = '\0';
		return(SubString);
	}
	if(Length > SizeOfString - Position) Length = SizeOfString - Position;
	if(Length <= SizeOfString - Position)
	{
		if(SubString != 0) delete[] SubString;
		SubString = new char[Length + 1];
		i = 0;
		for(i = 0; i < Length; i++)
		{
			SubString[i] = CharString[i + Position];
		}
		SubString[i] = '\0';
		return(SubString);
	}
	return(CharString);
}

STString& STString::Strip()
{
	if(ConstantLockedData == false)
	{
		unsigned int StartOfKeepData = MemorySizeAllocated + ExtendedBufferUsed + 1;
		unsigned int EndOfKeepData = 0;
		unsigned int i;
		char NextCharRead;

		for(i = 0; i < MemorySizeAllocated + ExtendedBufferUsed; i++)
		{
			NextCharRead = CharString[i];
			if((NextCharRead > 32) && (NextCharRead < 127))
			{
				StartOfKeepData = i;
				break;
			}
		}
		if(StartOfKeepData < MemorySizeAllocated + ExtendedBufferUsed)
		{
			if(StartOfKeepData > 0)
			{
				for(i = 0; i < MemorySizeAllocated + ExtendedBufferUsed - StartOfKeepData; i++)
				{
					CharString[i] = CharString[i + StartOfKeepData];
				}
				MoveBufferBoundary(StartOfKeepData);
			}
			EndOfKeepData = MemorySizeAllocated + ExtendedBufferUsed + 1;
			for(i = MemorySizeAllocated + ExtendedBufferUsed; i > 0; i--)
			{
				NextCharRead = CharString[i];
				if((NextCharRead > 32) && (NextCharRead < 127))
				{
					break;
				} else
				{
					EndOfKeepData = i;
				}
			}
			if(EndOfKeepData < MemorySizeAllocated + ExtendedBufferUsed)
			{
				MoveBufferBoundary(MemorySizeAllocated + ExtendedBufferUsed - EndOfKeepData);
			}
			CharString[MemorySizeAllocated + ExtendedBufferUsed] = '\0';
		} else
		{
			StartOfKeepData = MemorySizeAllocated + 1;
			CharString[0] = '\0';
			ExtendedBufferSize += MemorySizeAllocated;
			MemorySizeAllocated = 0;
			ExtendedBufferUsed = 0;
		}
		//printf("MemorySizeAllocated %d\tExtendedBufferUsed %d\tExtendedBufferSize %d total allocated memory %d\t'%s'\n", MemorySizeAllocated, ExtendedBufferUsed, ExtendedBufferSize, MemorySizeAllocated + ExtendedBufferSize, CharString);
	}
	return(*this);
}

STString& STString::operator =(const char* CharArray)
{
	if(ConstantLockedData == false)
	{
	   EmptyString();
   	return (Concatenate((const char*)CharArray));
	}
	return(*this);
}

STString& STString::operator =(const STString& StringObject)
{
	if(ConstantLockedData == false)
	{
	   EmptyString();
   	return (Concatenate((const char*)StringObject));
	}
	return(*this);
}

STString& STString::operator +=(const char Char)
{
	if(ConstantLockedData == false)
	{
		char TwoChars[2];

		TwoChars[0] = Char;
		TwoChars[1] = '\0';
		return (Concatenate(TwoChars));
	}
	return(*this);
}

STString& STString::operator +=(const char* CharArray)
{
   return (Concatenate(CharArray));
}

STString& STString::Wrapper(const char* NewData)
{
	if(ConstantLockedData == false)
	{
		delete[] CharString;
	}
	ConstantLockedData = true;
	MemorySizeAllocated = strlen(NewData);
	CharString = (char*)NewData;
	ExtendedBufferSize = 0;
	ExtendedBufferUsed = 0;
	return(*this);
}

STString& STString::Adopt(char* NewData)
{
	if(ConstantLockedData == false)
	{
		delete[] CharString;
	}
	ConstantLockedData = false;
	MemorySizeAllocated = strlen(NewData);
	CharString = (char*)NewData;
	ExtendedBufferSize = 0;
	ExtendedBufferUsed = 0;
	return(*this);
}

char* STString::Unwrap()
{
	char* AllocatedMemory = CharString;

	GrowExtendedBufferSize = DEFAULT_BUFFER_EXTEND;
	ExtendedBufferSize = DEFAULT_BUFFER_EXTEND;
	ExtendedLimitReachedCount = 0;
	ExtendedBufferUsed = 0;
	ConstantLockedData = false;
   CharString = new char[1 + ExtendedBufferSize];
	CharString[0] = '\0';
   MemorySizeAllocated = 0;
	return(AllocatedMemory);
}

STString& STString::operator +=(const STString& StringObject)
{
  	return (Concatenate((char*)StringObject));
}

unsigned int STString::Len() const
{
   return(MemorySizeAllocated + ExtendedBufferUsed);
}

unsigned int STString::Len(const char* CharArray)
{
   STString Temp = CharArray;
   return(Temp.Len());
}

unsigned int STString::Split(STStringList* TargetList)
{
	STString NewSubStr;
	TargetList->Clear();
	for(unsigned int i = 0; i < MemorySizeAllocated; i++)
	{
		if((CharString[i] > 32) && ((unsigned char)CharString[i] < 127))
		{
			NewSubStr += CharString[i];
		} else
		{
			if(NewSubStr.Len() > 0) TargetList->Add(NewSubStr);
			NewSubStr = "";
		}
	}
	if(NewSubStr.Len() > 0) TargetList->Add(NewSubStr);
	return(TargetList->Count());
}


unsigned int STString::Split(const char FieldDelimiter, STStringList* TargetList)
{
	STString NewSubStr;
	TargetList->Clear();
	for(unsigned int i = 0; i < MemorySizeAllocated; i++)
	{
		if(CharString[i] != FieldDelimiter)
		{
			NewSubStr += CharString[i];
		} else
		{
			if(NewSubStr.Len() > 0) TargetList->Add(NewSubStr);
			NewSubStr = "";
		}
	}
	if(NewSubStr.Len() > 0) TargetList->Add(NewSubStr);
	return(TargetList->Count());
}

int STString::Pos(const char SubChar) const
{
   char TwoChars[2];

   TwoChars[0] = SubChar;
   TwoChars[1] = '\0';
   return(Pos(TwoChars, 0));
}

int STString::Pos(const char* SubCharArray) const
{
   return(Pos(SubCharArray, 0));
}

int STString::Pos(const STString& SubString) const
{
   return(Pos((const char*)SubString, 0));
}

void STString::Insert(const char* NewText, unsigned int NewPosition)
{
	if(ConstantLockedData == false)
	{
		unsigned int i;
		unsigned int NewLength = strlen(NewText);
		unsigned int SourceLength = MemorySizeAllocated + ExtendedBufferUsed;
		if(NewPosition > SourceLength)
		{
			NewPosition = SourceLength;
		}
		if(NewLength > 0)
		{
			char* SourceString;
			char* TargetString;

			MemorySizeAllocated += NewLength;
			if(NewLength < ExtendedBufferSize)
			{
				SourceString = CharString;
				TargetString = CharString;
				ExtendedBufferSize -= NewLength;
			} else
			{
				// allocate enough space for old string + new string + ExtendedBufferSize
				// re-write string.
				if(ExtendedBufferSize < GrowExtendedBufferSize)
				{
					ExtendedBufferSize += GrowExtendedBufferSize;
				}
				SourceString = CharString;
				TargetString = new char[MemorySizeAllocated + 1 + ExtendedBufferSize];
				CharString = TargetString;

				// Copy over front of string from original.
				for(i = 0; i < NewPosition; i++)
				{
					TargetString[i] = SourceString[i];
				}
			}
			// Move ending data
			for(i = SourceLength; i > NewPosition; i--)
			{
				//printf("Move %c (0x%02X) from SourceString[%d] to TargetString[%d]\n", SourceString[i], SourceString[i], i, i + NewLength);
				TargetString[i + NewLength - 1] = SourceString[i - 1];
			}
			//printf("TargetString = '%s'\n", TargetString);
			// Move new data
			for(i = 0; i < NewLength; i++)
			{
				//printf("Move %c from NewText[%d] to TargetString[%d]\n", NewText[i], i, i + NewLength + 1);
				TargetString[i + NewPosition] = NewText[i];
			}
			TargetString[MemorySizeAllocated] = 0;
			ExtendedBufferUsed = 0;
			if(SourceString != TargetString)
			{
				delete[] SourceString;
			}
		}
	}
}

int STString::Pos(const char SubChar, const int StartPosition) const
{
   char TwoChars[2];

   TwoChars[0] = SubChar;
   TwoChars[1] = '\0';
   return(Pos(TwoChars, StartPosition));
}

int STString::Pos(const char* SubCharArray, const int StartPosition) const
{
   int StartPos = StartPosition;
   int FoundPosition, LastPosition, Direction, ALoopCounter, TempReturn, TargetCount, MatchCounter, MatchCounterReset;
   bool MatchFound;
   unsigned int SubStringLength;

   SubStringLength = strlen(SubCharArray);
	if(SubStringLength > MemorySizeAllocated + ExtendedBufferUsed) return(-1);

	// Quick Stepping: in the event the leading characters are repeated a lot as in the case of boundary markers, this can make searching faster.
	// This is a quick guess to scan forward so false positives are possible but will not cause a match to be missed.
	if((StartPos > 0) && (SubCharArray[0] == SubCharArray[1]))
	{
		unsigned int QuickStepIndex = 0;
		unsigned int QuickStepJumpSize = 0;
		unsigned int QsCounter = 0;
		for(QsCounter = 0; QsCounter <= SubStringLength; QsCounter++)
		{
			if(SubCharArray[0] == SubCharArray[QsCounter])
			{
				QuickStepJumpSize++;
			} else
			{
				break;
			}
		}
		// QuickStepping only helps when used on longer blocks of repeating chars.
		if(QuickStepJumpSize > 5)
		{
			bool DoneLooking4Block = false; // True if spotted or if no block found.
			while(DoneLooking4Block == false)
			{
				if((CharString[QuickStepIndex + StartPos + QuickStepJumpSize - 2] == SubCharArray[0]) && (CharString[QuickStepIndex + StartPos + QuickStepJumpSize - 1] == SubCharArray[0]))
				{
					// Possible match block coming up, run upto start of block.
					StartPos += QuickStepIndex;
					QuickStepIndex = 0;
					while(((unsigned int)StartPos < MemorySizeAllocated) && (CharString[StartPos] != SubCharArray[0]))
					{
						StartPos++;
					}
					bool FullBlockMatch = true;
					unsigned int FullBlockCount;
					for(FullBlockCount = 0; FullBlockCount < QuickStepJumpSize; FullBlockCount++)
					{
						if((CharString[StartPos + FullBlockCount] != SubCharArray[0]))
						{
							FullBlockMatch = false;
							// Step past the junk and the false positive.
							StartPos += FullBlockCount;
							break;
						}
					}
					if(FullBlockMatch == true)
					{
						DoneLooking4Block = true;
					}
				} else
				{
					QuickStepIndex += QuickStepJumpSize;
				}
				if((StartPos + QuickStepJumpSize) >= MemorySizeAllocated)
				{
					DoneLooking4Block = true;
					StartPos -= QuickStepJumpSize;
				}
			}
		}
	}

   TargetCount = SubStringLength;
   Direction = 1;
   MatchCounter = 0;
   LastPosition = MemorySizeAllocated + ExtendedBufferUsed;
   if (StartPos < 0)
   {
      MatchCounter = SubStringLength - 1;
      StartPos *= -1;
      StartPos--;
      StartPos = (MemorySizeAllocated + ExtendedBufferUsed) - StartPos;
      StartPos--;
      Direction = -1;
      LastPosition = -1;
      TargetCount = -1;
   }
   MatchCounterReset = MatchCounter;
   MatchFound = false;
   TempReturn = -1;
   if ((SubStringLength > 0) && (MemorySizeAllocated > 0) && ((unsigned int)StartPos < MemorySizeAllocated + ExtendedBufferUsed))
   {
      for (ALoopCounter = StartPos; ALoopCounter != LastPosition; ALoopCounter += Direction)
      {
         if (CharString[ALoopCounter] != SubCharArray[MatchCounter])
         {
            MatchCounter = MatchCounterReset;
            MatchFound = false;
				ALoopCounter = StartPos;
				StartPos += Direction;
         } else
         {
            MatchCounter += Direction;
            if ((MatchFound == false)||(Direction == -1))
            {
               MatchFound = true;
               FoundPosition = ALoopCounter;
            }
            if(MatchCounter == TargetCount)
            {
               TempReturn = FoundPosition;
               break;
            }
         }
      }
   }
	return(TempReturn);
}

void STString::SetStringDebug(bool NewDebugState)
{
	StringDebug = NewDebugState;
}

int STString::Pos(const STString& SubString, const int StartPosition) const
{
   return(Pos((const char*)SubString, StartPosition));
}

STString operator +(const STString& StringObject, const char* CharArray)
{
   STString TempObject(StringObject);
   return (TempObject.Concatenate(CharArray));
}

STString operator +(const char* CharArray, const STString& StringObject)
{
  	STString TempObject(CharArray);
  	return (TempObject.Concatenate(StringObject));
}

STString operator +(const STString& StringObject1, const STString& StringObject2)
{
   STString TempObject(StringObject1);
  	return (TempObject.Concatenate(StringObject2));
}

bool operator ==(const STString& StringObject, const char Char)
{
	return (strcmp((const char*)StringObject, (const char*)&Char) == 0);
}

bool operator ==(const STString& StringObject, const char* CharArray)
{
	return (strcmp((const char*)StringObject, CharArray) == 0);
}

bool operator ==(const STString& StringObject1, const STString& StringObject2)
{
	return (strcmp((const char*)StringObject1, (const char*)StringObject2) == 0);
}

bool operator !=(const STString& StringObject, const char Char)
{
	return (strcmp((const char*)StringObject, (const char*)&Char) != 0);
}

bool operator !=(const STString& StringObject, const char* CharArray)
{
	return (strcmp((const char*)StringObject, CharArray) != 0);
}

bool operator !=(const STString& StringObject1, const STString& StringObject2)
{
	return (strcmp((const char*)StringObject1, (const char*)StringObject2) != 0);
}
