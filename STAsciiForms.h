/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#ifndef STAsciiForms_included
#define STAsciiForms_included

#include <STString.h>

#define cnOff     00
#define cnGrey    30
#define cnRed   	31
#define cnGreen 	32
#define cnYellow  33
#define cnBlue    34
#define cnPurple  35
#define cnTeal    36
#define cnWhite 	37

#ifdef __ARM
wha tthe heck
#endif
class STGrid;

#include <STObjectController.h>
#include <STString.h>

/**
 * Provides stuff...
 *
 * @short Provide things.
 */
class STAsciiForms: public STObjectController
{
   public:
      /**
       * Constructor
       */
      STAsciiForms(STAsciiForms* parent, int NewXPosition, int NewYPosition, int NewWidth, int NewHeight);
		STAsciiForms(STAsciiForms* parent);

		void StandardConstructor();

      /**
       * Destructor
       */
      ~STAsciiForms();

		void ClearScreen();

		void DrawAsciiBox();

		void GoToX(int MoveToXPosition);

		void GoToXy(int MoveToXPosition, int MoveToYPosition);

		void SetColor(int ColorNumber);

		void Print(int LineNumber, const char *LineText);

		void Print(int LineNumber, STString LineText);

		void Print(int LineNumber, int LinePosition, const char *LineText);

		void Print(int LineNumber, int LinePosition, STString LineText);

		int GetRowCount();
		int GetColumnCount();

		void AsciiTable();
		void SetBorderColour(int NewBorderColour);
		void SetTextColour(int NewTextColour);
		void HBar(int LineNumber);
		void ClearBox();
		void SetGeometry(int NewXPosition, int NewYPosition, int NewWidth, int NewHeight);
		void Restore();
		void Hide();
		void Move(int NewXPosition, int NewYPosition);
		const char* AsciiGlyph(unsigned char CharValue);

	private:
		STAsciiForms* FormParent;
		char GlyphString[5];
		STGrid *Grid;
		int XPosition, YPosition, Width, Height;
		int BorderColour, TextColour;
		int PreviousColor;
		int Hidden;
		void NewGrid(int NewWidth, int NewHeight);
		void Map(int XPosition, int YPosition, int CellColor, int CellValue);
};

#endif // STAsciiForms_included
