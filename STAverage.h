/*=============================================================================

    Copyright (C) 2011 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STAverage.h
	Object Name:				STAverage
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:
   Ports Used:           	None
   Title of program:			Object library
   Description:				Simple automatic average calculation for integers.
=============================================================================*/
#ifndef STAverage_included
#define STAverage_included

#include <STIntList.h>

/**
 * Calculate averages by simply addnig numbers to a list of numbers.
 */
class STAverage: public STIntList
{
   public:
      /**
       * Constructor. Use an unlimited number of cells to allow.
       */
      STAverage();

		/**
       * Constructor.
		 * MaxCells is a limit of the number of cells to allow.
		 * MinCells will not calculate an average util the minimum number of cell have been populated. Average returned is zero until then.
       */
      STAverage(unsigned int SetMaxCells, unsigned int SetMinCells);

      /**
       * Add an integer to the list and returns the average.
       */
      unsigned int Add(unsigned int NewInt);

      /**
       * Returns the average.
       */
      unsigned int Average();

      /**
       * Returns the highest value.
       */
      unsigned int Max();

      /**
       * Returns the lowest value.
       */
      unsigned int Min();

      /**
       * Will automatically flush all values if new number is out of Range from Target;
       */
		void FlushRange(unsigned int NewTarget, unsigned int NewRange);

	private:
		unsigned int MaxCells;
		unsigned int MinCells;
		unsigned int MaxValue;
		unsigned int MinValue;
		unsigned int Target;
		unsigned int Range;
};

#endif // STAverage_included
