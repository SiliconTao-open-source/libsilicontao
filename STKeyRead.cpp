/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#include <STKeyRead.h>
#include <stdio.h>
#include <sys/unistd.h>
#include <fcntl.h>

STKeyRead::STKeyRead()
: STObjectController()
{
	// get current terminal settings
   tcgetattr(STDIN_FILENO, &orig);	

   changes = orig;
	
   // turn off canonical and echo
   changes.c_lflag &= ~(ICANON | ECHO);
   tcsetattr(STDIN_FILENO, TCSADRAIN, &changes);

	// Make this STDIN a nonblocking 	
	int opts;
   opts=fcntl(0, F_GETFL);
   opts=(opts | O_NONBLOCK);
   fcntl(0, F_SETFL, opts);	
}

STKeyRead::~STKeyRead()
{	
   // restore old settings
   tcsetattr(STDIN_FILENO, TCSADRAIN, &orig);	
}

int STKeyRead::GetKey()
{
   int retval;
	int TheChar = 0;

	FD_ZERO(&rfds);
	FD_SET(0,&rfds);
	tv.tv_sec = 0;
	tv.tv_usec = 0;
	retval = select(1, &rfds, NULL, NULL, &tv);
   if(retval)
   {
		TheChar = getchar();
		if(TheChar == 27)
		{
			char Next1 = getchar();
			char Next2 = getchar();
			if((Next1 > 0) && (Next2 > 0))
			{
				TheChar *= 100;
				TheChar += Next1;
				TheChar *= 100;
				TheChar += Next2;
			}
		}
	}
	return(TheChar);
}
