/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#ifndef STRingBuffer_included
#define STRingBuffer_included

#include <STCommonCore.h>

/**
 * A simple ring buffer.
 *
 * STRingBuffer is a simple ring buffer. Ring buffers are very light and fast.
 * Storing bytes puts them in the next location. If the buffer fills then
 * the oldest data is overwritten.
 */

class STRingBuffer
{
	public:
      /**
       * Returns the number of unread bytes in the buffer.
       */
      int Count();

      /**
       * Returns the number of unread bytes in the buffer. While adding a single given byte to the buffer.
       */
      int Write(byte NewByte);

      /**
       * Returns the number of unread bytes in the buffer. While adding a single given byte to the buffer.
       */
      int BlockWrite(byte *NewByteArray, int BlockSize);

      /**
       * Returns a single byte that is in the first unread location and removes that byte from the buffer.
       */
      byte Read();

      /**
       * Fills a given buffer with a maximum of the given size and returns the number of bytes read.
       */
      int BlockRead(byte *ReadBuffer, int MaximumSize);

		/**
       * Constructor.
       */
      STRingBuffer(int BufferSize);

      /**
       * Destructor
       */
      ~STRingBuffer();

	private:
		int MaximumBufferSize;
		int ReadPointer;
		int WritePointer;
		char *BufferArray;

};

#endif //  STRingBuffer_included
