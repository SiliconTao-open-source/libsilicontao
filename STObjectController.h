/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STMalloc.h
   Object Name: 				STMalloc
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Day Zero:					02152000, February 15, 2000
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:
   Ports Used:           	None
   Title of program:			Object library
   Description:				Provides C++ functionality of malloc and free.
  									Does not free the same memory more then once and
                           reports if memory not freed at destroy time.
=============================================================================*/

#ifndef STObjectController_included
#define STObjectController_included

#include <STString.h>

/**
 * Provides a methode to track and control decendent objects.
 * It does not work now but I would like to make it work soon.
 *
 * @short The parent of all ST objects.
 */

class STObjectController
{
   public:
		/**
       * Constructor assigns the object the name unknown.
       */
		STObjectController();

      /**
       * Constructor takes that const char* text that is the name of the object.
       */
		STObjectController(const char* NewObjectName);

      /**
       * Destructor
       */
      ~STObjectController();

      /**
       * Set the value of DebugLevel.
       */
      void SetDebugLevel(unsigned int NewLevel);

		/**
		 *	Returns the value of DebugLevel.
		 */
		unsigned int GetDebugLevel();

      /**
       * Print a debug message if ForDebugLevel >= DebugLevel
       */
      void DebugMessage(unsigned int ForDebugLevel, const char* TheMessage);

		/**
		 * Identify the object name for debugging.
		 */
		void SetObjectName(const char* NewName);

      /**
       * Returns the name of the object
       */
		const char* GetObjectName();

      /**
       * Returns the name of the object
       */
		const char* GetClassName();

      /**
       * Returns the Silicon Tao library version
       */
		const char* GetVersion();

	protected:
		STString InLineDbStr;

		void SetClassName(const char* NewName);
      unsigned int DebugLevel;
		char* ObjectName;
		char* ClassName;
};

#endif // STObjectController_included
