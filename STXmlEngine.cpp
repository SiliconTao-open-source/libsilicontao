/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:
      Currently the XML tags are loaded into a list, each tag having it's own
		position in the list. This is very slow because the list is constantly scanned
		in it's entreaty to find an entry using it's relation to the entry above and
		below it in the list. This list is basically a one dimensional array.

		I was thinking of using a two dimensional array to make the XML engine faster.
		This would allow for a restriction in zones so that a scan would only need to
		look in a limited range of XML entries. Having multiple lists in a two
		dimensional array is fine for reading but would require a master list to
		write values. That would make it harder to use by the programer.

		A three dimensional list is the answer. This would be as fast as a two dimensional
		list allowing for navigation through the entry tree in any direction including
		relative relations of child, parent and sibling nodes. Also each entry would
		be in full control of the reading and writing for it self. This would be as
		easy to use as the current one dimensional list is now.

=============================================================================*/

#include <STXmlEngine.h>
#include <STStringList.h>
#include <STParserOps.h>
#include <STStringsEx.h>
#include <STDriveOps.h>
#include <STList.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

STXmlEngine::STXmlEngine(const char* TheConfigFile)
: STObjectController()
{
	// This invalid entry is designed to segfault the code in the users program that makes calls to XmlEngine
	// InvalidEntry = (XmlEntry *)-1;
	ParserOps = new STParserOps();
	TextFile = new STStringList();
	//printf("STXmlEngine %s\n",TheConfigFile);
	ConfigFile = TheConfigFile;
	if(ConfigFile.Len() == 0)
	{
		#warning STXmlEngine should test for valid filename.
		ConfigFile = "unknown.xml";
	}
	//SetDebugLevel(80);
	TopList = new STList("STXmlEngine");
	//DebugMessage(3, DebugStr.Sprintf("STXmlEngine::STXmlEngine(%s)", (const char*)TheConfigFile));
	LoadXmlFile();
	UnsavedChanges = false;
}

STXmlEngine::~STXmlEngine()
{
	// DebugMessage(13, "STXmlEngine::~STXmlEngine()");
	SaveXmlFile();
	// DebugMessage(20, DebugStr.Sprintf("XML clear\n"));
	Clear();
	// DebugMessage(20, DebugStr.Sprintf("delete ParserOps;\n"));
	delete ParserOps;
	// DebugMessage(20, DebugStr.Sprintf("TextFile\n"));
	delete TextFile;
	// DebugMessage(20, DebugStr.Sprintf("TopList\n"));
	delete TopList;
	//fprintf(stderr, "STXmlEngine::~STXmlEngine() all done\n");
}

bool STXmlEngine::GetBoolValue(const STString& PathName)
{
	return(GetBoolValue(0, PathName));
}

bool STXmlEngine::GetBoolValue(XmlEntry* ParentEntry, const STString& PathName)
{
	bool TempReturn;
	if(ParentEntry == INVALID_ENTRY) return(false);
	STString TempString;
	STStringsEx XString;
	TempString = GetValue(ParentEntry, PathName);
	// DebugMessage(20, InLineDbStr.Sprintf("STXmlEngine::GetBoolValue(0x%08X, '%s')", ParentEntry, (const char*)PathName));

	TempReturn = false;
	if(TempString != "")
	{
		XString.Upper(TempString);
		TempReturn = true;
		if(TempString == "OFF") TempReturn = false;
		if(TempString == "NO") TempReturn = false;
		if(TempString == "FALSE") TempReturn = false;
	}
	// DebugMessage(3, InLineDbStr.Sprintf("return %d",TempReturn));
	return(TempReturn);
}

int STXmlEngine::GetIntValue(const STString& PathName)
{
	return(GetIntValue(0, PathName));
}

int STXmlEngine::GetIntValue(XmlEntry* ParentEntry, const STString& PathName)
{
	int TempReturn;
	STString TempString;

	if(ParentEntry == INVALID_ENTRY) return(0);

	TempString = GetValue(ParentEntry, PathName);
	TempReturn = TempString.ToInt();
	return(TempReturn);
}

bool STXmlEngine::GetBoolValueDefault(XmlEntry *ParentEntry, const STString& PathName, bool NewDefaultValue)
{
	if(ParentEntry == INVALID_ENTRY) return(false);

	STString TempString;
	STString WorkPathName = PathName;

	TempString = "False";
	// DebugMessage(20, InLineDbStr.Sprintf("STXmlEngine::GetBoolValueDefault(0x%08X, '%s', %d)", ParentEntry, (const char*)WorkPathName, NewDefaultValue));
	if(NewDefaultValue)
	{
		TempString = "True";
	}
	if(WorkPathName.Pos("=") == -1)
	{
		WorkPathName += " = ";
		WorkPathName += TempString;
	}

	// Set the default if needed
	GetValueDefault(ParentEntry, WorkPathName, TempString);

	return(GetBoolValue(ParentEntry, WorkPathName));
}

int STXmlEngine::GetIntValueDefault(XmlEntry *ParentEntry, const STString& PathName, int NewDefaultValue)
{
	if(ParentEntry == INVALID_ENTRY) return(0);
	STString TempString;
	STString WorkPathName = PathName;

	TempString.Sprintf("%d", NewDefaultValue);

	// Set the default if needed
	if(WorkPathName.Pos("=") == -1)
	{
		WorkPathName += " = ";
		WorkPathName += TempString;
	}
	GetValueDefault(ParentEntry, WorkPathName, TempString);

	return(GetIntValue(ParentEntry, WorkPathName));
}

void STXmlEngine::Clear()
{
	int ALoopCounter, NodesInList;
	XmlEntry* Entry;

	//printf("void STXmlEngine::Clear()\n");
	if(TopList)
	{
		NodesInList = TopList->Count();
		for(ALoopCounter = 0; ALoopCounter < NodesInList; ALoopCounter++)
		{
			Entry = (XmlEntry*)TopList->Item(0);
			DeleteNode(Entry);
			TopList->Delete(0);
		}
	}
}

void STXmlEngine::DeleteNode(XmlEntry* ThisEntry)
{
	if(ThisEntry == INVALID_ENTRY) return;

	XmlEntry* Entry;
	XmlEntry* ParentEntry;
	int NodesInList;
	int ALoopCounter;
	//, IndexInParent;
	STList *ParentList;

	ParentEntry = ThisEntry->ParentEntry;
	if(ParentEntry != 0)
	{
		ParentList = ParentEntry->EntryList;
	} else
	{
		ParentList = TopList;
	}

	// Remove all child nodes
	if(ThisEntry->EntryList)
	{
		NodesInList = ThisEntry->EntryList->Count();
		for(ALoopCounter = 0;ALoopCounter < NodesInList;ALoopCounter++)
		{
			Entry = (XmlEntry*)ThisEntry->EntryList->Item(0);
			DeleteNode(Entry);
			ThisEntry->EntryList->Delete(0);
		}
		// fprintf(stderr, "delete ThisEntry->EntryList; @ 0x%08X\n", ThisEntry->EntryList);
		//if(GetDebugLevel() >= 7) fprintf(stderr, "delete[] ThisEntry[0x%08X]->EntryList; @ 0x%08X\n", (unsigned int)ThisEntry, (unsigned int)ThisEntry->EntryList);
		delete ThisEntry->EntryList;
	}

	// Remove node values
	if(ThisEntry->PropertyName != 0)
	{
		//if(GetDebugLevel() >= 7) fprintf(stderr, "delete[] ThisEntry[0x%08X]->PropertyName '%s'; @ 0x%08X\n", (unsigned int)ThisEntry, (const char*)ThisEntry->PropertyName, (unsigned int)ThisEntry->PropertyName);
		delete[] ThisEntry->PropertyName;
	}
	if(ThisEntry->PropertyValue != 0)
	{
		//if(GetDebugLevel() >= 7) fprintf(stderr, "delete[] ThisEntry[0x%08X]->PropertyValue '%s'; @ 0x%08X\n", (unsigned int)ThisEntry, (const char*)ThisEntry->PropertyValue, (unsigned int)ThisEntry->PropertyValue);
		delete[] (ThisEntry->PropertyValue);
	}
	if(ThisEntry->SpecialOptions != 0)
	{
		delete[] ThisEntry->SpecialOptions;
	}
	// Free the node memory
	//if(GetDebugLevel() >= 7) fprintf(stderr, "delete[] ThisEntry @ 0x%08X\n", (unsigned int)ThisEntry);
	delete ThisEntry;

	// Remove node from parent list
	//IndexInParent = ParentList->GetIndex(ThisEntry);
	//ParentList->Delete(IndexInParent);
}

char* STXmlEngine::GetFileName()
{
	return((char*)ConfigFile);
}

void STXmlEngine::LoadXmlFile()
{
	STString Tag;
	STDriveOps Do;
	if(Do.FileExists(ConfigFile))
	{
		TextFile->LoadFromFile(ConfigFile);
		while(TextFile->Count() > 0)
		{
			LoadNextXmlTag(0);
		}
	}
}

char* STXmlEngine::CreateProperty(const STString& ValueString)
{
	char* TempReturn;
	TempReturn = new char[ValueString.Len() + 1];
	// if(GetDebugLevel() >= 7) fprintf(stderr, "new[] ThisEntry->PropertyName; @ 0x%08X\n", TempReturn);
	strcpy(TempReturn, (const char *)ValueString);
	TempReturn[ValueString.Len()] = '\0';
	return(TempReturn);
}

void STXmlEngine::LoadNextXmlTag(XmlEntry* ParentEntry)
{
	if(ParentEntry == INVALID_ENTRY) return;

	STList* ParentList;
	XmlEntry* Entry;
	STString TagNameString, Identifier, BodyString, EndMarker;

	// Create a new entry
	Entry = new XmlEntry;
	Entry->SpecialOptions = 0;
	// DebugMessage(30, DebugStr.Sprintf("New Entry at 0x%08X", Entry));
	if(ParentEntry != 0)
	{
		// DebugMessage(30, DebugStr.Sprintf("ParentEntry 0x%08X", ParentEntry));
		ParentList = ParentEntry->EntryList;
	} else
	{
		// DebugMessage(30, DebugStr.Sprintf("ParentEntry is TopList 0x%08X", TopList));
		ParentList = TopList;
	}

	ParentList->Add(Entry);
	Entry->EntryList = new STList("STXmlEngine::LoadNextXmlTag child list");
	Entry->ParentEntry = ParentEntry;
	Entry->PropertyName = 0;
	Entry->PropertyValue = 0;

	// Get property name
	TagNameString = TextFile->Item(0);
	//printf("-> %s 0x%08X\n",(const char*)TagNameString,Entry);
	// DebugMessage(30, DebugStr.Sprintf("LoadNextXmlTag %s 0x%08X", (const char*)TagNameString, Entry));

	TextFile->Delete(0);
	TagNameString.Strip();
	while((TagNameString == "")&&(TextFile->Count() > 0))
	{
		TagNameString = TextFile->Item(0);
		// DebugMessage(30, DebugStr.Sprintf("   Eating TagNameString = '%s'", (const char*)TagNameString));
		TextFile->Delete(0);
		TagNameString.Strip();
		// DebugMessage(30, DebugStr.Sprintf("   Eating TagNameString Stripped = '%s'", (const char*)TagNameString));
	}
	if(TagNameString != "")
	{
		if(TagNameString.Pos("<",0) != 0)
		{
			fprintf(stderr, "ERROR: STXmlEngine::LoadNextXmlTag tag not at beginning of '%s'\n",(const char*)TagNameString);
			exit(1);
		}
		TagNameString.Remove(0,1);
		TagNameString.Strip();
		if(! ParserOps->IsAlpha(TagNameString[0])) fprintf(stderr, "ERROR: STXmlEngine::LoadNextXmlTag tag is not a valid identifier while loading '%s'\n", (const char*)ConfigFile);
		Identifier = ParserOps->FindFirstIdentifier(TagNameString);
		Entry->PropertyName = CreateProperty(Identifier);
		TagNameString.Remove(0,Identifier.Len());
		if(TagNameString.Pos("/>",0) == 0)
		{
			// This is an empty XML tag
			//printf("Loading a blank tag name\n");
			BodyString = "";
			// DebugMessage(30, "   Loading a blank tag name");
			//printf("<-b %s\n",(const char*)BodyString);
			Entry->PropertyValue = CreateProperty(BodyString);
			//printf("Entry->PropertyValue = '%s'\n",Entry->PropertyValue);
		} else
		{
			// This is not an empty tag, read the content
			TagNameString.Remove(0,1);
			TagNameString.Strip();
			if(TagNameString == "")
			{
				TagNameString = TextFile->Item(0);
				TagNameString.Strip();
			}
			EndMarker = "</";
			EndMarker += Identifier;
			EndMarker += ">";
			BodyString = "";
			if((TagNameString.Pos("<",0) == 0) && (TagNameString.Pos(EndMarker,0) != 0))
			{
				while(TagNameString.Pos("<",0) == 0)
				{
					// This is a sub entry, go read it
					// DebugMessage(30, "This is a sub entry, go read it");
					LoadNextXmlTag(Entry);
					TagNameString = TextFile->Item(0);
					// DebugMessage(30, DebugStr.Sprintf("next TagNameString %s", (const char*)TagNameString));
					//printf("<-s %s\n",(const char*)TagNameString);
					TagNameString.Strip();
					if(TagNameString.Pos(EndMarker,0) > -1)
					{
						// DebugMessage(30, DebugStr.Sprintf("Found EndMarker in TagNameString %s", (const char*)TagNameString));
						TextFile->Delete(0);
						break;
					}
				}
			} else
			{
				// DebugMessage(30, DebugStr.Sprintf("Read until EndMarker TagNameString %s", (const char*)TagNameString));
				while(TagNameString.Pos(EndMarker,0) < 0)
				{
					if(BodyString != "")
					{
						BodyString += "\n";
					}
					BodyString += TagNameString;
					TagNameString = TextFile->Item(0);
					TextFile->Delete(0);
					if(TextFile->Count() < 1)
					{
						break;
					}
				}
				if(TagNameString.Pos(EndMarker,0) > -1)
				{
					// DebugMessage(30, DebugStr.Sprintf("Found EndMarker in TagNameString %s", (const char*)TagNameString));
					TagNameString.Remove(TagNameString.Pos(EndMarker,0),TagNameString.Len());
					BodyString += TagNameString;
					//TextFile->Delete(0);
				}
				//printf("<-v %s, BodyString.Len = %d\n",(const char*)BodyString,BodyString.Len());
				// DebugMessage(30, "CreateProperty(BodyString)");
				Entry->PropertyValue = CreateProperty(BodyString);
				/*if(BodyString.Len() == 0)
				{
					//printf("Entry->PropertyName = '%s'\n",Entry->PropertyName);
				}*/
			}
		}
	}
}

void STXmlEngine::SaveXmlFile()
{
	int ALoopCounter, NodesInList;
	XmlEntry* Entry;
	STString DebugStr;

	// DebugMessage(20, DebugStr.Sprintf("void STXmlEngine::SaveXmlFile()"));
	if(UnsavedChanges)
	{
	   // DebugMessage(20, DebugStr.Sprintf("Saving changes to XML file.\n"));
		UnsavedChanges = false;
		// DebugMessage(20, DebugStr.Sprintf("TextFile->Clear();\n"));
		TextFile->Clear();
		TextFileIndent = 0;

		NodesInList = TopList->Count();
		// DebugMessage(20, DebugStr.Sprintf("NodesInList = %d\n", NodesInList));
		// DebugMessage(20, DebugStr.Sprintf("TopList->Count() = %d",TopList->Count()));
		for(ALoopCounter = 0;ALoopCounter < NodesInList;ALoopCounter++)
		{
			Entry = (XmlEntry*)TopList->Item(ALoopCounter);
			// DebugMessage(20, DebugStr.Sprintf("SaveNextXmlTag(Entry[%d] = 0x%08X)", ALoopCounter, Entry));
			SaveNextXmlTag(Entry);
		}
		// DebugMessage(20, DebugStr.Sprintf("TextFile->SaveToFile(%s);",(const char*)ConfigFile));
		TextFile->SaveToFile(ConfigFile);
		// DebugMessage(20, DebugStr.Sprintf("done saving"));
	}
	// DebugMessage(20, DebugStr.Sprintf("Done saving XML file.\n"));
}

STString STXmlEngine::PadBuildString()
{
	int ALoopCounter;

	ReturnString = "";
	for(ALoopCounter = 0; ALoopCounter < TextFileIndent; ALoopCounter++)
	{
		ReturnString += "\t";
	}
	return(ReturnString);
}

void STXmlEngine::SaveNextXmlTag(XmlEntry* ThisEntry)
{
	STString BuildString, TempString;
	uint ALoopCounter;
	bool HasPropertyValue;
	XmlEntry* Entry;
	//bool ReloadList;
	uint NodesInList;
	STString DebugStr;

	if(ThisEntry == INVALID_ENTRY) return;

	NodesInList = 0;
	if(ThisEntry->EntryList)
	{
		NodesInList = ThisEntry->EntryList->Count();
	}

	// The leader for the tag on this node
	BuildString = PadBuildString();
	BuildString += "<";
	HasPropertyValue = false;
	if(ThisEntry->PropertyValue != 0)
	{
		STString TestingLen = ThisEntry->PropertyValue;
		if(TestingLen.Len() > 0)
		{
			HasPropertyValue = true;
		}
	}
	// DebugMessage(20, DebugStr.Sprintf("HasPropertyValue = %d",HasPropertyValue));
	if(HasPropertyValue)
	{
		BuildString += ThisEntry->PropertyName;
		if(ThisEntry->SpecialOptions != 0)
		{
			BuildString += " ";
			BuildString += ThisEntry->SpecialOptions;
		}
		BuildString += ">";
		BuildString += ThisEntry->PropertyValue;
		BuildString += "</";
		BuildString += ThisEntry->PropertyName;
		BuildString += ">";
		// DebugMessage(20, DebugStr.Sprintf("[ %s ] 0x%08X, P 0x%08X",(const char*)BuildString, ThisEntry, ThisEntry->ParentEntry));
		TextFile->Add(BuildString);
		// DebugMessage(20, DebugStr.Sprintf("%s",(const char*)BuildString));
	} else
	if(NodesInList > 0)
	{
		// DebugMessage(20, DebugStr.Sprintf("# NodesInList = %d",NodesInList));
		BuildString += ThisEntry->PropertyName;
		if(ThisEntry->SpecialOptions != 0)
		{
			// printf("ThisEntry->PropertyName = '%s'\tThisEntry->SpecialOptions = '%s'\tBuildString.Len = %d\n", ThisEntry->PropertyName, ThisEntry->SpecialOptions, BuildString.Len());
			BuildString += " ";
			BuildString += ThisEntry->SpecialOptions;
		}
		BuildString += ">";
		// DebugMessage(20, DebugStr.Sprintf("%s",(const char*)BuildString));
		// DebugMessage(20, DebugStr.Sprintf("-> %s 0x%08X, P 0x%08X",(const char*)BuildString, ThisEntry, ThisEntry->ParentEntry));
		TextFile->Add(BuildString);
		TextFileIndent++;
		for(ALoopCounter = 0; ALoopCounter < NodesInList; ALoopCounter++)
		{
			Entry = (XmlEntry*)ThisEntry->EntryList->Item(ALoopCounter);
			// Write the start of the current tag.
			SaveNextXmlTag(Entry);
		}
		TextFileIndent--;
		BuildString = PadBuildString();
		BuildString += "</";
		BuildString += ThisEntry->PropertyName;
		BuildString += ">";
		// DebugMessage(20, DebugStr.Sprintf("%s",(const char*)BuildString));
		// DebugMessage(20, DebugStr.Sprintf("<- %s 0x%08X, P 0x%08X",(const char*)BuildString, ThisEntry, ThisEntry->ParentEntry));
		TextFile->Add(BuildString);
		// DebugMessage(20, DebugStr.Sprintf("%s",(const char*)BuildString));
	} else
	{
		// DebugMessage(20, DebugStr.Sprintf("Saving a blank entry"));
		// DebugMessage(20, DebugStr.Sprintf("Entry->PropertyName = '%s'",ThisEntry->PropertyName));
		BuildString += ThisEntry->PropertyName;
		BuildString += "/>";
		// DebugMessage(20, DebugStr.Sprintf("%s",(const char*)BuildString));
		// DebugMessage(20, DebugStr.Sprintf("(%s) 0x%08X, P 0x%08X",(const char*)BuildString, ThisEntry, ThisEntry->ParentEntry));
		TextFile->Add(BuildString);
		// DebugMessage(20, DebugStr.Sprintf("%s",(const char*)BuildString));
	}
	// DebugMessage(20, DebugStr.Sprintf("Done here"));
}

int STXmlEngine::CountChildNodes(XmlEntry* ThisEntryObject)
{
	int TempReturn = 0;
	if(ThisEntryObject == INVALID_ENTRY) return(0);
	if(ThisEntryObject->EntryList)
	{
		TempReturn = ThisEntryObject->EntryList->Count();
	}
	return(TempReturn);
}

int STXmlEngine::GetDescendentEntryNames(XmlEntry *ParentEntry, const STString& Path, STStringList *ListToFill)
{
	ListToFill->Clear();
	if(ParentEntry == INVALID_ENTRY) return(0);

	STList* DescendentEntryList;
	XmlEntry* Entry;
	STString CompareString;
	STString CommonPath;
	int NodeCount, ALoopCounter;

	CommonPath = GetPath(ParentEntry);
	DescendentEntryList = new STList("STXmlEngine::GetDescendentEntryNames");

	NodeCount = GetDescendentEntries(ParentEntry, DescendentEntryList);
	//printf("NodeCount = %d\n",NodeCount);
	for(ALoopCounter = 0;ALoopCounter < NodeCount; ALoopCounter++)
	{
		Entry = (XmlEntry *)DescendentEntryList->Item(0);
		DescendentEntryList->Delete(0);
		// Look only at nodes with child list counts of zero
		if((Entry->EntryList) && (Entry->EntryList->Count() == 0))
		{
			// Match their path
			CompareString = GetPath(Entry);
			//printf("\n------------------ %d \nLook at '%s'\nLook for '%s'\nCommon '%s'\n",ALoopCounter,
			//  (const char*)CompareString,(const char*)Path,(const char*)CommonPath);
			if(CompareString.Pos(CommonPath, 0) == 0)
			{
				CompareString.Remove(0,CommonPath.Len());
				CompareString.Strip();
				//printf("Qualified '%s'\n",(const char*)CompareString);
				if(CompareString.Pos(Path, 0) == 0)
				{
					//printf("Contains '%s'\n",(const char*)Path);
					//CompareString = Entry->PropertyName;
					if(Entry->PropertyValue != 0)
					{
						CompareString += " = ";
						CompareString += Entry->PropertyValue;
					}
					ListToFill->Add(CompareString);
				}
			}
		}
	}
	delete DescendentEntryList;
	return(ListToFill->Count());
}

int STXmlEngine::GetDescendentEntries(XmlEntry *ParentEntry, STList* DescendentEntryList)
{
	DescendentEntryList->Clear();
	if(ParentEntry == INVALID_ENTRY) return(0);

	STList* ParentList;
	int NodeCount, ALoopCounter;
	XmlEntry* Entry;

	if(ParentEntry != 0)
	{
		ParentList = ParentEntry->EntryList;
	} else
	{
		ParentList = TopList;
	}
	NodeCount = 0;
	if(ParentList)
	{
		NodeCount = ParentList->Count();
	}
	for(ALoopCounter = 0;ALoopCounter < NodeCount;ALoopCounter++)
	{
		Entry = (XmlEntry*)ParentList->Item(ALoopCounter);
		DescendentEntryList->Add(Entry);
		GetDescendentEntries(Entry, DescendentEntryList);
	}
	if(DescendentEntryList)
	{
		return(DescendentEntryList->Count());
	}
	return(0);
}

int STXmlEngine::GetChildEntryNames(XmlEntry *ParentEntry, const STString& Path, STStringList *ListToFill)
{
	ListToFill->Clear();
	if(ParentEntry == INVALID_ENTRY) return(0);

	XmlEntry* ThisEntry;
	XmlEntry* ChildEntry;
	int NodesInList, ALoopCounter;
	STString TempString;

	// Get the Entry pointed to
	ThisEntry = ParentEntry;
	if(Path.Len() > 0)
	{
		ThisEntry = GetEntry(ParentEntry, Path);
	}

	// Populate list with child names
	NodesInList = 0;
	if(ThisEntry->EntryList)
	{
		NodesInList = ThisEntry->EntryList->Count();
	}
	for(ALoopCounter = 0; ALoopCounter < NodesInList; ALoopCounter++)
	{
		ChildEntry = (XmlEntry*)ThisEntry->EntryList->Item(ALoopCounter);
		TempString = ChildEntry->PropertyName;
		if(ChildEntry->PropertyValue != 0)
		{
			TempString += " = ";
			TempString += ChildEntry->PropertyValue;
		}
		ListToFill->Add(TempString);
	}
	return(ListToFill->Count());
}

int STXmlEngine::GetChildEntries(XmlEntry* ParentEntry, STList* EntryList)
{
	EntryList->Clear();
	if(ParentEntry == INVALID_ENTRY) return(0);

	XmlEntry* ChildEntry;
	int NodesInList, ALoopCounter;

	// Populate list with child names
	NodesInList = 0;
	if(ParentEntry->EntryList)
	{
		NodesInList = ParentEntry->EntryList->Count();
	}
	for(ALoopCounter = 0; ALoopCounter < NodesInList; ALoopCounter++)
	{
		ChildEntry = (XmlEntry*)ParentEntry->EntryList->Item(ALoopCounter);
		EntryList->Add(ChildEntry);
	}
	return(EntryList->Count());
}

XmlEntry* STXmlEngine::GetEntry(XmlEntry *ParentEntry, const STString& PathName)
{
	if(ParentEntry == INVALID_ENTRY) return(INVALID_ENTRY);

	XmlEntry* Entry;
	XmlEntry* ReturnEntry;
	int NodesInList;
	int ALoopCounter;
	STString WorkingPath, PartPath, DebugStr;
	bool FoundEntry;
	STList *ParentList;
	STString WorkPathName = PathName;

	// DebugMessage(20, DebugStr.Sprintf("STXmlEngine::GetEntry(XmlEntry 0x%08X, const STString& PathName \"%s\")", ParentEntry, (const char*)WorkPathName));

	if(ParentEntry != 0)
	{
		// DebugMessage(70, " -> Found parent entry");
		ParentList = ParentEntry->EntryList;
	} else
	{
		// DebugMessage(70, " -> No parent entry");
		ParentList = TopList;
	}

	Entry = 0;
	ReturnEntry = 0;
	FoundEntry = false;
	if((ParentEntry != 0) && (ParentEntry->EntryList))
	{
		if((ParentEntry->EntryList->Count() == 0) && (ParentEntry->PropertyValue != 0))
		{
			fprintf(stderr, "ERROR: STXmlEngine::GetEntry(XmlEntry '%s', '%s') Supplied XmlEntry is not a hierarchy tag but a property tag. Consider using XmlEntry->ParentEntry\n", (const char*)ParentEntry->PropertyName, (const char*)WorkPathName);
			return(INVALID_ENTRY);
		}
	}

	NodesInList = 0;
	if(ParentList)
	{
		NodesInList = ParentList->Count();
	}
	for(ALoopCounter = 0; ALoopCounter < NodesInList; ALoopCounter++)
	{
		Entry = (XmlEntry*)ParentList->Item(ALoopCounter);
		WorkPathName.Strip();
		//if(GetDebugLevel() >= 70) fprintf(stderr, "PathName = \"%s\"\n", (const char*)WorkPathName);
		if(! ParserOps->IsAlpha(WorkPathName[0])) fprintf(stderr, "ERROR: STXmlEngine::GetEntry line 722 tag '%s' is not a valid identifier! while working with '%s'\n", (const char*)WorkPathName, (const char*)ConfigFile);
		WorkingPath = ParserOps->FindFirstIdentifier(WorkPathName);
		//if(GetDebugLevel() >= 70) fprintf(stderr, "WorkingPath = '%s' ?= Entry->PropertyName '%s'\n", (const char*)WorkingPath, (const char*)Entry->PropertyName);
		// if(GetDebugLevel() >= 70) fprintf(stderr, "Parent Entry->PropertyName = \"%s\"\t", (const char*)Entry->PropertyName);
		if(WorkingPath == (const char*)(Entry->PropertyName))
		{
			//if(GetDebugLevel() >= 70) fprintf(stderr, "Parent Entry->PropertyName = \"%s\"\t", (const char*)Entry->PropertyName);
			WorkingPath = WorkPathName;
			PartPath = (const char*)(Entry->PropertyName);
			//if(GetDebugLevel() >= 70) fprintf(stderr, "PartPath = \"%s\"\t", (const char*)PartPath);
			WorkingPath.Remove(0, PartPath.Len());
			WorkingPath.Strip();
			//if(GetDebugLevel() >= 70) fprintf(stderr, "WorkingPath = \"%s\"\t", (const char*)WorkingPath);
			if(WorkingPath == "")
			{
				// DebugMessage(70, "We found the entry using the Parent and Path");
				ReturnEntry = Entry;
			} else
			if(WorkingPath.Pos("=", 0) == 0)
			{
				// DebugMessage(70, "Found the data part of WorkingPath");
				WorkingPath.Remove(0,1);
				WorkingPath.Strip();
				// DebugMessage(70, DebugStr.Sprintf("compare %s ?= %s",(const char*)WorkingPath, Entry->PropertyValue));
				/*************************************************************************************
				I need to document this because I think I have changed it back and forth a few times but do not remember why.
				It was set that it would only return this entry if the content matched the search entry content. The problem with that
				is it will only find an entry if the value matches. If the searching value is set as a default for missing entries then
				it will not find the entry. This causes the program to make another entry of the same name with all the default data.
				**************************************************************************************/
				// DebugMessage(70, DebugStr.Sprintf("Testing Entry 0x%08X for a PropertyValue '%s' that matches WorkingPath '%s'", Entry, (const char*)Entry->PropertyValue, (const char*)WorkingPath));
				//if(WorkingPath == (const char*)(Entry->PropertyValue))
				//{
					// We found the entry using the Parent, Path and content
					ReturnEntry = Entry;
				//}
			} else
			{
				// We are still looking for the entry
				ReturnEntry = GetEntry(Entry, WorkingPath);
				if(ReturnEntry == INVALID_ENTRY) return(INVALID_ENTRY);

				//printf("WorkingPath = %s\n",(const char*)WorkingPath);
			}
		}
		if(ReturnEntry)
		{
			// DebugMessage(70, " -> An entry was found.");
			break;
		}
	}
	return(ReturnEntry);
}

XmlEntry* STXmlEngine::GetEntryAutoCreate(XmlEntry *ParentEntry, const STString& PathNameValue)
{
	XmlEntry* Entry;
	Entry = 0;
	if(ParentEntry == INVALID_ENTRY) return(INVALID_ENTRY);

	// DebugMessage(3, InLineDbStr.Sprintf("STXmlEngine::GetEntryAutoCreate(XmlEntry 0x%08X, '%s')", ParentEntry, (const char*)PathNameValue));
	if(PathNameValue.Pos("=",0) < 0)
	{
		fprintf(stderr, "ERROR: STXmlEngine::GetEntryAutoCreate Cannot be used without a property value\n");
		if(PathNameValue.Len() > 0)
		{
			fprintf(stderr, "PathNameValue = '%s'\n", (const char*)PathNameValue);
		}
		return(INVALID_ENTRY);
	}
	Entry = GetEntry(ParentEntry, PathNameValue);
	if(Entry == 0)
	{
		Entry = AddEntry(ParentEntry, PathNameValue);
	}
	// DebugMessage(30, DebugStr.Sprintf("STXmlEngine::GetEntryAutoCreate returns 0x%08X", Entry));
	return(Entry);
}

char* STXmlEngine::GetValue(const STString& PathName)
{
	return(GetValue(0, PathName));
}

char* STXmlEngine::GetValue(XmlEntry *ParentEntry, const STString& PathName)
{
	XmlEntry*  Entry;

	ReturnString = "";
	if(ParentEntry == INVALID_ENTRY) return(ReturnString);

	Entry = GetEntry(ParentEntry, PathName);
	if(Entry != 0)
	{
		if(Entry->PropertyValue != 0)
		{
			ReturnString = Entry->PropertyValue;
		}
	} else
	{
		// If we could not find the property that we are looking for we will auto create it.
		// This was causing problems
		//Entry = SetValue(ParentEntry, PathName);
		//ReturnString = Entry->PropertyValue;
	}
	return((char* )ReturnString);
}

char* STXmlEngine::GetValueDefault(XmlEntry *ParentEntry, const STString& PathName, const STString& NewDefaultValue)
{
	XmlEntry*  Entry;
	bool SetTheDefaultValue, CreateAnEntry;

	ReturnString = "";
	if(ParentEntry == INVALID_ENTRY) return(ReturnString);

	//printf("STXmlEngine::GetValueDefault(0x%08X, '%s', '%s')\n", (unsigned int)ParentEntry, (const char*)PathName, (const char*)NewDefaultValue);
	SetTheDefaultValue = false;
	CreateAnEntry = false;
	Entry = GetEntry(ParentEntry, PathName);
	//printf("Entry = 0x%08X\n", (unsigned int)Entry);
	if(Entry != 0)
	{
		if(Entry->PropertyValue != 0)
		{
			ReturnString = Entry->PropertyValue;
		}
		if(ReturnString == "")
		{
			// We found the entry, it does exist. It does not have a value so let's set the value.
			//printf("call SetValue\n");
			SetValue(Entry, PathName, NewDefaultValue);
			ReturnString = NewDefaultValue;
		}
	} else
	{
		// We do not have that entry lets create it and set the value for it.
		//printf("call AddEntry\n");
		Entry = AddEntry(ParentEntry, PathName);
		Entry->PropertyValue = CreateProperty(NewDefaultValue);
		ReturnString = NewDefaultValue;
	}
	return(ReturnString);
}

XmlEntry* STXmlEngine::SetValue(XmlEntry* ParentEntry, const STString& PathName, bool NewValue)
{
	STString TempString;
	if(ParentEntry == INVALID_ENTRY) return(INVALID_ENTRY);
	//// DebugMessage(3, "STXmlEngine::SetValue(..., bool)");
	TempString = "False";
	if(NewValue)
	{
		TempString = "True";
	}
	return(SetValue(ParentEntry, PathName, TempString));
}

XmlEntry* STXmlEngine::SetValue(XmlEntry* ParentEntry, const STString& PathName, int NewValue)
{
	STString TempString;
	if(ParentEntry == INVALID_ENTRY) return(INVALID_ENTRY);
	//// DebugMessage(3, "STXmlEngine::SetValue(..., int)");
	//printf("STXmlEngine::SetValue %d\n",NewValue);
	TempString.Sprintf("%d",NewValue);
	return(SetValue(ParentEntry, PathName, TempString));
}

XmlEntry* STXmlEngine::SetValue(const STString& PathName)
{
	//// DebugMessage(3, InLineDbStr.Sprintf("STXmlEngine::SetValue('%s')", (const char*)PathName));
	return(SetValue(0, PathName));
}

XmlEntry* STXmlEngine::SetValue(const STString& PathName, const STString& NewValue)
{
	//// DebugMessage(3, InLineDbStr.Sprintf("STXmlEngine::SetValue('%s', '%s')", (const char*)PathName, (const char*)NewValue));
	return(SetValue(0, PathName, NewValue));
}

XmlEntry* STXmlEngine::SetValue(XmlEntry *ParentEntry, const STString& PathName)
{
	STString PropertyValue;
	STString NewPath;

	if(ParentEntry == INVALID_ENTRY) return(INVALID_ENTRY);

	//// DebugMessage(3, InLineDbStr.Sprintf("STXmlEngine::SetValue(XmlEntry 0x%08X, '%s')", ParentEntry, (const char*)PathName));
	PropertyValue = "";
	if(PathName.Pos("=",0) > -1)
	{
		PropertyValue = PathName;
		PropertyValue.Remove(0, PropertyValue.Pos("=",0) + 1);
		PropertyValue.Strip();
	}
	return(SetValue(ParentEntry, PathName, PropertyValue));
}

void STXmlEngine::SetSpecialOptions(XmlEntry *ParentEntry, const char* NewValue)
{
	if((ParentEntry == INVALID_ENTRY) || (ParentEntry == 0))
	{
		return;
	}
	if(ParentEntry->SpecialOptions != 0)
	{
		delete[] ParentEntry->SpecialOptions;
		ParentEntry->SpecialOptions = 0;
	}

	STString WorkString = NewValue;
	if(WorkString.Len() > 0)
	{
		ParentEntry->SpecialOptions = new char[WorkString.Len() + 1];
		strcpy(ParentEntry->SpecialOptions, (const char *)WorkString);
		ParentEntry->SpecialOptions[WorkString.Len()] = '\0';
	}
}

XmlEntry* STXmlEngine::SetValue(XmlEntry *ParentEntry, const char* PathName, const char* NewValue)
{
	if(ParentEntry == INVALID_ENTRY) return(INVALID_ENTRY);
	STString NewValueString = NewValue;
	STString PathNameString = PathName;
	return(SetValue(ParentEntry, PathNameString, NewValueString));
}

XmlEntry* STXmlEngine::SetValue(XmlEntry *ParentEntry, const STString& PathName, const STString& NewValue)
{
	XmlEntry* Entry = 0;
	uint NodesInList;
	STList* ParentList;

	if(ParentEntry == INVALID_ENTRY) return(INVALID_ENTRY);

	// DebugMessage(30, InLineDbStr.Sprintf("STXmlEngine::SetValue(XmlEntry 0x%08X, '%s', '%s')", ParentEntry, (const char*)PathName, (const char*)NewValue));
	//printf("STXmlEngine::SetValue(0x%08X, '%s', '%s')\n", ParentEntry, (const char*)PathName, (const char*)NewValue);
	ReturnString = "";

	if(ParentEntry != 0)
	{
		ParentList = ParentEntry->EntryList;
	} else
	{
		ParentList = TopList;
	}
	NodesInList = 0;
	if(ParentList)
	{
		NodesInList = ParentList->Count();
	}

	if(NodesInList > 0)
	{
		Entry = GetEntry(ParentEntry, PathName);
	}
	if(Entry == 0)
	{
		Entry = AddEntry(ParentEntry, PathName);
	} else
	{
		delete[] Entry->PropertyValue;
		Entry->PropertyValue = 0;
	}
	if((! Entry->EntryList) || (Entry->EntryList->Count() == 0))
	{
		if(Entry->EntryList)
		{
			if(Entry->EntryList->Count() == 0)
			{
				delete Entry->EntryList;
				Entry->EntryList = 0;
			}
			fprintf(stderr, "ERROR: STXmlEngine::SetValue line %d\n", __LINE__);
		}
		if(Entry->PropertyValue)
		{
			//if(GetDebugLevel() >= 7) fprintf(stderr, "delete[] ThisEntry[0x%08X]->PropertyValue; @ 0x%08X\n", (unsigned int)Entry, (unsigned int)Entry->PropertyValue);
			delete[] Entry->PropertyValue;
			Entry->PropertyValue = 0;
		}
		// DebugMessage(20, DebugStr.Sprintf("Entry @ 0x%08X -> PropertyValue = '%s'",Entry,(const char*)NewValue));
		//printf("   %s\n",(const char*)GetPath(Entry));
		Entry->PropertyValue = CreateProperty(NewValue);
		//if(GetDebugLevel() >= 7) fprintf(stderr, "new Entry[0x%08X]->PropertyValue @ 0x%08X = '%s'\n", (unsigned int)Entry, (unsigned int)Entry->PropertyValue, (const char*)Entry->PropertyValue);
		UnsavedChanges = true;
	} else
	{
		fprintf(stderr, "ERROR: STXmlEngine::SetValue(XmlEntry '%s', '%s', '%s') Supplied XmlEntry is an active hierarchy tag not a property tag. Cannot set values on active hierarchy tags.\n", (const char*)ParentEntry->PropertyName, (const char*)PathName, (const char*)NewValue);
		return(INVALID_ENTRY);
	}
	return(Entry);
}

char* STXmlEngine::GetPath(XmlEntry *ThisEntry)
{
	STString BuildingString, BuildingString2, TempString;
	XmlEntry* NextEntry;
	XmlEntry* CurrentEntry;
	bool Done;

	ReturnString = "";
	if(ThisEntry == INVALID_ENTRY) return(ReturnString);

	BuildingString = "";
	CurrentEntry = ThisEntry;
	BuildingString = "";
	Done = false;
	while((Done == false)&&(CurrentEntry != 0))
	{
		NextEntry = CurrentEntry->ParentEntry;
		TempString = CurrentEntry->PropertyName;
		BuildingString2 = BuildingString;
		BuildingString = TempString;
		if(BuildingString2 != "")
		{
			BuildingString += " ";
		}
		BuildingString += BuildingString2;
		BuildingString.Strip();
		if(NextEntry == 0)
		{
			Done = true;
		} else
		{
			CurrentEntry = NextEntry;
		}
	}
	ReturnString = BuildingString;
	return(ReturnString);
}

XmlEntry* STXmlEngine::FindDeepestMatch(XmlEntry *ParentEntry, const STString& PathNameValue)
{
	//XmlEntry *CurrentEntry;
	XmlEntry *TestEntry;
	XmlEntry *PreviousMatch;
	XmlEntry *TempReturn;
	int NodeCount, ALoopCounter;
	STString TestString;
	STList *ParentList;
	bool Done;
	STString ParsePath;
	STString NextIdentifier;

	if(ParentEntry == INVALID_ENTRY) return(INVALID_ENTRY);

	ParsePath = PathNameValue;
	ParsePath.Strip();

	if(ParentEntry != 0)
	{
		ParentList = ParentEntry->EntryList;
	} else
	{
		ParentList = TopList;
	}

	TestEntry = 0;
	TempReturn = 0;
	PreviousMatch = 0;
	Done = false;
	while(!Done && (ParsePath.Len() > 0))
	{
		Done = true;
		NodeCount = 0;
		if(ParentList)
		{
			NodeCount = ParentList->Count();
		}
		if(! ParserOps->IsAlpha(ParsePath[0])) fprintf(stderr, "ERROR: STXmlEngine::FindDeepestMatch tag '%s' is not a valid identifier! in '%s'\n", (const char*)ParsePath, (const char*)PathNameValue);
		NextIdentifier = ParserOps->FindFirstIdentifier(ParsePath);
		for(ALoopCounter = 0;ALoopCounter < NodeCount;ALoopCounter++)
		{
			PreviousMatch = TestEntry;
			TestEntry = (XmlEntry *)ParentList->Item(ALoopCounter);
			TestString = TestEntry->PropertyName;
			if(NextIdentifier == TestString)
			{
				ParsePath.Remove(0, NextIdentifier.Len());
				ParsePath.Strip();
				if(ParsePath.Pos("=") == 0)
				{
					// We are given a content value with the path to match.
					// Only match this entry if the content is a match.
					// If the content is not a match then the previous entry is also not a match.
					ParsePath.Remove(0,1);
					ParsePath.Strip();
					TestString = TestEntry->PropertyValue;
					if(ParsePath == TestString)
					{
						TempReturn = TestEntry;
					} else
					{
						TempReturn = PreviousMatch;
					}
				} else
				{
					Done = false;
					TempReturn = TestEntry;
					ParentList = TestEntry->EntryList;
				}
				break;
			}
		}
	}
	return(TempReturn);
}

XmlEntry* STXmlEngine::AddEntry(XmlEntry *ParentEntry, const STString& PathNameValue)
{
	STString WorkingPath, CommonPath, NextPath, PrePath;
	XmlEntry *Entry;
	XmlEntry *NextEntry;
	STList *ParentList;

	if(ParentEntry == INVALID_ENTRY) return(INVALID_ENTRY);

	Entry = 0;
	/*if(GetDebugLevel() > 2)
	{
		ifprintf(stderr, "STXmlEngine::AddEntry(0x%08X, '%s')\t", (unsigned int)ParentEntry, (const char*)PathNameValue);
		if(ParentEntry != 0)
		{
			fprintf(stderr, " ParentEntry->PropertyName = '%s'\t", (const char*)ParentEntry->PropertyName);
			if(ParentEntry->PropertyValue != 0)
			{
				fprintf(stderr, " ParentEntry->PropertyValue = '%s'", (const char*)ParentEntry->PropertyValue);
			} else
			{
				fprintf(stderr, " ParentEntry->PropertyValue = NULL");
			}
		}
		fprintf(stderr, "\n");
	}*/
	// We have been given a path to a tag to create. It does not exist but it may have parts in common with it's siblings.
	Entry = FindDeepestMatch(ParentEntry, PathNameValue);
	// DebugMessage(30, InLineDbStr.Sprintf("Target Entry 0x%08X", Entry));
	WorkingPath = PathNameValue;
	WorkingPath.Strip();
	if(Entry != 0)
	{
		// To prevent sibling conflicts we have rules for what makes a common path.
		// The common tag is ParentEntry if the NextPath will be unique
		// The common tag is ParentEntry->ParentEntry if the NextPath will not be unique

		// See if there is a tag below this one with the NextPath name
		PrePath = GetPath(ParentEntry);
		PrePath.Strip();
		WorkingPath = PrePath + " ";
		WorkingPath += PathNameValue;
		WorkingPath.Strip();
		CommonPath = GetPath(Entry);
		if(CommonPath.Len() > 0)
		{
			WorkingPath.Remove(0,CommonPath.Len());
			WorkingPath.Strip();
		}
		if(! ParserOps->IsAlpha(WorkingPath[0]))
		{
			fprintf(stderr, "ERROR: STXmlEngine::AddEntry tag '%s' is not a valid identifier! while working with '%s'\n", (const char*)WorkingPath, (const char*)ConfigFile);
		}
		NextPath = ParserOps->FindFirstIdentifier(WorkingPath);
		NextPath.Strip();
		NextEntry = FindDeepestMatch(Entry, NextPath);
		if(NextEntry != 0)
		{
			// Not a unique child name, move up one more level.
			NextEntry = Entry->ParentEntry;
		} else
		{
			// This is a unique name, put it here
			NextEntry = Entry;
		}
		PrePath = GetPath(ParentEntry);
		PrePath.Strip();
		WorkingPath = PrePath + " ";
		WorkingPath += PathNameValue;
		WorkingPath.Strip();
		CommonPath = GetPath(NextEntry);
		if(CommonPath.Len() > 0)
		{
			WorkingPath.Remove(0,CommonPath.Len());
			WorkingPath.Strip();
		}
	} else
	{
		NextEntry = ParentEntry;
	}
	if(! ParserOps->IsAlpha(WorkingPath[0]))
	{
		fprintf(stderr, "ERROR: STXmlEngine::AddEntry tag '%s' is not a valid identifier! while working with '%s'\n", (const char*)WorkingPath, (const char*)ConfigFile);
	}

	NextPath = ParserOps->FindFirstIdentifier(WorkingPath);
	NextPath.Strip();
	WorkingPath.Remove(0,NextPath.Len());
	WorkingPath.Strip();

	while(NextPath != "")
	{
		// DebugMessage(30, InLineDbStr.Sprintf("NextPath = '%s' so we continue to break down the path", (const char*)NextPath));
		// fprintf(stderr, "NextPath = '%s' so we continue to break down the path\n", (const char*)NextPath);
		if(NextEntry != 0)
		{
			if((NextEntry->PropertyValue == 0) && (NextEntry->EntryList == 0))
			{
				//fprintf(stderr, "Convert tag '%s' into a hierarchy tag\n", (const char*)NextEntry->PropertyName);
				NextEntry->EntryList = new STList("STXmlEngine::AddEntry");
				//if(GetDebugLevel() >= 7) fprintf(stderr, "new STList @ 0x%08X\n", (unsigned int)NextEntry->EntryList);
			}
			if(NextEntry->EntryList)
			{
				ParentList = NextEntry->EntryList;
				// DebugMessage(30, InLineDbStr.Sprintf("NextEntry != 0 so ParentList = NextEntry->EntryList; @ 0x%08X", ParentList));
			} else
			{
				fprintf(stderr, "ERROR: STXmlEngine::AddEntry(XmlEntry '%s', '%s') Supplied XmlEntry is not a hierarchy tag but a property tag. Consider using XmlEntry->ParentEntry\n", (const char*)ParentEntry->PropertyName, (const char*)PathNameValue);
				fprintf(stderr, "NextEntry->PropertyName = '%s'\tNextPath = '%s'\tWorkingPath = '%s'\t", (const char*)NextEntry->PropertyName, (const char*)NextPath, (const char*)WorkingPath);
				if(NextEntry->PropertyValue != 0)
				{
					fprintf(stderr, " NextEntry->PropertyValue = '%s'\t", (const char*)NextEntry->PropertyValue);
				}
				// DebugMessage(30, InLineDbStr.Sprintf("XmlEntry '%s' @ 0x%08X", (const char*)ParentEntry->PropertyName, ParentEntry));
				return(INVALID_ENTRY);
			}
		} else
		{
			ParentList = TopList;
			// DebugMessage(30, InLineDbStr.Sprintf("NextEntry == 0 so ParentList = TopList; @ 0x%08X", TopList));
		}
		// Create a new entry
		Entry = new XmlEntry;
		//// DebugMessage(30, InLineDbStr.Sprintf("New XmlEntry 0x%08X added to ParentList 0x%08X", Entry, ParentList));
		ParentList->Add(Entry);
		Entry->ParentEntry = NextEntry;
		Entry->EntryList = 0;
		Entry->SpecialOptions = 0;
		Entry->PropertyName = CreateProperty(NextPath);
		//if(GetDebugLevel() >= 7) fprintf(stderr, "new Entry->PropertyName @ 0x%08X = '%s'\n", (unsigned int)Entry->PropertyName, (const char*)Entry->PropertyName);
		Entry->PropertyValue = 0;
		if(WorkingPath.Pos("=",0) == 0)
		{
			WorkingPath.Remove(0,1);
			WorkingPath.Strip();
			// DebugMessage(30, InLineDbStr.Sprintf("Set Entry(0x%08X)->PropertyValue = '%s'", Entry, (const char*)WorkingPath));
			Entry->PropertyValue = CreateProperty(WorkingPath);
			//if(GetDebugLevel() >= 7) fprintf(stderr, "new Entry[0x%08X]->PropertyValue @ 0x%08X = '%s'\n", (unsigned int)Entry, (unsigned int)Entry->PropertyValue, (const char*)Entry->PropertyValue);
			NextPath = "";
			// DebugMessage(30, InLineDbStr.Sprintf("New XmlEntry '%s' @ 0x%08X added to ParentList 0x%08X managed by ParentEntry at 0x%08X and contains a value of '%s'", (const char*)Entry->PropertyName, Entry, ParentList, Entry->ParentEntry, (const char*)Entry->PropertyValue));
		} else
		{
			Entry->EntryList = new STList("STXmlEngine::AddEntry");
			if(! ParserOps->IsAlpha(WorkingPath[0]))
			{
				fprintf(stderr, "ERROR: STXmlEngine::AddEntry tag '%s' is not a valid identifier! while working with '%s'\n", (const char*)WorkingPath, (const char*)ConfigFile);
			}
			NextPath = ParserOps->FindFirstIdentifier(WorkingPath);
			NextPath.Strip();
			WorkingPath.Remove(0,NextPath.Len());
			WorkingPath.Strip();
			// DebugMessage(30, InLineDbStr.Sprintf("New XmlEntry '%s' @ 0x%08X added to ParentList 0x%08X managed by ParentEntry at 0x%08X and managing an EntryList at 0x%08X", (const char*)Entry->PropertyName, Entry, ParentList, Entry->ParentEntry, Entry->EntryList));
		}
		NextEntry = Entry;
		// DebugMessage(30, InLineDbStr.Sprintf("Advance NextEntry = Entry =  0x%08X", Entry));
		UnsavedChanges = true;
	}
	return(Entry);
}
