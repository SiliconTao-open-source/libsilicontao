/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:

=============================================================================*/

#include <STSystemAPI.h>
#include <STCommonCore.h>
#include <STIntList.h>
#include <STStringList.h>
#include <STStringsEx.h>
#include <STDriveOps.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <dirent.h>
#include <ctype.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pwd.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>

STSystemAPI::STSystemAPI()
: STObjectController()
{
}

STSystemAPI::~STSystemAPI()
{
}

int STSystemAPI::GetAppPid()
{
   pid_t ThisAppsPid;
   
   ThisAppsPid = getpid();
   return(ThisAppsPid);   
}

STString STSystemAPI::GetAppNameFromPid(int ProcessID)
{
   STString FileName, ResultData;
   int OpenFileIndex;
   char Buffer[1024];
   int ReadLength;
   STStringsEx StringX;
   
	// Clearing the string this way seems to leak memory
   ReturnString = "";
   FileName.Sprintf("/proc/%d/stat", ProcessID);

   // Open file, read in a block, close file
   OpenFileIndex = open((const char*)FileName,O_RDONLY);
	if(OpenFileIndex > -1)
	{
   	ReadLength = read(OpenFileIndex, Buffer, sizeof(Buffer) - 1);   
		close(OpenFileIndex);
		Buffer[ReadLength] = '\0';
		ResultData = Buffer;
   	ResultData = StringX.Remove(ResultData, 0,ResultData.Pos('(') + 1);            
   	ResultData = StringX.Remove(ResultData, ResultData.Pos(')'),ResultData.Len() + 1);          
   	ReturnString = ResultData;
	} else
	{
		// Just to let you know this is the way it should work. No need to print an error here.
	}	
   return(ReturnString);
}

int STSystemAPI::GetUserID()
{
	uid_t ThisUserId;
	
	ThisUserId = getuid();
	return(ThisUserId);
}

STString STSystemAPI::GetUserName(int UserID)
{
	struct passwd *PwdInfo = getpwuid(UserID);
	ReturnString = PwdInfo->pw_name;
	return(ReturnString);
}

STString STSystemAPI::GetUserHome(int UserID)
{
	struct passwd *PwdInfo = getpwuid(UserID);
	ReturnString = PwdInfo->pw_dir;
	return(ReturnString);
}

void STSystemAPI::GetIpAddresses(STStringList *FillAddressList)
{
   char *TheSystemName;
   int LengthOfName;
   struct hostent *HostInfo;
   char MoveChar;
   int ALoopCounter;
   int BLoopCounter;
   STString IpAddressString;
   STString ConvertString;
   
   LengthOfName = 150; // Any one with a computer name longer then 150 needs a beating.
   TheSystemName = new char[LengthOfName];
   if(gethostname(TheSystemName,(size_t)LengthOfName) == 0)
   {
      HostInfo = gethostbyname(TheSystemName);
      if(HostInfo != NULL)
      {
         BLoopCounter = 0;
         IpAddressString = "";
         for(ALoopCounter = 0; ALoopCounter < HostInfo->h_length; ALoopCounter++)
         {
            MoveChar = HostInfo->h_addr_list[BLoopCounter][ALoopCounter];
            ConvertString.Sprintf("%d",(unsigned char)MoveChar);
            IpAddressString += ConvertString;
            if((ALoopCounter > 0)&&((ALoopCounter + 1) % 4) == 0)
            {
               FillAddressList->Add(IpAddressString);
               IpAddressString = "";
               BLoopCounter++;
            } else
            {
               IpAddressString += ".";
            }
         }
      } else
      {
         herror("gethostbyname");      
      }      
   } else
   {
      perror("gethostname");
   }
   delete TheSystemName;
}

STString STSystemAPI::GetNetworkInfo(const STString& ALocalAddress)
{
	ReturnString = "";
	//printf("ERROR: STString STSystemAPI::GetNetworkInfo(STString ALocalAddress) does not work yet\n");
	return(ReturnString);
}

void STSystemAPI::GetPidList(const char* AppName, STIntList *PidList,bool WholeMatch)
{
   // For now WholeMatch does not work
   DIR *DirectoryPointer;
	struct dirent *DirectoryEntry;
   pid_t ProcessID;
   STString TheAppName;
   STString SearchAppName = AppName;
	
	//DebugMessage(3, "STSystemAPI::GetPidList(...)");
	PidList->Clear();
   DirectoryPointer = opendir("/proc/");
   
	if(DirectoryPointer)
	{
   	DirectoryEntry = readdir(DirectoryPointer);
		while(DirectoryEntry != NULL)
		{      
      	if(isdigit(DirectoryEntry->d_name[0]))
      	{
         	ProcessID = atoi(DirectoryEntry->d_name);
         	TheAppName = GetAppNameFromPid(ProcessID);
				//DebugMessage(3, InLineDbStr.Sprintf("ProcessID = %d", ProcessID));
				//DebugMessage(3, InLineDbStr.Sprintf("TheAppName = %s", (char*)TheAppName));
         	if((TheAppName.Pos(AppName) > -1) || (SearchAppName == ""))
         	{
					//DebugMessage(3, "Adding to list");
            	PidList->Add(ProcessID);
         	}
      	}
      	DirectoryEntry = readdir(DirectoryPointer);
		}
		closedir(DirectoryPointer);
	} else
	{
		fprintf(stderr, "STSystemAPI::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
	}
}

void STSystemAPI::GetPidList(STIntList *PidList)
{
   // For now WholeMatch does not work
   DIR *DirectoryPointer;
	struct dirent *DirectoryEntry;
   pid_t ProcessID;
	
	PidList->Clear();
   DirectoryPointer = opendir("/proc/");
   
	if(DirectoryEntry)
	{
   	DirectoryEntry = readdir(DirectoryPointer);
		while(DirectoryEntry != NULL)
		{      
      	if(isdigit(DirectoryEntry->d_name[0]))
      	{
         	ProcessID = atoi(DirectoryEntry->d_name);
            PidList->Add(ProcessID);
      	}
      	DirectoryEntry = readdir(DirectoryPointer);
		}
		closedir(DirectoryPointer);
	} else
	{
		fprintf(stderr, "STSystemAPI::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
	}
}

int STSystemAPI::SystemCommand(const char *CommandString, STStringList *FillOutPutList)
{
	int TempReturn = -1;
	STString CommandCall;
	STString TempFileName;
	STDriveOps Do;
	
	FillOutPutList->Clear();	
	
	// Old way fails on read-only systems and is a security risk by writing data to a file.
	//TempFileName = GetTempFileName("STSystemAPI_SystemCommand");
	//printf("%s > %s",(const char*)CommandString, (const char*)TempFileName);
	//CommandCall.Sprintf("%s > %s",(const char*)CommandString, (const char*)TempFileName); 
	//TempReturn = system((const char*)CommandCall);
	//FillOutPutList->LoadFromFile(TempFileName);
	//Do.KillFile((const char*)TempFileName);
	
	#define SAMPLE_SIZE 1024
	char TempBuff[SAMPLE_SIZE + 1];
	STString Output;
	int OutPutLen;
	FILE* CmdFileDescriptor = popen((const char*)CommandString, "r");
	if(CmdFileDescriptor)
	{
		while((OutPutLen = fread( TempBuff, sizeof(char), SAMPLE_SIZE, CmdFileDescriptor)))
		{	
			TempBuff[OutPutLen] = '\0';
			Output += TempBuff;
		}
		pclose(CmdFileDescriptor);
		Output.Split('\n', FillOutPutList);
		TempReturn = 0;
	}	
	return(TempReturn);
}

const char* STSystemAPI::GetTempFileName(const char*SeedString)
{
	ReturnString = "";
	int OurPid = GetAppPid();
	
	ReturnString.Sprintf("/tmp/%s.%d", SeedString, OurPid);
	return(ReturnString);
}
