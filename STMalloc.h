/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STMalloc.h
   Object Name: 				STMalloc
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Day Zero:					02152000, February 15, 2000
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:
   Ports Used:           	None
   Title of program:			Object library
   Description:				Provides C++ functionality of malloc and free.
  									Does not free the same memory more then once and
                           reports if memory not freed at destroy time.
=============================================================================*/

#ifndef STMalloc_included
#define STMalloc_included

class STList;
struct TMemController;

/**
 * Provides a safe way to allocate and free memory. This object prevents
 * memory leeks by controlling the allocation and freeing of space. Controlled
 * memory will never be freed twice nor allocated twice. At destroy time any
 * memory that was not freed will be automatically freed.
 * @li
 * Example of using STMalloc:
 * <PRE>
 * STMalloc *Memory;
 * char *TextPointer1;
 * char *TextPointer2;
 *
 * Memory = new STMalloc();
 *
 * // Allocation of memory is easy
 * TextPointer1 = (char *)Memory->MemAlloc(20);
 * TextPointer2 = (char *)Memory->MemAlloc(20);
 *
 * // No danger in accidentally freeing the same memory twice
 * Memory->Free(TextPointer1);
 * Memory->Free(TextPointer1);
 *
 * // Un-freed memory gets cleaned up in the destructor.
 * delete Memory;
 * </PRE>
 *
 * @short Provides a safe way to allocate and free memory.
 */
class STMalloc
{
   private:
      unsigned int LastCellMalloced, LastCellFreed;

      TMemController *Control;
      STList *ControlList;

   public:
      /**
       * Constructor
       */
      STMalloc();

      /**
       * Destructor. Any memory that was allocated with MemAlloc will
       * be freed here so you don't get a segmentation fault, but you will
       * get a warning about it.
       */
      ~STMalloc();

      /**
       * Allocates a block of memory and returns a pointer to it.
       */
      void *MemAlloc(int SizeOfMemToAlloc);

      /**
       * Frees a memory block that was previously allocated. It uses STList to track
       * memory locations that you use MemAlloc to allocate. If the memory address you
       * are trying to free is not in the list MemFree will not try to free it. This
       * also protects you against freeing the same location twice.
       */
      bool MemFree(void *AddressToFree);

      /**
       * Tells the number of memory block that have been allocated.
       * This is not bytes or size of memory, just a count of the number
       * of time MemAlloc - MemFree where called.
       */
      unsigned int Count();
};

#endif // STMalloc_included
