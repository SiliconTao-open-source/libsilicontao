/*=============================================================================

    Copyright (C) 2007 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STDynamicDataLink.h
	Object Name:				STDynamicDataLink
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Day Zero:					02152000, February 15, 2000
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:       	alloc.h
   Ports Used:           	None
   Title of program:			Object library
   Description:				Inter process communication across a system or across a network.
=============================================================================*/
#ifndef STDynamicDataLink_included
#define STDynamicDataLink_included

#include <STObjectController.h>
#include <STString.h>

class STSystemAPI;
class STStringList;
class STList;
class STHash;
class STCrc;
class STTime;
class STEthernetDevices;
class STXlog;

struct TReceipts;
struct SockReTez;
struct HoldingTank;
struct SControlEntry;

// DeBug
#define DB_STDDL_NONE		0x00000000 // Turn off debug
#define DB_STDDL_TXRX		0x00000001 // Transmit and receive packets except for NELLO, HELLO, HEART BEAT
#define DB_STDDL_DIAG		0x00000002 // Helpful diagnostic messages
#define DB_STDDL_HSCM		0x00000004 // HandShake Control Monitor
#define DB_STDDL_QUES		0x00000008 // Transmit and receive ques
#define DB_STDDL_SIDC		0x00000010 // Sender ID control
#define DB_STDDL_CHCK		0x00000020 // Check sum tests
#define DB_STDDL_INFB		0x00000040 // Data packet Info Blocks
#define DB_STDDL_CYCS		0x00000080
#define DB_STDDL_CYCF		0x00000100
#define DB_STDDL_SOCK		0x00000200 // Network sockets
#define DB_STDDL_UDP_SOCK	0x00000400 // UDP sockets
#define DB_STDDL_RCPT		0x00000800 // Receipts
#define DB_STDDL_CAL2		0x00001000 // Communication cycle function calls
#define DB_STDDL_RSBL		0x00002000 // Remote Side Block Logic
#define DB_STDDL_TXRX_ALL 	0x00004000 // Show even the NELLO, LIST and SELF TEST packets
#define DB_STDDL_CBFC 		0x00008000 // Call Back Function Calls
#define DB_STDDL_DELS 		0x00010000 // Message deletions
#define DB_STDDL_TIME		0x00020000 // Print time stamps at special events.
#define DB_STDDL_CALL		0x00040000 // Function calls
#define DB_STDDL_TRACK_CE  0x00080000 // Track the known Ce's
#define DB_STDDL_TCP_SOCK  0x00100000 // TCP sockets
#define DB_STDDL_RELAY  	0x00200000 // Message relay
#define DB_STDDL_TCP_EXPECTING	0x00400000 // Used to see if we can respond to incoming TCP sockets.
#define DB_STDDL_LIST		0x00800000 // Show list of known senders
#define DB_STDDL_WARN		0x01000000 // Warning messages
#define DB_STDDL_HTMEM		0x02000000 // Holding tank memory management.
#define DB_STDDL_TX			0x04000000 // Everything we send UDP, TCP & HSC
#define DB_STDDL_RX			0x08000000 // Everything we receive UDP, TCP & HSC
#define DB_STDDL_LRGM		0x10000000 // Large messages.
#define DB_STDDL_EXIT_REQ	0x20000000 // Program will terminate if a failure happens.
#define DB_STDDL_SKIP_CRC	0x40000000 // Assume everything works 100% perfectly and do not calculate CRC values, much faster.
#define DB_STDDL_ASKING		0x80000000 // Track the asking of questions and reply of answers.

// DeliverStatus
#define DS_STDDL_HOLDING	0x0000 // Message is still in the out going que, may have been sent but not acknowledged yet.
#define DS_STDDL_DELIVERED	0x0001 // Message was delivered and an acknowledgement was received.
#define DS_STDDL_DEAD		0x0002 // Message was sent but never acknowledged, message expired and was removed.
#define DS_STDDL_UNKNOWN	0x0004 // Used my GetDeliveryStatus if the MessageId could not be found.
#define DS_STDDL_DONTCARE	0x0008 // Message was sent but an acknowledge is not expected back.

// This limits the number of Senders that the program can communicate with but that is not the intention of the MAX_RECEIPTS_TO_TRACK limit.
// The reason is we now use the receipt list to track the delivery status of messages so we do not want to delete receipts until they are
// very old or the conversation is done. We have no way of knowing how old so we just limit this to reduce memory usage. The bad side effect
// is that the receipt list could be overrun by and extremely large number of programs.
#define MAX_RECEIPTS_TO_TRACK	1024

/**
 * STDynamicDataLink provides communication between programs running on a system or across a network.
 * Communications are done by sending messages to a UDP port. Each application has their own unique port.
 * Applications locate each other and their ports using a STDynamicDataLink Hand Shake Controller (HSC) acting as a hub.
 * Every running instance of this library on a system has the ability to act as the HSC for all communications.
 * When the HSC application is shutdown the next available program will take over as HSC.
 * The duties of the HSC application are very minor and thus do not require a lot of system resources, it
 * merely introduces two applications to each other. Once they have been introduced they will communicate directly
 * with each other and the HSC will no longer be involved.
 *
 *	All apps will send a HELLO message to the HSC using the common known UDP port and it will forward that message on
 *	to the receiver as soon as it knows the receivers port. When the receiver gets that message it will know the
 *	PID and port of the sender and will be able to send it an ACK message, this will inform the sender of the receivers
 *	PID and port and it will will send an ACK-ACK.
 *
 *	The purpose of an ACK message is commonly known but it is not common to have an ACK-ACK message. The function of the ACK-ACK
 *	message is to ensure that the original message was legitimately send by the sender. The sender sends a message to the
 *	receiver, the receiver then sends an ACK with a unique ACK code back to what it thinks is the real sender. If the sender
 *	really did send the first message it will send an ACK-ACK with matching ack code back to the receiver and the receiver
 *	will then accept the message as real. If the sender did not send the original message, because of error or an attempt
 *	to violate security, it can try to send an ACK-ACK but it will not have the ack code so the receiver will ignore the
 *	original message.
 *
 *	If the HSC needs to shutdown it will choose another app to perform the duties of HS Controller. It will
 *	choose the lowest PID from the list of known apps and send it the PID's and ports of all the known apps by sending
 *	it one or more CONTROL TRANSFER messages.
 *
 *	If the HSC dies without warning, apps that have already sent and received messages will still know about
 *	each other and still be able to communicate. Any new app will automatically take over the job of HSC but
 *	it will not have PID or port information about the other apps. This will cause an extended delay if a new app is
 *	started and want to send a message to an app that was already running. To solve this I recommend that every app send
 *	HEART BEAT messages regularly if not already sending messages. If your app sends 5 messages in a row that do not receive
 *	and ACK reply then it will automatically send a HEART BEAT message.
 *
 *	A HEART BEAT message is a special message sent to the HSC to inform it about the apps PID and port is still alive.
 * Like HELLO the HEART BEAT does not require an ACK.
 *
 *	All programs using STDynamicDataLink must check for incoming messages regularly.
 *	No messages can be received until after the application has sent a message to another app and thusly caused the automatic
 *	HELLO message to be sent to the HSC. If you do not intend to send any messages to other apps you must send a
 *	HELLO message to inform the HSC of you PID and port. You want to do this if your app will only be receiving messages.
 *
 *	<B><U>Message diagram</B></U><BR>
 *	Rx = Receiver<BR>
 *	Tx = Sender<BR>
 *	When you construct your call to send a message you only need to populate To and Message fields. This object will populate the From and Message Info fields.<BR>
 *	<PRE>
 *	To (Rx)                        | From (Tx)                      | Message Info          			 | Message
 *	Host, App Name, User, PID, Port; Host, App Name, User, PID, Port; Type, Length, Checksum, Receipt; The Message Body
 *	</PRE>
 *	<B><U>Message Types</B></U><BR>
 *	HELLO				: Used to inform the HSC about the apps PID and port. No ACK is given to this message. Message Body is the library version number.
 * NELLO				: Local HSC broadcasts Net-HELLO to any HSC's on the network. HSC's respond with an LIST. Message Body is the library version number.
 *	ACK				: Response to a TEXT, BINARY, HEART BEAT, NELLO or CONTROL TRANSFER.
 * ACKCSF			: Response to a message that Check Sum Failed, please resend last message.
 *	ACK-ACK			: Response to an ACK message
 *	TEXT MSG			: Message body contains ASCII text
 *	BINARY MSG		: Message body contains binary data
 *	HEART BEAT		: App requires an HB ACK from HSC
 *	CONTROL TRANS 	: HSC would like another app to take over the job as HSC. Don't bother to ACK, the old HSC is gone. Just try to take over the job.
 *	SELF TEST		: App is testing itself to see if it has become the HSC. No ACK is given to this message.
 *	HB ACK			: Special ACK to a HEART BEAT message, does not require an ACK-ACK
 * LIST				: Response to a NELLO from another HSC on a different system on the network. Contains a list of known apps and data about them. Listed info is Host, App Name, User, PID, Port.
 * LRGMSG			: Large Message, For messages that larger then the default UDP packet size. See Large Messages
 * UKMT				: UnKnown Message Type, available in new versions only. Tells sending that receiver does not understand.
 *
 *	<B><U>Transmit To combinations</B></U><BR>
 *	You can supply different combinations of data to select the To application to send your message to.
 *	Host			: Set the host IP or name to send the message to
 *	App Name		: Tries to send the message to the app by it's name. If port and PID are not set it will send to all apps by that name.
 *	User			: Limits the message to only be sent to the app that is being run but this user name or UID. Requires app name be set.
 *	PID			: Will only transmit to the app with this exact PID
 *	Port			: Will only transmit to the app at this exact Port
 *
 *	These fields are all optional but a minimum of App Name, PID or Port should be set other wise message will be sent to all known apps running STDynamicDataLink.
 *
 *
 * <B><U>Target String</U></B><BR>
 * To send a message to a target you can format a string as Host, App Name, User, PID, Port
 * Optionally you can use only the App Name and the host will be set to 127.0.0.1
 * If you need to use any field after App Name then all fields must be given even if they are blank.
 * Examples for how to send messages to a program call SomeApp where the PID is 3000, the user is root and the port is 7654:
 * SomeApp
 * 192.168.1.5,OtherApp
 * 192.168.1.5,OtherApp,root
 * 192.168.1.5,OtherApp,,3000
 * 192.168.1.5,OtherApp,,,7654
 * 192.168.1.5,OtherApp,root,,7654
 * 192.168.1.5,OtherApp,root,3000,7654
 *
 * <B><U>Target ID</U></B><BR>
 * Once you have received a messages from an app this object will keep an indexed entry for that app in a list,
 * from then on you only need to address that app using the Target ID.
 *
 * <B><U>Large Messages</U></B><BR>
 * To accumulate Large Messages, the first 4 bytes of the message body are two integers stored as words.
 * The first word is the number of this block and the second word is the total number of blocks.
 * The sending and receiving application do not need to worry about this. Large messages are automatically broken down on the sender and rebuilt on the receiver side. The receiver will not
 * indicate that a message has arrived until all blocks have been received and have passed check sum. By embedding the block index and count in the first 4 bytes of the message body
 * this object maintains compatibility with older versions of it self.
 *
 * <B><U>Conversations</U></B><BR>
 * When a sender wants to ask another sender a question and must receive a reply to that message. Conversations use the Receipt filed in the Message Info block to associate questions with replies.
 * The server side of a conversation will use @ref ListenForQuestion to watch for incoming questions. A server can not use normal @ref ReadMessage calls if it is using @ref ListenForQuestion because
 * @ref ReadMessage calls will not process message receipts. A client should communicate with conversations using @ref AskAQuestion to start a converstaion, this will return with a response
 * or a time out error. The server side of a conversation should be ready answers questions using a function that is registered using @ref RegisterConversationCallBack to deal with the questions.
 *
 *
 * <B><U>Compatibility</U></B><BR>
 * The UKMT message type was added so that older versions will be able to inform newer versions if they received a message but it was of a new type that the older version does not understand.
 * Very old versions did not send their version number in the HELLO or NELLO message so that tells the HSC that some messages may not arrive. If an older version receives a message that it
 * does not understand it will simply not send an ACK. When a newer version receives a message that it does not understand it will reply with UKMT.
 *
 * All applications running on a local system should have the same version of the library available as a shared object.
 *
 * @li
 * Example of using STDynamicDataLink:
 * <PRE>
 * STString InMsg
 * STDynamicDataLink* Link = new STDynamicDataLink();
 *
 * // Send message to app by name. Target app is prog2.
 * Link->SendMessage(",prog2,,,;", "Let me know when you are ready.");
 * while(! Link->MessageWaiting())
 * {
 * 	usleep(5000);
 * }
 * int SenderId = Link->ReadMessage(InMsg);
 * if(InMsg == "I am ready")
 * {
 * 	Link->SendMessage(SenderId, "Okay great. Now let's talk.");
 * }
 * delete Link;
 * </PRE>
 *
 * <B><U>Debug Options</U></B><BR>
 * <ul><li>DB_STDDL_NONE	turn debug off
 * <li>DB_STDDL_TXRX	transmit and receive messages
 * <li>DB_STDDL_CALL	function calls
 * <li>DB_STDDL_DIAG	diagnostic messages
 * <li>DB_STDDL_QUES	transmit and receive message ques
 * <li>DB_STDDL_HSCM HSC management
 * <li>DB_STDDL_CHCK message check sums
 * <li>DB_STDDL_INFB lots of  information about parsing message blocks
 * <li>DB_STDDL_CYCS start debug for only one cycle then pause and turn off debugging
 * <li>DB_STDDL_SOCK show details about socket creation and use
 * <li>DB_STDDL_CYCF do not use this it is used by DB_STDDL_CYCS
 * </ul>
 * <B>Example</B> SetNewDebugLevel(DB_STDDL_TXRX | DB_STDDL_CALL | DB_STDDL_DIAG | DB_STDDL_QUES);
 *
 * @short Inter-application text messaging. Used to send simple text messages to other running applications.
 */
class STDynamicDataLink: public STObjectController
{
   private:
		SControlEntry* HscCe;
		STCrc* Crc;
		bool SayingGoodByte;
		int UnsentMessages;
		bool InSideCallBack;
		STString OverRideAppName;
		int NextScanCycle;
		unsigned int SaveDebug; // Keep original debug settings while debug is altered.
		STString AppName;
		unsigned int AppPID, AppUID;
		unsigned int HSPort, UdpPort, TcpPort;
		unsigned int CeIdCounter;
		unsigned int MessageIdCounter, LastMessageIdCounter;
		unsigned int LastCrcCalculated;
		unsigned int RxByteCount;
      STString ReturnString;
      STSystemAPI* SystemApi;
		bool BuildingAReplyMsg;
		unsigned int UseThisReceiptForReply;
		STList* ReceiptList;
		STList* ControlList;
		STList* LrgMsgList;
		SockReTez *RxSockReTez, *HscSockReTez, *TcpSockReTez;
		int WeAreHsControl;
		int HeartBeatTimer;
		STList *RxHoldingList, *TxHoldingList, *RxMessageList, *LimboList;
		STString DdlVersion;
		STStringList* NodesList;
		STStringList* RxByProxyList;
		int SocketBuildCount;
		STTime* TimeInt;
		double LastScanTime, LastHeartBeat, LastTimeNetDeviceListUpdated;
		STStringList* FunctionCallHistory;
		STHash *CallStack;
		STList* PendingTcpSockets;
		int LastSocketError;
		bool AcceptHSCResponsibility;
		int SocketErrorCount;
		int TcpLinkMode;
		bool AcceptLocalConnectionsOnly, BlockBroadcasting;
		STEthernetDevices* EthDev;
		// bool LoEthDevTested;
		// STString LastTestedDevice, LastTestedAddress;
		STHash* EthernetTestedDevices;
		int PadString;
		STXlog* Logger;

		STString IsOurIpAddress(const STString& Address);
		//int AddSenderToControlList(STString MessageRxd);
		//int AddSenderToControlList(STString MessageRxd, unsigned int SenderId, int SenderPort, STString Host, int SenderPid);
		unsigned int AddSenderToControlList(struct HoldingTank* Ht);
		unsigned int AddToTxStack(const STString& Host, int Port, const STString& Message, int HoldForAcknowledgement);
		void AddTrackFunctionCalls(const char* FunctionName);
		void AnnounceMySelf(struct HoldingTank* Ht);
		void AssumeHSControl();
		STString BuildTargetStringFromSenderId(unsigned int SenderId);
		void BuildUdpSocket(SockReTez *NewSockReTez, int NewPort, int Receiving, int Broadcast);
		void CloseUdpSocket(SockReTez *SockCtrl);
		void CloseTcpLink(unsigned int SenderID);
		SControlEntry* CreateNewControlEntry(struct SenderInfoBlock* RemoteSide);
		unsigned int CreateNewReceipt();
		void UpdateMsgIdOfReceipt(unsigned int OldMsgId, unsigned int NewMsgId);
		void CreateUniquePort();
		void CreateTcpPort();
		struct HoldingTank* CreateHoldingTank(const char* MessageBuffer, bool ForSending);
		void DeleteHoldingTank(struct HoldingTank *Ht);
		void FlushInvalidCeItems();
		void GetAppDetails();
		STString GetBlockName(int BlockType);
		SControlEntry* GetCeFromSenderId(unsigned int SenderId);
		SControlEntry* GetCeFromHoldingTank(struct HoldingTank *Ht);
		unsigned int GetCheckSum(const STString& MessageToCheck);
		STString GetInfoBlockFromNewMsg(const STString& MessageData, int BlockName);
		struct HoldingTank* GetHoldingTankFromMsgId(unsigned int MessageId);
		int GetMessageType(const STString& MessageTypeText);
		STString GetMessageType(int MessageType);
		struct HoldingTank* GetNewMessage(SockReTez *SockCtrl);
		int GetPortOfSenderId(unsigned int LookUpId);
		unsigned int GetReceiptIntFromMsgId(unsigned int MessageId);
		unsigned int GetSenderId(const STString& Host, const STString& TargetAppName);
		unsigned int GetSenderId(struct HoldingTank* Ht);
		unsigned int GetSenderId(const STString& Host, unsigned int Port, unsigned int Pid);
		struct SenderInfoBlock* GetSenderInfoBlockOfRemoteSide(struct HoldingTank* Ht);
		struct TReceipts* GetReceiptFromMsgId(unsigned int MessageId);
      void GoodBye();
		void HsControlProcesses();
		void Initialization();
		void MarkSenderAsHsc(unsigned int SenderId);
		void MarkSenderDead(unsigned int SenderId);
		unsigned int MessageRelay(struct HoldingTank* Ht);
		void PrintFunctionCallHistory(const char* ErrorReport);
		void PrintHtInfo(struct HoldingTank* Ht);
		bool ProcessCommunicationCycle();
		void ProcessHoldingTanks();
		int QualifyRxMessage(struct HoldingTank* Ht);
		void RebuildLargeMessage(unsigned int SenderId, struct HoldingTank* Ht);
		void ReceiveList(struct HoldingTank* Ht);
		void RecognizeHsc(struct HoldingTank* Ht);
		void RegisterReceipt(struct HoldingTank* Ht);
		void RemoveOldSenderByHostPort(struct HoldingTank* Ht, unsigned int NotThisId);
		void RemoveControlEntry(unsigned int CeIndex);
		void RemoveHeldMsg(STList* HoldList, unsigned int HtIndex);
		void RemoveLrgMsgFromSender(unsigned int SenderId);
		void RemoveReceipt(unsigned int Receipt);
		STString ReplaceInfoBlock(const STString& MessageData, int BlockName, const char* NewText);
		void ResendLastMessage(unsigned int SenderId);
		bool ScanNetwork();
		void SendAck(struct HoldingTank* Ht);
		void SendAckAck(struct HoldingTank* Ht);
		bool SendFromTxStack();
		void SendHsControlTrans();
		//void SendInfoList(int SenderId);
		void SendLargeMessage(unsigned int TargetId, const STString& TheMessage);
		void SendList(struct HoldingTank* Ht);
		// unsigned int SendTcpMessage(struct SControlEntry *Ce, STString MessageToSend);
		unsigned int SendTcpMessage(struct HoldingTank* Ht);
		unsigned int SendUdpMessage(SockReTez *SockCtrl, const char *TargetIp, const STString& MessageToSend, int HoldForAcknowledgement);
		unsigned int SendUdpMessage(SockReTez *SockCtrl, const char*TargetIp, struct HoldingTank* Ht);
		void SetDeliveryStatus(unsigned int MessageId, unsigned int NewStatus);
		const char* GetDeliveryStatusName(unsigned int NewStatus);
		bool TcpSocketStatus(int Socket);
		void UnhandledMessages(struct HoldingTank* Ht);
		void UnknownMessageType(unsigned int SenderId);
		bool IsLocalSenderAndStillAlive(struct SControlEntry* Ce);

		// When I went looking for this member function I found it commented out of the CPP file but still used in the header file. I wonder if that caused problems?
		// Any why is there ** here??? void UpdateControlEntry(char** EntryField, const STString& NewData);

		void VerifyAccept(struct HoldingTank* Ht);
		unsigned int ReceiveByProxy(struct HoldingTank* Ht);
		void DeleteMessageById(unsigned int MessageId);
		void DeleteMessageByReceipt(unsigned int Receipt);
		void ReadStaticLink(struct HoldingTank* Ht);
		void SendStaticLink(struct HoldingTank* Ht);
		bool SendABackSlap(struct HoldingTank *Ht);

		// Diagnostics
		void PrintInfoBlock(struct SenderInfoBlock* QueryInfoBlock);
		const char* PrintDebugSettings();
		void SetColor(int ColorNumber);
		void ShowHoldingTanks();
		int GetDebugIntFromCommand(const char* DebugCommand);
		void ReceiveDebugSetting(struct HoldingTank* Ht);
		void SetStartupDebugging();
		void ReadDebugSettingsFromFile();
		void Echo(struct HoldingTank* Ht);

   public:
      /**
       * Constructor
       */
      STDynamicDataLink();

      /**
       * Constructor with the option to have debug turned on or not.
       */
      STDynamicDataLink(bool StartUpDebugging);

      /**
       * Constructor using a different Hand Shake Port. Use this to isolate a group of programs from others.
       */
      STDynamicDataLink(int AlternateHsPort);

      /**
       * Constructor with a custom AppName and option to have debug turned on or not. Use the custom AppName to have multiple links from the same program.
       */
		STDynamicDataLink(const char* NewOverRideAppName, bool StartUpDebugging);

      /**
       * Destructor
       */
      ~STDynamicDataLink();

		/**
		 * Set the STXlog to send debug information to.
		 */
		void SetLogger(STXlog* NewXLog);

      /**
       * Returns the number of messages that are waiting to be read.
       */
      int MessageCount();

      /**
		 * Sends a message to a string formatted target. Returns message ID.
		 */
      unsigned int SendMessage(const STString& Target, const STString& TheMessage);

		/**
		 * Sends a message to a target using a known Target ID. Get the Target ID from @ref ReadMessage. Returns message ID.
		 */
      unsigned int SendMessage(unsigned int TargetId, const STString& TheMessage);

      /**
		 * Returns true if the message you sent out is still in the holding que awaiting an ACK from the receiver.
		 * Old messages automatically expire and are removed from the que after a time without ever receiving an ACK.
		 */
      bool HoldingMessage(unsigned int MsgId);

      /**
		 * HoldingMessage is too limited. It will not tell you if the message expired from age or not, just if it is still in the que.
		 * GetDeliveryStatus will return one of these DS_STDDL_HOLDING, DS_STDDL_DELIVERED, DS_STDDL_DEAD, DS_STDDL_UNKNOWN, DS_STDDL_DONTCARE
		 */
		unsigned int GetDeliveryStatus(unsigned int MessageId);

      /**
       * Returns the Sender ID or 0 if no message to read. It puts the message into the given string object.
       */
      unsigned int ReadMessage(STString* MessageBuffer);

		/**
		 *	Sends a harmless message to the STDynamicDataLink object of the receiving target. Sender should echo back with a HELLO message and then the SenderID of the target should be known.
		 */
		unsigned int Ping(const STString& Target);

      /**
       * Returns the next available message if one exists.
       */
      STString ReadMessage();

		/**
		 * Returns the next available message from a sender, will skip over messages from other senders.
		 */
		STString ReadMessageFromSender(unsigned int SenderId);

		/**
		 * Returns the MessageID for a received message that is a reply to a conversation with another sender. Returns zero if no reply has been received.
		 * <B><BIG>This function is part of the conversations feature and is not working yet.<BR>Conversations are not yet supported in this version.</BIG></B>
		 */
		int LookForReply(unsigned int MessageID);

		/**
		 * Reads a message that was received and is identified by the MessageID.
		 */
		STString ReadMessageById(unsigned int MessageID);

		/**
		 * Returns the MessageID for the next question message waiting to be read. Returns zero for no question waiting. Ignores messages that are answers.
		 */
		unsigned int GetNextQuestionId();

		/**
		 * Send a message to a program by SenderID to change the debug level of the STDynamicDataLink.
		 */
		unsigned int SendDebugSetting(unsigned int TargetId, const STString& TheMessage);

		/**
		 * Send a message to a program by HOST,APP_NAME string to change the debug level of the STDynamicDataLink.
		 */
		unsigned int SendDebugSetting(const STString& Target, const STString& TheMessage);

      /**
		 * A wrapper for client side of a conversation of messages. This function is blocking and will only return when a reply has been received or when a
		 * timeout happens. This will locate a sender to communicate with, ask it a question and return the reply. To perform a client side conversation in
		 * non-blocking mode see @ref SendMessage, @ref LookForReply, @ref ReadMessageById
		 * <PRE>
		 * STDynamicDataLink Link;
		 * STString TheAnswer = Link.AskAQuestion("192.168.0.54, OtherApp", "WHATS UP");
		 * printf("54 says %s\n", (const char*) TheAnswer);
		 * </PRE>
		 */
      STString AskAQuestion(const STString& Target, const STString& TheMessage);

		/**
		 * Ask a question and change the amount of time you are willing to wait for a reply. Default is 20000
		 */
		STString AskAQuestion(const STString& Target, const STString& TheMessage, int LimitedTimeBonus);

      /**
		 * Register a call back event handler for the server side of a conversation of messages. The EntryPoint is the pointer to the C function that is
		 * known to the Parent object, the Trigger will match the leading text of the message body to decided what callback to call.
		 * Leave the trigger blank to match all call back events. Once a call back is registered a server must call @ref ListenForQuestion frequently.
		 * Your call back function MUST end with a @ref Reply even if the message was not understood.
		 */
      void RegisterConversationCallBack(const char* Trigger, void* Parent, void* EntryPoint);

      /**
		 * A wrapper for server side of a conversation of messages. This is a non-blocking call that will look for conversations and invoke callbacks that are
		 * registered from using @ref RegisterConversationCallBack. When a message is received the return value is true but it makes no attempt to inform if
		 * the call back was handled correctly or not, the developer must handle that. Call this often.
		 */
		bool ListenForQuestion();

		/**
		 * Sends a reply to a previously received message.  Returns message ID.
		 */
		unsigned int Reply(unsigned int MessageID, char* Message);

		/**
		 * Returns the Sender ID of the next message waiting without moving it off the message stack. Returns 0 if no messages are waiting.
		 * Useful when communicating between more then one app, sending a message to an app and needing to handle the reply from it.
		 */
		unsigned int GetSenderIdOfNextMsg();

		/**
		 * Returns the Sender ID of the known message by using the given message ID.
		 */
		unsigned int GetSenderIdOfMsgId(unsigned int MessageId);

		/**
		 * Returns the unique port for this instance.
		 */
		int UniquePort();

		/**
		 * Returns the number of bytes received.
		 */
		unsigned int RxCount();

		/**
		 * Gathers data and returns a string list with information about all known apps on all known hosts.
		 * The app in the list at location 0 is the current app.
		 */
		STStringList* List();

		/**
		 * This will force the program to never accept the responsibilities of the HSC. Use this when you program cannot reliably maintain these duties.
		 * You should set this before the program is no longer able to maintain the HSC duties. If the program is HSC it will resign. The program will try to become
		 * HSC at constructor time and then will resign when this function is called. This will also call @ref NeverBroadcast()
		 */
		void NeverTheHSC();

		/**
		 * This will prevent the program from actively seeking out other programs on the LAN. Calling this function alone will not obstruct Rx or Tx of communications on the LAN. If you wish to prevent Rx and Tx on the LAN use @ref LocalConnectionsOnly()
		 * Calling this function alone will not prevent the program from becoming HSC but it will not be able to do a very good job of being the HSC. If you wish to prevent the program from becoming HSC use @ref NeverTheHSC()
		 */
		void NeverBroadcast();

		/**
		 * This prevents the application from accepting messages from any hosts that are not on 127.0.0.1. Will also drop messages relayed through local HSC from other hosts.
		 * This also forces the application to use @ref NeverTheHSC() and @ref NeverBroadcast()
		 */
		void LocalConnectionsOnly();

		/**
		 * Populates a STHash with easy to navigate data about a given sender ID. Fields of info are SenderId, Host, AppName, User, Pid, Port
		 * Returns true if sender ID found or false if not.
		 */
		bool GetSenderInfo(STHash* Data, unsigned int SenderID);

		/**
		 * Change the debug level and print the new debug settings.
		 */
		void SetNewDebugLevel(int NewDebugValue);

		/**
		 * Returns true if SControlEntry::ResponseTimeOut exceeded limit while waiting for an ACK message to a TEXT message.
		 */
		bool IsSenderDead(unsigned int SenderId);

		/**
		 * Enables faster communication using TCP sockets. This will put the link into TCP seek mode where it will try to make a socket link with all other senders. TCP cannot be enabled if it was previously disabled. Default it to allow but not seek TCP links.
		 */
		void EnableStaticLink();

		/**
		 * Blocks communication using TCP sockets. This will put the link into TCP deny mode where it will not make a socket link with any other senders. TCP cannot be disabled if it was previously enabled. Default it to allow but not seek TCP links.
		 */
		void DisableStaticLink();

		/**
		 * This it to forget everything about other senders. It will force the systime to re-discover any senders it needs to talk to.
		 */
		void FlushSenders();
};

#endif // STDynamicDataLink_included
