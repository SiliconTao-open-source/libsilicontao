# libSiliconTao

This C++ library was originally created in 2002 for embedded systems with limited RAM.

STPortMap & STXcom were used with Microchip PIC microcontrollers.

STString & STHash were used in an [Embedded Binary Web Server](https://gitlab.com/SiliconTao-open-source/EmbeddedBinaryWebServer) that was small, fast and ran from a PC104 that used a stripped down version of Debian with only a 512MB SD card.

Git source for [ST Libs](https://gitlab.com/SiliconTao-open-source/libSiliconTao)

## STAsciiForms
ASCII boxes and shapes for text and input on the terminal window.

## STCrc
Simple CRC calculations

## STDriveOps
Perform file path name manipulation and actions.

## STDynamicDataLink
STDynamicDataLink provides communication between programs running on a system or across a network. Communications are done by sending messages to a UDP port. Each application has their own unique port. Applications locate each other and their ports using a STDynamicDataLink Hand Shake Controller (HSC) acting as a hub. Every running instance of this library on a system has the ability to act as the HSC for all communications. When the HSC application is shutdown the next available program will take over as HSC. The duties of the HSC application are very minor and thus do not require a lot of system resources, it merely introduces two applications to each other. Once they have been introduced they will communicate directly with each other and the HSC will no longer be involved.

All apps will send a HELLO message to the HSC using the common known UDP port and it will forward that message on to the receiver as soon as it knows the receivers port. When the receiver gets that message it will know the PID and port of the sender and will be able to send it an ACK message, this will inform the sender of the receivers PID and port and it will will send an ACK-ACK.

The purpose of an ACK message is commonly known but it is not common to have an ACK-ACK message. The function of the ACK-ACK message is to ensure that the original message was legitimately send by the sender. The sender sends a message to the receiver, the receiver then sends an ACK with a unique ACK code back to what it thinks is the real sender. If the sender really did send the first message it will send an ACK-ACK with matching ack code back to the receiver and the receiver will then accept the message as real. If the sender did not send the original message, because of error or an attempt to violate security, it can try to send an ACK-ACK but it will not have the ack code so the receiver will ignore the original message.

If the HSC needs to shutdown it will choose another app to perform the duties of HS Controller. It will choose the lowest PID from the list of known apps and send it the PID's and ports of all the known apps by sending it one or more CONTROL TRANSFER messages.

If the HSC dies without warning, apps that have already sent and received messages will still know about each other and still be able to communicate. Any new app will automatically take over the job of HSC but it will not have PID or port information about the other apps. This will cause an extended delay if a new app is started and want to send a message to an app that was already running. To solve this I recommend that every app send HEART BEAT messages regularly if not already sending messages. If your app sends 5 messages in a row that do not receive and ACK reply then it will automatically send a HEART BEAT message.

A HEART BEAT message is a special message sent to the HSC to inform it about the apps PID and port is still alive. Like HELLO the HEART BEAT does not require an ACK.

All programs using STDynamicDataLink must check for incoming messages regularly. No messages can be received until after the application has sent a message to another app and thusly caused the automatic HELLO message to be sent to the HSC. If you do not intend to send any messages to other apps you must send a HELLO message to inform the HSC of you PID and port. You want to do this if your app will only be receiving messages.

## STEthernetDevices
Configures ethernet devices for communications.

## STFreeBase
A simple flat file database.

## STHash
A Perl style hashed array for C++

## STKeyRead
To read input from the keyboard.

## STList
A fast light-weight linked list.

## STMalloc
A memory allocator that protects against common memory errors.

## STOnlyOne
Prevents another instance of the same program from running.

## STParserOps
String parsing.

## STPortMap
Provides PORT I/O pin simulation for embedded chip emulators.

## STRingBuffer
STRingBuffer is a simple ring buffer. Ring buffers are very light and fast. Storing bytes puts them in the next location. If the buffer fills then the oldest data is overwritten.

## STString
Provides common string functionality. There are many string objects, all are very good and easy to use so why would I make another. Not a bloated object that has thousands of members, but a thin faster object to do the job. Used all through libSiliconTao.

## STStringList
STStringList is an easy to use string list that takes char* or STStrings and returns STStrings. STStringList can load it's strings from a common text file and save it's list in to a text file.

## STSuperIni
Read and write configuration text files.

## STSystemAPI
Interactions with the Linux OS.

## STTime
Time and date operations.

## STXcom
Provides a easy way to do serial port communication under Linux.

## STXmlEngine
Read and Write simple well formatted XML files without having to manage the hierarchy directly. Manipulation of tag is done using the XmlEntry interface. Creating or locating a tag returns the interface from there you can set or read the values.
