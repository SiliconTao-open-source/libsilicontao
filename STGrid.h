/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#ifndef STGrid_included 
#define STGrid_included 

class STList;

#include <STObjectController.h>

/**
 * Provides stuff...
 *
 * @short Provide things.
 */
class STGrid: public STObjectController
{
	public:
      /**
       * Constructor
       */
		STGrid(int XCells, int YCells, int CellSize);

      /**
       * Destructor
       */
		~STGrid();
	
		STList *RowArray;
		void FreeGrid();
		void CreateGrid();
		void* CellPointer(int CellX, int CellY);
	
	private:
		int CellByteSize;
		int Width;
		int Height;
		int GridActive;	
};

#endif // STGrid_included
