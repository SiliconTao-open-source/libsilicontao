/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#ifndef STPortMap_included
#define STPortMap_included

#include <STCommonCore.h>

/**
 * Provides PORT I/O pin simulation for embedded chip emulators.
 *
 * This is an object so that events can be trigered when the state of the bits change.
 * If you want to emulate an I/O port and you do not need events then just use a simple
 * bit field and a union like so.
 * <PRE>
 * struct SPins
 * {
 *		unsigned BIT0: 1;
 *		unsigned BIT1: 1;
 *		unsigned BIT2: 1;
 *		unsigned BIT3: 1;
 *		unsigned BIT4: 1;
 *		unsigned BIT5: 1;
 *		unsigned BIT6: 1;
 *		unsigned BIT7: 1;
 * };
 *
 * struct SPort
 * {
 *		union
 *		{
 *			BYTE ThePortValue;
 *			struct SPins TheBits;
 *		}u;
 * };
 *
 * struct SPort TheAPort;
 * struct SPort TheBPort;
 * struct SPort TheCPort;
 *
 * #define PORTA TheAPort.u.ThePortValue;
 *	#define RA0 TheAPort.u.BIT0;
 *	#define RA1 TheAPort.u.BIT1;
 *	#define RA2 TheAPort.u.BIT2;
 *	#define RA3 TheAPort.u.BIT3;
 *	#define RA4 TheAPort.u.BIT4;
 *	#define RA5 TheAPort.u.BIT5;
 *	#define RA6 TheAPort.u.BIT6;
 *	#define RA7 TheAPort.u.BIT7;
 *
 * #define PORTB TheBPort.u.ThePortValue;
 *	#define RB0 TheBPort.u.BIT0;
 *	#define RB1 TheBPort.u.BIT1;
 *	#define RB2 TheBPort.u.BIT2;
 *	#define RB3 TheBPort.u.BIT3;
 *	#define RB4 TheBPort.u.BIT4;
 *	#define RB5 TheBPort.u.BIT5;
 *	#define RB6 TheBPort.u.BIT6;
 *	#define RB7 TheBPort.u.BIT7;
 *
 * #define PORTC TheCPort.u.ThePortValue;
 *	#define RC0 TheCPort.u.BIT0;
 *	#define RC1 TheCPort.u.BIT1;
 *	#define RC2 TheCPort.u.BIT2;
 *	#define RC3 TheCPort.u.BIT3;
 *	#define RC4 TheCPort.u.BIT4;
 *	#define RC5 TheCPort.u.BIT5;
 *	#define RC6 TheCPort.u.BIT6;
 *	#define RC7 TheCPort.u.BIT7;
 * </PRE>
 * @short Provides PORT I/O pin simulation for embedded chip emulators.
 */
class STPortMap
{
   public:
      BYTE PortValue;

      bool BIT7,BIT6,BIT5,BIT4,BIT3,BIT2,BIT1,BIT0;

      STPortMap &operator=(const BYTE NewValue);
      STPortMap &operator++(int);
      STPortMap &operator+(const int Incroment);
      STPortMap &operator+=(const int Incroment);
      STPortMap &operator+=(const STPortMap& );
      STPortMap &operator>>=(const int RotateRight);
      STPortMap &operator<<=(const int RotateLeft);
      STPortMap &operator|=(const int OrValue);
      STPortMap &operator&=(const int AndValue);
      STPortMap &operator!=(const int NotValue);
      operator BYTE();

      void SetPort();
      void SetBits(BYTE NewBits);
	STPortMap();
};

#endif // STPortMap_included
