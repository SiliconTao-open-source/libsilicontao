/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:
      - test speed of new VS malloc VS STMalloc
		- Make list faster by changing MoveToCell to use the PointerToPreviousCell for moving backwards in the list

=============================================================================*/

/*=============================================================================
   Note:
      - STMalloc uses STList to track memory leeks so STList can not use
        STMalloc to get memory.

=============================================================================*/

// #define DEBUG_MEMORY_TRACKING

#include <STList.h>
#include <STCommonCore.h>

// Do not use STMalloc here, would cause recursive loop.
#include <stdio.h>
#include <stdlib.h>

STList::STList(const char* NewObjectName)
: STObjectController(NewObjectName)
{
	CommonConstructor();
}

STList::STList()
: STObjectController()
{
	CommonConstructor();
}

void STList::CommonConstructor()
{
	STString TestClassName = GetClassName();
	if(TestClassName == "unknown") SetClassName("STList");

	//fprintf(stderr, "STList::STList(%s)\n", GetObjectName());
   // FirstCell = (NodePointer *)malloc(sizeof(NodePointer));
   FirstCell = new NodePointer;
   FirstCell->PointerToNextCell = NULL;
   FirstCell->PointerToPreviousCell = NULL;
   FirstCell->Item = NULL;
   LastCell = FirstCell;
   CurrentCell = FirstCell;
	CurrentIndex = 0;
   NodeCount = 0;
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "FirstCell @ 0x%08X\n", FirstCell);
#endif
}

STList::~STList()
{
   Clear();
   FirstCell->Item = NULL;
   delete FirstCell;
}

void STList::PrintCellInfo()
{
	fprintf(stderr, "NodeCount = %d\n", NodeCount);
	fprintf(stderr, "FirstCell @ 0x%08X\tLastCell @ 0x%08X\n", (unsigned int)FirstCell, (unsigned int)LastCell);
	fprintf(stderr, "CurrentCell @ 0x%08X\n", (unsigned int)CurrentCell);
	fprintf(stderr, "\t->Item = 0x%08X\n", (unsigned int)CurrentCell->Item);
	fprintf(stderr, "\t->PointerToPreviousCell = 0x%08X\n", (unsigned int)CurrentCell->PointerToPreviousCell);
	fprintf(stderr, "\t->PointerToNextCell = 0x%08X\n", (unsigned int)CurrentCell->PointerToNextCell);
}

unsigned int STList::Add(void *NewItemToAdd)
{
   NodePointer *NewCell;
	unsigned int PreviousIndex = NodeCount;
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "\n%s::Add(void *NewItemToAdd)\n", (const char*)ClassName);
#endif
   MoveToCell(NodeCount);
   CurrentCell->Item = NewItemToAdd;
   // NewCell = (NodePointer *)malloc(sizeof(NodePointer));
   NewCell = new NodePointer;
   if(NewCell != NULL)
   {
      NewCell->Item = NULL;
      NewCell->PointerToNextCell = NULL;
      NewCell->PointerToPreviousCell = CurrentCell;
      CurrentCell->PointerToNextCell = NewCell;
#ifdef DEBUG_MEMORY_TRACKING
	PrintCellInfo();
	fprintf(stderr, "NewCell @ 0x%08X\n", NewCell);
#endif
      CurrentCell = NewCell;
		CurrentIndex++;
      LastCell = NewCell;
      NodeCount++;
   }
	return(PreviousIndex);
}

unsigned int STList::Insert(unsigned int Position, void *NewItemToAdd)
{
	unsigned int NewIndex = Position;

	if(Position >= NodeCount)
	{
		NewIndex = Add(NewItemToAdd);
	} else
	{
		// create a new cell
		MoveToCell(Position);
	   NodePointer* NewCell = new NodePointer;
		if(NewCell != NULL)
		{
			NodePointer *CellBefore = CurrentCell->PointerToPreviousCell;
			NodePointer *CellAfter = CurrentCell;

			NewCell->PointerToPreviousCell = 0;
			// lift the previous pointer from the next cell, replace with new cell address
			if(CellBefore != 0)
			{
				NewCell->PointerToPreviousCell = CellBefore;
				CellBefore->PointerToNextCell = NewCell;
			} else
			{
				NewCell->PointerToPreviousCell = 0;
				FirstCell = NewCell;
			}
			// lift the next pointer from the preceding cell, replace with new cell address
			if(CellAfter != 0)
			{
				NewCell->PointerToNextCell = CellAfter;
				CellAfter->PointerToPreviousCell = NewCell;
			}

			// use next and previous cell addresses in new cell
			CurrentCell = NewCell;
			CurrentCell->Item = NewItemToAdd;
			NodeCount++;
		}
	}
	return(NewIndex);
}

unsigned int STList::Replace(unsigned int Index, void *NewItemToAdd)
{
   if(! MoveToCell(Index))
	{
   	CurrentCell->Item=NewItemToAdd;
	}
   return(NodeCount);
}

unsigned int STList::Count()
{
   return(NodeCount);
}

int STList::GetIndex(void *AddressOfData)
{
	int TempReturn, ALoopCounter;
	void *TestItem;

	TempReturn = -1;
	for(ALoopCounter = 0;ALoopCounter < (int)NodeCount;ALoopCounter++)
	{
		TestItem = Item(ALoopCounter);
		if(TestItem == AddressOfData)
		{
			TempReturn = ALoopCounter;
			break;
		}
	}
   return(TempReturn);
}

void STList::Delete(unsigned int CellToKill)
{
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "%s::Delete(%d)\n", (const char*)ClassName, CellToKill);
#endif
   bool RemoveNode;

   if((CellToKill >= 0)&&(CellToKill < NodeCount))
   {
      MoveToCell(CellToKill);
#ifdef DEBUG_MEMORY_TRACKING
	PrintCellInfo();
#endif
      if(CurrentCell != NULL)
      {
         RemoveNode = false;
         // Deleting first cell
         if((CellToKill == 0)&&(NodeCount > 0))
         {
            RemoveNode = true;
            FirstCell = CurrentCell->PointerToNextCell;
            FirstCell->PointerToPreviousCell = NULL;
            PreviousCell = FirstCell;
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "Deleting first cell; NodeCount = %d, CurrentIndex = %d\n", NodeCount, CurrentIndex);
	fprintf(stderr, "FirstCell = 0x%08X\n", FirstCell);
	fprintf(stderr, "\t->Item = 0x%08X\n", FirstCell->Item);
	fprintf(stderr, "\t->PointerToPreviousCell = 0x%08X\n", FirstCell->PointerToPreviousCell);
	fprintf(stderr, "\t->PointerToNextCell = 0x%08X\n", FirstCell->PointerToNextCell);
#endif
         } else
         // Deleting last cell
			// We do not use this because it must work the same as deleting a middle cell because LastCell is not part of the list.
			// LastCell is the pointer to the un-used cell that is at the end of the list and should not be touched here.
			// So that means that deleting the last cell is just like deleting a middle cell.
         /*if((CellToKill == NodeCount - 1)&&(NodeCount > 1))
         {
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "Deleting last cell; NodeCount = %d\n", NodeCount);
#endif
            RemoveNode = true;
            //LastCell = CurrentCell->PointerToPreviousCell;
            //PreviousCell = LastCell;
				//LastCell->PointerToNextCell = CurrentCell->PointerToNextCell;
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "LastCell = 0x%08X\n", LastCell);
	fprintf(stderr, "\t->Item = 0x%08X\n", LastCell->Item);
	fprintf(stderr, "\t->PointerToPreviousCell = 0x%08X\n", LastCell->PointerToPreviousCell);
	fprintf(stderr, "\t->PointerToNextCell = 0x%08X\n", LastCell->PointerToNextCell);
#endif
         } else */
         // Deleting middle cell,
         if((CellToKill > 0)&&(CellToKill < NodeCount))
         {
            RemoveNode = true;
            PreviousCell = CurrentCell->PointerToPreviousCell;
            PreviousCell->PointerToNextCell = CurrentCell->PointerToNextCell;
            NextCell = CurrentCell->PointerToNextCell;
            NextCell->PointerToPreviousCell = PreviousCell;
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "Deleting middle cell; CellToKill = %d, NodeCount = %d\n", CellToKill, NodeCount);
	fprintf(stderr, "PreviousCell = 0x%08X\n", PreviousCell);
	fprintf(stderr, "\t->Item = 0x%08X\n", PreviousCell->Item);
	fprintf(stderr, "\t->PointerToPreviousCell = 0x%08X\n", PreviousCell->PointerToPreviousCell);
	fprintf(stderr, "\t->PointerToNextCell = 0x%08X\n", PreviousCell->PointerToNextCell);

	fprintf(stderr, "NextCell = 0x%08X\n", NextCell);
	fprintf(stderr, "\t->Item = 0x%08X\n", NextCell->Item);
	fprintf(stderr, "\t->PointerToPreviousCell = 0x%08X\n", NextCell->PointerToPreviousCell);
	fprintf(stderr, "\t->PointerToNextCell = 0x%08X\n", NextCell->PointerToNextCell);
#endif
         }

         if(RemoveNode == true)
         {
            CurrentCell->Item = NULL;
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "delete Cell @ 0x%08X\t", CurrentCell);
#endif
            delete CurrentCell;
            CurrentCell = PreviousCell;
				if(CurrentIndex) CurrentIndex--;
            NodeCount--;
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "CurrentCell = 0x%08X\n", CurrentCell);
	fprintf(stderr, "\t->Item = 0x%08X\n", CurrentCell->Item);
	fprintf(stderr, "\t->PointerToPreviousCell = 0x%08X\n", CurrentCell->PointerToPreviousCell);
	fprintf(stderr, "\t->PointerToNextCell = 0x%08X\n", CurrentCell->PointerToNextCell);
#endif
         } else
         if((NodeCount == 1)&&(CellToKill == 0))
         {
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "Wipe first cell\n");
#endif
            NodeCount = 0;
            FirstCell->PointerToNextCell = NULL;
            FirstCell->PointerToPreviousCell = NULL;
            FirstCell->Item = NULL;
         }
         if(NodeCount == 0)
         {
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "Re build first cell\n");
#endif
            FirstCell->PointerToNextCell = NULL;
            FirstCell->PointerToPreviousCell = NULL;
            FirstCell->Item = NULL;
            LastCell = FirstCell;
            CurrentCell = FirstCell;
         }
      }
   } else
   {
      fprintf(stderr, "%s(%s)::Delete, Warning! Attempt to index %d ignored.\n", (const char*)ClassName, GetObjectName(), CellToKill);
		// throw;
   }
}

void STList::Clear()
{
   NodePointer *KillCell, *NextKillCell;

   if((FirstCell->PointerToNextCell != NULL)&&(NodeCount > 0))
   {
      KillCell = FirstCell;
      NextKillCell = KillCell->PointerToNextCell;
      if(KillCell->Item != NULL)
      {
         fprintf(stderr, "Warning: %s[%s]::Clear() You may not have freed the items in your list!\n", (const char*)ClassName, GetObjectName());
         fprintf(stderr, "You may have forgotten to NULL it after you freed it. It will be NULL now.\n");
      }
      KillCell->Item = NULL;
      delete KillCell;
      NodeCount--;
      KillCell = NextKillCell;
      if((KillCell != NULL) && (NodeCount >= 0))
      {
         while((KillCell->PointerToNextCell != NULL)&&(NodeCount >= 0))
         {
            NextKillCell = KillCell->PointerToNextCell;
            if(KillCell->Item != NULL)
            {
               fprintf(stderr, "Warning: %s[%s]::Clear() You may not have freed the items in your list!\n",(const char*)ClassName, GetObjectName());
               fprintf(stderr, "You may have forgotten to NULL it after you freed it. It will be NULL now.\n");
            }
            KillCell->Item = NULL;
            delete KillCell;
            NodeCount--;
            KillCell = NextKillCell;
         }
         if(KillCell->PointerToNextCell != NULL)
         {
            fprintf(stderr, "Warning: %s[%s]::Clear() Your empty list has a non-NULL value in the\n", (const char*)ClassName, GetObjectName());
            fprintf(stderr, "pointer to next cell. Something other then this list may have written to\n");
            fprintf(stderr, "this memory location. You may have a bug in your code.\n");
         }
         if(KillCell->Item != NULL)
         {
            fprintf(stderr, "Warning: %s[%s]::Clear() You may not have freed the items in your list!\n", (const char*)ClassName, GetObjectName());
            fprintf(stderr, "You may have forgotten to NULL it after you freed it. It will be NULL now.\n");
         }
         KillCell->Item = NULL;
         delete KillCell;
      }

		// This line has been out for a long time but valgrind is reporting that FirstCell was killed in loop above, and that looks to be true.
      // FirstCell = (NodePointer *)malloc(sizeof(NodePointer));
      FirstCell = new NodePointer;

      FirstCell->PointerToNextCell = NULL;
      FirstCell->PointerToPreviousCell = NULL;
      FirstCell->Item = NULL;
      LastCell = FirstCell;
      CurrentCell = FirstCell;
      NodeCount = 0;
   }
}

void *STList::Item(unsigned int ItemIndex)
{
   void *TempReturn = 0;

   // NodeCount is one more then the node indexes.
   // If there was 10 items
   // NodeCount 1 to 10
   // ItemIndex can be a valid index
   // If there was 10 items
   // ItemIndex 0 to 9
   if((ItemIndex >= 0)&&(ItemIndex < NodeCount))
   {
      TempReturn = NULL;
      MoveToCell(ItemIndex);
      if(CurrentCell != NULL)
      {
         TempReturn = CurrentCell->Item;
      }
   } else
   {
      fprintf(stderr, "STList(%s)::Item, Warning! Attempt to index %d ignored.\n", GetObjectName(), ItemIndex);
		// using throw does not help because C++ has no proper way to deal with exceptions gracefully.
		// throw;
   }
   return(TempReturn);
}

int STList::MoveToCell(unsigned int CellToGoto)
{
	int ReturnError;
   unsigned int FindCounter;
#ifdef DEBUG_MEMORY_TRACKING
	fprintf(stderr, "MoveToCell(%d); NodeCount = %d, CurrentIndex = %d\n", CellToGoto, NodeCount, CurrentIndex);
#endif

	ReturnError = -1;
   FindCounter = 0;
   // NodeCount is one more then the node indexes.
   // If there was 10 items
   // NodeCount 1 to 10
   // ItemIndex can be a valid index
   // If there was 10 items
   // ItemIndex 0 to 9
   // MoveToCell can go one node farther because there is a new blank node at index + 1
   if((CellToGoto >= 0)&&(CellToGoto <= NodeCount))
   {
		ReturnError = 0;
		if(CellToGoto == 0)
		{
			CurrentIndex = 0;
			CurrentCell = FirstCell;
		}
      if(CellToGoto < CurrentIndex)
		{
			while((CurrentCell->PointerToPreviousCell != NULL) && (CellToGoto < CurrentIndex))
			{
				CurrentCell = CurrentCell->PointerToPreviousCell;
				if(CurrentCell == NULL)
				{
					fprintf(stderr, "Error: %s::MoveToCell(%d) Attempting to move to non-existant cell!\n", (const char*)ClassName, CellToGoto);
					ReturnError = -1;
					CurrentIndex = 0;
					break;
				} else
				{
					CurrentIndex--;
				}
			}
		} else
		if(CellToGoto > CurrentIndex)
		{
			while((CurrentCell->PointerToNextCell != NULL) && (CellToGoto > CurrentIndex))
			{
				CurrentCell = CurrentCell->PointerToNextCell;
				if(CurrentCell == NULL)
				{
					fprintf(stderr, "Error: %s::MoveToCell(%d) Attempting to move to non-existant cell!\n", (const char*)ClassName, CellToGoto);
					ReturnError = -1;
					CurrentIndex = 0;
					break;
				} else
				{
					CurrentIndex++;
				}
			}
		}
   } else
   {
      fprintf(stderr, "%s(%s)::MoveToCell, Warning! Attempt to index %d ignored.\n", (const char*)ClassName, GetObjectName(), CellToGoto);
		// using throw does not help because C++ has no proper way to deal with exceptions gracefully.
		// throw;
   }
	return(ReturnError);
}

void STList::MoveTo(unsigned int SourceIndex, unsigned int DestIndex)
{
   fprintf(stderr, "%s::MoveTo, Not implemented yet!\n", (const char*)ClassName);
}
