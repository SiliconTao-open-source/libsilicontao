/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

#include <STSuperIni.h>
#include <STCommonCore.h>
#include <STStringList.h>
#include <STStringsEx.h>
#include <stdio.h>

bool STSuperIni::GetCheck(const STString& ValueName)
{
   bool TempReturn;
   STString BoolValue;
   STStringsEx StringX;

   if(DebugLevel > 2)
   {
      printf("STSuperIni::GetCheck\n");
   }
   TempReturn = false;
   BoolValue = GetValue(ValueName);

   StringX.Upper(BoolValue);
   if((BoolValue.Pos("T") == 0)||(BoolValue == "1")||(BoolValue == "ON"))
   {
      TempReturn = true;
   }

   return(TempReturn);
}

bool STSuperIni::SetDebugLevel(int NewDebugLevel)
{
   if(DebugLevel != NewDebugLevel)
   {
      DebugLevel = NewDebugLevel;
      //PortDialog->SetDebugLevel(DebugLevel);
      printf("STSuperIni::SetDebugLevel = %d\n",DebugLevel);
   }
   return(true);
}

STString STSuperIni::GetValue(const char *ValueName)
{
   STString TempString;

   TempString = ValueName;
   ReturnString = GetValue(TempString);
   return(ReturnString);
}

bool STSuperIni::ScanForString(const STString& ValueName)
{
   unsigned int ALoopCounter;
   STString LocalValueName,FieldName;
   STStringsEx StringX;
   ReturnString = "";
   LocalValueName = ValueName+" = ";
	for(ALoopCounter = 0; ALoopCounter < Count(); ALoopCounter++)
	{
		FieldName = Item(ALoopCounter);
		if(FieldName.Pos(LocalValueName)==0)
		{
			ReturnString = Item(ALoopCounter);
			ReturnString = StringX.Remove(ReturnString, 0,ReturnString.Pos('=')+1);
			ReturnString = StringX.StripChars(ReturnString,' ');
			return(true);
			break;
		}
	}
	return(false);
}

STString STSuperIni::GetValue(const STString& ValueName)
{
   ReturnString = "";
   if(SifFileName.Len() > 0)
   {
		bool DataFieldFound = false;

		if(Count() == 0)
		{
			LoadFromFile(SifFileName);
			DataFieldFound = ScanForString(ValueName);
		}
		if(DataFieldFound == false)
		{
			LoadFromFile(SifFileName);
			DataFieldFound = ScanForString(ValueName);
		}
   }
   return(ReturnString);
}

STString STSuperIni::GetValueDef(const char* ValueName, const char* DefaultValue)
{
   STString TempValueName, TempDefaultValue;

   TempValueName = ValueName;
   TempDefaultValue = DefaultValue;
   ReturnString = GetValueDef(TempValueName,TempDefaultValue);
   return(ReturnString);
}

STString STSuperIni::GetValueDef(const STString& ValueName, const char* DefaultValue)
{
   STString TempValueName, TempDefaultValue;

   TempValueName = ValueName;
   TempDefaultValue = DefaultValue;
   ReturnString = GetValueDef(TempValueName,TempDefaultValue);
   return(ReturnString);
}

STString STSuperIni::GetValueDef(const char* ValueName, const STString& DefaultValue)
{
   STString TempValueName, TempDefaultValue;

   TempValueName = ValueName;
   TempDefaultValue = DefaultValue;
   ReturnString = GetValueDef(TempValueName,TempDefaultValue);
   return(ReturnString);
}

STString STSuperIni::GetValueDef(const STString& ValueName, const STString& DefaultValue)
{
   STString PossibleResult;
   //int SuspendSubCallDebug;

   if(DebugLevel > 2)
   {
      printf("STSuperIni::GetValueDef\n");
   }
   //SuspendSubCallDebug = DebugLevel;
   //DebugLevel = 0;
   PossibleResult=GetValue(ValueName);
   if(PossibleResult == "")
   {
			SetValue(ValueName, DefaultValue);
      PossibleResult=DefaultValue;
   }
   ReturnString = PossibleResult;

   //DebugLevel = SuspendSubCallDebug;
   if(DebugLevel > 4)
   {
      printf("%s = %s\n",(const char *)ValueName,(const char *)ReturnString);
   }
   return(ReturnString);
}

void STSuperIni::SetCheck(const STString& ValueName, bool Value)
{
   //printf("STSuperIni::SetCheck is not yet working!\n");
   STString TempSend;
   int SuspendSubCallDebug;

   if(DebugLevel > 2)
   {
      printf("STSuperIni::SetCheck\n");
   }
   SuspendSubCallDebug = DebugLevel;
   DebugLevel = 0;
   if(Value == true)
   {
      TempSend = "True";
   } else
   {
      TempSend = "False";
   }
   SetValue(ValueName,TempSend);

   DebugLevel = SuspendSubCallDebug;
   if(DebugLevel > 4)
   {
      printf("%s = %s\n",(const char *)ValueName,(const char *)TempSend);
   }
}

void STSuperIni::SetValue(const char*ValueName, const char* Value)
{
   STString TempString1,TempString2;

   TempString1 = ValueName;
   TempString2 = Value;
   SetValue(TempString1,TempString2);
}

void STSuperIni::SetValue(const STString& ValueName, const char* Value)
{
   STString TempString1,TempString2;

   TempString1 = ValueName;
   TempString2 = Value;
   SetValue(TempString1,TempString2);
}

void STSuperIni::SetValue(const char* ValueName, const STString& Value)
{
   STString TempString1,TempString2;

   TempString1 = ValueName;
   TempString2 = Value;
   SetValue(TempString1,TempString2);
}

void STSuperIni::SetValue(const STString& ValueName, const STString& Value)
{
   unsigned int ALoopCounter;
   STString LocalValueName,FieldName,LocalValue;
   STStringsEx StringX;

   if(DebugLevel > 2)
   {
      printf("STSuperIni::SetValue\n");
   }

   LocalValue = StringX.StripChars(Value,' ');
   LocalValueName = ValueName+" = ";
   if(SifFileName.Len()>0)
   {
      LoadFromFile(SifFileName);
      for(ALoopCounter = 0;ALoopCounter < Count();ALoopCounter++)
      {
         FieldName = Item(ALoopCounter);
         if(FieldName.Pos(LocalValueName)==0)
         {
            Remove(ALoopCounter);
            ALoopCounter--;
         }
      }
      LocalValueName = ValueName+" = "+LocalValue;
      while(LocalValueName.Pos("\n") > -1)
      {
         if(DebugLevel > 4)
         {
            printf("Remove strange line feed.\n");
         }
         LocalValueName = StringX.Remove(LocalValueName, LocalValueName.Pos("\n"),1);
      }

      Add(LocalValueName);
      SaveToFile(SifFileName);
   }
   if(DebugLevel > 4)
   {
      printf("%s = %s\n",(const char *)ValueName,(const char *)LocalValue);
   }
}

STSuperIni::STSuperIni(const STString& PreSetFileName)
{
   InitSTSuperIni(PreSetFileName);
}

STSuperIni::STSuperIni(const char* PreSetFileName)
{
   STString QPreSetFileName;

   QPreSetFileName = PreSetFileName;
   InitSTSuperIni(QPreSetFileName);
}

void STSuperIni::InitSTSuperIni(const STString& PreSetFileName)
{
   DebugLevel = 0;
   SifFileName=PreSetFileName;
}

STSuperIni::~STSuperIni()
{

}
