
/*=============================================================================

    Copyright (C) 2001 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:
      - create CRC via table for faster CRC's

=============================================================================*/

#include <STCrc.h>

STCrc::STCrc()
{
   Crc16TableCreated = false;
   Crc16Polynomial = 0;

   Crc32TableCreated = false;
   Crc32Polynomial = 0;
}

STCrc::~STCrc()
{

}

WORD STCrc::GetCRC16(const char *Pointer2DataStream, DWORD DataSize, WORD Polynomial)
{
	DWORD ALoopCounter;
   WORD Remainder, CarryMaskBit, TempShitWord;
	BYTE BLoopCounter, NextByte, PolyBitCount;

   Crc16Polynomial = Polynomial;

   TempShitWord = Polynomial;
   for(BLoopCounter = 0;BLoopCounter < 16;BLoopCounter++)
   {
      if(TempShitWord & 0x0001)
      {
         PolyBitCount = BLoopCounter;
      }
      TempShitWord >>= 1;
   }
   CarryMaskBit = 1;
   CarryMaskBit <<= PolyBitCount;

	Remainder=Pointer2DataStream[0];
   Remainder <<= 8;
   Remainder|=(BYTE)Pointer2DataStream[1];

	for(ALoopCounter=2;ALoopCounter<DataSize+2;ALoopCounter++)
	{
      NextByte = 0;
      if(ALoopCounter < DataSize)
      {
         NextByte=Pointer2DataStream[ALoopCounter];
      }

		for(BLoopCounter=0;BLoopCounter<8;BLoopCounter++)
		{
			if(Remainder & CarryMaskBit)
			{
            Remainder=(Remainder ^ Polynomial);
         }

			Remainder=(Remainder << 1);
         Remainder &= 0xFFFE;
         if(NextByte & 0x80)
         {
            Remainder|=0x0001;
         }
         NextByte <<= 1;
         NextByte &= 0xFE;

         if((ALoopCounter == DataSize+1)&&(BLoopCounter == 6))
         {
            break;
         }
		}
	}
	Remainder&=0xFFFF;
	return(Remainder);
}

WORD STCrc::GetCRC16Fixed(const char *Pointer2DataStream, DWORD DataSize)
{
	DWORD ALoopCounter;
   WORD Remainder;
	BYTE BLoopCounter, NextByte;

   Crc16Polynomial = 0x8005;

	Remainder=Pointer2DataStream[0];
   Remainder <<= 8;
   Remainder|=(BYTE)Pointer2DataStream[1];

	for(ALoopCounter=2;ALoopCounter<DataSize+2;ALoopCounter++)
	{
      NextByte = 0;
      if(ALoopCounter < DataSize)
      {
         NextByte=Pointer2DataStream[ALoopCounter];
      }

		for(BLoopCounter=0;BLoopCounter<8;BLoopCounter++)
		{
			if(Remainder & 0x8000)
			{
            Remainder=(Remainder ^ 0x8005);
         }

			Remainder=(Remainder << 1);
         Remainder &= 0xFFFE;
         if(NextByte & 0x80)
         {
            Remainder|=0x0001;
         }
         NextByte <<= 1;
         NextByte &= 0xFE;

         if((ALoopCounter == DataSize+1)&&(BLoopCounter == 6))
         {
            break;
         }
		}
	}
	Remainder&=0xFFFF;
	return(Remainder);
}

WORD STCrc::GetCRC16Tabled(const char *Pointer2DataStream, DWORD DataSize, WORD Polynomial)
{
   Crc16Polynomial = Polynomial;
   VerifyCrc16Table(Polynomial);

   return(0);
}

WORD STCrc::GetCRC16Continued(const char *Pointer2DataStream, DWORD DataSize)
{
   VerifyCrc16Table(Crc16Polynomial);

   return(0);
}


DWORD STCrc::GetCRC32(const char *Pointer2DataStream, DWORD DataSize, DWORD Polynomial)
{
   Crc32Polynomial = Polynomial;

   return(0);
}


DWORD STCrc::GetCRC32Fixed(const char *Pointer2DataStream, DWORD DataSize)
{

   return(0);
}


DWORD STCrc::GetCRC32Tabled(const char *Pointer2DataStream, DWORD DataSize, DWORD Polynomial)
{
   Crc32Polynomial = Polynomial;
   VerifyCrc32Table(Polynomial);

   return(0);
}


DWORD STCrc::GetCRC32Continued(const char *Pointer2DataStream, DWORD DataSize)
{
   VerifyCrc32Table(Crc32Polynomial);

   return(0);
}

void STCrc::CreateCrc16Table(WORD NewPolynomial)
{

}

void STCrc::CreateCrc32Table(DWORD NewPolynomial)
{

}

void STCrc::VerifyCrc16Table(WORD NewPolynomial)
{
   Crc16Polynomial = NewPolynomial;
   if((Crc16Polynomial != Crc16TablePolynomial)||(Crc16TableCreated == false))
   {
      CreateCrc16Table(Crc16Polynomial);
      Crc16TablePolynomial = Crc16Polynomial;
      Crc16TableCreated = true;
   }
}

void STCrc::VerifyCrc32Table(DWORD NewPolynomial)
{
   Crc32Polynomial = NewPolynomial;
   if((Crc32Polynomial != Crc32TablePolynomial)||(Crc32TableCreated == false))
   {
      CreateCrc32Table(Crc32Polynomial);
      Crc32TablePolynomial = Crc32Polynomial;
      Crc32TableCreated = true;
   }
}
