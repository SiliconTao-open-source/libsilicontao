/*=============================================================================

    Copyright (C) 2005 Silicon Tao Technology Systems
    E-mail:  Support <support@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   File Name:					STCrc.h
   Object Name: 				STCrc
   Programmer Name:			Royce Souther
   By Command of:				Silicon Tao Technology Systems
   License:						GNU general public license. See GNU.
   Day Zero:					10152001, October 15, 2001
   Target Platform:			Linux
   Registers Used:
   Compiler Used:        	GNU g++
   Compiled Settings:
   Resources Used:
   Libraries Used:
   Ports Used:           	None
   Title of program:			Object library
   Description:				Provides CRC creation and testing of a data array.
=============================================================================*/

#ifndef STCrc_included
#define STCrc_included

#include <STCommonCore.h>

/**
 * Provides a CRC of a data array.
 *
 * @short CRC creation and testing object.
 */

class STCrc
{
   public:
      /**
       * Constructor
       */
		STCrc();

      /**
       * Destructor
       */
      ~STCrc();

      /**
       * Performs a 16 bit CRC calculation on a data stream of a given length.
       * Returns the CRC value as a WORD. You must also give a polynomial.
       */
      WORD GetCRC16(const char *Pointer2DataStream, DWORD DataSize, WORD Polynomial);

      /**
       * Performs a 16 bit CRC calculation on a data stream of a given length.
       * Returns the CRC value as a WORD. This function will use a fixed polynomial
       * of 0x8005 for it's calculations. This is faster then @ref GetCRC16
       */
      WORD GetCRC16Fixed(const char *Pointer2DataStream, DWORD DataSize);

      /**
       * Performs a 16 bit CRC calculation on a data stream of a given length.
       * Returns the CRC value as a WORD. You must also give a polynomial.
       * This generates a table the first time it is called, that is slow. Ever time
       * after that (as long as you don't change the polynomial) it will be faster
       * then @ref GetCRC16 or @ref GetCRC16Fixed
       */
      WORD GetCRC16Tabled(const char *Pointer2DataStream, DWORD DataSize, WORD Polynomial);

      /**
       * Continues a previously started 16 bit CRC calculation on a data stream of a
       * given length. Returns the CRC value as a WORD. It will use the previous polynomial.
       * It will use a table and generate it if needed. This is usefull if you have a
       * part of a data stream and are waiting to receive the rest and want to get started
       * on the CRC calculation, like a continues stream.
       */
      WORD GetCRC16Continued(const char *Pointer2DataStream, DWORD DataSize);

      /**
       * Performs a 32 bit CRC calculation on a data stream of a given length.
       * Returns the CRC value as a DWORD. You must also give a polynomial.
       */
      DWORD GetCRC32(const char *Pointer2DataStream, DWORD DataSize, DWORD Polynomial);

      /**
       * Performs a 32 bit CRC calculation on a data stream of a given length.
       * Returns the CRC value as a DWORD. This function will use a fixed polynomial
       * of 0x8005 for it's calculations. This is faster then @ref GetCRC32
       */
      DWORD GetCRC32Fixed(const char *Pointer2DataStream, DWORD DataSize);

      /**
       * Performs a 32 bit CRC calculation on a data stream of a given length.
       * Returns the CRC value as a DWORD. You must also give a polynomial.
       * This generates a table the first time it is called, that is slow. Ever time
       * after that (as long as you don't change the polynomial) it will be faster
       * then @ref GetCRC32 or @ref GetCRC32Fixed
       */
      DWORD GetCRC32Tabled(const char *Pointer2DataStream, DWORD DataSize, DWORD Polynomial);

      /**
       * Continues a previously started 32 bit CRC calculation on a data stream of a
       * given length. Returns the CRC value as a DWORD. It will use the previous polynomial.
       * It will use a table and generate it if needed. This is usefull if you have a
       * part of a data stream and are waiting to receive the rest and want to get started
       * on the CRC calculation, like a continues stream.
       */
      DWORD GetCRC32Continued(const char *Pointer2DataStream, DWORD DataSize);

   private:
      bool Crc16TableCreated;
      WORD Crc16Polynomial,Crc16TablePolynomial;
      char *Crc16Table;

      bool Crc32TableCreated;
      DWORD Crc32Polynomial,Crc32TablePolynomial;
      char *Crc32Table;

      void CreateCrc16Table(WORD NewPolynomial);
      void CreateCrc32Table(DWORD NewPolynomial);
      void VerifyCrc16Table(WORD NewPolynomial);
      void VerifyCrc32Table(DWORD NewPolynomial);
};

#endif // STCrc_included
