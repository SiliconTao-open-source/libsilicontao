/*=============================================================================

    Copyright (C) 2006 Silicon Tao Technology Systems Inc.
    E-mail:  Support <sales@SiliconTao.com>
    Web:     www.SiliconTao.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
=============================================================================*/

/*=============================================================================
   ToDo:

=============================================================================*/

/*=============================================================================
   Note:

=============================================================================*/

#include <STDriveOps.h>
#include <STStringList.h>
#include <STParserOps.h>
#include <STStringsEx.h>
#include <STObjectController.h>
#include <STTime.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

STDriveOps::STDriveOps()
: STObjectController()
{
   ParserOps1 = new STParserOps();
   Time1 = new STTime();
	PaternMatch = "";
   DefaultPermissions = 777;
   //ResetSearchPeramiters();
}

STDriveOps::~STDriveOps()
{
   delete ParserOps1;
   delete Time1;
}

void STDriveOps::SetMatch(const char* Patern)
{
	PaternMatch = Patern;
}

/*void STDriveOps::ResetSearchPeramiters()
{
   SearchPeramiters.FileAndPathMatch = "";

   SearchPeramiters.FileAndPathAvoid = "";
   SearchPeramiters.FileMatch = "";
   SearchPeramiters.FileAvoid = "";
   SearchPeramiters.FileExtentionMatch = "";
   SearchPeramiters.FileExtentionAvoid = "";
   SearchPeramiters.PathMatch = "";
   SearchPeramiters.PathAvoid = "";
   SearchPeramiters.DriveMatch = "";
   SearchPeramiters.DriveAvoid = "";

   SearchPeramiters.FileSizeMin = 0;
   SearchPeramiters.FileSizeMax = 0;
   SearchPeramiters.FileMinDate = 0;
   SearchPeramiters.FileMaxDate = 0;
   SearchPeramiters.FileAttributesMatch = 0;
   SearchPeramiters.FileAttributesAvoid = 0;

   // Many of these to not apply in Linux but I kept them
   // to be compatible with my Delphi app so I can port them.
   ReturnParts.IncludeDrive = false;
   ReturnParts.IncludePath = false;
   ReturnParts.IncludeFileName = false;
   ReturnParts.IncludeFileExtention = false;
   ReturnParts.IncludeFileSize = false;
   ReturnParts.IncludeFileDate = false;
   ReturnParts.IncludeFileAttibutes = false;
   ReturnParts.IncludePathSize = false;
   ReturnParts.IncludePathDate = false;
   ReturnParts.IncludeDirectoryAttributes = false;
   ReturnParts.IncludeFileAttributes = false;

}*/

double STDriveOps::GetFileMDate(const char* PathFileName)
{
   STString TempString;

   TempString = PathFileName;
   return(GetFileMDate(TempString));
}

double STDriveOps::GetFileMDate(const STString& PathFileName)
{
   double TempReturn;
   struct stat StatPointer; 	// Pointer to entery stats
   STTime TimeConvert;

   TempReturn = 0;

   if(FileExists(PathFileName) == true)
   {
      if(stat(PathFileName, &StatPointer)==0)
      {
         TempReturn = TimeConvert.TimeInFracOfDay(StatPointer.st_mtime);
         TempReturn += TimeConvert.DaysCount(StatPointer.st_mtime);
      } else
		{
			fprintf(stderr, "STDriveOps::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
		}
   }

   return(TempReturn);
}

const char* STDriveOps::ExtractFileName(const char* PathFileName)
{
   STString TheCharSting;

   TheCharSting = PathFileName;
   ReturnString = ExtractFileName(TheCharSting);
   return(TheCharSting);
}

const char* STDriveOps::ExtractFileName(const STString& PathFileName)
{
   STString ParsingString;
   STStringsEx Stringx;

   // DebugMessage(2, InLineDbStr.Sprintf("ExtractFileName(%s)\n",(const char*)PathFileName));

   ParsingString = PathFileName;
   if(ParsingString.Pos("/") > -1)
   {
      ParsingString = Stringx.Remove(ParsingString,0,Stringx.LastPos("/",ParsingString)+1);
   }
   ReturnString = ParsingString;
   return(ReturnString);
}

bool STDriveOps::IsFileOpen(const char* FileName)
{
	STString TempFileName = FileName;
	return(IsFileOpen(TempFileName));
}

bool STDriveOps::ValidFileName(const STString& FileName)
{
	return(ValidFileName((const char*)FileName));
}

bool STDriveOps::ValidFileName(const char* FileName)
{
	STString StripTest = FileName;
	STString ValidChars;
	for(char L = 'A'; L <= 'Z'; L++)
	{
		ValidChars += L;
	}
	for(char L = 'a'; L <= 'z'; L++)
	{
		ValidChars += L;
	}
	for(char L = '0'; L <= '9'; L++)
	{
		ValidChars += L;
	}
	ValidChars += "_-,.~+=^#@[]{}%";

	bool TempReturn = false;
	char TestChar;
	if(StripTest.Len() > 0)
	{
		TempReturn = true;
		while(StripTest.Len())
		{
			TestChar = StripTest[0];
			StripTest.Remove(0, 1);
			if(ValidChars.Pos(TestChar) == -1)
			{
				TempReturn = false;
				printf("ValidFileName('%s') is not a valid filename, '%c'=%d is not allowed\n", FileName, TestChar, TestChar);
				return(TempReturn);
			}
		}
	} else
	{
		printf("ValidFileName() is not a valid filename, it is to short\n");
		return(TempReturn);
	}

	printf("ValidFileName('%s') is a valid filename\n", FileName);
	return(TempReturn);
}

bool STDriveOps::IsFileOpen(const STString& FileName)
{
	bool FoundFile = false;
	STString TargetFile = ResolveLink(FileName);

	if(FileExists(TargetFile))
	{
		STString OriginalDir = GetDir();
		STString PathAndDirEntry, FdLink;
		struct stat StatPointer;
		STStringList MapReader;

		DIR *DirPointer;
		struct dirent *DirEnt;

		DIR *FileDiscriptorsForPid;
		struct dirent *FileDiscriptorEntry;

		DirPointer = opendir("/proc");
		if(DirPointer)
		{
			DirEnt = readdir(DirPointer);

			while((DirEnt != NULL) && (FoundFile == false))
			{
				// We only want PID directories and they are numbers that do not start with 0
				if((DirEnt->d_name[0] >= '1') && (DirEnt->d_name[0] <= '9'))
				{
					PathAndDirEntry = "/proc/";
					PathAndDirEntry += DirEnt->d_name;
   				if(stat((const char*)PathAndDirEntry, &StatPointer) == 0)
					{
      				if(S_ISDIR(StatPointer.st_mode) && (! S_ISLNK(StatPointer.st_mode)))
						{
							// Check if listed in the maps file
							PathAndDirEntry += "/maps";
							MapReader.LoadFromFile(	PathAndDirEntry );
							while(MapReader.Count() > 0)
							{
								if(TargetFile == MapReader.Item(0))
								{
									FoundFile = true;
									MapReader.Clear();
								} else
								{
									MapReader.Delete(0);
								}
							}

							// Check if open in a file handle
							PathAndDirEntry = "/proc/";
							PathAndDirEntry += DirEnt->d_name;
							PathAndDirEntry += "/fd";

							FileDiscriptorsForPid = opendir(PathAndDirEntry);
							if(FileDiscriptorsForPid)
							{
								PathAndDirEntry += "/";
								FileDiscriptorEntry = readdir(FileDiscriptorsForPid);
								while((FileDiscriptorEntry != NULL) && (FoundFile == false))
								{
									FdLink = PathAndDirEntry + FileDiscriptorEntry->d_name;
									if(TargetFile == ResolveLink(FdLink))
									{
										FoundFile = true;
									}
									FileDiscriptorEntry = readdir(FileDiscriptorsForPid);
								}
								closedir(FileDiscriptorsForPid);
							} else
							{
								fprintf(stderr, "STDriveOps::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
							}
						}
					} else
					{
						fprintf(stderr, "STDriveOps::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
					}
				}
				DirEnt = readdir(DirPointer);
			}
			closedir(DirPointer);
		} else
		{
			fprintf(stderr, "STDriveOps::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
		}
	}
	return(FoundFile);
}

const char* STDriveOps::BaseName(const STString& TargetObject)
{
	STString ChewItUp = TargetObject;
	while(ChewItUp.Pos("/") > -1)
	{
		ChewItUp.Remove(0, ChewItUp.Pos("/") + 1);
	}
	ReturnString = ChewItUp;
	return(ReturnString);
}

const char* STDriveOps::BaseName(const char* TargetObject)
{
	STString TargetStr = TargetObject;
	return(BaseName(TargetStr));
}

const char* STDriveOps::DirName(const STString& TargetObject)
{
	int LastPos = 0;
	STString ChewItUp = TargetObject;
	while(ChewItUp.Pos("/", LastPos) > -1)
	{
		LastPos = ChewItUp.Pos("/", LastPos) + 1;
	}
	ReturnString = ChewItUp.Left(LastPos);
	return(ReturnString);
}

const char* STDriveOps::DirName(const char* TargetObject)
{
	STString TargetStr = TargetObject;
	return(DirName(TargetStr));
}

const char* STDriveOps::ResolveLink(const STString& LinkPath)
{
	STString PointsTo;
	bool FoundObject = false;
	struct stat StatPointer;
	char *GrowBuffer;
	int BufferSize = 256;
	int ResultSize = 0;
	STString OriginalDir = GetDir();

	chdir(DirName(LinkPath));

	PointsTo = BaseName(LinkPath);
	while((PointsTo.Len() > 0) && (FoundObject == false))
	{
		if(lstat((const char*)PointsTo, &StatPointer) == 0)
		{
			if(S_ISLNK(StatPointer.st_mode))
			{
				// GrowBuffer = (char*)malloc(BufferSize + 1);
				GrowBuffer = new char[BufferSize + 1];
				ResultSize = readlink((const char*)PointsTo, GrowBuffer, BufferSize);
				while(ResultSize == BufferSize)
				{
					delete GrowBuffer;
					BufferSize += 256;
					// GrowBuffer = (char*)malloc(BufferSize + 1);
					GrowBuffer = new char[BufferSize + 1];
					ResultSize = readlink((const char*)PointsTo, GrowBuffer, BufferSize);
				}
				GrowBuffer[ResultSize] = 0;
				PointsTo = GrowBuffer;
				chdir(DirName(PointsTo));
				PointsTo = BaseName(PointsTo);
			} else
			{
				FoundObject = true;
			}
		} else
		{
			PointsTo = "";
		}
	}
	ReturnString = "";
	if(PointsTo.Len() > 0)
	{
		ReturnString = GetDir();
		ReturnString += PointsTo;
	}
	chdir(OriginalDir);
	return(ReturnString);
}

const char* STDriveOps::ResolveLink(const char* LinkPath)
{
	STString LinkPathStr = LinkPath;
   return(ResolveLink(LinkPathStr));
}

const char* STDriveOps::ExtractPath(const char* ProvidedPathFile)
{
   STString TempString;

   TempString = ProvidedPathFile;
   ReturnString = ExtractPath(TempString);
   return(ReturnString);
}

const char* STDriveOps::ExtractPath(const STString& ProvidedPathFile)
{
   STString ParsingString;
   STStringsEx Stringx;

   // DebugMessage(2, InLineDbStr.Sprintf("ExtractPath(%s)\n",(const char*)ProvidedPathFile));

   ParsingString = ProvidedPathFile;
   if(ParsingString.Pos("/") > -1)
   {
      ParsingString = Stringx.Remove(ParsingString,Stringx.LastPos("/",ParsingString),ParsingString.Len()+1);
   }
   ReturnString = ParsingString;
   return(ReturnString);
}

bool STDriveOps::FileExists(const char* PathFileName)
{
   STString TheStringToTest;

   TheStringToTest = PathFileName;
   return(FileExists(TheStringToTest));
}

bool STDriveOps::FileExists(const STString& PathFileName)
{
   struct stat StatPointer; 	// Pointer to entery stats
   bool TempReturn;

   // DebugMessage(2, InLineDbStr.Sprintf("FileExists(%s)\n",(const char*)PathFileName));

   TempReturn = false;
	//printf("STDriveOps::FileExists('%s')\n", (const char*)PathFileName);
   if(stat(PathFileName, &StatPointer)==0)
   {
		//printf("STDriveOps::FileExists stat ok\n");
      if(S_ISREG(StatPointer.st_mode))
      {
			//printf("STDriveOps::FileExists is true\n");
         TempReturn = true;
      }
   } else
	{
		// Just to let you know this is the way it should work. Do not print an error here.
		//printf("STDriveOps::FileExists is false\n");
		TempReturn = false;
	}
   return(TempReturn);
}

bool STDriveOps::DirExists(const char* PathFileName)
{
   STString TheStringToTest;

   TheStringToTest = PathFileName;
   return(DirExists(TheStringToTest));
}

bool STDriveOps::DirExists(const STString& PathFileName)
{
   struct stat StatPointer; 	// Pointer to entery stats
   bool TempReturn = false;
   int ErrorCode;

   ErrorCode = stat((const char*)PathFileName, &StatPointer);
   if(ErrorCode == 0)
   {
      if(S_ISDIR(StatPointer.st_mode))
      {
         if(! S_ISLNK(StatPointer.st_mode))
         {
            TempReturn = true;
         }
      }
   }
   return(TempReturn);
}

bool STDriveOps::MakePath(const char* PathToCreate)
{
   STString TempPathName;

   TempPathName = PathToCreate;
   return(MakePath(TempPathName));
}

bool STDriveOps::MakePath(const STString& PathToCreate)
{
   STString NewSub,PathToMake,FullPath;
   bool Sucessfull;
   int Permissions, OwnerPermissions, GroupPermissions, OtherPermissions, CarryOver;
   STStringsEx StringX;

   OtherPermissions = DefaultPermissions % 10;
   CarryOver = (int)DefaultPermissions / 10;

   GroupPermissions = CarryOver % 10;
   CarryOver = (int)CarryOver / 10;

   OwnerPermissions = CarryOver;

   Permissions = OwnerPermissions;
   Permissions <<= 4;
   Permissions += GroupPermissions;
   Permissions <<= 4;
   Permissions += OtherPermissions;

   Sucessfull = true;
   PathToMake = PathToCreate;
   if(PathToMake[PathToMake.Len()-1] != '/')
   {
      PathToMake += "/";
   }
   FullPath = "";
   while(PathToMake != "")
   {
      NewSub = PathToMake;
      if(NewSub.Pos("/") > -1)
      {
         NewSub = StringX.Remove(NewSub, NewSub.Pos("/"),NewSub.Len());
         PathToMake = StringX.Remove(PathToMake,0,PathToMake.Pos("/")+1);
      }
      FullPath += "/"+NewSub;
      while(FullPath.Pos("//") > -1)
      {
         FullPath = StringX.Remove(FullPath,FullPath.Pos("//"),1);
      }
      if(FullPath.Len() > 200)
      {
         printf("STDriveOps::MakePath: Warning possible infinite recursion loop!\n");
         printf("New path creation is delayed by 5 seconds.\n");
         sleep(5);
      }
      // DebugMessage(2, InLineDbStr.Sprintf(" -MakePath mkdir = %s\n",(const char *)FullPath));
      // DebugMessage(2, InLineDbStr.Sprintf("  with permissions of %d\n",Permissions));

      if((FileExists(FullPath) == false)&&(DirExists(FullPath) == false))
      {
			// DebugMessage(2, InLineDbStr.Sprintf("mkdir(\"%s\",%d)",(const char*)FullPath,Permissions));
         mkdir(FullPath,511); //Permissions);
      } else
      {
			//printf("what is this = %s\n",(const char *)FullPath);
         if(FileExists(FullPath) == true)
         {
            Sucessfull = false;
            break;
         }
      }
   }
   return(Sucessfull);
}

const char* STDriveOps::GetDir()
{
   unsigned int PathNameSize;
   bool BufferIsLargeEnough;
   int BufferIncroments;
   char *BufferPointer;

   PathNameSize = 0;
   BufferPointer = NULL;
   BufferIncroments = 1024;
   ReturnString = "";
   BufferIsLargeEnough = false;
   while(BufferIsLargeEnough == false)
   {
      if(BufferPointer != NULL)
      {
         delete[] BufferPointer;
         BufferPointer = NULL;
      }
      PathNameSize += BufferIncroments;
      // BufferPointer = (char *)malloc(PathNameSize);
      BufferPointer = new char[PathNameSize];
      getcwd(BufferPointer,PathNameSize);

      if(strlen(BufferPointer) < PathNameSize - 2)
      {
         BufferIsLargeEnough = true;
      }
   }
   ReturnString = BufferPointer;

   if(ReturnString != "")
   {
      if(ReturnString[ReturnString.Len()-1] != '/')
      {
         ReturnString+="/";
      }
   }

   if(BufferPointer != NULL) delete[] BufferPointer;
   return(ReturnString);
}

void STDriveOps::RecurseFiles(STStringList* FilesStringList, int LevelOfRecurtion, const STString& DirectoryToStartIn)
{
   STString OrininalDir;

   FilesStringList->Clear();
   OrininalDir = GetDir();

   if((DirectoryToStartIn != "") && (DirExists(DirectoryToStartIn) == true))
   {
      if(chdir(DirectoryToStartIn) == 0)
      {
         RecurseFiles(FilesStringList, LevelOfRecurtion);
      } else
      {
         fprintf(stderr, "Cannot change to %s directroy, permission denied.\n",(const char*)DirectoryToStartIn);
      }
      chdir(OrininalDir);
   }
}

void STDriveOps::RecurseFiles(STStringList* FilesStringList, int LevelOfRecurtion, const char *DirectoryToStartIn)
{
   STString DirToStartInString;

   DirToStartInString = DirectoryToStartIn;
   RecurseFiles(FilesStringList,LevelOfRecurtion,DirToStartInString);
}

void STDriveOps::RecurseFiles(STStringList* FilesStringList, int LevelOfRecurtion)
{
   STStringList *DirStringList, *AppendFilesList;
   STString CurrentDirName,AppendString,PathToFile;
   STString FullPathAndFile, SizeString;

   CurrentDirName = GetDir();

   DirStringList = new STStringList();
   AppendFilesList = new STStringList();
   FilesStringList->Clear();

   if(LevelOfRecurtion != 1)
   {
      RecursePath(DirStringList,LevelOfRecurtion-1);
   }

   DirStringList->Add(CurrentDirName);

   while(DirStringList->Count() > 0)
   {
      PathToFile = DirStringList->Item(0);
      if(chdir(PathToFile) == 0)
      {
         // Debugline
         // DebugMessage(2, InLineDbStr.Sprintf("chdir(%s)\n",(const char*)PathToFile));

         GetFiles(AppendFilesList);

         // DebugMessage(2, InLineDbStr.Sprintf("AppendFilesList->Count() = %d\n",AppendFilesList->Count()));

         while(AppendFilesList->Count() > 0)
         {
            FullPathAndFile = PathToFile;
            FullPathAndFile += AppendFilesList->Item(0);
            AppendString = "";
            if(ReturnParts.IncludePath == true)
            {
               AppendString = PathToFile;
            }

            AppendString += AppendFilesList->Item(0);

            /*if(ReturnParts.IncludeFileSize == true)
            {
               SizeString.Sprintf("%d",FileSize(FullPathAndFile));
               AppendString +=":"+SizeString;
            }*/
            /*if(ReturnParts.IncludeFileDate == true)
            {
               AppendString +=":"+Time1->DateTime2String(GetFileMDate(FullPathAndFile),0);
            } */
            FilesStringList->Add(AppendString);

            AppendFilesList->Delete(0);
         }
         DirStringList->Delete(0);
      }
   }
   chdir(CurrentDirName);
   delete DirStringList;
   delete AppendFilesList;
}

const char* STDriveOps::GetParentDir(const STString& ChildPath)
{
   STString EditingString,UpSearchPath;
   int PresumedParent;
   bool DoneUpSearching;
   STStringsEx StringX;

   // Absolute path
   if(EditingString.Pos("/") == 0)
   {
      if(EditingString[EditingString.Len()-1] == '/')
      {
         EditingString = StringX.Remove(EditingString,EditingString.Len()-1,1);
      }
      if(EditingString.Len() > 0)
      {
         EditingString = StringX.Remove(EditingString, EditingString.Pos("/"),EditingString.Len());
      }
      if(EditingString.Len() == 0)
      {
         EditingString = "/";
      }
      ReturnString = EditingString;
   } else
   // If the ChildPath starts with ../
   if(EditingString.Pos("../") == 0)
   {
      PresumedParent = 0;
      EditingString = ChildPath;
      while(EditingString.Pos("../") == 0)
      {
         PresumedParent++;
      }
      UpSearchPath = GetDir();
      DoneUpSearching = false;
      while(DoneUpSearching == false)
      {
         if((UpSearchPath.Len() > 0)&&(PresumedParent > 0))
         {
            UpSearchPath = StringX.Remove(UpSearchPath, UpSearchPath.Pos("/"),UpSearchPath.Len());
            PresumedParent--;
         } else
         {
            DoneUpSearching = true;
         }
      }
      ReturnString = UpSearchPath;
   } else
   // If the ChildPath starts with ./
   if(EditingString.Pos("./") == 0)
   {
      EditingString = GetDir();
      if(EditingString[EditingString.Len()-1] == '/')
      {
         EditingString = StringX.Remove(EditingString, EditingString.Len()-1,1);
      }
      if(EditingString.Pos("/") > 0)
      {
         EditingString = StringX.Remove(EditingString, EditingString.Pos("/"),EditingString.Len());
      }
      ReturnString = EditingString;
   } else
   // Assumed the path is a sub of the current, the parent of the sub would be the current.
   {
      ReturnString = GetDir();
   }
   return(ReturnString);
}

void STDriveOps::GetDirectories(STStringList* DirectoryList)
{
   STString CurrentDir;

   CurrentDir = GetDir();
   GetDirectories(DirectoryList,CurrentDir);
}

void STDriveOps::GetDirectories(STStringList *DirectoryList, const STString& StartingDir)
{
   DIR *DirPointer;  			// Poniter to directory informoation.
   struct dirent *DirEnt; 		// Structure of directory enteries
   struct stat StatPointer; 	// Pointer to entery stats
   STString DirectoryName;
   STString StartHereDir;
   STString DirPrefix,FullEnteryName;
   int StatError;
   STStringsEx StringX;

   DirectoryList->Clear();

   StartHereDir = StartingDir;

   DirPrefix = StartHereDir;
   if(StartHereDir.Pos("./") != 0)
   {
      while(StartHereDir.Pos("../") == 0)
      {
         DirPrefix = GetParentDir(DirPrefix);
         StartHereDir = StringX.Remove(StartHereDir,0,3);
      }
   }
   //printf("StartHereDir=%s\n",(const char *)StartHereDir);
   //chdir(StartHereDir);
   DirPointer = opendir(StartHereDir);
   if(DirPointer)
   {
      DirEnt = readdir(DirPointer);

      while(DirEnt != NULL)
      {
         //printf("%s\n",DirEnt->d_name);

         if((strcmp(DirEnt->d_name, ".") != 0) && (strcmp(DirEnt->d_name, "..") != 0))
         {
            FullEnteryName = DirPrefix+DirEnt->d_name;
            StatError = stat(FullEnteryName, &StatPointer);
            if(StatError==0)
            {
               if(S_ISDIR(StatPointer.st_mode))
               {
                  if(! S_ISLNK(StatPointer.st_mode))
                  {
                     DirectoryName = DirEnt->d_name;
                     if(DirectoryName[DirectoryName.Len()-1] != '/')
                     {
                        DirectoryName+="/";
                     }
                     if(FullEnteryName.Pos(DirPrefix) > -1)
                     {
                        FullEnteryName = StringX.Remove(FullEnteryName, 0,DirPrefix.Len());
                     }
                     //printf("adding %s\n",(const char *)FullEnteryName);

                     DirectoryList->Add(DirectoryName);
                  }
               }
            } else
				{
					fprintf(stderr, "STDriveOps::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
				}
         }
         DirEnt = readdir(DirPointer);
      }
      closedir(DirPointer);
   } else
	{
		fprintf(stderr, "STDriveOps::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
	}
}

int STDriveOps::KillDir(const STString& DirToDelete)
{
   STString OringinalDir;
   unsigned int ALoopCounter;
   int TempReturn;
   STStringList TheKillList;
   STString KillThisObject;

   OringinalDir = GetDir();
   TempReturn = 0;

   // Empty the dir of all files and files in sub directories.
   //ResetSearchPeramiters();
   ReturnParts.IncludePath = true;
   RecurseFiles(&TheKillList,0,DirToDelete);
   for(ALoopCounter=0;ALoopCounter<TheKillList.Count();ALoopCounter++)
   {
      KillThisObject = TheKillList.Item(ALoopCounter);
      TempReturn = KillFile(KillThisObject);
      if(TempReturn != 0)
      {
         printf("STDriveOps::KillDir: could not delete %s\n",(const char *)KillThisObject);
         break;
      }
   }

   // Empty the dir of all sub directories.
   if(TempReturn == 0)
   {
      TheKillList.Clear();
      //ResetSearchPeramiters();
      ReturnParts.IncludePath = true;
      RecursePath(&TheKillList,0,DirToDelete);
      for(ALoopCounter=TheKillList.Count();ALoopCounter>0;ALoopCounter--)
      {
         KillThisObject = TheKillList.Item(ALoopCounter-1);
         TempReturn = rmdir(KillThisObject);
         if(TempReturn != 0)
         {
            printf("STDriveOps::KillDir: could not delete %s\n",(const char *)KillThisObject);
            break;
         }
      }
   }

   // Finaly remove the dir that we were asked to.
   if(TempReturn == 0)
   {
      TempReturn = rmdir(DirToDelete);
      if(TempReturn != 0)
      {
         printf("STDriveOps::KillDir: could not delete %s\n",(const char *)KillThisObject);
      }
   }

   return(TempReturn);
}

int STDriveOps::KillDir(const char* DirToDelete)
{
   STString DirString;

   DirString = DirToDelete;
   return(KillDir(DirString));
}

int STDriveOps::KillFile(const STString& FileToDelete)
{
   char *TempChars;
   int TempReturn;

   // TempChars = (char *)malloc(FileToDelete.Len()+1);
   TempChars = new char[FileToDelete.Len()+1];
   strcpy(TempChars,FileToDelete);
   TempChars[FileToDelete.Len()]='\0';
   TempReturn = KillFile(TempChars);
   delete[] TempChars;

   return(TempReturn);
}

int STDriveOps::KillFile(const char *FileToDelete)
{
   int ReturnError;

   // DebugMessage(2, InLineDbStr.Sprintf("remove %s\n",FileToDelete));

   ReturnError = remove(FileToDelete);
   if(ReturnError != 0)
   {
		//STString WhatErrorToReport;
		//WhatErrorToReport.Sprintf("KillFile(%s)", FileToDelete);
      // FileIOErrorCodes(WhatErrorToReport);
		fprintf(stderr, "STDriveOps::KillFile line %d errno=%d '%s'\n", __LINE__, errno, strerror(errno));
   }
   // DebugMessage(1, InLineDbStr.Sprintf("ReturnError = %d\n",ReturnError));

   return(ReturnError);
}

void STDriveOps::GetFiles(STStringList *FilesList)
{
   STString CurrentDir;

   CurrentDir = GetDir();
   GetFiles(FilesList,CurrentDir);
}

void STDriveOps::GetFiles(STStringList *FilesList, const STString& StartingDir)
{
   DIR *DirPointer;  			// Poniter to directory informoation.
   struct dirent *DirEnt; 		// Structure of directory enteries
   struct stat StatPointer; 	// Pointer to entery stats
   STString FileName;
   bool FileIsQualified;
   STString DirPrefix,FullEnteryName;
   STString StartHereDir;
   int StatError;
   STStringsEx StringX;

   // DebugMessage(2, InLineDbStr.Sprintf("GetFiles(STStringList* , '%s'\n",(const char*)StartingDir));

   FilesList->Clear();

   // ParserOps1->SetDebugLevel(GetDebugLevel());

   StartHereDir = StartingDir;

   if(StartHereDir[StartHereDir.Len()-1] != '\0')
   {
      StartHereDir += "\0";
   }

   DirPrefix = StartHereDir;
   if(StartHereDir.Pos("./") > -1)
   {
      DirPrefix = "";
      while(StartHereDir.Pos("../") == 0)
      {
         DirPrefix = GetParentDir(DirPrefix);
         StartHereDir = StringX.Remove(StartHereDir, 0,3);
      }
   }
   // DebugMessage(2, InLineDbStr.Sprintf("DirPrefix = '%s'\n",(const char*)DirPrefix));

	if(DirPrefix.Pos("/", -1) != (int)DirPrefix.Len() - 1)
	{
		DirPrefix += "/";
	}
   // DebugMessage(2, InLineDbStr.Sprintf("Looking in '%s'\n",(const char*)StartingDir));

   DirPointer = opendir(StartHereDir);
   if(DirPointer)
   {
      DirEnt = readdir(DirPointer);

      while(DirEnt != NULL)
      {
         if((strcmp(DirEnt->d_name, ".") != 0) && (strcmp(DirEnt->d_name, "..") != 0))
         {
            FullEnteryName = DirPrefix+DirEnt->d_name;
            StatError = stat(FullEnteryName, &StatPointer);
            if(StatError != 0)
            {
					fprintf(stderr, "STDriveOps::%s line %d errno=%d '%s'\n", __FUNCTION__, __LINE__, errno, strerror(errno));
			return;
            } else
            {
               if(S_ISREG(StatPointer.st_mode))
               {
                  if(! S_ISLNK(StatPointer.st_mode))
                  {
                     FileName = DirEnt->d_name;
                     FileIsQualified = true;
							if(PaternMatch != "")
                     {
                        FileIsQualified = false;

                        if(ParserOps1->WildCardCompairStrings(PaternMatch, FileName, false) == true)
                        {
                           FileIsQualified = true;
                        }
                     }

                     if(FileIsQualified == true)
                     {
                        FilesList->Add(FileName);
                     }
                  }
               }
            }
         }
         DirEnt = readdir(DirPointer);
      }
      closedir(DirPointer);
   } else
	{
		fprintf(stderr, "STDriveOps::GetFiles line %d errno=%d '%s'\n", __LINE__, errno, strerror(errno));
	}
}

void STDriveOps::RecursePath(STStringList* PathStringList, int LevelOfRecurtion, const STString& DirectoryToStartIn)
{
   STString OrininalDir;

   // DebugMessage(2, InLineDbStr.Sprintf("RecursePath() in %s\n",(const char*)DirectoryToStartIn));


   PathStringList->Clear();
   OrininalDir = GetDir();
   if((DirectoryToStartIn != "") && (DirExists(DirectoryToStartIn) == true))
   {
      if(chdir(DirectoryToStartIn) == 0)
      {
         RecursePath(PathStringList,LevelOfRecurtion);
      }
      chdir(OrininalDir);
   }
}

void STDriveOps::RecursePath(STStringList* PathStringList, int LevelOfRecurtion, const char* DirectoryToStartIn)
{
   STString DirToStartInString;

   DirToStartInString = DirectoryToStartIn;
   RecursePath(PathStringList,LevelOfRecurtion,DirToStartInString);
}

void STDriveOps::RecursePath(STStringList* PathStringList, int LevelOfRecurtion)
{
   STString CurrentDirName;
   STString ASubDirName;
   unsigned int ALoopCounter; //, ErrorCode;
   STStringList *SubDirectories;

   CurrentDirName = GetDir();

   // DebugMessage(2, InLineDbStr.Sprintf("CurrentDirName = %s\n",(const char*)CurrentDirName));

   // Directory Enteries
   SubDirectories = new STStringList();
   GetDirectories(SubDirectories);
   //printf("SubDirectories->Count() = %d\n",SubDirectories->Count());

   for(ALoopCounter = 0;ALoopCounter < SubDirectories->Count();ALoopCounter++)
   {
      ASubDirName = SubDirectories->Item(ALoopCounter);
      PathStringList->Add(CurrentDirName+ASubDirName);

      if(LevelOfRecurtion != 1)
      {
         if(chdir(ASubDirName) == 0)
         {
            RecursePath(PathStringList,LevelOfRecurtion-1);
         }
         chdir(CurrentDirName);
      }
   }
   chdir(CurrentDirName);
   delete SubDirectories;
}

int STDriveOps::FileSize(const STString& FilePathName)
{
   int ReturnValue, SucessTest;
   struct stat StatusBuffer;

   // DebugMessage(2, InLineDbStr.Sprintf("STDriveOps::FileSize(%s)\n",(const char *)FilePathName));


   ReturnValue = -1;
   SucessTest = lstat(FilePathName,&StatusBuffer);
   if(SucessTest > -1)
   {
      ReturnValue = StatusBuffer.st_size;
   } else
	{
		fprintf(stderr, "STDriveOps::FileSize line %d errno=%d '%s'\n", __LINE__, errno, strerror(errno));
	}
   // DebugMessage(2, InLineDbStr.Sprintf("ReturnValue = %d\n",ReturnValue));

   return(ReturnValue);
}


const char* STDriveOps::StripParentPath(const STString& OriginalPath)
{
	STString WorkStr = OriginalPath;
	if(WorkStr.Len() > 0)
	{
		if(WorkStr.Pos("/") > -1)
		{
			WorkStr.Remove(WorkStr.Pos("/"), WorkStr.Len());
		} else
		{
			WorkStr = "";
		}
	}
	//printf("PrePath = %s",(const char*)PrePath);
	if(WorkStr.Len() == 0)
	{
		WorkStr = "/";
	}
	//printf("PrePath = %s",(const char*)PrePath);
	//printf("Pos of / = %d\n",PrePath.Pos("/",-1));
	if(WorkStr[WorkStr.Len() - 1] != '/')
	{
		WorkStr += "/";
	}
	//printf("PrePath = %s",(const char*)PrePath);
	ReturnString = WorkStr;
	return(ReturnString);
}

const char* STDriveOps::StripPathRedirection(const STString& OriginalPath)
{
	STString WorkStr;
	WorkStr = OriginalPath;
	STStringList PathParts;
	while(WorkStr.Pos("//") > -1)
	{
		WorkStr.Remove(WorkStr.Pos("//"), 1);
	}
	WorkStr.Split('/', &PathParts);
	ReturnString = "";

	// Re-direction is allowed only at the start of the path.
	for(unsigned int i = 0; i < PathParts.Count(); i++)
	{
		if(PathParts.Item(i) == "..")
		{
			if((i > 0) && (PathParts.Item(i-1) != ".."))
			{
				// Re-direction found in the middle of the path eats the previous directory.
				PathParts.Replace(i-1, "");
			}
		}
	}
	ReturnString = "";
	if(PathParts.Item(0) != "..") ReturnString = "/";
	for(unsigned int i = 0; i < PathParts.Count(); i++)
	{
		if(PathParts.Item(i) != "")
		{
			ReturnString += PathParts.Item(i);
			ReturnString += "/";
		}
	}
	return(ReturnString);
}
